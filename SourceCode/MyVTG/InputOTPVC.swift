//
//  ActivateAccountViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

@objc protocol InputOTPDelegate {
    
    @objc func inputOTPDone(otp: String)
    @objc optional func resendOTP()
}

class InputOTPVC: BaseVC {
    
    // MARK: - Outlets
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var resentLoadingImage: UIImageView!
    @IBOutlet weak var resentButton: UIButton!
    @IBOutlet weak var otpErrorMessageLabel: UILabel!
    @IBOutlet weak var inputOTPDescLabel: UILabel!
    @IBOutlet weak var languageSwitch: UILanguageSwitch!
    
    // MARK: - Variables
    var delegate: InputOTPDelegate? = nil
    var serviceType:ServiceType = .None
    
    // MARK: - Setup UI
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func underlineButton() {
        let attrs = [
            NSFontAttributeName : UIFont(name: "roboto-regular", size: 15)!,
            NSForegroundColorAttributeName : Const.COLOR_ORANGE,
            NSUnderlineStyleAttributeName : 1] as [String : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:resentButton.localizedText, attributes:attrs)
        attributedString.append(buttonTitleStr)
        resentButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    func setupUI() {
        self.languageSwitch.IsAnimation = false
        self.languageSwitch.TitleOne = Const.LAOS_LANGUAGE_CODE
        self.languageSwitch.TitleTwo = Const.ENGLISH_LANGUAGE_CODE
        self.languageSwitch.ValueOne = Const.ENGLISH_LANGUAGE
        self.languageSwitch.ValueTwo = Const.LAOS_LANGUAGE
        languageSwitch.IsOpen = L102Language.currentAppleLanguage() == Const.LANG_EN
        self.languageSwitch.IsAnimation = true
        
        self.languageSwitch.ValueChanged = { value in
            let lang: String = self.languageSwitch.IsOpen ? Const.LANG_EN : Const.LANG_LOCAL
            self.changeLanguage(lang) {
                let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "InputOTPVC")
                self.navigationController?.pushViewController(vc, animated: false)
                
                var backstack = self.navigationController?.viewControllers
                if let index = backstack?.index(of: self){
                    backstack?.remove(at: index)
                }
                
                self.navigationController?.viewControllers = backstack!
                
            }
        }
        
        switch serviceType {
        case .RegisterUser:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DangKy02", comment: ""))
            actionButton.localizedText = NSLocalizedString("SignUp_ActivateButton_KichHoat", comment: "")
            break
            
        case .GetOTP:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DangKy02", comment: ""))
            actionButton.localizedText = NSLocalizedString("LoginLabel_DangNhap", comment: "")
//            inputOTPDescLabel.isHidden = true
            break
            
        default:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DangNhap01", comment: ""))
        }
        
        //        otpTextField.title = NSLocalizedString("SignUp_ActivateLabel_NhapMaOTP", comment: "")
        //        otpTextField.setTitleVisible(true)
    }
    
    // MARK: - Actions handle
    
    @IBAction func onTextFieldChanged(_ sender: Any) {
        otpTextField.text = ValidateUtil.limitLength(otp: otpTextField.text)
        let (isCorrect, msg) = ValidateUtil.validate(otp: otpTextField.text)
        actionButton.isEnabled = isCorrect
//        otpErrorMessageLabel.text = msg
    }
    
    
    @IBAction func resendTapped(_ sender: UIButton) {
//        resentButton.isHidden = true
//        resentLoadingImage.isHidden = false
//        resentLoadingImage.startProgress()
        delegate?.resendOTP?()
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func onActionButonTaped(_ sender: Any) {
        delegate?.inputOTPDone(otp: otpTextField.text!)
    }
    
    //Call func when resentOtpDone
    func resentOtpDone() {
//        resentButton.isHidden = false
//        resentLoadingImage.isHidden = true
//        resentLoadingImage.stopProgress()
    }
    
    @IBAction func BtnBackPressClickEvent(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
}
