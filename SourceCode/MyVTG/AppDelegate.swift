//
//  AppDelegate.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import AlamofireObjectMapper
import Alamofire
import AlamofireImage
import AVFoundation
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    let client = Client.getInstance()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBNxdTyaV_2C7bACXM5piPDHb-XgOX8Ld4")
        GMSPlacesClient.provideAPIKey("AIzaSyBNxdTyaV_2C7bACXM5piPDHb-XgOX8Ld4")
//        GMSServices.provideAPIKey(Const.GOOGLE_API_KEY)
//        GMSPlacesClient.provideAPIKey(Const.GOOGLE_API_KEY)
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        net?.listener = { status in
            if net?.isReachable ?? false {
                
                switch status {
                    
                case .reachable(.ethernetOrWiFi):
                    
//                    let params = [
//                        "limit": 3
//                    ]
//
//                    if (self.getIP() != "") {
//
//                        let urlString = "https://vtcapigw.viettelglobal.net/CoreService/UserRouting"
//                        var apiParams: [String : Any] = [:]
//                        if (params != nil) {
//                            apiParams = params
//                        }
//
//                        apiParams["ip"] = self.getIP()
//
//                        // prepare post params
//                        var postParams: [String : Any] = [
//                            "wsCode": "wsDetachIP",
//                            "sessionId": ""
//                        ]
//
//                        postParams["wsRequest"] = apiParams
//
//                        Alamofire.request(urlString, method: .post, parameters: postParams, encoding: JSONEncoding.default).responseJSON { response in
//                            print("The network is reachable over the WiFi connection \(response)")
//                            switch response.result {
//                            case .success:
//                                if let value = response.result.value {
//
//                                    let result = value as! JSONDictionary
//
//                                    let data : [String : Any] = result["result"] as! [String : Any]
//
//                                    if data["userMsg"] != nil {
//
//                                    }else{
//                                        let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
//                                        UIApplication.shared.keyWindow?.rootViewController = vc
//                                    }
//                                }else{
//                                    let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
//                                    UIApplication.shared.keyWindow?.rootViewController = vc
//                                }
//                            case .failure(let error):
//                                let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
//                                UIApplication.shared.keyWindow?.rootViewController = vc
//                            }

//                        }
//                    }
                    
                    
                    let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
                    UIApplication.shared.keyWindow?.rootViewController = vc
                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                    let params = [
                        "limit": 3
                    ]
                    if (self.getIP() != "") {
                        
                        self.client.requestLogin(self.getIP()!, params: params, complete: {(response: DataResponse<Any>) -> Void in
                            print("The network is reachable over the WiFi connection \(response)")
                            switch response.result {
                                
                            case .success:
                                if let value = response.result.value {
                                    let result = value as! JSONDictionary
                                    let data : [String : Any] = result["result"] as! [String : Any]
                                    if data["userMsg"] != nil {
                                        let isdn:String = data["userMsg"] as! String
                                        print("The network is reachable over the WiFi connection \(isdn)")
                                        
                                        //                                            showProgress()
                                        
                                        self.client.autoLogin(isdn:isdn, complete: {(resp: DataResponse<Any>) -> Void in
                                            
                                            //                                                hideProgress()
                                            
                                            //                                                if (handleApiError(resp, apiCode: "UserLogin")) {
                                            //                                                    return
                                            //                                                }
                                            
                                            guard let modelUser: ModelUserLogin = ModelHelper.from(response: resp) else {
                                                return
                                            }
                                            ModelHelper.saveToCache(modelUser)
                                            self.client.userLogin = modelUser
                                            
                                            let mainVC = UIStoryboard.getViewController(storyBoardName: "Main", viewControllerName: "rootTabBar")
                                            UIApplication.shared.keyWindow?.rootViewController = mainVC
                                        })
                                        
                                    }else{
                                        let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
                                        UIApplication.shared.keyWindow?.rootViewController = vc
                                    }
                                }else{
                                    let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
                                    UIApplication.shared.keyWindow?.rootViewController = vc
                                }
                            case .failure(let error):
                                let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
                                UIApplication.shared.keyWindow?.rootViewController = vc
                                
                            }
                        })
                    }
                    
                case .notReachable:
                    print("The network is not reachable")
                    
                case .unknown :
                    print("It is unknown whether the network is reachable")
                    
                }
            }
        }
        
        if let gai = GAI.sharedInstance() {
            gai.tracker(withTrackingId: "UA-100361075-1")
            // Optional: automatically report uncaught exceptions.
            gai.trackUncaughtExceptions = true
            // Optional: set Logger to VERBOSE for debug information.
            // Remove before app release.
            gai.logger.logLevel = .verbose;
        } else {
            LogUtil.d("AppDelegate", "Google Analytics not configured correctly");
        }
        
        FIRApp.configure()
        return true
    }
    
    func getIP()-> String? {
        
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next } // memory has been renamed to pointee in swift 3 so changed memory to pointee
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                    
                }
            }
            freeifaddrs(ifaddr)
        }
        
        return address
    }
    
    
    
}
