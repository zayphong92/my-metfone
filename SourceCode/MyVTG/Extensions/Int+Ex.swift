//
//  Int+Ex.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/22/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

extension Int {
    
    static func formatIntegerWithSeparator(_ int: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
//        formatter.locale = Locale(identifier: L102Language.currentAppleLanguage())
        formatter.decimalSeparator = Const.FORMAT_DECIMAL_SEPARATOR
        formatter.groupingSeparator = Const.FORMAT_GROUPING_SEPARATOR
        if let result = formatter.string(from: NSNumber(value: int)) {
            return result
        }
        return String(int)
    }
    
    static func withSeparator(_ double: Double) -> String {
        return formatIntegerWithSeparator(Int(round(double)))
    }
}
