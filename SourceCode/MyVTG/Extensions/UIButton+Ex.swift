//
//  UIButton+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/20/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UIButton {
    
    @available(*, deprecated: 1.0, message: "Don't use this anymore, use localizedText instead")
    var localizedTextNormal: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .normal)
        }
        get {
            return title(for: .normal)!
        }
    }
    
    @available(*, deprecated: 1.0, message: "Don't use this anymore, use localizedText instead")
    var localizedTextHilight: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .highlighted)
        }
        get {
            return title(for: .highlighted)!
        }
    }
    
    var localizedText: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .normal)
            setTitle(NSLocalizedString(key, comment: ""), for: .highlighted)
        }
        get {
            return title(for: .normal)!
        }
    }

}
