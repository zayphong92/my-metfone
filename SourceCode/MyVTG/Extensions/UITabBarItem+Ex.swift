//
//  UILabel+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UITabBarItem {
    var localizedText: String {
        set (key) {
            title = NSLocalizedString(key, comment: "")
        }
        get {
            return title!
        }
    }
}
