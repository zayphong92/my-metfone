//
//  NSAttributedString+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

extension NSAttributedString {
    func join(sequence: [NSAttributedString]) -> NSAttributedString {
        guard sequence.count > 0 else {
            return NSAttributedString(string: "")
        }
        
        let mutableString = NSMutableAttributedString(attributedString: sequence[0])
        for index in 1 ..< sequence.count {
            mutableString.append(self)
            mutableString.append(sequence[index])
        }
        return NSAttributedString(attributedString: mutableString)
    }
}
