//
//  UILabel+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UILabel {
    var substituteFontName: String {
        get {
            return self.font.fontName
        }
        set {
            self.font = UIFont(name: newValue, size: self.font.pointSize)
        }
    }
    
    var localizedText: String {
        set (key) {
            text = NSLocalizedString(key, comment: "")
        }
        get {
            return text!
        }
    }
    
    
    func underline(withText text: String) {
        let textRange = NSMakeRange(0, text.characters.count)
        let attrText = NSMutableAttributedString(string: text)
        attrText.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        self.attributedText = attrText
    }
    
    
    func setHTMLFromString(htmlText: String)  {
        let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font.pointSize)\">%@</span>" as NSString, htmlText) as String
        
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
    


    
    
    
    
}
