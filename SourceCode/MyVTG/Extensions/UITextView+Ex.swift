//
//  UITextView+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UITextView {
    func setStyleSimilarUILabel() {
        textContainerInset = UIEdgeInsets.zero;
        textContainer.lineFragmentPadding = 0;
        isScrollEnabled = false
        isEditable = false
        isSelectable = false
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
    }
    
    func setHTMLFromString(htmlText: String)  {
        let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(String(describing: self.font?.pointSize))\">%@</span>" as NSString, htmlText) as String
        
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}
