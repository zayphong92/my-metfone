//
//  UIImageView+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

let listImageNames = [
    "ic_loading_01",
    "ic_loading_02",
    "ic_loading_03",
    "ic_loading_04",
    "ic_loading_05",
    "ic_loading_06",
    "ic_loading_07",
    "ic_loading_08",
]

extension UIImageView {
    
    func startProgress() {
        if (self.animationImages == nil) {
            var listImages: [UIImage] = []
            for imageName in listImageNames {
                listImages.append(UIImage.init(named: imageName)!)
            }
            self.animationImages = listImages
            self.animationDuration = 0.3
            self.animationRepeatCount = 0
        }
        self.isHidden = false
        self.backgroundColor = .white
        self.contentMode = .scaleAspectFit
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 5, height: 10)
        self.layer.shadowRadius = 5

        self.startAnimating()
    }
    
    func stopProgress() {
        self.stopAnimating()
        self.isHidden = true
    }
}
