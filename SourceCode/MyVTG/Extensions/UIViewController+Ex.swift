//
//  UIViewController+Ex.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

// MARK: - Variables

// global constants
private let spacer:CGFloat = 16
private let btnSize:CGFloat = 44
private let tagLabelTitle: Int = 100
private let titleTextFont: UIFont = UIFont(name: "Roboto-Medium", size: 16.0)!
private let titleTextFontSmall: UIFont = UIFont(name: "Roboto-Medium", size: 12.0)!

// global variables
private var spacerLeft: UIBarButtonItem?
private var spacerRight: UIBarButtonItem?
private var rightBarButtonItem: UIBarButtonItem?
private var searchBarVisible: Bool = false
private var inputSearchBar: UITextField?
private var headerView: UIView?

public enum SearchType {
    case api
    case local
}
private var navigationBarTitle: String = ""
private var searchTextField: UITextField?

private var rightBarButton: UIButton?

private var searchType: SearchType? = .api

private var listeningToTextFieldDidChange: Bool = false

extension UIViewController: UITextFieldDelegate {
    
    // MARK: - Set up navigation bar
    
    func setupNavBar(title: String) {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let imBg = UIImage(named: "bg_nav")
        imBg?.resizableImage(withCapInsets: UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0))
        let nav = self.navigationController?.navigationBar
//        nav?.setBackgroundImage(imBg, for: UIBarMetrics.default)
        //lifesup_sprint_2_T8_2018_start
            nav?.setBackgroundImage(imBg?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0), resizingMode: .stretch), for: UIBarMetrics.default)
        //lifesup_sprint_2_T8_2018_end
        nav?.shadowImage = UIImage()    // remove bottom line
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = true;
        
        let headerWidth = headerViewWidth()
        headerView = UIView.init(frame: CGRect(0, 0, headerWidth, btnSize))
        //headerView?.backgroundColor = UIColor.cyan
        
        // left
        let leftImageName = getNavBarLeftImageName()
        let rightImageName = getNavBarRightImageName()
        if (leftImageName.len() == 0) {
            if (rightImageName.len() > 0) {
                // bên phải có 1 button nhưng bên trái ko có button nào --> add thêm 1 khoảng trống vào bên trái để căn giữa title cho đẹp
                addBlankLeftBtn()
            }
        } else {
            let iconL: UIImage = UIImage(named:leftImageName)!
            let iconL_s: UIImage = UIImage(named:"\(leftImageName)_s")!
            let actionL: Selector = #selector(onBtnLeftNavBar)
            let buttonL: UIButton = UIButton(type: .custom) as UIButton
            buttonL.frame = CGRect(0, 0, btnSize, btnSize)
            buttonL.setImage(iconL, for: UIControlState.normal)
            buttonL.setImage(iconL_s, for: UIControlState.highlighted)
            buttonL.addTarget(self, action: actionL, for: UIControlEvents.touchUpInside)
            headerView?.addSubview(buttonL)
        }
        
        // center title
        setHeaderTitle(title)
        
        // right
        if (rightImageName.len() == 0) {
            if (leftImageName.len() > 0) {
                // bên trái có 1 button nhưng bên phải ko có button nào --> add thêm 1 khoảng trống vào bên phải để căn giữa title cho đẹp
                addBlankRightBtn()
            }
        } else {
            let iconR: UIImage = UIImage(named:rightImageName)!
            let iconR_s: UIImage = UIImage(named:"\(rightImageName)_s")!
            let actionR: Selector = #selector(onBtnRightNavBar)
            let buttonR: UIButton = UIButton(type: .custom) as UIButton
            buttonR.frame = CGRect(ScreenSize.WIDTH-btnSize, 0, btnSize, btnSize)
            buttonR.setImage(iconR, for: UIControlState.normal)
            buttonR.setImage(iconR_s, for: UIControlState.highlighted)
            buttonR.addTarget(self, action: actionR, for: UIControlEvents.touchUpInside)
            headerView?.addSubview(buttonR)
            rightBarButton = buttonR
            // search bar
            addSearchBarToHeaderView(headerView!, textFont: titleTextFont)
        }
        
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: headerView!)
        spacerLeft = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacerLeft?.width = -spacer;
        self.navigationItem.leftBarButtonItems = [spacerLeft!, leftBarButtonItem]
    }
    
    private func headerViewWidth() -> CGFloat {
        let rightImageName = getNavBarRightImageName()
        if (rightImageName.len() == 0) {
            return ScreenSize.WIDTH
        } else {
            return ScreenSize.WIDTH*2-btnSize
        }
    }
    
    private func searchBarWidth() -> CGFloat {
        return ScreenSize.WIDTH-btnSize-10
    }
    
    private func addSearchBarToHeaderView(_ view: UIView, textFont: UIFont) {
        inputSearchBar = UITextField.init(frame: CGRect(ScreenSize.WIDTH, 4, searchBarWidth(), btnSize-8))
        inputSearchBar?.delegate = self
        inputSearchBar?.background = UIImage.init(named: "bg_edittext1")
        inputSearchBar?.font = textFont
        inputSearchBar?.textColor = UIColor.white
        inputSearchBar?.returnKeyType = .search
        searchTextField = inputSearchBar
        let placeHolderAttrs = [ NSFontAttributeName: textFont,
                                 NSForegroundColorAttributeName: Const.COLOR_WHITE_ALPHA60 ]
        inputSearchBar?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Label_TimKiem", comment: ""), attributes: placeHolderAttrs)
        let paddingView: UIView = UIView.init(frame: CGRect(0, 0, 20, btnSize))
        inputSearchBar?.rightView = paddingView;
        inputSearchBar?.rightViewMode = .always;
        
        let btnClose:UIButton = UIButton.init(frame: CGRect(2*ScreenSize.WIDTH-2*btnSize, 0, btnSize, btnSize))
        btnClose.setImage(UIImage.init(named: "ic_close_white"), for: .normal)
        btnClose.setImage(UIImage.init(named: "ic_close_white_s"), for: .highlighted)
        btnClose.addTarget(self, action: #selector(onCloseSearchBar), for: UIControlEvents.touchUpInside)
        view.addSubview(inputSearchBar!)
        view.addSubview(btnClose)
    }
    
    func setHeaderTitle(_ title: String) {
        if (title.len() == 0) {
            return
        }
        let attrLine1 = [ NSFontAttributeName: titleTextFont ]
        let attrLine2 = [ NSFontAttributeName: titleTextFontSmall ]
        let strArr = title.components(separatedBy: "\n")
        navigationBarTitle = strArr[0]
        var strLine1: NSMutableAttributedString
        if (strArr.count > 1) {
            strLine1 = NSMutableAttributedString(string: "\(strArr[0])\n", attributes: attrLine1)
            let strLine2 = NSMutableAttributedString(string: strArr[1], attributes: attrLine2)
            strLine1.append(strLine2)
        } else {
            strLine1 = NSMutableAttributedString(string: title, attributes: attrLine1)
        }
        let leftImageName = getNavBarLeftImageName()
        let rightImageName = getNavBarRightImageName()
        var labelWidth = ScreenSize.WIDTH
        var labelX: CGFloat = 0
        if (leftImageName.len() > 0 || rightImageName.len() > 0) {
            labelWidth = ScreenSize.WIDTH - btnSize*2
            labelX = btnSize
        }
        var label: UILabel? = headerView?.viewWithTag(tagLabelTitle) as? UILabel
        if (label == nil) {
            label = UILabel(frame: CGRect(labelX, 0, labelWidth, btnSize))
            label?.numberOfLines = 2
            label?.textColor = UIColor.white
            label?.textAlignment = .center
            label?.tag = tagLabelTitle
            headerView?.addSubview(label!)
        }
        label!.attributedText = strLine1
        
        //label?.backgroundColor = UIColor.blue
    }
    
    private func addBlankRightBtn() {
        let button: UIButton = UIButton(type: .custom) as UIButton
        button.frame = CGRect(0, 0, btnSize, btnSize)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: button)
        spacerRight = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacerRight?.width = -spacer;
        self.navigationItem.rightBarButtonItems = [spacerRight!, rightBarButtonItem]
    }
    
    private func addBlankLeftBtn() {
        let button: UIButton = UIButton(type: .custom) as UIButton
        button.frame = CGRect(0, 0, btnSize, btnSize)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: button)
        spacerLeft = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacerLeft?.width = -spacer;
        self.navigationItem.leftBarButtonItems = [spacerLeft!, leftBarButtonItem]
    }
    
    // MARK: - Nav bar actions
    
    func onBtnLeftNavBar() {
        goBack()
    }
    
    func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func onBtnRightNavBar() {
        
    }
    
    // MARK: - "Get" functions
    
    func getNavBarTitle() -> String {
        return navigationBarTitle
    }
    
    func getNavBarLeftImageName() -> String {
        return "back"
    }
    
    func getNavBarRightImageName() -> String {
        return ""
    }
    
    func getNavigationBarHeight() -> CGFloat {
        return self.navigationController!.navigationBar.frame.height
    }

    func getRightBarButton() -> UIButton? {
        return rightBarButton
    }
    
    func getNavBarView() -> UIView? {
        return headerView
    }
    
    func getSearchTextField() -> UITextField? {
        return searchTextField
    }
    
    // MARK: - Search support functions
    
    func onCancelSearch() {
        textFieldDidEndEditing(inputSearchBar!)
        toggleSearchBar()
        inputSearchBar?.text = ""
    }
    
    func onCloseSearchBar() {
        onCancelSearch()
    }
    
    func showSearchBar() {
        UIView.animate(withDuration: 0.3, animations: {
            headerView?.frame.origin.x = btnSize - ScreenSize.WIDTH
        }, completion: {_ in
            inputSearchBar?.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        UIView.animate(withDuration: 0.3, animations: {
            headerView?.frame.origin.x = 0
        }, completion: {_ in
            inputSearchBar?.resignFirstResponder()
        })
    }
    
    func toggleSearchBar() {
        if (searchBarVisible) {
            searchBarVisible = false
            hideSearchBar()
        } else {
            searchBarVisible = true
            showSearchBar()
        }
    }
    
    func onHeaderSearch(keyword: String) {
        
    }
    
    func setSearchType(_ type: SearchType) {
        if type == .local {
            searchType = .local
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var result: Bool = true
        if textField == searchTextField {
            onHeaderSearch(keyword: textField.text!)
            result = false
        } else {
            result = true
        }
        textField.resignFirstResponder()
        return result
    }
    
    func listenToTextFieldDidChange() {
        listeningToTextFieldDidChange = true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if (searchType == .local && textField == searchTextField) || listeningToTextFieldDidChange {
            LogUtil.d("Add target to listen for textFieldDidChange")
            textField.addTarget(self,
                                action: #selector(textFieldDidChange),
                                for: UIControlEvents.editingChanged)
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if (searchType == .local && textField == searchTextField) || listeningToTextFieldDidChange {
            LogUtil.d("Remove target for textFieldDidChange")
            textField.removeTarget(self,
                                   action: #selector(textFieldDidChange),
                                   for: UIControlEvents.editingChanged)
            textField.resignFirstResponder()
        }
    }
    
    func textFieldChanged() {
        
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        onHeaderSearch(keyword: textField.text!)
        textFieldChanged()
    }
    
    func setTextFieldAsInputSearchBar(_ textField: UITextField) {
        searchTextField = textField
    }
}











