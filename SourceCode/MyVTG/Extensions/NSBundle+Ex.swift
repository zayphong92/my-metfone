//
//  Bundle+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Bundle {
    
    class func readLocalJSON(fileName: String) -> JSON {
        
        let path : String = Bundle.main.path(forResource: fileName, ofType: "json")!
        let fileHandle : FileHandle = FileHandle(forReadingAtPath: path)!
        let data = fileHandle.readDataToEndOfFile()

        return JSON(data: data)
    }
    
    public static func localize(key: String, comment: String = "") -> String {
        return NSLocalizedString(key, comment: comment)
    }
}
