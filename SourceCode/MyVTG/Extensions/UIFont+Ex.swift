//
//  UIFont+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UIFont {
    func sizeOfString (string: String, constrainedToHeight height: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: Double.greatestFiniteMagnitude, height: height),
                                                             options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                             attributes: [NSFontAttributeName: self],
                                                             context: nil).size
    }
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),
                                                             options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                             attributes: [NSFontAttributeName: self],
                                                             context: nil).size
    }
}
