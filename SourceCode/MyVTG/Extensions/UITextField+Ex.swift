//
//  UILabel+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UITextField {
    var localizedPlaceHolder: String {
        set (key) {
            placeholder = NSLocalizedString(key, comment: "")
        }
        get {
            return placeholder!
        }
    }
}
