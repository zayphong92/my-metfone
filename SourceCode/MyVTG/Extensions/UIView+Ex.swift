//
//  UIView+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func anchorWithConstantsTo(top: NSLayoutYAxisAnchor? = nil,
                               left: NSLayoutXAxisAnchor? = nil,
                               bottom: NSLayoutYAxisAnchor? = nil,
                               right: NSLayoutXAxisAnchor? = nil,
                               topConstant: CGFloat = 0,
                               leftConstant: CGFloat = 0,
                               bottomConstant: CGFloat = 0,
                               rightConstant: CGFloat = 0) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: topConstant).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: leftConstant).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -rightConstant).isActive = true
        }
    }
    
    func setConstraintsTo(view: UIView?,
                          topConstant: CGFloat,
                          bottomConstant: CGFloat,
                          leftConstant: CGFloat,
                          rightConstant: CGFloat) {
        
        // scrollView.top = viewPager.top * 1.0 + 0
        let topContraints = NSLayoutConstraint(item: self,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: view,
                                               attribute: .top,
                                               multiplier: 1.0,
                                               constant: topConstant)
        
        // scrollView.bottom = viewPager.bottom * 1.0 + 0
        let bottomContraints = NSLayoutConstraint(item: self,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: view,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -bottomConstant)
        
        // scrollView.leading = viewPager.leading * 1.0 + 0
        let leftContraints = NSLayoutConstraint(item: self,
                                                attribute: .leadingMargin,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: .leadingMargin,
                                                multiplier: 1.0,
                                                constant: leftConstant)
        
        // scrollView.trailing = viewPager.trailing * 1.0 + 0
        let rightContraints = NSLayoutConstraint(item: self,
                                                 attribute: .trailingMargin,
                                                 relatedBy: .equal,
                                                 toItem: view,
                                                 attribute: .trailingMargin,
                                                 multiplier: 1.0,
                                                 constant: -rightConstant)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([topContraints, rightContraints, leftContraints, bottomContraints])
    }
}
