//
//  Dictionary+Ex.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension Dictionary {
    mutating func update(_ other: Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
