//
//  UIColor+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

extension UIColor {
    class func rgbValue(hexValue:UInt64) -> UIColor {
        let r = CGFloat((hexValue & 0xFF0000) >> 16)/255.0
        let g = CGFloat((hexValue & 0xFF00) >> 8)/255.0
        let b = CGFloat((hexValue & 0xFF))/255.0
        
        return UIColor(red:r, green:g, blue:b, alpha: 1.0)
    }
    
    class func argbValue(hexValue:UInt64) -> UIColor {
        let a = CGFloat((hexValue & 0xFF000000) >> 24)/255.0
        let r = CGFloat((hexValue & 0xFF0000) >> 16)/255.0
        let g = CGFloat((hexValue & 0xFF00) >> 8)/255.0
        let b = CGFloat((hexValue & 0xFF))/255.0
        
        return UIColor(red:r, green:g, blue:b, alpha: a)
    }
}
