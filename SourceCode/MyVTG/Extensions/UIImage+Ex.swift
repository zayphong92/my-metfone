//
//  UIImage+Ex.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImage {
    
    static func imageWithColor(color: UIColor, rect: CGRect = CGRect(0, 0, 1, 1)) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize((rect.origin.x*2)+rect.size.width, (rect.origin.y*2)+rect.size.height), false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        //self.init(CGImage: image?.cgImage)
        return image
    }
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    static func imageFromURL(_ url: URL) -> UIImage? {
        var image: Image? = nil
        let downloader = ImageDownloader()
        let urlRequest = URLRequest(url: url)
        downloader.download(urlRequest) { response in
            if let downloadedImage = response.result.value {
                LogUtil.d("Successfully get image")
                image = downloadedImage
            }
        }
        return image
    }
    
//    func alpha(value:CGFloat) -> UIImage {
//        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
//        
//        let ctx = UIGraphicsGetCurrentContext();
//        let area = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height);
//        
//        CGContextScaleCTM(ctx, 1, -1);
//        CGContextTranslateCTM(ctx, 0, -area.size.height);
//        CGContextSetBlendMode(ctx, CGBlendMode.Multiply);
//        CGContextSetAlpha(ctx, value);
//        CGContextDrawImage(ctx, area, self.CGImage);
//        
//        let newImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        return newImage;
//    }
    
//    func imageWithInsets(insets: UIEdgeInsets) -> UIImage {
//        UIGraphicsBeginImageContextWithOptions(CGSize(self.size.width + insets.left + insets.right, self.size.height + insets.top + insets.bottom), false, self.scale)
//        let origin = CGPoint(x: insets.left, y: insets.top)
//        self.drawAtPoint(origin)
//        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return imageWithInsets
//    }
}
