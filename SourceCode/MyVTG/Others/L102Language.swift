//
//  L102Language.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/9/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

let APPLE_LANGUAGE_KEY = "AppleLanguages"

class L102Language {
    
    /// get current Apple language
    class func currentAppleLanguage() -> String {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    class func setAppleLanguageTo(lang: String) {
        let userdef = UserDefaults.standard
        
        let langs = [lang,currentAppleLanguage()]
        userdef.set(langs, forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}
