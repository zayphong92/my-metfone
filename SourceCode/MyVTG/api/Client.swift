//
//  Client.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Alamofire
import SwiftyJSON
import ObjectMapper

final class Client {
    public static let ERR_CODE_API_GATEWAY_SUCCESS: String = "S200"
    public static let ERR_CODE_BACKEND_SUCCESS: String = "0"
    public static let ERR_CODE_BACKEND_SUCCESS_1000: String = "1000"
    public static let PAGE_SIZE: Int = 10
    static let TIMEOUT = 10.0
    
    let API_PREFIX: String = "855"   // prefix for Metfone market
    let APP_CODE: String = "MyVTG"   // prefix for Metfone market
    
    static let BASE_IP = "https://vtcapigw.viettelglobal.net/"        // server Cambodia
   //   static let BASE_IP = "http://36.37.242.104/ApigwGateway/"        // server Cambodia
    static let BASE_URL = BASE_IP + "CoreService/"
    static let USER_ROUNTING = BASE_URL + "UserRouting"
    static let USER_LOGIN = BASE_URL + "UserLogin"
    static let USER_LOGIN_OTP = BASE_URL + "UserLoginOtp"
    static let USER_LOGOUT = BASE_URL + "UserLogout"
    static let USER_LOGIN_AUTO = BASE_URL + "UserLoginAuto"
    
    public static let WS_GET_APP_CONFIG: String = "wsGetAppConfig";
    public static let WS_GET_SERVICES: String = "wsGetServices";
    public static let WS_GET_SERVICES_BY_GROUP: String = "wsGetServicesByGroup";
    public static let WS_GET_CURRENT_USED_SERVICES: String = "wsGetCurrentUsedServices";
    public static let WS_GET_CURRENT_USED_SUB_SERVICES: String = "wsGetCurrentUsedSubServices";
    public static let WS_GET_SERVICE_DETAIL: String = "wsGetServiceDetail";
    public static let WS_GET_SUBINFO: String = "wsGetSubInfo";
    public static let WS_GET_CAREERS: String = "wsGetCareers";
    public static let WS_GET_HOBBIES: String = "wsGetHobbies";
    public static let WS_UPDATE_SUB_INFO: String = "wsUpdateSubInfo";
    public static let WS_GET_SUB_MAIN_INFO: String = "wsGetSubMainInfo";
    public static let WS_GET_SUB_ACCOUNT_INFO: String = "wsGetSubAccountInfo";
    public static let WS_GET_CALL_ACCOUNT_DETAIL: String = "wsGetCallAccountDetail";
    public static let WS_GET_ACCOUNTS_DETAIL: String = "wsGetAccountsDetail";
    public static let WS_GET_DATA_ACCOUNT_DETAIL: String = "wsGetDataAccountDetail";
    public static let WS_GET_NEAREST_STORES: String = "wsGetNearestStores";
    public static let WS_FIND_STORE_BY_ADDR: String = "wsFindStoreByAddr";
    public static let WS_GET_PROVINCES: String = "wsGetProvinces";
    public static let WS_GET_DISTRICTS: String = "wsGetDistricts";
    public static let WS_GET_HOT_NEWS: String = "wsGetHotNews";
    public static let WS_GET_ALL_DATA_PACKAGES: String = "wsGetAllDataPackages";
    public static let WS_GET_DATA_VOLUME_LEVEL_TO_BUY: String = "wsGetDataVolumeLevelToBuy";
    public static let WS_GET_DATA_PACKAGE_INFO: String = "wsGetDataPackageInfo";
    public static let WS_DO_BUY_DATA: String = "wsDoBuyData";
    public static let WS_DO_ACTION_SERVICE: String = "wsDoActionService";
    public static let WS_DO_ACTION_BY_WEBSERVICE: String = "wsDoActionByWebService";
    public static let WS_DO_RECHARGE: String = "wsDoRecharge";
    public static let WS_GET_POSTAGE_INFO: String = "wsGetPostageInfo";
    public static let WS_GET_POSTAGE_DETAIL_INFO: String = "wsGetPostageDetailInfo";
    public static let WS_GET_SERVICE_INFO: String = "wsGetServiceInfo";
    public static let WS_GET_ADVANCED_SERVICE_INFO: String = "wsGetAdvancedServiceInfo";
    public static let WS_GET_SERVICE_FUNCTION_PAGE: String = "wsGetServiceFuntionPage";
    public static let WS_FIND_ISDN_TO_BUY: String = "wsFindIsdnToBuy";
    public static let WS_REGISTER_BUY_ISDN: String = "wsRegisterBuyIsdn";
    public static let WS_DO_BUY_ISDN: String = "wsDoBuyIsdn";
    public static let WS_DO_RESTORE_ISDN: String = "wsDoRestoreIsdn";
    public static let WS_DO_EXCHANGE_ISDN: String = "wsDoExchangeIsdn";
    public static let WS_DO_CHANGE_SIM: String = "wsDoChangeSIM";
    public static let WS_DO_LOCK_CALL_GO_ISDN: String = "wsDoLockCallGoIsdn";
    
    public static let WS_GET_NEW_PROMOTIONS: String = "wsGetNewPromotions";
    public static let WS_GET_PROMOTION_INFO: String = "wsGetPromotionInfo";
    public static let WS_GET_ALL_APPS: String = "wsGetAllApps";
    public static let WS_GET_NEWS_DETAIL: String = "wsGetNewsDetail";
    public static let WS_DO_SEND_EMAIL: String = "wsDoSendEmail";
    public static let WS_GET_CHANGE_POSTAGE_RULE: String = "wsGetChangePostageRule";
    public static let WS_GET_NEWS: String = "wsGetNews";
    
    public static let WS_GET_EXPERIENCE_LINK_3G_4G: String = "wsGetExperienceLink3G4G";
    public static let WS_UNREGISTER_DATA_PACKAGE: String = "wsDoActionService";
    
    public static let USER_CHANGE_PASSWORD: String = "UserChangePassword";
    public static let USER_FORGOT_PASSWORD: String = "UserForgotPassword";
    public static let USER_REGISTER: String = "UserRegister";
    
    private static let _instance = Client()
    
    private init() {}
    
    static func getInstance() -> Client {
        _instance.lang = L102Language.currentAppleLanguage()
        return _instance;
    }
    
    var userLogin: ModelUserLogin?
    var subMainInfo: ModelSubMainInfo?
    var subAccountInfo: ModelSubAccountInfo?
    var appConfig: ModelAppConfig?
    var lang: String = Const.LANG_LOCAL
    
    func getIsdn() -> String {
        return userLogin == nil ? "" : userLogin!.username
    }
    
    func getSubId() -> Int {
        return subMainInfo == nil ? 0 : subMainInfo!.subId
    }
    
    func getSubType() -> Int {
        return subMainInfo == nil ? 0 : subMainInfo!.subType
    }
    
    func getMainAcc() -> Double {
        return subAccountInfo == nil ? 0 : subAccountInfo!.mainAcc
    }
    
    func getProAcc() -> Double {
        return subAccountInfo == nil ? 0 : subAccountInfo!.proAcc
    }
    
    func isPrePaid() -> Bool {
        return getSubType() == ModelSubMainInfo.SUBTYPE_TRATRUOC
    }
    
    func getHomeConfig() -> [ModelHomeConfig] {
        return appConfig == nil ? [] : appConfig!.home
    }
    
    func getUtilitiesConfig() -> [ModelUtilitiesConfig] {
        return appConfig == nil ? [] : appConfig!.utilities
    }
    
    func getMoreConfig() -> [ModelMoreConfig] {
        return appConfig == nil ? [] : appConfig!.more
    }
    
    func getEmptyPages() -> [ModelEmptyPage] {
        return appConfig == nil ? [] : appConfig!.emptyPages
    }
    
    private func onLogout() {
        userLogin = nil
        ModelHelper.onLogout()
    }
    
    //private var authHeader: String! = ""
    //private var acccessToken: String! = ""
    
    //func setAccessToken(token: String) {
        //acccessToken = token
        //authHeader = "Bearer " + token
    //}
    
    private func isError(code: Int) -> Bool {
        switch code {
        case 200, 201:
            return false
        default:
            return true
        }
    }
    
    private func doGET(_ url: String, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        LogUtil.d("GET " + url)
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        Alamofire.request(request).responseJSON { (response: DataResponse<Any>) in
            complete(response)
        }
    }
    
    private func doPOST(_ url: String, params: [String : Any]?, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        LogUtil.d("POST " + url)
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if (params != nil) {
            request.httpBody = try! JSONSerialization.data(withJSONObject: params!, options: [])
            LogUtil.d("Params: " + LogUtil.getParamString(dictionary: params))
        }
        
        Alamofire.request(request).responseJSON { (response: DataResponse<Any>) in
            complete(response)
        }
    }
    
    func loginOtp(_ otp: String, isdn: String, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        let params: [String : Any] = [
            "username": isdn,
            "otp": otp,
            "prefix": API_PREFIX,
            "appCode": APP_CODE
        ]
        doPOST(Client.USER_LOGIN_OTP, params: params, complete: complete)
    }
    
    func login(isdn: String, password: String, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        let params: [String : Any] = [
            "username": isdn,
            "password": password,
            "prefix": API_PREFIX,
            "appCode": APP_CODE
        ]
        doPOST(Client.USER_LOGIN, params: params, complete: complete)
    }
    
    func logout() {
        let params: [String : Any] = [
            "username": getIsdn(),
            "sessionId": (userLogin != nil ? userLogin!.sessionId : ""),
            "prefix": API_PREFIX,
            "appCode": APP_CODE
        ]
        doPOST(Client.USER_LOGOUT, params: params)
        // Mặc định logout lúc nào cũng thành công nên sau khi call API thì clear data luôn
        onLogout()
    }
    
    func getOtp(isdn: String, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        let url: String = Client.BASE_URL + "getOtp?username=" + isdn
        doGET(url, complete: complete)
    }
    
    func request(_ apiCode: String, params: [String : Any]?, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        // prepare api params
        var apiParams: [String : Any] = [:]
        if (params != nil) {
            apiParams = params!
        }
        
        apiParams["isdn"] = getIsdn()
        apiParams["language"] = lang
        
        // prepare post params
        var postParams: [String : Any] = [
            "wsCode": apiCode,
            "language": lang
        ]
        
        var otpParamVal = apiParams.removeValue(forKey: "otpOutside")
        if (otpParamVal != nil) {
            postParams["otp"] = otpParamVal
        }
        
        if (userLogin != nil) {
            postParams["sessionId"] = userLogin!.sessionId
            postParams["token"] = userLogin!.token
        }
        
        postParams["wsRequest"] = apiParams
        
        doPOST(Client.USER_ROUNTING, params: postParams, complete: complete)
    }
    
    func requestLogin(_ apiCode: String, params: [String : Any]?, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        // prepare api params
        var apiParams: [String : Any] = [:]
        if (params != nil) {
            apiParams = params!
        }
        
       apiParams["ip"] = apiCode
        
        // prepare post params
        var postParams: [String : Any] = [
            "wsCode": "wsDetachIP",
            "sessionId": ""
        ]

        postParams["wsRequest"] = apiParams
        
        doPOST(Client.USER_ROUNTING, params: postParams, complete: complete)
    }
    
    func requestApiGateway(apiPart: String, params: [String : Any]?, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        let url:String = Client.BASE_URL + apiPart
        doPOST(url, params: params, complete: complete)
    }

    func autoLogin(isdn:String, complete: @escaping(DataResponse<Any>) -> () = {_ in}) {
        let params : [String: Any] = [
            "username":isdn,
            "prefix": API_PREFIX,
            "appCode": APP_CODE
        ]
        doPOST(Client.USER_LOGIN_AUTO, params: params, complete: complete)
    }
}
