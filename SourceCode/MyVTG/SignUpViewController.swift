//
//  SignUpViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class SignUpViewController: BaseVC, InputOTPDelegate {

    @IBOutlet weak var phoneNumberTxt: UITextFieldRegular01!
    @IBOutlet weak var phoneErrorMsgLabel: UILabel!
    
    @IBOutlet weak var passwordTxt: UITextFieldRegular01!
    @IBOutlet weak var passErrorMsgLabel: UILabel!
    
    @IBOutlet weak var confirmPassworkTxt: UITextFieldRegular01!
    @IBOutlet weak var confrimPassErrorMsgLabel: UILabel!
    
    @IBOutlet weak var singupButton: UIButtonRegular01!
    
    private var inputOTPVC:InputOTPVC? = nil
    
    private var isPhoneNumberCorrect:Bool = false
    private var isPassCorrect:Bool = false
    private var isConfirmPassCorrect:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_DangKy01", comment: ""))
        setupTextFields()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapedOnNavigationBar(_:)))
        navigationController?.navigationBar.addGestureRecognizer(tapGesture)
    }
    
    func tapedOnNavigationBar(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    
    func setupTextFields() {
        phoneNumberTxt.placeholder = Const.PHONE_NUMBER_PLACEHOLDER
        phoneNumberTxt.title = NSLocalizedString("Label_SoDienThoai", comment: "")
        phoneNumberTxt.setTitleVisible(true)
        
        passwordTxt.title = NSLocalizedString("SignUpLabel_MatKhau", comment: "")
        passwordTxt.setTitleVisible(true)
        
        confirmPassworkTxt.title = NSLocalizedString("SignUpLabel_XacNhanMatKhau", comment: "")
        confirmPassworkTxt.setTitleVisible(true)
    }
    
    // MARK: - Actions handle
    
    @IBAction func onPhoneNumberChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        
        textField.text = ValidateUtil.limitLength(phoneNumber: textField.text)
        let (isCorrect, msg) = ValidateUtil.validate(phoneNumber: textField.text)
        isPhoneNumberCorrect = isCorrect
        phoneErrorMsgLabel.text = msg
        singupButton.isEnabled = isPhoneNumberCorrect && isPassCorrect && isConfirmPassCorrect
    }
    
    @IBAction func onPasswordChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        //reset confirm pass
        confirmPassworkTxt.text = ""
        confrimPassErrorMsgLabel.text =  NSLocalizedString("Label_confirm_the_password", comment: "")
        isConfirmPassCorrect = false
        
        textField.text = ValidateUtil.limitLength(password: textField.text)
        let (isCorrect, msg) = ValidateUtil.validate(password: textField.text)
        isPassCorrect = isCorrect
        passErrorMsgLabel.text = msg
        singupButton.isEnabled = isPhoneNumberCorrect && isPassCorrect && isConfirmPassCorrect
    }
    
    @IBAction func onConfirmPasswordChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        
        textField.text = ValidateUtil.limitLength(password: textField.text)
        let (isCorrect, msg) = ValidateUtil.isPasswordMatched(pass: passwordTxt.text, confirmPass: textField.text)
        isConfirmPassCorrect = isCorrect
        confrimPassErrorMsgLabel.text = msg
        singupButton.isEnabled = isPhoneNumberCorrect && isPassCorrect && isConfirmPassCorrect
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func signUpTapped(_ sender: UIButtonRegular01) {
        view.endEditing(true)
        
        doRegisterUser(otp: nil, complete: { (success: Bool, data:ModelDefault?) -> Void in
            if(!success) {return}
            
            self.inputOTPVC = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "InputOTPVC") as? InputOTPVC
            self.inputOTPVC?.delegate = self
            self.inputOTPVC?.serviceType = .RegisterUser
            self.navigationController?.pushViewController(self.inputOTPVC!, animated: true)
        })
    }
    
    func inputOTPDone(otp: String) {
        doRegisterUser(otp: otp, complete:  {(seccess: Bool, data: ModelDefault?) -> Void in
            if(seccess) {
                let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "SignUpSuccessVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    func resendOTP() {
        doRegisterUser(otp: nil, isShowProgress: false, complete: {(seccess: Bool, data: ModelDefault?) -> Void in
            if(self.inputOTPVC != nil) {
                self.inputOTPVC?.resentOtpDone()
            }
        })
    }
    
    func doRegisterUser(otp: String?, isShowProgress: Bool = true, complete: @escaping (Bool, ModelDefault?) -> ()) {
        let phoneNumber:String = phoneNumberTxt.text!
        let pass:String = passwordTxt.text!
        let confrimPass:String = confirmPassworkTxt.text!
        
        if(pass != confrimPass) {
            showError(NSLocalizedString("More_DoiMatKhauLabel_ThongBao3", comment: ""))
            return
        }
        
        let params : [String: Any] = [
            "username": phoneNumber,
            "password": pass,
            "prefix": client.API_PREFIX,
            "appCode": client.APP_CODE,
            "otp": otp ?? ""
        ]
        requestAction02(Client.USER_REGISTER, params: params, isShowProgress: isShowProgress, actionComplete: complete)
    }
    
}
