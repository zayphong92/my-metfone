//
//  AlertUtil.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 8/15/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class AlertUtil: NSObject {
    
    static func showNoti(message: String, isSuccessful: Bool) {
        guard let vc = UIApplication.shared.keyWindow?.rootViewController else {
            return
        }
        
        let ICON_WIDTH: CGFloat = 20
        let ICON_HEIGHT = ICON_WIDTH
        
        let NOTIVIEW_WIDTH: CGFloat = vc.view.frame.width - 32
        let NOTIVIEW_HEIGHT: CGFloat = 50
        
        let notiView = UIView(frame: CGRect(x: 0, y: 0, width: NOTIVIEW_WIDTH, height: NOTIVIEW_HEIGHT))
        vc.view.addSubview(notiView)
        
        notiView.center.x = vc.view.center.x
        notiView.center.y = 130
        notiView.layer.cornerRadius = 3
        notiView.layer.masksToBounds = true
        notiView.alpha = 0.9
        notiView.isUserInteractionEnabled = false
        
        let statusIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: ICON_WIDTH, height: ICON_HEIGHT))
        notiView.addSubview(statusIconView)
        statusIconView.contentMode = .scaleAspectFit
        if isSuccessful {
            statusIconView.image = UIImage(named: "ic_success")
            notiView.backgroundColor = UIColor.rgbValue(hexValue: 0x8dc63f) // green
        } else {
            statusIconView.image = UIImage(named: "ic_fail")
            notiView.backgroundColor = UIColor.rgbValue(hexValue: 0xed1c24) // red
        }
        statusIconView.setConstraintsTo(view: notiView,
                                        topConstant: (notiView.frame.size.height - ICON_HEIGHT) / 2,
                                        bottomConstant: (notiView.frame.size.height - ICON_HEIGHT) / 2,
                                        leftConstant: 16,
                                        rightConstant: notiView.frame.size.width - ICON_WIDTH - 16)
        
        let notiLabel = UILabel()
        notiView.addSubview(notiLabel)
        notiLabel.anchorWithConstantsTo(top: notiView.topAnchor,
                                        left: statusIconView.rightAnchor,
                                        bottom: notiView.bottomAnchor,
                                        right: notiView.rightAnchor,
                                        topConstant: 0,
                                        leftConstant: 8,
                                        bottomConstant: 0,
                                        rightConstant: 16)
        notiLabel.text = message
        notiLabel.font = UIFont(name: "roboto-regular", size: 13)
        notiLabel.textColor = UIColor.white
        notiLabel.numberOfLines = 3
        notiLabel.sizeToFit()
        
        UIView.animate(withDuration: 1,
                       delay: Const.TIME_DISPLAY_NOTIFICATION,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0.5,
                       options: [],
                       animations: {
            notiView.alpha = 0
        },
                       completion: { (success: Bool) in
            notiView.removeFromSuperview()
        })
    }
    
    static func showSuccess(_ message: String) {
        AlertUtil.showNoti(message: message, isSuccessful: true)
    }
    
    static func showError(_ message: String) {
        AlertUtil.showNoti(message: message, isSuccessful: false)
    }

}
