//
//  LogUtil.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class LogUtil: NSObject {
    #if DISABLE_LOG
    static let enable: Bool = false
    #else
    static let enable: Bool = true
    #endif

    static func d(_ tag: String, _ message: String) {
        if (!enable) {
            return
        }
        print("MQLog: [" + tag + "] " + message)
    }
    
    static func d(_ message: String) {
        if (!enable) {
            return
        }
        print("MQLog: " + message)
    }
    
    static func getParamString(dictionary: [String: Any]?) -> String {
        if (!enable) {
            return ""
        }
        if let theJSONData = try? JSONSerialization.data(withJSONObject: dictionary as Any, options: []) {
            return String(data: theJSONData, encoding: .utf8)!
        }
        return ""
    }
}
