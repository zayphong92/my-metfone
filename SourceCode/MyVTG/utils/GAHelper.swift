//
//  GAHelper.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/23/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

class GAHelper: NSObject {
    static func logGA(category: String, actionGA: String, labelGA: String?, valueGA: NSNumber?) {

        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: category,
                                                       action: actionGA,
                                                       label: labelGA,
                                                       value: valueGA).build() as! [AnyHashable : Any]!)
    }
}
