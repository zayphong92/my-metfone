//
//  ValidateUtil.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 8/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ValidateUtil: NSObject {
    
    /* Common func */
    
    static func limitLength(string:String?, length:Int) -> String? {
        guard let str = string, str.count > length  else {
            return string
        }
        return str.substring(to: str.index(str.startIndex, offsetBy: length))
    }
    
    static func validate(number:String, length:Int) -> Bool {
        let numberRegEx = String(format: "^[0-9]{%d}", length)
        let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let result = numberTest.evaluate(with: number)
        return result
    }
    
    /* End common func */
    
    
    /* Validate email */
    static func validate(email:String) -> (Bool, String?) {
        return (true, nil)
    }
    
    
    /* Validate phone number */
    static func limitLength(phoneNumber: String?) -> String? {
        return ValidateUtil.limitLength(string: phoneNumber, length: Vailidate.MAX_PHONE_NUMBER_LENGTH)
    }
    
    static func validate(phoneNumber: String?) -> (iscorrect: Bool, msg: String?) {
        var msg: String? = nil
        guard let phoneNumber: String = phoneNumber, phoneNumber.count > 0 else {
            msg = NSLocalizedString("Label_enter_the_phone_number", comment: "")
            return (false, msg)
        }
        
        if !ValidateUtil.checkPhoneNumberFormat(phoneNumber) {
            msg = NSLocalizedString("Msg_phone_number_not_correct_format", comment: "")
            return (false, msg)
        }
        return (true, nil)
    }
    
    private static func checkPhoneNumberFormat(_ phone: String) -> Bool {
        //let phoneRegEx = String(format: "[0-9]$", Vailidate.PREFIX_PHONE_NUMBER)
//        let phoneRegEx = String(format: "[0-9]")
//        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
//        let result = phoneTest.evaluate(with: phone)
//        return result
        return true
    }
    
    
    /* Validate OTP */
    static func limitLength(otp: String?) -> String? {
        return ValidateUtil.limitLength(string: otp, length: Vailidate.MAX_OTP_LENGTH)
    }
    
    static func validate(otp: String?) -> (isCorrect: Bool, msg: String?) {
        var msg: String? = nil
        guard let _otp: String = otp, _otp.count > 0 else {
            msg = NSLocalizedString("Label_enter_otp", comment: "")
            return (false, msg)
        }
        
        if !ValidateUtil.validate(number: _otp, length: Vailidate.MAX_OTP_LENGTH ) {
            msg = NSLocalizedString("Msg_otp_not_correct_format", comment: "")
            return (false, msg)
        }
        return (true, nil)
    }
    
    
    /* Vailidate Password */
    
    static func limitLength(password: String?) -> String? {
        return ValidateUtil.limitLength(string: password, length: Vailidate.MIN_PASS_LENGTH)
    }
    
    static func validate(password: String?) -> (Bool, String?) {
        var msg: String? = nil
        
        guard let pass:String = password , pass.count > 0 else {
            msg = NSLocalizedString("Label_enter_the_password", comment: "")
            return (false, msg)
        }
        
        if pass.count < Vailidate.MIN_PASS_LENGTH {
            msg = String(format: NSLocalizedString("Msg_Password_short_lenght", comment: ""), Vailidate.MIN_PASS_LENGTH)
            return (false, msg)
        }
        
        if !ValidateUtil.validate(number: pass, length: Vailidate.MIN_PASS_LENGTH) {
            msg = NSLocalizedString("Msg_Password_not_correct_format", comment: "")
            return (false, msg)
        }
        
        return (true, nil)
    }
    
    static func isPasswordMatched(pass:String?, confirmPass:String?) -> (Bool, String?) {
        var msg:String? = nil
        
        guard let confirmPass:String = confirmPass , confirmPass.count > 0 else {
            msg = NSLocalizedString("Label_confirm_the_password", comment: "")
            return (false, msg)
        }
        
        guard let pass:String = pass , pass.characters.count > 0, pass == confirmPass else {
            msg = NSLocalizedString("Msg_Password_not_match", comment: "")
            return (false, msg)
        }

        return (true, nil)
    }
    

}
