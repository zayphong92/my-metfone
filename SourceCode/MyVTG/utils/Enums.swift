//
//  Enums.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

enum HomeFunctions: String {
    case TraCuoc = "charge_his"
    case NapCuoc = "recharge"
    case MuaThemData = "data_purchase"
    case ChuyenTien = "i_share"
    case UngTien = "airtime"
    case DoiGoiData = "data_plans"
    case TimCuaHang = "find_a_store"
    case MuaSo = "number_purchase"
    case Emoney = "e_money"
    case DoiSIM = "change_sim"
}

enum UtilitiesFunctions: Int {
    case TraCuoc = 0
    case NapCuoc = 1
    case MuaThemData = 2
    case DoiGoiData = 3
    case MuaSo = 4
    case DoiSo = 5
    case KhoiPhucSo = 6
    case DoiSim = 7
    case KhoaGoiDi = 8
    case ChuyenTien = 9
    case UngTien = 10
    case TimCuaHang = 11
}

enum EmptyPages: Int {
    case DoiGoiData = 0
    case DichVuDaDangKy = 1
}

enum UtilitiesItems: String {
    // Map NSLocalization key
    case TraCuoc = "UtilitiesItem_TraCuoc"
    case NapCuoc = "UtilitiesItem_NapCuoc"
    case MuaThemData = "UtilitiesItem_MuaData"
    case DoiGoiData = "UtilitiesItem_DoiGoiData"
    case MuaSo = "UtilitiesItem_MuaSo"
    case DoiSo = "UtilitiesItem_DoiSo"
    case KhoiPhucSo = "UtilitiesItem_KhoiPhucSo"
    case DoiSim = "UtilitiesItem_DoiSim"
    case KhoaGoiDi = "UtilitiesItem_KhoaGoiDi"
    case ChuyenTien = "UtilitiesItem_ChuyenTien"
    case UngTien = "UtilitiesItem_UngTien"
    case TimCuaHang = "UtilitiesItem_TimCuaHang"
}

enum MoreFunctions: String {
    case KhuyenMai = "MoreLabel_KhuyenMai"
    case ChamSocKhachHang = "MoreLabel_ChamSocKhachHang"
    case KhachHangThanThiet = "MoreLabel_KhachHangThanThiet"
    case TinTuc = "MoreLabel_TinTuc"
    case TraiNghiem = "MoreLabel_TraiNghiem"
    case DanhGia = "MoreLabel_DanhGia"
    case DoiNgonNgu = "MoreLabel_DoiNgonNgu"
    case DoiMatKhau = "MoreLabel_DoiMatKhau"
    case DangXuat = "MoreLabel_DangXuat"
}

enum CustomerServiceItems: String {
    case ChatOnline = "More_ChamSocKhachHangLabel_ChatOnline"
    case CongDong = "More_ChamSocKhachHangLabel_CongDong"
    case TongDai = "More_ChamSocKhachHangLabel_TongDai"
    case GuiMail = "More_ChamSocKhachHangLabel_GuiMail"
    case CauHoi = "More_ChamSocKhachHangLabel_CauHoi"
}

enum ChargeTypes: String {
    case TaiKhoanChinh = "Label_TaiKhoanChinh"
    case TaiKhoanKhuyenMai = "Label_TaiKhoanKhuyenMai"
    case ThueBaoThang = "Home_TraCuocLabel_ThueBaoThang"
    case CuocGoi = "Home_TraCuocLabel_CuocGoi"
    case TinNhanSMS = "Home_TraCuocLabel_TinNhanSMS"
    case Khac = "Home_TraCuocLabel_Khac"
    case Data = "Home_TraCuocLabel_Data"
    case Vas = "Home_TraCuocLabel_Vas"
}

enum ServiceCodes: String {
    case MuaSo = "BUY_ISDN"
    case DoiSo = "CHANGE_ISDN"
    case DoiSIM = "CHANGE_SIM"
    case KhoaChieuGoiDi = "BLOCK_GOING_CALL"
    case ReSorteIsdn = "RESTORE_ISDN"
}

enum ServiceAction: Int {
    case Unknown = -1
    case Register = 0
    case UnRegister = 1
    
    var name: String {
        switch self {
        case .Unknown: return "Unknown"
        case .Register: return "Register"
        case .UnRegister: return "UnRegister"
        }
    }
}

enum ServiceRegisterStatus: Int {
    case DangSuDung = 1
    case ChuaDangKy = 2
    
    var localizableStringForBtnSubmit: String {
        switch self {
        case .DangSuDung: return NSLocalizedString("Services_HuyDangKyButton", comment: "")
        case .ChuaDangKy: return NSLocalizedString("Services_DangKyButton", comment: "")
        }
    }
    
    var serviceAction: ServiceAction {
        switch self {
        case .DangSuDung: return .UnRegister
        case .ChuaDangKy: return .Register
        }
    }
}

enum RegisterState {
    case Unknown
    case Registering    // đang gọi API để đăng ký
    case UnRegistering  // đang gọi API để hủy đăng ký
    case Registered     // đã đăng ký xong
    case UnRegistered   // đã huỷ đăng ký xong
    
    var localizableStringForBtnSubmit: String {
        switch self {
        case .Registered: return NSLocalizedString("Services_HuyDangKyButton", comment: "")
        case .UnRegistered: return NSLocalizedString("Services_DangKyButton", comment: "")
        default: return ""
        }
    }
    
    var serviceAction: ServiceAction {
        switch self {
        case .Registered: return .UnRegister
        case .UnRegistered: return .Register
        default: return .Unknown
        }
    }
    
    static func willUpdate(state: RegisterState) -> RegisterState {
        switch state {
        case .Registered: return .UnRegistering
        case .UnRegistered: return .Registering
        default: return .Unknown
        }
    }
    
    static func didUpdate(state: RegisterState, success: Bool) -> RegisterState {
        switch state {
        case .UnRegistering: return success ? .UnRegistered : .Registered
        case .Registering: return success ? .Registered : .UnRegistered
        default: return .Unknown
        }
    }
}

enum LoyaltySchemeServices: String {
    case DoiCuoc = "More_KhachHangThanThietLabel_DoiCuoc"
    case DoiData = "More_KhachHangThanThietLabel_DoiData"
    case DoiQua = "More_KhachHangThanThietLabel_DoiQua"
    case ChonQua = "More_KhachHangThanThietLabel_ChonQua"
}

enum ActionType: Int {
    case Register = 0
    case Unregister = 1
}

enum PromotionAction:Int {
    case None = 0
    case Rechange = 1
    case Register = 2
}


enum ServiceType: Int {
    case None
    case BuyPhoneNumber
    case ChangePhoneNumber
    case ReStorePhoneNumber
    case LockSim
    case ChangeDataPackage
    case BuyData
    case ChangeSIM
    case Airtime
    case IShare
    case RegisterUser
    case ForgotPassword
    case Promotion
    case News
    case Services
    case GetOTP
}

enum SubType:Int{
    case Prepay = 1 //Tra truoc
    case Postpaid = 2 //Tra sau
}

enum HotNewsType: Int {
    case Services = 0
    case Promotion = 1
    case News = 2
}




