//
//  NetworkUtil.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/9/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import CoreTelephony
import ReachabilitySwift

class NetworkUtil {
    
    static func getNetworkType() -> String {
        let reachability = Reachability()!
        do {
            try reachability.startNotifier()
            let status = reachability.currentReachabilityStatus
            switch status {
            case .notReachable:
                return "NotConnect"
            case .reachableViaWiFi:
                return "Wifi"
            case .reachableViaWWAN:
                return "3G4G"
            }
        } catch {
            return "Error"
        }
    }
}
