//
//  GoogleAnalyticsConstants.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/23/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

enum ScreenNames: String {
    case Home = "Home"
    case AccountInfo = "Account"
    case PersonalInfo = "SubInfo"
    case Utilities = "Utilities"
    case Services = "Services"
    case Apps = "AppStores"
    case More = "More"
    case ChargeHistory = "Charge history"
    case Recharge = "Recharge"
    case ScratchCard = "NapTheCao"
    case BuyData = "BuyData"
    case ChangeDataPackage = "ChangeDataPackage"
    case PhoneNumberPurchase = "BuyIsdn"
    case ChangePhoneNumber = "ChangeIsdn"
    case RestorePhoneNumber = "RestoreIsdn"
    case ChangeSIM = "ChangeSIM"
    case BlockCalls = "LockIsdn"
    case IShare = "Ishare"
    case AirTime = "AirTime"
    case FindStore = "FindStore"
    case Promotions = "Promotions"
    case CustomerServices = "CustomerCare"
    case LoyaltyScheme = "Privilege"
    case News = "News"
    case HotNews = "HotNews"
    case Experience = "Experience 3G/4G"
    case RatingApp = "Rating app"
    case ChangePassword = "ChangePwd"
    case Logout = "Logout"
}

enum Action: String {
    case View = "View"
    case Click = "Click"
    case Routing = "Routing"
    case Call = "Call"
}
