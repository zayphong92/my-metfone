//
//  Defaults.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 9/28/16.
//  Copyright © 2016 MQ Solutions. All rights reserved.
//

import Foundation

class Defaults {
    static let Key_AccessToken = "AccessToken"
    static let Key_RefreshToken = "RefreshToken"
    static let Key_FirstRun = "FirstRun"
    
    static let userDefaults = UserDefaults.standard
    
    static func setFirstRun() {
        userDefaults.set(true, forKey: Key_FirstRun)
    }
    static func isFirstRun() -> Bool {
        return userDefaults.object(forKey: Key_FirstRun) == nil
    }
    
    static func setAccessToken(token: String?) {
        userDefaults.set(token, forKey: Key_AccessToken)
    }
    static func getAccessToken() -> String? {
        return userDefaults.string(forKey: Key_AccessToken)
    }
    
    static func setRefreshToken(token: String?) {
        userDefaults.set(token, forKey: Key_RefreshToken)
    }
    static func getRefreshToken() -> String? {
        return userDefaults.string(forKey: Key_RefreshToken)
    }
}
