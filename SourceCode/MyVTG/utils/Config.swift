//
//  Config.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 9/21/16.
//  Copyright © 2016 MQ Solutions. All rights reserved.
//

import UIKit

struct OSVersion {
    static func EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(
            version as String,
            options: NSString.CompareOptions.numeric) == .orderedSame
    }
    
    static func GREATER_THAN(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(
            version as String,
            options: NSString.CompareOptions.numeric) == .orderedDescending
    }
    
    static func GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(
            version as String,
            options: NSString.CompareOptions.numeric) != .orderedAscending
    }
    
    static func LESS_THAN(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(
            version as String,
            options: NSString.CompareOptions.numeric) == .orderedAscending
    }
    
    static func LESS_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
        return UIDevice.current.systemVersion.compare(
            version as String,
            options: NSString.CompareOptions.numeric) != .orderedDescending
    }
}

struct ScreenSize {
    static let WIDTH_IPHONE5: CGFloat        = 320.0        // 4 inch
    static let WIDTH_IPHONE6: CGFloat        = 375.0        // 4.7 inch
    static let WIDTH_IPHONE6P: CGFloat       = 414.0        // 5.5 inch
    
    static let HEIGHT_IPHONE5: CGFloat       = 568.0        // 4 inch
    static let HEIGHT_IPHONE6: CGFloat       = 667.0        // 4.7 inch
    static let HEIGHT_IPHONE6P: CGFloat      = 736.0        // 5.5 inch
    static let HEIGHT_IPAD: CGFloat          = 1024.0
    
    static let WIDTH: CGFloat         = UIScreen.main.bounds.size.width
    static let HEIGHT: CGFloat        = UIScreen.main.bounds.size.height
    static let MAX_LENGTH: CGFloat    = max(ScreenSize.WIDTH, ScreenSize.HEIGHT)
    static let MIN_LENGTH: CGFloat    = min(ScreenSize.WIDTH, ScreenSize.HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.MAX_LENGTH < ScreenSize.HEIGHT_IPHONE5
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.MAX_LENGTH == ScreenSize.HEIGHT_IPHONE5
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.MAX_LENGTH == ScreenSize.HEIGHT_IPHONE6
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.MAX_LENGTH == ScreenSize.HEIGHT_IPHONE6P
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.MAX_LENGTH == ScreenSize.HEIGHT_IPAD
}

struct Const {
    static let GOOGLE_API_KEY = "AIzaSyCBS6HFJPHG-3_QNT6sVFNcxRINwCOZN4g"
    //login screen
    static let COLOR_START = UIColor.argbValue(hexValue: 0xdca90e13)
    static let COLOR_END = UIColor.argbValue(hexValue: 0xd9f26522)
    //
    
    static let COLOR_ORANGE = UIColor.argbValue(hexValue: 0xff01A19A)
    static let COLOR_GRAY = UIColor.rgbValue(hexValue: 0xff8c8c8c)
    static let COLOR_ORANGE_ALPHA40 = UIColor.argbValue(hexValue: 0x6601A19A)
    //primary colors
    static let COLOR_MAIN = UIColor.argbValue(hexValue: 0xff01A19A)
    static let COLOR_MAIN_HILIGHTED = UIColor.argbValue(hexValue: 0xffD4FAFC)
    
    //home screen
    static let COLOR_GRAY_FOR_NORMAL_TEXT = UIColor.argbValue(hexValue: 0xff777777)
    static let COLOR_BLACK_FOR_SELECTED_TEXT = UIColor.argbValue(hexValue: 0xff333333)
    static let COLOR_BLACK_ALPHA15 = UIColor.argbValue(hexValue: 0x22000000)
    static let COLOR_GRAY_FOR_BACKGROUND = UIColor.argbValue(hexValue: 0xfff0f0f0)
    static let COLOR_WHITE_ALPHA60 = UIColor.argbValue(hexValue: 0x9affffff)
    static let COLOR_TEXT_DLG_COLOR_01 = UIColor.argbValue(hexValue: 0xff636466)
    static let COLOR_TEXT_HOME_COLOR_01 = UIColor.argbValue(hexValue: 0xff9d9fa2)
    static let COLOR_TEXT_TAB_N = UIColor.argbValue(hexValue: 0xff9d9fa2)
    static let COLOR_PLACE_HOLDER_IMAGE = UIColor.rgbValue(hexValue: 0xf5f5f5)
    static let COLOR_GRAY_FOR_LABEL = Const.COLOR_GRAY
    static let COLOR_GRAY_FOR_BORDER = UIColor.rgbValue(hexValue: 0xcdcdcd)
    
    static let LANG_LOCAL = "km"
    static let LANG_EN = "en"
    
    static let LOADING_VIEW_TAG: Int = 66666
    static let ALERT_VIEW_TAG: Int = 888888
    static let EMPTY_PAGE_TAG: Int = 11110000
    
    static let PHONE_NUMBER_PLACEHOLDER = "ex:977xxxxxxx"
    
    static let KEY_FOR_AVATAR = "avatar"
    static let KEY_FOR_AVATAR_ORIENTATION = "avatar_orientation"
    static let LAST_ACCOUNT_LOGIN_KEY: String = "LAST_ACCOUNT_LOGIN_KEY"
    static let FORMAT_GROUPING_SEPARATOR: String = ","
    static let FORMAT_DECIMAL_SEPARATOR: String = "."
    static let MAX_PHONE_PRICE: Int64  = 9999999999
    
    static let SUPPORT_MANY_RECHARGE_METHODS =  false
    static let MARKET_NAME: MarketName = .Local
    
    static let TIME_DISPLAY_NOTIFICATION =  3.5
    static let APP_VERSION = "1.1.0"
    
    static let ENGLISH_LANGUAGE = "English"
    static let ENGLISH_LANGUAGE_CODE = "EN"
    
    static let LAOS_LANGUAGE = "Khmer"
    static let LAOS_LANGUAGE_CODE = "KH"
}

public enum MarketName: String {
    case Local
    case All
}

struct Vailidate {
    static let MAX_PHONE_NUMBER_LENGTH: Int = 15
    static let MIN_PASS_LENGTH: Int = 6
    static let MAX_OTP_LENGTH: Int = 6
    static let PREFIX_PHONE_NUMBER: String = "977"
}













