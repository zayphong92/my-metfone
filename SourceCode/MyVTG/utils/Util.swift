//
//  Util.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 9/23/16.
//  Copyright © 2016 MQ Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

class Util {
    static func showWarning(message: String) {
        if OSVersion.LESS_THAN(version: "8.0") {
            let alert = UIAlertView()
            alert.message = message
            alert.addButton(withTitle: NSLocalizedString("Button_OK", comment: ""))
            alert.show()
        } else {
            let range = NSRange(location:0,length:message.characters.count)
            let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
            var attr = NSMutableAttributedString()
            let fontAttr = [NSFontAttributeName:UIFont(name: "HiraKakuProN-W3", size: 16.0)!]
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 10.0
            paragraphStyle.alignment = NSTextAlignment.center
            attr = NSMutableAttributedString(string: message, attributes: fontAttr)
            attr.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range)
            attr.addAttributes([NSParagraphStyleAttributeName : paragraphStyle], range: range)
            alert.setValue(attr, forKey: "attributedMessage")
            let action = UIAlertAction(title: NSLocalizedString("Button_OK", comment: ""), style: UIAlertActionStyle.default, handler: nil)
            //action.setValue(attr, forKey: "attributedMessage")
            alert.addAction(action)
            let root: UIViewController! = UIApplication.shared.keyWindow?.rootViewController
            root.present(alert, animated: true, completion: nil)
        }
    }
    
    static func changeTextKeepAttributes(textView: UITextView, text: String) {
        textView.isSelectable = true
        textView.text = text
        textView.isSelectable = false
    }
    
    // get NSDate after today
    static func dateAfter(days: Int, atHour: Int, atMinute: Int) -> NSDate {
        let today = NSDate()
        let cal:NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        var targetDay: NSDate! = cal.date(byAdding: NSCalendar.Unit.day, value: days, to: today as Date, options: []) as NSDate!
        targetDay = cal.date(bySettingHour: atHour, minute: atMinute, second: 0, of: targetDay as Date, options: []) as NSDate!
        return targetDay
    }
    
    static func getFirstDateOfThisWeek() -> Date {
        let today: Date = Date()
        let cal: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let weekDay = cal.components(.weekday, from: today).weekday!
        var daysBefore: Int = 0;
        switch weekDay {
        case 1: // sunday
            daysBefore = 6
            break
        case 2:
            daysBefore = 0;
            break
        case 3:
            daysBefore = 1;
            break
        case 4:
            daysBefore = 2;
            break
        case 5:
            daysBefore = 3;
            break
        case 6:
            daysBefore = 4;
            break
        case 7:
            daysBefore = 5;
            break
        default:
            break
        }
        var comp:DateComponents = DateComponents()
        comp.day = -daysBefore
        return cal.date(byAdding: comp, to: today, options: NSCalendar.Options(rawValue: 0))!
    }
    
    static func getFirstDateOfThisMonth() -> Date {
        let today: Date = Date()
        let cal: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components = cal.components([.day], from: today)
        var comp: DateComponents = DateComponents()
        comp.day = 1 - components.day!
        return cal.date(byAdding: comp, to: today, options: NSCalendar.Options(rawValue: 0))!
    }
    
    static func getLastDateOfLastMonth() -> Date {
        let date = getFirstDateOfThisMonth()
        let cal: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        var comp: DateComponents = DateComponents()
        comp.day = -1
        return cal.date(byAdding: comp, to: date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    static func getFirstDateOfLastMonth() -> Date {
        let date = getLastDateOfLastMonth()
        let cal: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components = cal.components([.day], from: date)
        var comp: DateComponents = DateComponents()
        comp.day = 1 - components.day!
        return cal.date(byAdding: comp, to: date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    static func miliseconds(since date: Date) -> Double {
        return date.timeIntervalSince1970 * 1000.0
    }
    
    static func formatCurrency(_ value: NSNumber) -> String {
        let currencyUnit: String = NSLocalizedString("Currency_Unit", comment: "")
        let formatter = NumberFormatter()
        formatter.groupingSeparator = Const.FORMAT_GROUPING_SEPARATOR
        formatter.decimalSeparator = Const.FORMAT_DECIMAL_SEPARATOR
        formatter.numberStyle = .decimal
        var _value:NSNumber = value
        if (value.doubleValue >= 1000000.0) {
            _value = NSNumber(value: value.doubleValue.rounded())
        } else {
            let tmp = Double(round(100*value.doubleValue)/100)
            _value = NSNumber(value: tmp)
        }
        let str: String = formatter.string(from: _value)!
        return str + " " + currencyUnit
    }
    
    static func number2String(_ number: NSNumber, numberOfDecimal:Int) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = Const.FORMAT_GROUPING_SEPARATOR
        formatter.decimalSeparator = Const.FORMAT_DECIMAL_SEPARATOR
        formatter.numberStyle = .decimal
        let div:Double = pow(10, Double(numberOfDecimal))
        let rounded = Double(round(number.doubleValue * div)/div)
        let str: String = formatter.string(from: NSNumber(value: rounded))!
        return str
    }
    
    static func getCurrencyValue(_ _currencyText:  String) -> NSNumber {
        let currencyUnit: String = NSLocalizedString("Currency_Unit", comment: "")
        var currencyText = _currencyText.replacingOccurrences(of: " " + currencyUnit, with: "")
        currencyText = currencyText.replacingOccurrences(of: Const.FORMAT_GROUPING_SEPARATOR, with: "")
        
        if let value = Double(currencyText) {
            return NSNumber(value:value)
        }
        return 0
    }
    
    static func formatDuration(_ timeInterval: Double) -> String {
        let interval = Int(timeInterval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = interval / 3600
        let hourUnit = NSLocalizedString("Time_Unit_Hour", comment: "")
        let minuteUnit = NSLocalizedString("Time_Unit_Minute", comment: "")
        let secondUnit = NSLocalizedString("Time_Unit_Second", comment: "")
        if hours > 0 {
            return String(format: "%d \(hourUnit) %d \(minuteUnit) %d \(secondUnit)", hours, minutes, seconds)
        } else if minutes > 0 {
            return String(format: "%d \(minuteUnit) %d \(secondUnit)", minutes, seconds)
        } else {
            return String(format: "%d \(secondUnit)", seconds)
        }
    }
    
    
    static func formatDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = NSLocalizedString("Date_Format", comment: "")
        return "" + dateFormatter.string(from: date)
    }

    static func formatMB(_ value: Int) -> String {
        return String(value) + " MB"
    }
    
    static func openWebUrl(_ url: String) {
        UIApplication.shared.openURL(NSURL(string: url)! as URL)
    }
    
    static func getAutoPurgingImageCache() -> AutoPurgingImageCache {
        return AutoPurgingImageCache(memoryCapacity: 300_000_000, preferredMemoryUsageAfterPurge: 200_000_000)
    }
    
    static func setImage(_ btn: UIButton, url: String?, defaultImageName: String? = nil) {
        btn.contentMode = .scaleAspectFill
        btn.clipsToBounds = true
        
        //Init default image
        var defaultImage:Image = UIImage.imageWithColor(color: Const.COLOR_PLACE_HOLDER_IMAGE)
        if let defaultImageName = defaultImageName {
            defaultImage = UIImage.init(named: defaultImageName)!
        }
        btn.setImage(defaultImage, for: .normal)
        guard let url = url, url.characters.count > 0 else { return }
        
        //Load image from cache
        let imageCache = AutoPurgingImageCache()
        let cacheImgae:UIImage? = imageCache.image(withIdentifier: url)
        if cacheImgae != nil { btn.setImage(cacheImgae, for: .normal) }
        
        //Load image from cache
        btn.af_setImage(for: .normal, url: URL(string:url)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, completion: { response in
            if (response.result.value == nil) {
                if cacheImgae != nil {btn.setImage(cacheImgae, for: .normal) }
                else  { btn.setImage(defaultImage, for: .normal) }
            } else {
                imageCache.add(response.result.value!, withIdentifier: url)
            }
        })
        return
    }
    
    static func setImage (_ imv: UIImageView, url: String?, defaultImageName: String? = nil) {
        imv.contentMode = .scaleToFill
        imv.clipsToBounds = true
        
        //Init default image
        var defaultImage:Image = UIImage.imageWithColor(color: Const.COLOR_PLACE_HOLDER_IMAGE)
        if let defaultImageName = defaultImageName {
            defaultImage = UIImage.init(named: defaultImageName)!
        }
        imv.image = defaultImage
        guard let url = url, url.characters.count > 0 else { return }
        
        //Load image from cache
        let imageCache = AutoPurgingImageCache()
        let cacheImgae:UIImage? = imageCache.image(withIdentifier: url)
        if cacheImgae != nil { imv.image = cacheImgae }
        
        //Load image from url
        imv.af_setImage(withURL: URL(string: url)!, completion: {response in
            if (response.result.value == nil) {
                if cacheImgae != nil { imv.image = cacheImgae }
                else  { imv.image = defaultImage }
            } else {
                imageCache.add(response.result.value!, withIdentifier: url)
            }
        })
    }
    
    static func validateMonny(_ value: Double) -> Bool {
        return value <= 999999999999
    }
    
    static func isValidEmail(testStr: String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func setGradientLayer(view: UIView, startColor: UIColor, endColor: UIColor) {
        let gradientColors: [CGColor] = [startColor.cgColor, endColor.cgColor]
        let gradientLocations: [Float] = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations as [NSNumber]
        
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}






