//
//  BaseVC.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AVFoundation

class BaseVC: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var pagerHeaderView: UIView!
    
    // MARK: - Static variables
    var appDelegate: AppDelegate!
    let client = Client.getInstance()
    let myGray = UIColor.argbValue(hexValue: 0xFF8C8C8C)
    
    let loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum nisi non blandit fermentum. Vestibulum lacinia sit amet odio et iaculis. Donec sagittis lectus ut fermentum facilisis. Mauris sagittis sodales odio. Etiam consequat lorem ut ipsum pretium facilisis. Nullam condimentum ex leo, ut lacinia nisl consequat a. Nam quis semper ligula, et vestibulum justo. Integer consequat lobortis leo, in imperdiet est interdum non. Integer consectetur ex quis consectetur cursus. Vestibulum ac eleifend nunc."
    
    var baseScrollView: UIScrollView?
    var baseActiveField: UITextField?
    var baseDefaultContentInset: UIEdgeInsets?
    var screenName: String?
    var actionGA: String?
    var labelGA: String?
    var valueGA: NSNumber?
    var refreshControl: UIRefreshControl? = nil
    private var dlgLoading: DlgLoading? = nil
    
    // MARK: - View life cycle
    private var images: [UIImageView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.statusBarStyle = .lightContent
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        initTabbarItem()
    }
    
    func initTabbarItem() {
        guard let tabBarController: UITabBarController = self.tabBarController else {
            return
        }
        
        let vcCount: Int = (tabBarController.viewControllers?.count)!
        
        for i in 0..<vcCount {
            let item = tabBarController.tabBar.subviews[i + 1]
            let image = item.subviews.first as! UIImageView
            image.tag = i
            image.contentMode = .center
            images.append(image)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        baseDefaultContentInset = baseScrollView?.contentInset
        if  let scrollView = baseScrollView {
            registerForKeyboardNotifications()
            scrollView.keyboardDismissMode = .interactive
        }
        
        if screenName != nil && actionGA != nil {
            GAHelper.logGA(category: screenName!,
                           actionGA: actionGA!,
                           labelGA: labelGA,
                           valueGA: valueGA)
        }
        
        let tc = self.transitionCoordinator
        if (tc != nil) && tc?.initiallyInteractive != nil {
            tc?.notifyWhenInteractionEnds({(context: UIViewControllerTransitionCoordinatorContext) -> Void in
                if !context.isCancelled {
                    self.contextIsNotCancelled()
                }
            })
            if (tc?.isInteractive)! == false {
                self.normalVCPop()
            }
        }
        
        DispatchQueue.main.async {
            self.makeAnimationForTabbarItem()
        }
    }
    
    func normalVCPop() {
        LogUtil.d(#function)
    }
    
    func contextIsNotCancelled() {
        LogUtil.d(#function)
    }
    
    func contextIsCancelled() {
        LogUtil.d(#function)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if baseScrollView != nil {
            deregisterFromKeyboardNotifications()
        }
        let tc = self.transitionCoordinator
        if (tc != nil) && tc?.initiallyInteractive != nil {
            tc?.notifyWhenInteractionEnds({(context: UIViewControllerTransitionCoordinatorContext) -> Void in
                if context.isCancelled {
                    self.contextIsCancelled()
                }
            })
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // MARK: - Support Pull To Refresh / Load more
    var isRefreshable: Bool = false
    // Gọi hàm này để xử lý pull down to refresh cho tất cả các class kế thừa từ BaseVC.
    // Tham số có thể là UIScrollView, UITableView, UICollectionView.
    // Khi nào search thì gọi lại hàm này với enable = false, khi nào cancel search thì lại gọi với enable = true
    func setupPullRefreshForScrollable(_ scrollableView: UIScrollView, enable: Bool) {
        if (enable && refreshControl == nil) {
            refreshControl = UIRefreshControl()
//            refreshControl!.attributedTitle = NSAttributedString(string: NSLocalizedString("Msg_PullToRefresh", comment: ""))
//            refreshControl!.addTarget(self, action: #selector(self.refreshData), for: .valueChanged)
            scrollableView.addSubview(refreshControl!)
            scrollableView.alwaysBounceVertical = true
            
            if (scrollableView.isKind(of: UITableView.self)) {
                (scrollableView as! UITableView).tableFooterView = UIView.init(frame: CGRect.zero)
                //UIView(frame: CGRect(x: 0, y: 0, width: scrollableView.frame.size.width, height: 1))
            }
        }
        
        if (!enable && refreshControl != nil) {
            refreshControl?.removeFromSuperview()
            refreshControl = nil
        }
        isRefreshable = enable
    }
    
    var lastOffset: CGPoint = CGPoint.zero
    var lastOffsetCapture: TimeInterval = 0
    var isScrollingFast: Bool = false
    var isScrollingDown: Bool = false
    var isContentOffsetFarEnough: Bool = false
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isRefreshable {
            let currentOffset = scrollView.contentOffset
            let currentTime = Util.miliseconds(since: Date())
            let timeDiff = currentTime - lastOffsetCapture
            if timeDiff > 0.1 {
                let distance = currentOffset.y - lastOffset.y
                let scrollSpeedNotAbs = (distance * 10) / 1000
                let scrollSpeed = fabsf(Float(scrollSpeedNotAbs))
                if -currentOffset.y > UIScreen.main.bounds.height / 8 {
                    isContentOffsetFarEnough = true
                    if distance < 0 && currentOffset.y <= 0 {
                        isScrollingDown = true
                    }
                }
                
                if scrollSpeed > 0.25 {
                    isScrollingFast = true
                    if distance < 0 && currentOffset.y <= 0 {
                        isScrollingDown = true
                    }
                }
                
                if distance > 0 {
                    isScrollingDown = false
                }
            }
            lastOffset = currentOffset
            lastOffsetCapture = currentTime
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if isRefreshable && isScrollingDown && (isScrollingFast || isContentOffsetFarEnough) {
            refreshData()
            refreshControl?.beginRefreshing()
        }
        if !(NetworkReachabilityManager()?.isReachable)! {
            refreshControl?.endRefreshing()
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        lastOffset = .zero
        lastOffsetCapture = 0
        isScrollingFast = false
        isScrollingDown = false
        isContentOffsetFarEnough = false
    }
    
    // Nếu class kế thừa nào có gọi setupPullRefreshForScrollable thì phải override lại hàm này.
    func refreshData() {

    }
    
    // Nếu class kế thừa nào có gọi setupPullRefreshForScrollable thì phải override hàm refreshData.
    // Sau khi gọi refreshData lấy dữ liệu từ API xong rồi thì phải gọi hàm này để kết thúc quá trình refresh.
    func refreshDataDone() {
        if (refreshControl != nil) {
            refreshControl!.endRefreshing()
        }
    }
    
    func isRefreshing() -> Bool {
        return refreshControl != nil && refreshControl!.isRefreshing
    }
    
    func enableLoadMore(_ enable: Bool, tableView: UITableView) {
        
        guard let networkManager = NetworkReachabilityManager() else {
            LogUtil.d("No network manager")
            return
        }
        
        if !networkManager.isReachable {
            LogUtil.d("no network")
            return
        }
        if (enable) {
            let footerView = UIView.init(frame: CGRect(0, 0, tableView.frame.size.width, 60))
            let imv: UIImageView = UIImageView.init(frame: CGRect(0, 0, 30, 30))
            footerView.addSubview(imv)
            imv.center = footerView.center
            imv.startProgress()
            tableView.tableFooterView = footerView
            LogUtil.d("have network")
        }
    }
    
    func goLogin() {
        let vc = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "loginNC")
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    func goHome() {
        let mainVC = UIStoryboard.getViewController(storyBoardName: "Main", viewControllerName: "rootTabBar")
        UIApplication.shared.keyWindow?.rootViewController = mainVC
    }
    
    func logout() {
        client.logout()
        goLogin()
    }

    func TAG() -> String {
        return String(describing: type(of: self))
    }
        
    // MARK: - Activity indicator
    func showProgress() {
        if (dlgLoading == nil) {
            dlgLoading = UIStoryboard.getViewController(storyBoardName: "DlgLoading", viewControllerName: "DlgLoading") as? DlgLoading
            dlgLoading?.modalPresentationStyle = .overCurrentContext
        }
        present(dlgLoading!, animated: false, completion: nil)
    }
    
    func hideProgress(completion: (() -> Swift.Void)? = nil) {
        if (dlgLoading != nil) {
            if(dlgLoading?.view.window != nil) {
                self.dlgLoading!.dismiss(animated: true, completion: completion)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    self.dlgLoading!.dismiss(animated: false, completion: completion)
                }
            }
        }
    }
    
    func warningRequireTextField(_ tf: UITextField, msg: String) -> Bool {
        if (tf.text?.characters.count == 0) {
            showError(msg)
            return true
        }
        return false
    }
    
    // MARK: - Custom alert
    
    func showAlert(title: String = "",
                   message: String,
                   cancelLabel: String,
                   agreeLabel: String,
                   onOK: @escaping () -> Void = {},
                   onCancel: @escaping () -> Void = {}) {
        let vc = UIStoryboard.getViewController(storyBoardName: "DlgConfirmYesNo",
                                                viewControllerName: "DlgConfirmYesNo") as! DlgConfirmYesNo
        vc.modalPresentationStyle = .overCurrentContext
        vc.strTitle = title
        vc.strContent = message
        vc.lbOK = agreeLabel
        vc.lbCancel = cancelLabel
        vc.onConfirmYes = onOK
        vc.onConfirmNo = onCancel
        present(vc, animated: true, completion: nil)
    }
    
    func showSuccess(_ message: String) {
        AlertUtil.showSuccess(message)
    }
    
    func showError(_ message: String) {
        AlertUtil.showError(message)
    }
    
    // MARK: - Miscellaneous
    
    func setAvatarInView(_ avatarView: UIImageView) {
        
        if let dictionary = UserDefaults.standard.dictionary(forKey: client.getIsdn()),
            let avatarData = dictionary[Const.KEY_FOR_AVATAR] as? NSData {
            let avatarImage = UIImage(data: avatarData as Data)
            let orientation = dictionary[Const.KEY_FOR_AVATAR_ORIENTATION] as! Int
            avatarView.image = UIImage(cgImage: (avatarImage?.cgImage)!, scale: 1.0, orientation: UIImageOrientation(rawValue: orientation)!)
            avatarView.contentMode = .scaleAspectFill
        }
    }
    
    func underlineLabel(_ label: UILabel, withText text: String) {
        label.underline(withText: text)
    }
    
    func addBorderTo(_ view: UIView) {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0,
                              y: view.frame.size.height - width,
                              width: view.frame.size.width,
                              height: view.frame.size.height)
        
        border.borderWidth = width
        view.layer.addSublayer(border)
        view.layer.masksToBounds = true
    }
    
    func addRoundBorderTo(_ view: UIView) {
        view.layer.borderWidth = 0.2
        view.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func goNextHideTabbar(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func goNext(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeLanguage(_ lang: String, reloadScreen: () -> ()) {
        if L102Language.currentAppleLanguage() == lang {
            return
        }
        client.lang = lang
        L102Language.setAppleLanguageTo(lang: lang)
        L012Localizer.DoTheSwizzling()
        reloadScreen()
    }
    
    func showEmptyPageOn(view: UIView, emptyPage: EmptyPages) {
        let emptyPage = client.getEmptyPages()[emptyPage.rawValue]
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        imageView.tag = Const.EMPTY_PAGE_TAG
        
        var url: String = ""
        if L102Language.currentAppleLanguage() == Const.LANG_LOCAL {
            url = emptyPage.localLink
        } else if L102Language.currentAppleLanguage() == Const.LANG_EN {
            url = emptyPage.enLink
        }
        Util.setImage(imageView, url: url)
    }
    
    func hideEmptyPageOn(view: UIView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let imageView = view.viewWithTag(Const.EMPTY_PAGE_TAG)
            imageView?.removeFromSuperview()
        }
    }
    
    // MARK: - Handle scroll view overlapping text field when keyboard appears
    
    func registerForKeyboardNotifications() {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWasShown(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillShow,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillHide,
                                                  object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.baseScrollView?.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.baseScrollView?.contentInset = contentInsets
        self.baseScrollView?.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.baseActiveField {
            if (!aRect.contains(activeField.frame.origin)) {
                self.baseScrollView?.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        self.baseScrollView?.contentInset = baseDefaultContentInset!
        self.baseScrollView?.scrollIndicatorInsets = baseDefaultContentInset!
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        super.textFieldDidBeginEditing(textField)
        baseActiveField = textField
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        super.textFieldDidEndEditing(textField)
        baseActiveField = nil
    }
    
    // MARK: - Segue functions
    
    func segueToAirtime() {
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        vc.headerTitleString = "AIRTIME"
        vc.listServices = []
        vc.serviceCode = HomeFunctions.UngTien.rawValue
        for config in client.getHomeConfig() {
            if config.uri?.serviceCode == HomeFunctions.UngTien.rawValue {
                if L102Language.currentAppleLanguage() == Const.LANG_EN {
                    vc.headerTitleString = config.enTitle
                } else {
                    vc.headerTitleString = config.localTitle
                }
            }
        }
        vc.setPageCount(1, startPosition: 0)
        goNextHideTabbar(vc)
        //goNextHideTabbar(VASFunctionsVC())

    }
    
    func segueToIshare() {
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        vc.serviceCode = HomeFunctions.ChuyenTien.rawValue
        for config in client.getHomeConfig() {
            if config.uri?.serviceCode == HomeFunctions.ChuyenTien.rawValue {
                if L102Language.currentAppleLanguage() == Const.LANG_EN {
                    vc.headerTitleString = config.enTitle
                } else {
                    vc.headerTitleString = config.localTitle
                }
            }
        }
        vc.listServices = []
        vc.setPageCount(1, startPosition: 0)
        goNextHideTabbar(vc)
    }
    
    func segueToVasService(serviceCode : String, title : String) {
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        vc.headerTitleString = title
        vc.listServices = []
        vc.setPageCount(1, startPosition: 0)
        vc.serviceCode = serviceCode
        
        goNextHideTabbar(vc)
    }
    
    func segueToPhoneNumberPurchase() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
        vc.serviceType = .BuyPhoneNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToChangePhoneNumber() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
        vc.serviceType = .ChangePhoneNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToRecharge(showsTabBar: Bool) {
        if (!Const.SUPPORT_MANY_RECHARGE_METHODS) {
            let nextVC = UIStoryboard.getViewController(storyBoardName: "NapCuoc", viewControllerName: "rechargeVC")
            self.navigationController?.pushViewController(nextVC, animated: true)
            
        } else {
            let vc = UIStoryboard.getViewController(storyBoardName: "DlgNapCuoc", viewControllerName: "DlgNapCuoc") as! DlgNapCuoc
            tabBarController?.tabBar.isHidden = true
            vc.modalPresentationStyle = .overCurrentContext
            if showsTabBar {
                vc.showTabBar = {
                    self.tabBarController?.tabBar.isHidden = false
                }
            }
            vc.option1Selected = {
                let nextVC = UIStoryboard.getViewController(storyBoardName: "NapCuoc", viewControllerName: "rechargeVC")
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            
            vc.option2Selected = {
                let url = URL(string: "https://itunes.apple.com/vn/app/bankplus/id661973727?l=vi&mt=8")!
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url)
                }
            }
            
            vc.option3Selected = {
                let url = URL(string: "https://pay.viettel.vn/Index.do?")!
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url)
                }
            }
            present(vc, animated: false, completion: nil)
        }
    }
    
    func segueToChangeSIM() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
        vc.serviceType = .ChangeSIM
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToChargeHistory() {
        let chargeHistoryVC = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "chargeHistoryVC")
        self.navigationController?.pushViewController(chargeHistoryVC, animated: true)
    }
    
    func segueToBuyData() {
        let buyDataVC = UIStoryboard.getViewController(storyBoardName: "BuyData", viewControllerName: "buyDataVC")
        self.navigationController?.pushViewController(buyDataVC, animated: true)
    }
    
    func segueToChangeDataPackage() {
        let changeDataPackageVC = UIStoryboard.getViewController(storyBoardName: "ChangeDataPackage", viewControllerName: "changeDataPackgeVC")
        self.navigationController?.pushViewController(changeDataPackageVC, animated: true)
    }
    
    func segueToRestorePhoneNumber() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
        vc.serviceType = .ReStorePhoneNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToLockSIM() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
        vc.serviceType = .LockSim
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func segueToFindStores() {
        let findStoresVC = UIStoryboard.getViewController(storyBoardName: "FindStores", viewControllerName: "MapVC")
        self.navigationController?.pushViewController(findStoresVC, animated: true)
    }
    
    func segueToCustomerServices() {
        let customerServicesVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ChamSocKhachHang", viewControllerName: "customerServicesVC")
        self.navigationController?.pushViewController(customerServicesVC, animated: true)
    }
    
    func segueToPromotionScreen() {
        let promotionVC = UIStoryboard.getViewController(storyBoardName: "TabMore_KhuyenMai", viewControllerName: "promotionVC")
        self.navigationController?.pushViewController(promotionVC, animated: true)
    }
    
    func segueToListNews(){
        let newsVC = UIStoryboard.getViewController(storyBoardName: "TabMore_TinTuc", viewControllerName: "newsVC")
        self.navigationController?.pushViewController(newsVC, animated: true)
    }
    
    func presentQRScannerController(withResultFilledIn textField: UITextField) {
        let qrScanner = QRScannerController()
        let navController = UINavigationController(rootViewController: qrScanner)
        qrScanner.didFindResult = { (result: String) in
            textField.text = result
        }
        present(navController, animated: true, completion: nil)
    }

    // MARK: - request API
    
    func requestObject<T: ModelBase>(_ apiCode: String, params: [String : Any]?, subCacheKey:String? = nil, complete: @escaping(Bool, T?) -> () = {_ in}) {
        
        if getNavBarRightImageName() == "ic_search" {
            getRightBarButton()?.isEnabled = false
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        client.request(apiCode, params: params, complete: {(resp: DataResponse<Any>) -> Void in
            //LogUtil.d(self.TAG(), "requestObject complete " + apiCode)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if (self.handleApiError(resp, apiCode: apiCode)) {
                complete(false, nil)
                return
            }
            guard let model: T = ModelHelper.from(response: resp) else {
                LogUtil.d(self.TAG(), "requestObject " + apiCode + " success but data nil. Expected data is " + String(describing: T.self))
                complete(false, nil)
                return
            }
            ModelHelper.saveToCache(model, subCacheKey: subCacheKey)
            complete(true, model)
        })
    }
    
    func requestList<T: ModelBase>(_ apiCode: String, params: [String : Any]?, complete: @escaping(Bool, [T]?) -> () = {_ in}) {
        if getNavBarRightImageName() == "ic_search" {
            getRightBarButton()?.isEnabled = false
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        client.request(apiCode, params: params, complete: {(resp: DataResponse<Any>) -> Void in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if (self.handleApiError(resp, apiCode: apiCode)) {
                complete(false, nil)
                return
            }
            guard let arr: [T] = ModelHelper.from(response: resp) else {
                LogUtil.d(self.TAG(), "requestList " + apiCode + " success but data nil. Expected data is array of " + String(describing: T.self))
                complete(false, nil)
                return
            }
            complete(true, arr)
        })
    }
    
    private var actionComplete: (Bool, ModelDoAction?) -> () = {_ in}
    private var actionComplete2: (Bool, ModelDefault?) -> () = {_ in}
    
    func requestAction(_ apiCode: String, params: [String : Any]?, actionComplete: @escaping(Bool, ModelDoAction?) -> () = {_ in}) {
        showProgress()
        self.actionComplete = actionComplete
        let completeCallback = {(success: Bool, data: ModelDoAction?) -> Void in
            self.hideProgress(completion: {
                if (data != nil) {
                    if (data!.userMsg.characters.count > 0) {
                        if (data!.isSuccess()) {
                            self.showSuccess(data!.userMsg)
                        } else {
                            self.showError(data!.userMsg)
                        }
                    }
                } else {
                    LogUtil.d(self.TAG(), "API " + apiCode + " done but ModelDoAction nil")
                }
                self.actionComplete(success, data)
            })
        }
        
        requestObject(apiCode, params: params, complete: completeCallback)
    }
    
    func requestAction02(_ actionCode: String, params: [String : Any]?, isShowProgress: Bool = true, actionComplete: @escaping(Bool, ModelDefault?) -> () = {_ in}) {
        
        if isShowProgress {showProgress()}
        self.actionComplete2 = actionComplete
        let completeCallback = {(success: Bool, data: ModelDefault?) -> Void in
            let completeBlock:()-> Void = {()-> Void in
                if (data != nil) {
                    if (data!.errorMessage.characters.count > 0) {
                        if !data!.isSuccess() {
                            self.showError(data!.errorMessage)
                        }
                    }
                } else {
                    LogUtil.d(self.TAG(), "API " + actionCode + " done but ModelDoAction nil")
                }
                self.actionComplete2(success, data)
            }
            
            if isShowProgress {self.hideProgress(completion: completeBlock)}
            else {completeBlock()}
        }
        
        client.requestApiGateway(apiPart: actionCode, params: params, complete: {(resp: DataResponse<Any>) -> Void in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let model:ModelDefault? = ModelHelper.from(response: resp) ?? nil
            if model == nil {
                completeCallback(false, nil)
                return
            }
            
            if (self.handleApiError(resp, apiCode: actionCode)) {
                completeCallback(false, model!)
                return
            }
            completeCallback(true, model!)
        })
    }
    
    func requestAction03(_ apiCode: String, params: [String : Any]?, actionComplete: @escaping(Bool, ModelDefault?) -> () = {_ in}) {

        showProgress()
        let completeCallback = {(success: Bool, data: ModelDefault?) -> Void in
            self.hideProgress(completion: {
                if (data != nil) {
                    if (data!.errorMessage.characters.count > 0) {
                        if (data!.isSuccess()) {
                            self.showSuccess(data!.errorMessage)
                        } else {
                            self.showError(data!.errorMessage)
                        }
                    }
                } else {
                    LogUtil.d(self.TAG(), "API " + apiCode + " done but ModelDoAction nil")
                }
                actionComplete(success, data)
            })
        }
        requestObject(apiCode, params: params, complete: completeCallback)
    }
    
    
    private var regUnregComplete: (Bool, ModelDoAction?) -> () = {_ in}
    
    // Đăng ký/Huỷ đăng ký dịch vụ
    func regUnregService(model: ModelRegistable,
                         extraParams: [String: Any]? = nil,
                         needConfirm: Bool = true,
                         labelConfirmOK: String = "",
                         labelConfirmCancel: String = "",
                         onCancel: @escaping () -> () = {_ in},
                         onComplete: @escaping (Bool, ModelDoAction?) -> () = {_ in}) {
        var msg: String = ""
        let action: Int = model.action.rawValue
        self.regUnregComplete = onComplete
        let performRequest: () -> () = {
            var params:[String:Any] = [:]
            if (extraParams != nil && extraParams!.count > 0) {
                params = extraParams!
            }
            params.updateValue(action, forKey: "actionType")
            params.updateValue(model.code, forKey: "serviceCode")
            model.willUpdateRegisterState()
            self.requestAction(Client.WS_DO_ACTION_SERVICE, params: params, actionComplete: {(success: Bool, data: ModelDoAction?) -> Void in
                model.didUpdateRegisterState(success: success)
                self.regUnregComplete(success, data)
            })
        }
        
        if (!needConfirm) {
            performRequest()
            return
        }
        
        if (model.action == .Register) {
            if (model.price > 0) {
                let format = NSLocalizedString("Msg_Confirm_Service_Subscribe_WithPrice", comment: "")
                let priceStr = Util.formatCurrency(NSNumber(value: model.price))
                msg = String.init(format: format, model.name, priceStr)
            } else {
                let format = NSLocalizedString("Msg_Confirm_Service_Subscribe", comment: "")
                msg = String.init(format: format, model.name)
            }
        } else {
            let format = NSLocalizedString("Msg_Confirm_Service_UnSubscribe", comment: "")
            msg = String.init(format: format, model.name)
        }
        let labelOK = labelConfirmOK == "" ? NSLocalizedString("Button_OK", comment: "") : labelConfirmOK
        let labelCancel = labelConfirmCancel == "" ? NSLocalizedString("Button_Cancel", comment: "") : labelConfirmCancel
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: msg,
                  cancelLabel: labelCancel,
                  agreeLabel: labelOK,
                  onOK: {
                    performRequest()
        },
                  onCancel: {
                    onCancel()
        })
    }
    
    // handle API error
    // return true means failure, otherwise success
    func handleApiError(_ resp: DataResponse<Any>, apiCode: String) -> Bool {
        let error: ModelApiError? = checkApiError(resp)
        if (error == nil) {
            return false
        }
        LogUtil.d(TAG(), "API " + apiCode + " error -> " + error!.dbgMsg);
        
        if isNetworkNotAvailabel(resp) {
            showError(NSLocalizedString("More_Network_not_available", comment: ""))
            return true
        }
        
        if (error!.shouldReLogin()) {
            let vc = UIStoryboard.getViewController(storyBoardName: "DlgConfirmYesNo", viewControllerName: "DlgConfirmYesNo") as! DlgConfirmYesNo
            vc.modalPresentationStyle = .overCurrentContext
            vc.strTitle = NSLocalizedString("Label_XacNhanThongBao", comment: "")
            vc.strContent = NSLocalizedString("Msg_SessionExpired", comment: "")
            vc.isCloseButtonHidden = true
            vc.onConfirmYes = {
                self.logout()
            }
            vc.onConfirmNo = {
                self.logout()
            }
            self.present(vc, animated: true, completion: nil)
            return true
        }
        if (error!.userMsg.characters.count > 0) {
            showError(error!.userMsg)
        }
        return true
    }
    
    func isLostConnectionToServer(_ resp: DataResponse<Any>) -> Bool {
        guard let error = resp.error else {
            return false
        }
        return error._code == NSURLErrorTimedOut || error._code == -1005
    }
    
    func isNetworkNotAvailabel(_ resp: DataResponse<Any>) -> Bool {
        guard let error = resp.error else {
            return false
        }
        return error._code == -1009
    }
    
    
    private func checkApiError(_ resp: DataResponse<Any>) -> ModelApiError? {
        if (resp.result.isFailure) {
            let statusCode:Int = resp.error!._code
            let dbgMsg:String = "Request error. Code: \(statusCode), message: \(resp.error!.localizedDescription)"
            return ModelApiError(dbgMsg: dbgMsg, userMsg: "")
        }
        
        let value = resp.result.value as! [String: Any]
        var errorCode_ApiGateway: String = Client.ERR_CODE_API_GATEWAY_SUCCESS
        var errorMessage_ApiGateway: String = ""
        if (value.index(forKey: "errorCode") != nil) {
            errorCode_ApiGateway = value["errorCode"] as! String
        }
        if (value.index(forKey: "errorMessage") != nil) {
            errorMessage_ApiGateway = value["errorMessage"] as! String
        }
        if (errorCode_ApiGateway != Client.ERR_CODE_API_GATEWAY_SUCCESS) {
            // Error from API Gateway
            return ModelApiError(dbgMsg: "Api Gateway error. Code: " + errorCode_ApiGateway + ", message: " + errorMessage_ApiGateway,
                                 userMsg: errorMessage_ApiGateway)
        }
        if (value.index(forKey: "result") != nil) {
            var errorCode_Backend: String = Client.ERR_CODE_BACKEND_SUCCESS
            var userMsg: String = ""
            var message: String = ""
            if let result = value["result"] as? [String: Any] {
                if (result.index(forKey: "userMsg") != nil) {
                    if let val = result["userMsg"] as? String {
                        userMsg = val
                    }
                }
                if (result.index(forKey: "message") != nil) {
                    if let val = result["message"] as? String {
                        message = val
                    }
                }
                if (result.index(forKey: "errorCode") != nil) {
                    if let val = result["errorCode"] as? String {
                        errorCode_Backend = val
                    }
                }
                if (errorCode_Backend != Client.ERR_CODE_BACKEND_SUCCESS && errorCode_Backend != Client.ERR_CODE_BACKEND_SUCCESS_1000) {
                    // API Gateway success but error from Backend
                    return ModelApiError(dbgMsg: "Backend error. " + message, userMsg: userMsg)
                }
            }
        }
        return nil
    }
    
    func registerService(serviceCode: String) {
        let params: [String : Any] = [
            "actionType": ActionType.Register.rawValue,
            "serviceCode": serviceCode
        ]
        requestAction(Client.WS_DO_ACTION_SERVICE, params: params)
    }
    
    func registerDataPackage(_ packInfo : ModelUserService, actionComplete: @escaping(Bool, ModelDoAction?) -> () = {_ in}) {
         let message: String = String.init(format: NSLocalizedString("Home_DoiGoiDataLabel_ThongBao1", comment: ""), packInfo.code, packInfo.shortDes)
        
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: message,
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: {
                    let params: [String : Any] = ["actionType": 0, "serviceCode": packInfo.code]
                    self.requestAction(Client.WS_DO_ACTION_SERVICE, params: params, actionComplete: actionComplete)
        })
    }
    
    func buyExtraData(package: String, dataVolume: ModelPackageDataToBuy) {
        let volume: String = "\(Util.formatMB(Int(dataVolume.volume)))"
        let price: String = "\(Util.formatCurrency(NSNumber(value: dataVolume.price)))"
        let message: String = String(format:NSLocalizedString("Home_ThemDataLaebl_ThongBaoMuaData", comment: ""), volume, price)
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: message,
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: { () -> Void in
                    let params = [
                        "subType": self.client.getSubType(),
                        "packageCode":  package,
                        "volume": dataVolume.volume,
                        "price": dataVolume.price
                        ] as [String : Any]
                    self.requestAction(Client.WS_DO_BUY_DATA, params: params)
        })
    }
    
    // MARK: - Show/hide loading view, alert
    
    func showProgressOnView(_ view: UIView) {
        hideProgressOnView(view)
        
        func createProgress() -> Void {
            let loading_size : CGFloat = 50.0
            var x: CGFloat
            var y: CGFloat
            x = (view.frame.size.width - loading_size)/2
            y = (view.frame.size.height - loading_size)/2
            let loadingImage: UIImageView = UIImageView.init(frame: CGRect.init(x, y, loading_size, loading_size))
            view.addSubview(loadingImage)
            loadingImage.startProgress()
            loadingImage.tag = Const.LOADING_VIEW_TAG
        }
        
        if (isViewLoaded && (view.window != nil)) {
            createProgress()
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                createProgress()
            }
        }
    }
    
    func hideProgressOnView(_ view: UIView) {
        func removeLoadingFromView(_ view: UIView) {
            let loadingImage: UIImageView? = view.viewWithTag(Const.LOADING_VIEW_TAG) as? UIImageView
            loadingImage?.stopProgress()
            loadingImage?.removeFromSuperview()
            if loadingImage != nil {
                LogUtil.d("Fetched loading image")
            }
        }
        
        if (isViewLoaded && (view.window != nil)) {
            removeLoadingFromView(view)
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                removeLoadingFromView(view)
            }
        }
    }
    
    func showAlertOnView(_ view: UIView, _ message: String) {
        hideAlertOnView(view)
        func createAlert() -> Void {
            let size: CGSize = message.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
            let w: CGFloat = view.frame.size.width
            let lines: Int = Int(ceilf(Float(size.width/w)))
            let h: CGFloat = size.height * CGFloat (lines) + CGFloat(lines - 1) * 3.0
            let x: CGFloat = 0
            let y: CGFloat = (view.bounds.size.height - h)/2
            let label:UILabel = UILabel.init(frame: CGRect(x, y, w, h))
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.textColor = Const.COLOR_GRAY_FOR_LABEL
            label.text = message
            
            view.addSubview(label)
            label.tag = Const.ALERT_VIEW_TAG
        }
        
        // viewController is visible
        if (isViewLoaded && (view.window != nil)) {
            createAlert()
        } else {
            let when = DispatchTime.now() + 0.2
            DispatchQueue.main.asyncAfter(deadline: when) {
                createAlert()
            }
        }
    }
    
    func hideAlertOnView(_ view: UIView) {
        let alertView: UIView? = view.viewWithTag(Const.ALERT_VIEW_TAG)
        alertView?.removeFromSuperview()
    }
    
    func hidenKeyboard() {
        view.endEditing(true)
    }
    
    func getOtp(isdn: String, complete:  @escaping(_ success:Bool) -> () = {_ in}) {
        client.getOtp(isdn:isdn, complete: {(resp: DataResponse<Any>) -> Void in
            if (self.handleApiError(resp, apiCode: "getOtp")) {
                complete(false)
                return
            }
            complete(true)
        })
    }
    
    func loginWithOtp(otp:String, isdn: String) -> () {
        showProgress()
        client.loginOtp(otp, isdn: isdn, complete: {(resp: DataResponse<Any>) -> Void in
            self.hideProgress()
            if (self.handleApiError(resp, apiCode: "LoginOtp")) {
                return
            }
            
            guard let modelUser: ModelUserLogin = ModelHelper.from(response: resp) else {
                return
            }
            ModelHelper.saveToCache(modelUser)
            self.client.userLogin = modelUser
            self.goHome()
        })
    }
    
    func scrollUpWhenAppear(_ scrollView : UIScrollView) {
        DispatchQueue.main.async {
            scrollView.setContentOffset(CGPoint.init(0, -50), animated: false)
            scrollView.setContentOffset(CGPoint.init(0, 0), animated: true)
        }
    }
    
    func makeAnimationForTabbarItem() {
        let selectedIndex = self.tabBarController?.selectedIndex ?? 0
        for image in images {
            if image.tag == selectedIndex { scaleUp(image)}
            else { scaleDown(image)}
        }
    }
    
    func scaleUp(_ item:UIView) {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: UIViewAnimationOptions(), animations: { () -> Void in
            
            let rotate = CGAffineTransform(scaleX: 1.1, y: 1.1)
            item.transform = rotate
        }, completion: nil)
    }
    
    func scaleDown(_ item:UIView) {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: UIViewAnimationOptions(), animations: { () -> Void in
            let rotate = CGAffineTransform(scaleX: 0.9, y: 0.9)
            item.transform = rotate
        }, completion: nil)
    }
    
    
    func doLockSim() {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: "Do you want to block out-going way of number \(client.getIsdn())",
            cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
            agreeLabel: NSLocalizedString("Button_OK", comment: ""),
            onOK: {
                let params = [
                    "subType": self.client.getSubType()
                ]
                self.requestAction(Client.WS_DO_LOCK_CALL_GO_ISDN, params: params)
        })
    }

    
}

class ModelApiError {
    var dbgMsg: String = ""
    var userMsg: String = ""
    
    init(dbgMsg: String, userMsg: String) {
        self.dbgMsg = dbgMsg
        self.userMsg = userMsg
    }
    
    func shouldReLogin() -> Bool {
        return userMsg.contains("Session does not exist") || userMsg.contains("Token invalid")
    }
}











