//
//  FindStoresViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/1/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class FindStoresViewController: BaseVC, UITableViewDataSource, UITableViewDelegate, GMSMapViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var provinceTxtField: UITextFieldRegular01!
    @IBOutlet weak var districtTxtField: UITextFieldRegular01!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var notiView: UIView!
    @IBOutlet weak var notiLabel: UILabel!
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    
    // MARK: - Variables
    
    var placesClient: GMSPlacesClient!
    var locationManager = CLLocationManager()
    var zoomLevel: Float = 8.0
    var mapView: GMSMapView!
    var defaultTableHeaderHeight: CGFloat = 0
    
    var listStores = [ModelStore]()
    var filteredStores = [ModelStore]()
    
    var province: ModelProvince? = nil
    var district: ModelDistrict? = nil
    var searchActive = false
    var tableIsUp: Bool = false
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var didAskForEnablingLocationServices: Bool = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang01", comment: ""))
        self.notiView.isHidden = true
        screenName = ScreenNames.FindStore.rawValue
        actionGA = Action.View.rawValue
        setSearchType(.local)
        placesClient = GMSPlacesClient.shared()
        
        setupLocationManager()
        getRightBarButton()?.isEnabled = false
        addMapView()
        
        self.tableView.tableFooterView = UIView()
        notiLabel.alpha = 0
        defaultTableHeaderHeight = (tableView.tableHeaderView?.frame.size.height)!
        setSearchType(.local)
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
    }
    
    func addMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: 17.970898,
                                              longitude: 102.637671,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: self.mapContainerView.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        // Add the map to the view, hide it until we've got a location update.
        mapContainerView.addSubview(mapView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang01", comment: ""))
        tabBarController?.tabBar.isHidden = true
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.province == nil && listStores.count == 0 {
            if CLLocationManager.locationServicesEnabled() && self.locationManager.location != nil {
                self.getNearestStores()
            } else {
                if !self.didAskForEnablingLocationServices {
                    self.askToEnableLocationServices()
                    self.didAskForEnablingLocationServices = true
                }
                self.getDefaultStores()
            }
        }
    }
    
    func askToEnableLocationServices() {
        self.showAlert(title: NSLocalizedString("Home_TimCuaHangLabel_BatGPS", comment: ""),
                       message: NSLocalizedString("Home_TimCuaHangLabel_ThongBao3", comment: ""),
                       cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                       agreeLabel: NSLocalizedString("Home_TimCuaHangButton_CauHinh", comment: ""),
                       onOK: {
                        UIApplication.shared.openURL(URL(string: "App-Prefs:root=Privacy&path=LOCATION")!)
        },
                       onCancel: {})
    }
    
    // MARK: - UI handle
    
    func addMarkers(_ data: [ModelStore]) {
        mapView.clear()
        var index = 0
        for store in data {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: store.latitude, longitude: store.longitude))
            let markerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
            markerView.image = #imageLiteral(resourceName: "ic_store1")
            marker.iconView = markerView
            marker.title = store.name
            marker.snippet = store.addr
            marker.map = self.mapView
            markerView.alpha = 0
            UIView.animate(withDuration: 1,
                           delay: (0.02 + Double(index) / 200) * Double(index),
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: [.curveEaseOut],
                           animations: {
                            markerView.alpha = 1
            },
                           completion: {_ in

            })
            index += 1
        }
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        toggleSearchBar()
        moveTableUp()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        moveTableDown()
    }
    
    func moveTableUp() {
        if tableView.contentOffset.y < (self.tableView.tableHeaderView?.frame.size.height)! && tableView.visibleCells.count < tableView.numberOfRows(inSection: 0) {
            self.tableViewBottom.constant = -(self.tableView.tableHeaderView?.frame.size.height)! + tableView.contentOffset.y
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           options: [.allowAnimatedContent, .curveEaseInOut],
                           animations: {_ in
                            self.tableView.contentOffset = CGPoint.zero
                            self.tableView.visibleCells.forEach {
                                $0.frame.origin.y -= (self.tableView.tableHeaderView?.frame.size.height)!
                            }
                            self.selectionView.subviews.forEach({ $0.alpha = 0 })
                            self.tableView.tableHeaderView?.frame.size.height = 0
                            self.view.layoutIfNeeded()
            },
                           completion: {_ in
                            self.tableViewBottom.constant = 0
                            self.tableView.reloadData()
            })
        })
        tableIsUp = true
    }
    
    func moveTableDown() {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: [.allowAnimatedContent, .curveEaseInOut],
                       animations: {_ in
                        self.tableView.contentOffset = CGPoint.zero
                        self.selectionView.subviews.forEach({ $0.alpha = 1 })
                        self.tableView.tableHeaderView?.frame.size.height = self.defaultTableHeaderHeight
                        self.tableView.visibleCells.forEach {
                            $0.frame.origin.y += (self.tableView.tableHeaderView?.frame.size.height)!
                        }
                        self.view.layoutIfNeeded()
        },
                       completion: {_ in
                        self.searchActive = false
                        self.tableView.reloadData()
        })
        tableIsUp = false
    }
    
    // MARK: - API request handle
    
    func getNearestStores() {
        showProgressOnView(notiView)
        notiView.isHidden = false
        notiLabel.alpha = 0
        province = nil
        district = nil
        provinceTxtField.text = nil
        districtTxtField.text = nil
        let params = [
            "longitude": locationManager.location?.coordinate.longitude as Any,
            "latitude": locationManager.location?.coordinate.latitude as Any
        ] as [String: Any]
        requestList(Client.WS_GET_NEAREST_STORES, params: params, complete: {(success: Bool, data: [ModelStore]?) -> Void in
            self.hideProgressOnView(self.notiView)
            self.notiView.isHidden = true
            if (success && data != nil && data!.count != 0) {
                self.listStores = data!
                self.addMarkers(data!)
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.getRightBarButton()?.isEnabled = false
                self.listStores = []
                self.mapView.clear()
            }
            self.tableView.reloadData()
            self.animateTable()
            self.setupNotiView()
        })
    }
    
    
    func findStoreByAddr() {
        showProgressOnView(notiView)
        notiView.isHidden = false
        notiLabel.alpha = 0
        let params = [
            "provinceId": province?.id as Any,
            "districtId": district?.id as Any,
            "longitude": locationManager.location?.coordinate.longitude as Any,
            "latitude": locationManager.location?.coordinate.latitude as Any
        ] as [String: Any]
        requestList(Client.WS_FIND_STORE_BY_ADDR, params: params, complete: {(success: Bool, data: [ModelStore]?) -> Void in
            self.hideProgressOnView(self.notiView)
            self.notiView.isHidden = true
            if (success && data != nil && data!.count != 0) {
                self.listStores = data!
                let newPosition = GMSCameraPosition.camera(withLatitude: data![0].latitude,
                                                           longitude: data![0].longitude,
                                                           zoom: self.zoomLevel)
                self.mapView.animate(to: newPosition)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.addMarkers(data!)
                })
                self.getRightBarButton()?.isEnabled = true
                self.locationManager.stopUpdatingLocation()
            } else {
                self.getRightBarButton()?.isEnabled = false
                self.listStores = []
                self.mapView.clear()
            }
            self.tableView.reloadData()
            self.animateTable()
            self.setupNotiView()
        })
    }
    
    func getDefaultStores() {
        showProgressOnView(notiView)
        notiView.isHidden = false
        notiLabel.alpha = 0
        let params = [
            "provinceId": "PNP",
            "districtId": "-1",
            "longitude": locationManager.location?.coordinate.longitude as Any,
            "latitude": locationManager.location?.coordinate.latitude as Any
        ]
        requestList(Client.WS_FIND_STORE_BY_ADDR, params: params, complete: {(success: Bool, data: [ModelStore]?) -> Void in
            self.hideProgressOnView(self.notiView)
            self.notiView.isHidden = true
            if (success && data != nil && data!.count != 0) {
                self.listStores = data!
                self.addMarkers(data!)
                self.mapView.camera = GMSCameraPosition.camera(withLatitude: 17.970898,
                                                               longitude: 102.637671,
                                                               zoom: self.zoomLevel)
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.getRightBarButton()?.isEnabled = false
                self.listStores = []
                self.mapView.clear()
            }
            self.tableView.reloadData()
            self.animateTable()
            self.setupNotiView()
        })
    }
    
    // This view notices user when there is no store in the list.
    func setupNotiView() {
        if listStores.count == 0 {
            notiView.isHidden = false
            UIView.animate(withDuration: 0.8, animations: {
                self.notiLabel.alpha = 1
            })
        } else {
            notiView.isHidden = true
        }
    }
    
    var loadingMapView = UIView()
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        let status = CLLocationManager.authorizationStatus()
        if !CLLocationManager.locationServicesEnabled() || !(status == .authorizedAlways || status == .authorizedWhenInUse) {
            askToEnableLocationServices()
        } else {
            if locationManager.location != nil {
                moveToCurrentLocation()
                getNearestStores()
            } else {
                loadingMapView = UIView(frame: self.mapContainerView.bounds)
                loadingMapView.backgroundColor = UIColor.argbValue(hexValue: 0xAAFFFFFF)
                let loadingLabel = UILabel(frame: loadingMapView.bounds)
                loadingLabel.center = loadingMapView.center
                loadingLabel.textAlignment = .center
                loadingLabel.text = NSLocalizedString("Home_TimCuaHangLabel_ThongBao5", comment: "")
                loadingLabel.font = UIFont(name: "roboto-regular", size: 16)
                loadingLabel.textColor = Const.COLOR_GRAY_FOR_LABEL
                loadingMapView.addSubview(loadingLabel)
                self.mapContainerView.addSubview(loadingMapView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    while (true) {
                        if self.locationManager.location == nil {
                            continue
                        } else {
                            self.loadingMapView.removeFromSuperview()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                                self.moveToCurrentLocation()
                                self.getNearestStores()
                            })
                            break
                        }
                    }
                })
            }
        }
        return true
    }
    
    func moveToCurrentLocation() {
        let currentPosition = getCurrentPosition()!
        self.mapView.animate(to: currentPosition)
    }
    
    func getCurrentPosition() -> GMSCameraPosition? {
        var currentPosition: GMSCameraPosition?
        if let currentLocation = locationManager.location {
            currentPosition = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude,
                                                           longitude: currentLocation.coordinate.longitude,
                                                           zoom: self.zoomLevel)
        }
        return currentPosition
    }
    
    // MARK: - Table handle
    
    func animateTable() {
        let cells = tableView.visibleCells
        let tableHeight: CGFloat = tableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1,
                           delay: 0.05 * Double(index),
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: [],
                           animations: {
                            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            },
                           completion: nil)
            index += 1
        }
    }
    
    override func onHeaderSearch(keyword: String) {
        filteredStores.removeAll()
        var store: ModelStore
        
        for i in 0..<listStores.count {
            store = listStores[i]
            let address = store.addr
            let name = store.name
            let range1 = address.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            let range2 = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range1 != nil || range2 != nil {
                filteredStores.append(store)
            }
        }
        searchActive = keyword != ""
        self.tableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredStores.count
        }else {
            number = listStores.count
        }
        //lifesup_sprint_2_T8_2018_start
        if provinceTxtField.text != "" {
            number = listStores.count
        }else{
            number = 0
        }
        //lifesup_sprint_2_T8_2018_end
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "store", for: indexPath) as! StoreCell
        let store: ModelStore = getModelAtIndex(indexPath.row)
        
        cell.nameLabel.text = store.name
        cell.addressLabel.text = store.addr
        cell.distanceLabel.text = "\(String(format: "%.2f", store.distance)) Km"

        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelStore {
        if searchActive {
            return filteredStores[index]
        } else {
            return listStores[index]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let store: ModelStore = getModelAtIndex(row)
        let vc = UIStoryboard.getViewController(storyBoardName: "FindStores", viewControllerName: "storeDetail") as! StoreDetailViewController
        vc.store = store
        vc.mapView = mapView
        if searchActive {
            onCancelSearch()
        } else {
            if tableIsUp {
                moveTableDown()
            }
        }
        navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        getRightBarButton()?.isEnabled = false
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        getRightBarButton()?.isEnabled = true
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            getRightBarButton()?.isEnabled = true
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == provinceTxtField {
            let chooseProvinceVC = UIStoryboard.getViewController(storyBoardName: "FindStores", viewControllerName: "chooseProvinceVC") as! ChooseCityViewController
            chooseProvinceVC.afterPopVC = {
                self.province = chooseProvinceVC.province
                self.district = nil
                if self.province != nil {
                    self.provinceTxtField.text = self.province!.name
                    self.findStoreByAddr()
                }
                self.districtTxtField.text = nil
            }
            chooseProvinceVC.province = self.province
            self.navigationController?.pushViewController(chooseProvinceVC, animated: true)
            return false
        } else if textField == districtTxtField {
            if province == nil {
                showError(NSLocalizedString("Home_TimCuaHangLabel_ThongBao2", comment: ""))
            } else {
                let chooseDistrictVC = UIStoryboard.getViewController(storyBoardName: "FindStores", viewControllerName: "chooseDistrictVC") as! ChooseDistrictViewController
                chooseDistrictVC.province = province
                chooseDistrictVC.afterPopVC = {
                    self.district = chooseDistrictVC.district
                    if self.district != nil {
                        self.districtTxtField.text = self.district!.name
                        self.findStoreByAddr()
                    }
                }
                chooseDistrictVC.district = self.district
                self.navigationController?.pushViewController(chooseDistrictVC, animated: true)
            }
            return false
        }
        return true
    }
}

// MARK: - Extension

extension FindStoresViewController: CLLocationManagerDelegate {
    //Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
        LogUtil.d("Did update location")
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.settings.myLocationButton = true
            mapView.settings.compassButton = true
            mapView.isMyLocationEnabled = true
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
    
    //Handle authorization for the location manager
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location was restricted")
        case .denied:
            print("User denied access to location")
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK")
        }
    }
    
    //Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}






