//
//  DetailViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

class DetailViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabelLight01!
    @IBOutlet weak var subnameLabel: UILabelLight01!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var actionButton: UIButtonRegular01!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBotConstraint: NSLayoutConstraint!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var firstTimeRequestInfo: Bool = true
    var coverImage: UIImage?
    var navTitle: String = ""
    var imgUrl: String?
    var name: String?
    var subname: String?
    var content: String?
    var notice: String?
    var actionName: String?
    var leftBarButton = "back"
    var isButtonEnabled: Bool = true
    var buttonTapped: () -> Void = {}
    var getData: () -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getData()
    }
    
    // MARK: - UI Handle
    
    func setupUI() {
        setupNavBar(title: navTitle)
        Util.setImage(coverImageView, url: imgUrl, defaultImageName: "chuyen_tien_image")
        nameLabel.text = name
        subnameLabel.text = subname
        contentLabel.text = content
        //noticeLabel.text = notice
        actionButton.setTitle(actionName, for: .normal)
        contentLabel.sizeToFit()
        noticeLabel.sizeToFit()
        noticeLabel.isHidden = true
        noticeLabel.isEnabled = false
        containerViewHeight.constant = 268 + 20 + contentLabel.frame.size.height + noticeLabel.frame.size.height
        if !isButtonEnabled {
            actionButton.isHidden = true
            bottomView.isHidden = true
            scrollViewBotConstraint.constant = 0
        }
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func setButtonName(_ name: String) {
        actionButton.setTitle(name, for: .normal)
    }
    
    func setNoticeText(_ notice: String) {
        //noticeLabel.text = notice
    }
    
    override func getNavBarLeftImageName() -> String {
        return leftBarButton
    }
    
    // MARK: - Actions handle
    
    @IBAction func actionTapped(_ sender: UIButtonRegular01) {
        buttonTapped()
    }
    
}
