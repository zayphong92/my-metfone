//
//  FeedbackEmailViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class FeedbackEmailViewController: BaseVC, UITextViewDelegate {
    
    // MARK: - Outlets & variables
    
    @IBOutlet weak var fromLabel: UILabelRegular03!
    @IBOutlet weak var emailTextField: UITextFieldRegular01!
    @IBOutlet weak var subjectLabel: UILabelRegular03!
    @IBOutlet weak var subjectTextField: UITextFieldRegular01!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sendEmailButton: UIButtonRegular01!
    
    var contentTextViewIsBeingEdited = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTextFields()
        setupUI()
    }
    
    func setupUI() {
        baseScrollView = scrollView
        contentTextView.layer.masksToBounds = true
        contentTextView.textColor = Const.COLOR_BLACK_FOR_SELECTED_TEXT
        setupNavBar(title: NSLocalizedString("ScreenTitle_GuiMailGopY01", comment: ""))
        self.automaticallyAdjustsScrollViewInsets = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hidenKeyboard))
        self.navigationController?.navigationBar.addGestureRecognizer(tapGesture)
    }
    
    func setupTextFields() {
        emailTextField.placeholder = NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_NguoiGuiPlaceholder", comment: "")
        emailTextField.title = NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_NguoiGui", comment: "")
        emailTextField.errorColor = .red
        listenToTextFieldDidChange()
        subjectTextField.placeholder = NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_TieuDePlaceholder", comment: "")
        subjectTextField.title = NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_TieuDe", comment: "")
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    // MARK: - Tap functions
    
    @IBAction func closeClicked(_ sender: UIButton) {
        discardConfirm()
    }
    
    func discardConfirm() {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_ThongBao1", comment: ""),
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: { self.navigationController?.popViewController(animated: true) },
                  onCancel: {})
    }

    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        deregisterFromKeyboardNotifications()
    }
    
    @IBAction func sendEmail(_ sender: UIButtonRegular01) {
        
        guard emailTextField.text != nil, emailTextField.text! != "" else {
            showError(NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_ThongBao2", comment: ""))
            return
        }
        
        guard Util.isValidEmail(testStr: emailTextField.text!) else {
            showError(NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_ThongBao3", comment: ""))
            return
        }
        
        guard !contentTextViewIsEmpty() else {
            showError(NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_ThongBao4", comment: ""))
            return
        }
        
        let params = [
            "fromAddr": emailTextField.text!,
            "subject": subjectTextField.text!,
            "content": contentTextView.text
        ] as [String : Any]
        requestAction(Client.WS_DO_SEND_EMAIL, params: params)
    }
    
    func contentTextViewIsEmpty() -> Bool {
        return contentTextView.text == ""
    }
}











