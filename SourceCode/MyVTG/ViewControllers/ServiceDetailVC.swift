//
//  ServiceDetailVC.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ServiceDetailVC: CarouselVC {
    var serviceCode: String = ""
    var listServices: [ModelServiceItem] = []
    var listServiceDetails: [String: ModelServiceDetail] = [:]
    var regStt: ServiceRegisterStatus = .ChuaDangKy
    var headerTitleString: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib("ServiceDetailPage", reuseId: "serviceDetailPage")
        listServiceDetails = [:]
        setupNavBar(title: getTitleStringForPosition(startPosition))
    }

    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func pageDidChanged(_ position: Int) {
        setHeaderTitle(getTitleStringForPosition(position))
    }
    
    func getModelServiceItemAt(_ index: Int) -> ModelServiceItem {
        return listServices[index]
    }
    
    func getServiceDetailByCode(_ code: String) -> ModelServiceDetail? {
        if let model: ModelServiceDetail = listServiceDetails[code] {
            return model
        }
        return nil
    }
    
    func putServiceDetail(_ model: ModelServiceDetail) {
        listServiceDetails.updateValue(model, forKey: model.code)
    }
    
    override func initPageCellAt(_ index: Int) -> CellCarouselItem {
        let cell: ServiceDetailPage = pagerView.dequeueReusableCell(withReuseIdentifier: "serviceDetailPage", at: index) as! ServiceDetailPage
        cell.owner = self
        return cell
    }
    
    private func getTitleStringForPosition(_ position: Int) -> String {
        if listServices.count == 0 {
            return headerTitleString
        }
        let model = listServices[position]
        if (headerTitleString.characters.count == 0) {
            headerTitleString = NSLocalizedString("ScreenTitle_DichVu03", comment: "")
        }
        return headerTitleString + "\n" + model.name
    }
}
