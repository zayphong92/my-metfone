//
//  LoyaltySchemeViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class LoyaltySchemeViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var tableView: UITableView!
    
    var loyaltySchemeStore = LoyaltySchemeStore()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.LoyaltyScheme.rawValue
        actionGA = Action.View.rawValue
        loyaltySchemeStore.initAllLoyaltySchemeServices()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet01", comment: ""))
        self.automaticallyAdjustsScrollViewInsets = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
    
    override func onBtnRightNavBar() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "detailScreen") as! DetailViewController
        vc.navTitle = NSLocalizedString("ScreenTitle_KhachHangThanThiet02", comment: "")
        vc.coverImage = #imageLiteral(resourceName: "chuyen_tien_image")
        vc.name = NSLocalizedString("MoreLabel_KhachHangThanThiet", comment: "")
        vc.content = loremIpsum
        vc.isButtonEnabled = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat(15.0)
        }
        return CGFloat(0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltySchemeStore.allLoyaltySchemeServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "loyaltySchemeServiceCell", for: indexPath) as! LoyaltySchemeServiceCell
        let service = loyaltySchemeStore.allLoyaltySchemeServices[indexPath.row]
        cell.iconImage.image = service.icon
        cell.nameLabel.text = service.name
        if indexPath.row == loyaltySchemeStore.allLoyaltySchemeServices.count - 1 {
            cell.separatorInset = UIEdgeInsets(top: 0, left: self.view.frame.size.width, bottom: 0, right: 0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let value = loyaltySchemeStore.allLoyaltySchemeServices[indexPath.row].value
        switch value {
        case .DoiCuoc:
            GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
            let redeemForCreditVC = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "redeemForCreditVC")
            self.navigationController?.pushViewController(redeemForCreditVC, animated: true)
        case .DoiData:
            GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
            let redeemForDataVC = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "redeemForDataVC")
            self.navigationController?.pushViewController(redeemForDataVC, animated: true)
        case .DoiQua:
            GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
            let redeemForGiftsVC = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "redeemForGiftsVC")
            self.navigationController?.pushViewController(redeemForGiftsVC, animated: true)
        case .ChonQua:
            GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
            let chooseBirthdayGiftsVC = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "chooseBirthdayGiftsVC")
            self.navigationController?.pushViewController(chooseBirthdayGiftsVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}









