//
//  ServiceDetail01VC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 8/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class ServiceDetail01VC: BaseVC, UITableViewDataSource, UITableViewDelegate, CellServiceDetailDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var service: ModelServiceItem? = nil
    
    var modelServiceDetail: ModelServiceDetail? = nil
    var regStt: ServiceRegisterStatus = .ChuaDangKy
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar(title: NSLocalizedString("ScreenTitle_DichVu03", comment: ""))
        tableView.backgroundColor = UIColor.white
        tableView.register(UINib.init(nibName: "CellServiceDetail01", bundle: nil), forCellReuseIdentifier: "cellServiceDetail01")
        tableView.register(UINib.init(nibName: "CellServiceDetail02", bundle: nil), forCellReuseIdentifier: "cellServiceDetail02")
        tableView.register(UINib.init(nibName: "CellServiceDetail03", bundle: nil), forCellReuseIdentifier: "cellServiceDetail03")
        tableView.register(UINib.init(nibName: "CellServiceDetail04", bundle: nil), forCellReuseIdentifier: "cellServiceDetail04")
        
        getData()
    }
    
    func getData() {
        showProgressOnView(tableView)
        let params = ["serviceCode": service?.code ?? ""]
        requestObject(Client.WS_GET_SERVICE_DETAIL, params: params, complete: {(success: Bool, data: ModelServiceDetail?) -> Void in
   
            self.hideProgressOnView(self.tableView)
            if (success && data != nil) {
                data!.isRegistered = self.regStt == .DangSuDung
                self.modelServiceDetail = data
                self.tableView.reloadData()
                
                self.setupNavBar(title: (data?.name)!)
                self.hideAlertOnView(self.tableView)
            } else {
                let message:String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.tableView, message)
            }
        })
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return modelServiceDetail == nil ? 0 : 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return modelServiceDetail!.hasSubPackages() ? modelServiceDetail!.packages!.count : 1
        case 2: return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isCell01(indexPath)) {
            return 240
        }
        if (isCell02(indexPath) || isCell04(indexPath)) {
            return 80
        }
        if (isCell03(indexPath)) {
            let width = self.view.frame.size.width - 20
            let tv = UITextViewRegular01()
            tv.setHTMLFromString(htmlText: modelServiceDetail!.fullDes)
            let size = tv.sizeThatFits(CGSize(width, CGFloat.greatestFiniteMagnitude))
            return size.height + 20
        }
        return 0
    }

    
    private func isCell01(_ indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    private func isCell02(_ indexPath: IndexPath) -> Bool {
        if (indexPath.section == 1) {
            return modelServiceDetail!.hasSubPackages()
        }
        return false
    }
    
    private func isCell03(_ indexPath: IndexPath) -> Bool {
        if (modelServiceDetail!.hasSubPackages()) {
            return indexPath.section == 2
        } else {
            return indexPath.section == 1
        }
    }
    
    private func isCell04(_ indexPath: IndexPath) -> Bool {
        if (modelServiceDetail!.hasSubPackages()) {
            return false
        } else {
            return indexPath.section == 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (isCell01(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellServiceDetail01", for: indexPath) as! CellServiceDetail01
            cell.setupUI(modelServiceDetail!)
            return cell
        }
        if (isCell02(indexPath)) {
            let subPkg: ModelServiceDetailSubPackage = modelServiceDetail!.packages![indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellServiceDetail02", for: indexPath) as! CellServiceDetail02
            cell.delegate = self
            cell.setupUI(subPkg, isRegisterAble: modelServiceDetail!.isRegisterAble)
            return cell
        }
        if (isCell03(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellServiceDetail03", for: indexPath) as! CellServiceDetail03
            cell.setupUI(modelServiceDetail!)
            return cell
        }
        if (isCell04(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellServiceDetail04", for: indexPath) as! CellServiceDetail04
            cell.delegate = self
            cell.setupUI(modelServiceDetail!, regStt: regStt)
            return cell
        }
        return UITableViewCell.init()
    }
    
    func regUnregService(_ model: ModelRegistable) {
        regUnregService(model: model, onCancel: {
            self.registerServiceDone(false, model: model)
        }, onComplete: {(success: Bool, data: ModelDoAction?) -> Void in
            self.registerServiceDone(success, model: model)
        })
    }
    
    private func registerServiceDone(_ success: Bool, model: ModelRegistable) {
        tableView.reloadData()
    }

}
