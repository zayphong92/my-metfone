//
//  AppStoreViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class AppStoreViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    fileprivate var modelAppStore: ModelAppStore? = nil
    fileprivate var filteredApps = [ModelApp]()
    fileprivate var searchActive: Bool = false
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.Apps.rawValue
        actionGA = Action.View.rawValue
        
        viewPager.dataSource = self as ViewPagerDataSource
        self.automaticallyAdjustsScrollViewInsets = false
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        setSearchType(.local)
        let headerView = tableView.tableHeaderView
        headerView?.frame.size.height = ScreenSize.WIDTH / 5 * 2
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_Apps01", comment: ""))
        navigationController?.setNavigationBarHidden(false, animated: false)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        getData()
        if modelAppStore != nil { scrollUpWhenAppear(tableView) }
    }
    

    override func refreshData() {
        getData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        requestObject(Client.WS_GET_ALL_APPS, params: nil, complete: {(success: Bool, data: ModelAppStore?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil && (data?.apps.count)! > 0) {
                //remove if it has not IOS link
                //self.filterData(data!)
                self.modelAppStore = data!
                self.viewPager.reloadData()
                self.tableView.reloadData()
                self.hideAlertOnView(self.tableView)
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.showAlertOnView(self.tableView, NSLocalizedString("Msg_DataNotFound", comment: ""))
                self.getRightBarButton()?.isEnabled = false
            }
        })
    }
    
    func filterData(_ data: ModelAppStore) {
        var index = 0
        while index < data.apps.count {
            if data.apps[index].iosLink.slice(from: "app/", to: "/id") != nil {
                index += 1
                continue
            }
            data.apps.remove(at: index)
        }
    }
    
    override func getNavBarLeftImageName() -> String {
        return ""
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredApps.removeAll()
        var app: ModelApp
        if modelAppStore != nil {
            for i in 0..<modelAppStore!.apps.count {
                app = modelAppStore!.apps[i]
                let name = app.name
                let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
                if range != nil {
                    filteredApps.append(app)
                }
            }
        }
        
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int = 0
        if searchActive {
            number = filteredApps.count
        } else {
            if modelAppStore != nil {
                number = modelAppStore!.apps.count
            }
        }
        return number
    }
    
    private let appURLSchemes = ["fb120722621443198"]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "app", for: indexPath) as! AppCell
        let app = getModelAtIndex(indexPath.row)
        let appUrlArr = app?.iosLink.components(separatedBy: "?")
        let appUrl = appUrlArr![0]
        let appUrlSchema = appUrlArr![1]
        Util.setImage(cell.iconImage, url: app?.iconUrl, defaultImageName: "main_data_pack")
        cell.nameLabel.text = app?.name
        cell.explanationLabel.text = app?.shortDes
        guard let appName = app?.iosLink.slice(from: "app/", to: "/id"),
            let url = URL(string: appUrlSchema) else {
                cell.statusButton.isHidden = true
                return cell
        }
        if UIApplication.shared.canOpenURL(url) {
            cell.statusButton.setTitle(NSLocalizedString("Apps_OpenButton", comment: ""), for: .normal)
            LogUtil.d("Can open app")
        } else {
            cell.statusButton.setTitle(NSLocalizedString("Apps_InstallButton", comment: ""), for: .normal)
            LogUtil.d("Can not open app")
        }
        cell.statusButton.isHidden = false
        cell.iconImage.clipsToBounds = true
        
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelApp? {
        if searchActive {
            return filteredApps[index]
        } else {
            return modelAppStore?.apps[index]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let app = modelAppStore?.apps[indexPath.row] else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        let infoVC = UIStoryboard.getViewController(storyBoardName: "DlgConfirmYesNo", viewControllerName: "DlgConfirmYesNo") as! DlgConfirmYesNo
        infoVC.modalPresentationStyle = .overCurrentContext
        infoVC.heightOfCloseButton = 0
        infoVC.heightOfOKButton = 0
        infoVC.strTitle = app.name
        infoVC.strContent = app.fullDes.html2String
        present(infoVC, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func openTapped(_ sender: UIButtonRegular01) {
        if let cell = sender.superview?.superview as? AppCell {
            let indexPath = tableView.indexPath(for: cell)
            var url: URL?
            
            func getUrlFromApp(_ app: ModelApp) -> URL? {
                if cell.statusButton.currentTitle == NSLocalizedString("Apps_OpenButton", comment: "") {
                    return URL(string: "fb120722621443198://")
                } else {
                    return URL(string: app.iosLink)
                }
            }
            
            if searchActive {
                let app = filteredApps[(indexPath?.row)!]
                url = getUrlFromApp(app)
            } else {
                let app = modelAppStore?.apps[(indexPath?.row)!]
                if app != nil {
                    url = getUrlFromApp(app!)
                }
            }
            
            if (url != nil && UIApplication.shared.canOpenURL(url!)) {
                UIApplication.shared.openURL(url!)
                LogUtil.d("Opening \(url!)")
            }
        }
        
        if searchActive {
            onCancelSearch()
        }
    }
}

// MARK: - Table handle

extension AppStoreViewController: ViewPagerDataSource {
    
    //Tells how many pages we have
    func numberOfItems(viewPager: ViewPager) -> Int {
        if modelAppStore != nil {
            return modelAppStore!.adBanner.count
        }
        return 0
    }
    
    //Load views for each page
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        var newView = view
        var imageView = UIImageView()
        if newView == nil {
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (newView?.frame.width)!, height: (newView?.frame.height)!))
            Util.setImage(imageView, url: modelAppStore?.adBanner[index].adImgUrl, defaultImageName: "chuyen_tien_image")
            newView?.addSubview(imageView)
            
            imageView.setConstraintsTo(view: newView, topConstant: 0, bottomConstant: 0, leftConstant: 0, rightConstant: 0)
        }
        return newView!
    }
    
    func didSelectedItem(index: Int) {
        if let url = URL(string: (modelAppStore?.adBanner[index - 1].sourceLink)!) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
