//
//  StorePaymentViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class StorePaymentViewController: BaseVC {
    @IBOutlet weak var alertView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_MuaSo04", comment: ""))
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOpacity = 1
        alertView.layer.shadowOffset = CGSize.zero
        alertView.layer.shadowRadius = 2
        alertView.layer.shadowPath = UIBezierPath(rect: alertView.bounds).cgPath
        alertView.layer.shouldRasterize = true
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    @IBAction func closeIconTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
