//
//  ChooseCityViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/5/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChooseCityViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var cityTableView: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var listProvinces = [ModelProvince]()
    var filteredProvinces = [ModelProvince]()
    var province: ModelProvince? = nil
    var searchActive: Bool = false
    var afterPopVC: () -> Void = {}
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang02", comment: ""))
        setupPullRefreshForScrollable(cityTableView, enable: !searchActive)
        setTextFieldAsInputSearchBar(searchBar)
        cityTableView.tableFooterView = UIView()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang02", comment: ""))
        cityTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(cityTableView.frame.size.width), height: 1))
    }
    
    func animateTable() {
        cityTableView.reloadData()
        
        let cells = cityTableView.visibleCells
        let tableHeight: CGFloat = cityTableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 0.8,
                           delay: 0.05 * Double(index),
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: [],
                           animations: {
                            cell.transform = CGAffineTransform(translationX: 0, y: 0);
            },
                           completion: nil)
            index += 1
        }
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func refreshData() {
        getData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            cityTableView.isHidden = true
        }
        requestList(Client.WS_GET_PROVINCES, params: nil, complete: {(success: Bool, data: [ModelProvince]?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.cityTableView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil) {
                self.listProvinces = data!
                self.cityTableView.reloadData()
                self.animateTable()
            }
        })
    }
    
    // MARK: - Table handle

    override func onHeaderSearch(keyword: String) {
        filteredProvinces.removeAll()
        var provinceToCompare: ModelProvince
        for i in 0..<listProvinces.count {
            provinceToCompare = listProvinces[i]
            let name = provinceToCompare.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredProvinces.append(provinceToCompare)
            }
        }
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(cityTableView, enable: !searchActive)
        cityTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredProvinces.count
        } else {
            number = listProvinces.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var province: ModelProvince
        let cell = tableView.dequeueReusableCell(withIdentifier: "city", for: indexPath) as! ChooseCityCell
        if searchActive {
            province = filteredProvinces[indexPath.row]
        } else {
            province = listProvinces[indexPath.row]
        }
        
        cell.nameLabel.text = province.name
        cell.icon.image = UIImage(named: "ic_tick_s")
        cell.icon.isHidden = true
        if let selectedProvince = self.province {
            if selectedProvince.id == province.id {
                cell.icon.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var chosenProvince: ModelProvince
        if searchActive {
            chosenProvince = filteredProvinces[indexPath.row]
        } else {
            chosenProvince = listProvinces[indexPath.row]
        }
        province = chosenProvince
        cityTableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { 
            self.navigationController?.popViewController(animated: true)
            self.afterPopVC()
            if self.searchActive {
                self.searchBar.text = ""
                self.searchActive = false
                self.cityTableView.reloadData()
                self.view.endEditing(true)
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
