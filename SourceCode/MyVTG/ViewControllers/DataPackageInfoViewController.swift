//
//  DataPackageInfoViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/12/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class DataPackageInfoViewController: BaseVC {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var explanation: UITextViewRegular01!
    @IBOutlet weak var cancelPackageButton: UIButton!
    
    var dataPackage: DataPackage!
    
    @IBAction func cancelPackage(_ sender: UIButton) {
        
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: NSLocalizedString("Home_ThemDataLabel_ThongBao1", comment: ""),
                  cancelLabel: NSLocalizedString("Label_KhongDongY", comment: ""),
                  agreeLabel: NSLocalizedString("Label_DongY", comment: ""))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_MuaThemData02", comment: ""))
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = dataPackage.name
        explanation.text = dataPackage.explanation
    }
}
