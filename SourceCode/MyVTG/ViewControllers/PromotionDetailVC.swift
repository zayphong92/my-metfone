//
//  PromotionDetailVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PromotionDetailVC: CarouselVC {
    
    var promotions: [ModelPromotion] = []
    var promotionDetails: [String: ModelPromotionInfo] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib("PromotionDatailPage", reuseId: "PromotionDatailPage")
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhuyenMaiChoBan02", comment: ""))
        promotionDetails = [:]
    }
    
    override func initPageCellAt(_ index: Int) -> CellCarouselItem {
        let cell: PromotionDatailPage = pagerView.dequeueReusableCell(withReuseIdentifier: "PromotionDatailPage", at: index) as! PromotionDatailPage
        cell.controller = self
        return cell
    }
    
    func getPromotionAtIndex(_ index: Int) -> ModelPromotion {
        return promotions[index]
    }
    
    func getPromotionDetailByCode(_ code: String) -> ModelPromotionInfo? {
        if let model: ModelPromotionInfo = promotionDetails[code] {return model}
        return nil
    }
    
    func savePromotionDetail(_ model: ModelPromotionInfo) {
        promotionDetails.updateValue(model, forKey: model.code)
    }
    
    override func pageDidChanged(_ position: Int) {
        setHeaderTitle(promotions[position].name)
    }
    

}
