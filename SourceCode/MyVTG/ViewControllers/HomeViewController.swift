//
//  HomeViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class HomeViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhoneNo: UILabel!
    @IBOutlet weak var layoutHotNews: UIView!
    
    @IBOutlet weak var lbMainPkgName: UILabel!
    @IBOutlet weak var lbDataPkgName: UILabel!
    @IBOutlet weak var lbValueTkChinh: UILabel!
    @IBOutlet weak var lbValueTkKm: UILabel!
    @IBOutlet weak var lbValueData: UILabel!
    @IBOutlet weak var lbTkChinh: UILabel!
    @IBOutlet weak var lbTkKm: UILabel!
    @IBOutlet weak var lbData: UILabel!
    @IBOutlet weak var lbUnitTkChinh: UILabel!
    @IBOutlet weak var lbUnitTkKm: UILabel!
    @IBOutlet weak var lbUnitData: UILabel!
    
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var avatarMarginRight: NSLayoutConstraint!
    @IBOutlet weak var personalInfoLeading: NSLayoutConstraint!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    let btnTags = [
        1001,       // btnTraCuoc
        1002,       // btnNapCuoc
        1003,       // btnThemData
        1004,       // btnChuyenTien
        1005,       // btnUngTien
        1006,       // btnDoiGoiData
        1007,       // btnTimCuaHang
        1008,       // btnMuaSo
        1009        // btnEMoney
    ]
    
    let btnLblTags = [
        101,    // TraCuoc
        102,    // NapDuoc
        103,    // ThemData
        104,    // ChuyenTien
        105,    // UngTien
        106,    // DoiGoiData
        107,    // TimCuaHang
        108,    // MuaSo
        109     // Emoney
    ]
    
    let defaultIcons = [
        "tra_cuoc",    // TraCuoc
        "nap_cuoc",    // NapDuoc
        "them_data",    // ThemData
        "chuyen_tien",    // ChuyenTien
        "ung_tien",    // UngTien
        "doi_goi_data",    // DoiGoiData
        "tim_cua_hang",    // TimCuaHang
        "mua_so",    // MuaSo
        "e_money"     // Emoney
    ]

    
    var modelAppConfig: ModelAppConfig?
    var hotNews: [ModelHotNews]? = nil
    var hotNewHeight: CGFloat = 0.0
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPager.dataSource = self as ViewPagerDataSource
        setupGAIdentity()
        setupUI()
        getData()
        configFontWeight()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        tabBarController?.tabBar.isHidden = false
        setupPullRefreshForScrollable(scrollView, enable: true)
        setAvatarInView(avatarView)
    }
    
    private func setupGAIdentity() {
        screenName = ScreenNames.Home.rawValue
        actionGA = Action.View.rawValue
    }
    
    private func setupUI() {
        setupBars()
        setupViews()
        setupButtons()
        setupSpecificForDeviceModel()
    }
    
    private func setupBars() {
        navigationController?.setNavigationBarHidden(true, animated: true)
        tabBarController?.tabBar.isHidden = false
        automaticallyAdjustsScrollViewInsets = false
    }
    
    private func setupViews() {
        if ScreenSize.WIDTH > ScreenSize.WIDTH_IPHONE5 {
            contentViewHeight.constant = ScreenSize.HEIGHT - (tabBarController?.tabBar.frame.height)!
        }
        avatarView.layer.cornerRadius = avatarView.frame.size.height / 2
        avatarView.layer.masksToBounds = true
        view.backgroundColor = UIColor.rgbValue(hexValue: 0xf2f2f2)
        let iOSver = (UIDevice.current.systemVersion as NSString).floatValue
        if (iOSver >= 11) {
            contentViewTopConstraint.constant -= 20
        }
    }
    
    private func setupButtons() {
        btnSeeAll.layer.cornerRadius = 3
        for tag in btnTags {
            if let btn: UIButton = self.view.viewWithTag(tag) as? UIButton {
                btn.imageView?.contentMode = .scaleAspectFit
            }
        }
    }
    
    private func configFontWeight() {
        if ScreenSize.WIDTH > ScreenSize.WIDTH_IPHONE6P {
            lbName.font = UIFont(name: "roboto-medium", size: 21)!
            lbPhoneNo.font = UIFont(name: "roboto-regular", size: 19)!
            lbMainPkgName.font = UIFont(name: "roboto-light", size: 20)!
            lbDataPkgName.font = UIFont(name: "roboto-light", size: 20)!
            lbTkChinh.font = UIFont(name: "roboto-regular", size: 17)!
            lbTkKm.font = UIFont(name: "roboto-regular", size: 17)!
            lbData.font = UIFont(name: "roboto-regular", size: 17)!
            lbValueTkChinh.font = UIFont(name: "roboto-bold", size: 23)!
            lbValueTkKm.font = UIFont(name: "roboto-bold", size: 23)!
            lbValueData.font = UIFont(name: "roboto-bold", size: 23)!
            lbUnitTkChinh.font = UIFont(name: "roboto-bold", size: 19)!
            lbUnitTkKm.font = UIFont(name: "roboto-bold", size: 19)!
            lbUnitData.font = UIFont(name: "roboto-bold", size: 19)!
            btnSeeAll.titleLabel?.font = UIFont(name: "roboto-medium", size: 23)!
        }
    }
    
    override func refreshData() {
        getData()
    }
    
    private func getData() {
        getSubMainInfo()
        getHotNews()
        getAppConfig()
    }
    
    // Vì nếu 1 trong 3 request (getSubMainInfo hoặc getSubAccountInfo hoặc getAppConfig) fail đều sẽ hiện thông báo nên cần 1 biến cờ để không bị hiện nhiều lần thông báo
    var needToLogOutPopUp: Bool = false
    
    private func showLogOutPopUp() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DlgConfirmYesNo", viewControllerName: "DlgConfirmYesNo") as! DlgConfirmYesNo
        vc.modalPresentationStyle = .overCurrentContext
        vc.strTitle = NSLocalizedString("Label_XacNhanThongBao", comment: "")
        vc.strContent = NSLocalizedString("Msg_FunctionNotReady", comment: "")
        vc.isCloseButtonHidden = true
        vc.onConfirmYes = {
            self.logout()
        }
        vc.onConfirmNo = {
            self.logout()
        }
        present(vc, animated: true, completion: nil)
    }
    
    private func getSubMainInfo() {
        setupUISubMainInfo(ModelHelper.loadFromCache())
        requestObject(Client.WS_GET_SUB_MAIN_INFO, params: nil, complete: {(success: Bool, data: ModelSubMainInfo?) -> Void in
            self.refreshDataDone()
            if (success && data != nil) {
                self.setupUISubMainInfo(data)
                self.client.subMainInfo = data
                // Phải đợi getSubMainInfo xong rồi mới lấy được subType để gọi getSubAccountInfo
                self.getSubAccountInfo()
            } else {
                self.handleGetConfigError(request: Client.WS_GET_SUB_MAIN_INFO)
            }
        })
    }
    
    private func setupUISubMainInfo(_ model: ModelSubMainInfo?) {
        if (model == nil) {
            return
        }
        if model!.subType == 1 {
            lbTkChinh.text = NSLocalizedString("HomeLabel_TraTruoc_TkChinh", comment: "")
            lbTkKm.text = NSLocalizedString("HomeLabel_TraTruoc_TkKm", comment: "")
        } else if model!.subType == 2 {
            lbTkChinh.text = NSLocalizedString("HomeLabel_TraSau_TkChinh", comment: "")
            lbTkKm.text = NSLocalizedString("HomeLabel_TraSau_TkKm", comment: "")
        }
        lbName.text = model?.name
        lbPhoneNo.text = client.getIsdn()
    }
    
    private func handleGetConfigError(request: String) {
        var model: ModelBase?
        switch request {
        case Client.WS_GET_SUB_MAIN_INFO:
            model = client.subMainInfo
        case Client.WS_GET_SUB_ACCOUNT_INFO:
            model = client.subAccountInfo
        case Client.WS_GET_APP_CONFIG:
            model = client.appConfig
        default:
            break
        }
        if (model == nil) && !needToLogOutPopUp { // Nếu model == nil tức là mới đăng nhập, còn đăng nhập rồi thì sẽ nhảy vào else và không log out
            showLogOutPopUp()
            needToLogOutPopUp = true
        } else {
            showError(NSLocalizedString("Msg_FunctionNotReady", comment: ""))
        }
    }
    
    private func getSubAccountInfo() {
        setupUISubAccountInfo(ModelHelper.loadFromCache())
        let params = [
            "subType": client.getSubType()
        ]
        requestObject(Client.WS_GET_SUB_ACCOUNT_INFO, params: params, complete: {(success: Bool, data: ModelSubAccountInfo?) -> Void in
            if (success && data != nil) {
                self.setupUISubAccountInfo(data)
                self.client.subAccountInfo = data
            } else {
                self.handleGetConfigError(request: Client.WS_GET_SUB_ACCOUNT_INFO)
            }
        })
    }
    
    private func setupUISubAccountInfo(_ model: ModelSubAccountInfo?) {
        if (model == nil) {
            return
        }
        
        if (model!.name.characters.count > 0) {
            lbMainPkgName.text = model!.name
        } else {
            lbMainPkgName.text = ""
        }
        
//        if (model!.dataPkgName.characters.count > 0 ) {
//            var pkgName = ""
//            if model!.name.characters.count > 0 {
//                pkgName = " - " + model!.dataPkgName
//            }
//            lbDataPkgName.text = pkgName
//        } else {
//            lbDataPkgName.text = ""
//        }
        
        switch client.getSubType() {
        case SubType.Prepay.rawValue:
            lbValueTkChinh.text = Util.number2String(NSNumber(value: model!.mainAcc), numberOfDecimal: 2)
            lbValueTkKm.text = Util.number2String(NSNumber(value: model!.proAcc), numberOfDecimal: 2)
            lbValueData.text = Util.number2String(NSNumber(value: model!.dataVolume), numberOfDecimal: 0)
            
            let currencyUnit = NSLocalizedString("Currency_Unit", comment: "")
            lbUnitTkChinh.text = currencyUnit
            lbUnitTkKm.text = currencyUnit
            lbUnitData.text = "MB"
            break
            
        case SubType.Postpaid.rawValue:
            lbValueTkChinh.text = Util.number2String(NSNumber(value: model!.prePost), numberOfDecimal: 2)
            lbValueTkKm.text = Util.number2String(NSNumber(value: model!.debPost), numberOfDecimal: 2)
            lbValueData.text = Util.number2String(NSNumber(value: model!.dataVolume), numberOfDecimal: 0)
            
            let currencyUnit = NSLocalizedString("Currency_Unit", comment: "")
            lbUnitTkChinh.text = currencyUnit
            lbUnitTkKm.text = currencyUnit
            lbUnitData.text = "MB"
            break
            
        default:
            break
            
        }
    }
    
    private func getAppConfig() {
        if let cacheData: ModelAppConfig = ModelHelper.loadFromCache() {
            self.client.appConfig = cacheData
            self.setupUIConfig(cacheData)
        }
        
        let params = [
            "version": Const.APP_VERSION
        ]
        requestObject(Client.WS_GET_APP_CONFIG, params: params, complete: {(success: Bool, data: ModelAppConfig?) -> Void in
            if (success && data != nil) {
                self.client.appConfig = data!
                self.setupUIConfig(data!)
            } else {
                self.handleGetConfigError(request: Client.WS_GET_APP_CONFIG)
            }
        })
    }
    
    private func setupUIConfig(_ appConfig: ModelAppConfig?) {
        if appConfig == nil {
            return
        }
        guard let homeConfig = appConfig?.home else {
            return
        }
        for i in 0..<homeConfig.count {
            let button = view.viewWithTag(btnTags[i]) as! UIButton
            let url = homeConfig[i].normalIconUrl
            Util.setImage(button, url: url, defaultImageName: defaultIcons[i])
            
            let label = view.viewWithTag(btnLblTags[i]) as! UILabel
            let title = L102Language.currentAppleLanguage() == Const.LANG_LOCAL ? homeConfig[i].localTitle : homeConfig[i].enTitle
            label.text = title
        }
    }
    
    
    @IBAction func vietnameseSelected(_ sender: UIButton) {
        changeLanguage(Const.LANG_LOCAL, reloadScreen: {
        })
    }
    
    @IBAction func englishSelected(_ sender: UIButton) {
        changeLanguage(Const.LANG_EN, reloadScreen: {
        })
    }
    
    private func getHotNews() {
        setupUIHotNews(nil)
        let params = [
            "limit": 5
        ]
        requestList(Client.WS_GET_HOT_NEWS, params: params, complete: {(success: Bool, data: [ModelHotNews]?) -> Void in
            if (success && data != nil && data!.count > 0) {
                self.setupUIHotNews(data)
            }
        })
    }
    
    private func setupUIHotNews(_ list: [ModelHotNews]?) {
        if (list == nil) {
            hideHotNewsLayout()
            return
        }
        self.hotNews = list
        showHotNewsLayout()
        viewPager.reloadData()
    }
    
    private func hideHotNewsLayout() {
        if (layoutHotNews.isHidden) {
            return
        }
        layoutHotNews.isHidden = true
    }
    
    private func showHotNewsLayout() {
        if (layoutHotNews.isHidden) {
            layoutHotNews.isHidden = false
        }
    }
    
    private func setupSpecificForDeviceModel() {
        let screenW = ScreenSize.WIDTH
        if (screenW >= ScreenSize.WIDTH_IPHONE6 && screenW < ScreenSize.WIDTH_IPHONE6P) {
            personalInfoLeading.constant = 12
            avatarMarginRight.constant = 15
        } else if (screenW >= ScreenSize.WIDTH_IPHONE6P) {
            personalInfoLeading.constant = 12
            avatarMarginRight.constant = 20
        }
    }
    
    // MARK: - Functions
    
    @IBAction func avatarTapped(_ sender: UIButton) {
        let personalInfoVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ThongTinCaNhan", viewControllerName: "personalInfoVC")
        self.navigationController?.pushViewController(personalInfoVC, animated: true)
    }
    
    func getHomeConfig(_ i: Int) -> ModelHomeConfig? {
        guard client.getHomeConfig().count > i else {
            return nil
        }
        return client.getHomeConfig()[i]
    }
    
    @IBAction func chargeHistoryTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(0))
    }
    
    @IBAction func rechargeTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(1))
    }
    
    @IBAction func dataPurchaseTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(2))
    }
    
    @IBAction func iShareTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(3))
    }
    
    @IBAction func airtimeTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(4))
    }
    
    @IBAction func dataPackagesTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(5))
    }
    
    @IBAction func findAStoreTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(6))
    }
    
    @IBAction func buyNumberTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(7))
    }
    
    @IBAction func eMoneyTapped(_ sender: UIButton) {
        segueIfFunctionIsEnabled(config: getHomeConfig(8))
    }
    
    @IBAction func onBtnSeeAll(_ sender: UIButton) {
        let _hotNews: ModelHotNews = (hotNews?[viewPager.currentPosition - 1])!
        switch _hotNews.type {
        case HotNewsType.Services.rawValue:
            tabBarController?.selectedIndex = 2
            break
            
        case HotNewsType.Promotion.rawValue:
            segueToPromotionScreen()
            break
            
        case HotNewsType.News.rawValue:
            segueToListNews()
            break
            
        default:
            return
        }
    }
    
    private func segueIfFunctionIsEnabled(config: ModelHomeConfig?) {
        guard let config = config else {
            return
        }
        
        if config.isActive == 3 || config.isActive == client.getSubType() {
            if config.functType == 0 {
                guard let functionName = config.uri?.functionName else {
                    return
                }
                switch functionName {
                case HomeFunctions.TraCuoc.rawValue:
                    segueToChargeHistory()
                case HomeFunctions.NapCuoc.rawValue:
                    segueToRecharge(showsTabBar: true)
                case HomeFunctions.MuaThemData.rawValue:
                    segueToBuyData()
                case HomeFunctions.DoiGoiData.rawValue:
                    segueToChangeDataPackage()
                case HomeFunctions.TimCuaHang.rawValue:
                    segueToFindStores()
                case HomeFunctions.MuaSo.rawValue:
                    segueToPhoneNumberPurchase()
                case HomeFunctions.DoiSIM.rawValue:
                    segueToChangeSIM()
                case HomeFunctions.Emoney.rawValue:
                    let url = "https://itunes.apple.com/vn/app/bankplus/id661973727?l=vi&mt=8"
                    if UIApplication.shared.canOpenURL(URL(string: url)!) {
                        UIApplication.shared.openURL(URL(string: url)!)
                    }
                default:
                    break
                }
            } else if config.functType == 1 {
                let title : String = L102Language.currentAppleLanguage() == Const.LANG_EN ? config.enTitle : config.localTitle
                let serviceCode : String = (config.uri?.serviceCode)!
                let vc : VASFunctionsVC = VASFunctionsVC(navBarTitle: title, serviceCode: serviceCode)
                
                vc.setupNavBar(title: title)
                goNextHideTabbar(vc)
                
            } else if config.functType == 2 {
                guard let serviceCode = config.uri?.serviceCode else {
                    return
                }
                
                segueToVasService(serviceCode: serviceCode, title: L102Language.currentAppleLanguage() == Const.LANG_EN ? config.enTitle : config.localTitle)
            }
        } else {
            let msg = L102Language.currentAppleLanguage() == Const.LANG_EN ? config.enMsg : config.localMsg
            showError(msg)
        }
    }
}

// MARK: - Extension

extension HomeViewController: ViewPagerDataSource {
    
    //Tells how many pages we have
    func numberOfItems(viewPager: ViewPager) -> Int {
        return hotNews == nil ? 0 : hotNews!.count;
    }
    
    //Load views for each page
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        var newView = view
        var imageView = UIImageView()
        if newView == nil {
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (newView?.frame.width)!, height: (newView?.frame.height)!))
            Util.setImage(imageView, url: self.hotNews?[index].advImgUrl)
            newView?.addSubview(imageView)
            imageView.setConstraintsTo(view: newView, topConstant: 0, bottomConstant: 0, leftConstant: 0, rightConstant: 0)
        }
        return newView!
    }
    
    func didSelectedItem(index: Int) {
        let _hotNews: ModelHotNews = (hotNews?[index - 1])!
        switch _hotNews.type {
        case HotNewsType.Services.rawValue:
            let vc:ServiceDetail01VC = UIStoryboard.getViewController(storyBoardName: "TabServices", viewControllerName: "ServiceDetail01VC") as! ServiceDetail01VC
            let service: ModelServiceItem = Mapper<ModelServiceItem>().map(JSONObject: [:])!
            service.code = _hotNews.newsId
            vc.service = service
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case HotNewsType.Promotion.rawValue:
            let vc:DetailVC = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
            vc.serviceType = .Promotion
            let promotion: ModelPromotion = Mapper<ModelPromotion>().map(JSONObject: [:])!
            promotion.code = _hotNews.newsId
            vc.inputData = promotion
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case HotNewsType.News.rawValue:
            let vc:DetailVC = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
            vc.serviceType = .News
            let news: ModelNews = Mapper<ModelNews>().map(JSONObject: [:])!
            news.id = _hotNews.newsId
            vc.inputData = news
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        default:
            return
        }
        
    }
}
