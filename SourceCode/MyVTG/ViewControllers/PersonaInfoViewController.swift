//
//  PersonaInfoViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PersonalInfoViewController: BaseVC {
    
    // MARK: - Variables and outlets
    
    var modelSubInfo: ModelSubInfo? = nil
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    @IBOutlet weak var hobbiesLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var imvLoading: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        screenName = ScreenNames.PersonalInfo.rawValue
        actionGA = Action.View.rawValue
        
        getData()
        
        setupNavBar(title: NSLocalizedString("ScreenTitle_ThongTinCaNhan01", comment: ""))
        avatar.layer.cornerRadius = avatar.frame.size.height / 2
        avatar.layer.masksToBounds = true
        phoneNumberLabel.text = client.getIsdn()
        
        getRightBarButton()?.isEnabled = false
        tabBarController?.tabBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if modelSubInfo != nil {
            setupUI(modelSubInfo!)
        }
        setAvatarInView(avatar)
    }
    
    // MARK: - Setup UI
    
    func setupUI(_ data: ModelSubInfo?) {
        display(data?.name, in: fullNameLabel)
        if data?.birthday == nil {
            dateOfBirthLabel.text = NSLocalizedString("More_ThongTinCaNhan01Label_ChuaCoThongTinNgaySinh", comment: "")
            dateOfBirthLabel.textColor = Const.COLOR_GRAY_FOR_LABEL
            dateOfBirthLabel.font = UIFont(name: "roboto-light", size: 15)
        } else {
            dateOfBirthLabel.text = dateFormatter.string(from: Date.init(timeIntervalSince1970: (data?.birthday)!/1000))
            dateOfBirthLabel.textColor = Const.COLOR_MAIN
            dateOfBirthLabel.font = UIFont(name: "roboto-light", size: 19)
        }
        display(data?.job, in: occupationLabel)
        display(data?.hobby, in: hobbiesLabel)
        display(data?.email, in: emailLabel)
    }
    
    func display(_ text: String?, in label: UILabel) {
        if text == "" || text == nil {
            switch label {
            case occupationLabel:
                label.text = NSLocalizedString("More_ThongTinCaNhan01Label_ChuaCoThongTinNgheNghiep", comment: "")
            case hobbiesLabel:
                label.text = NSLocalizedString("More_ThongTinCaNhan01Label_ChuaCoThongTinSoThich", comment: "")
            case emailLabel:
                label.text = NSLocalizedString("More_ThongTinCaNhan01Label_ChuaCoThongTinEmail", comment: "")
            default:
                break
            }
            label.textColor = Const.COLOR_GRAY_FOR_LABEL
        } else {
            label.text = text!
            label.textColor = Const.COLOR_MAIN
        }
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_edit"
    }
    
    override func onBtnRightNavBar() {
        let editPersonalInfoVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ThongTinCaNhan", viewControllerName: "editPersonalInfoVC") as! EditPersonalViewController
        editPersonalInfoVC.modelSubInfo = modelSubInfo
        editPersonalInfoVC.updateSuccessfully = {
            self.showSuccess(NSLocalizedString("More_ThongTinCaNhan02Label_Thongbao1", comment: ""))
        }
        navigationController?.pushViewController(editPersonalInfoVC, animated: true)
    }
    
    // MARK: - Actions
    
    private var firstTimeLoadView: Bool = true
    
    func getData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            scrollView.isHidden = true
        }
        let params = [
            "subType": client.getSubType()
        ]
        requestObject(Client.WS_GET_SUBINFO, params: params, complete: {(success: Bool, data: ModelSubInfo?) -> Void in
            self.imvLoading.stopProgress()
            self.scrollView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil) {
                self.modelSubInfo = data!
            }
            self.setupUI(data)
            self.getRightBarButton()?.isEnabled = true
        })
    }

    @IBAction func findStores(_ sender: UIButtonRegular01) {
        let mapVC = UIStoryboard.getViewController(storyBoardName: "FindStores", viewControllerName: "MapVC")
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
}












