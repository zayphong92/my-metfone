//
//  ChooseGiftViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChooseGiftViewController: BaseVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet08", comment: ""))
    }
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
}
