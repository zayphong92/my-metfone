//
//  FirstVC.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class FirstVC: BaseVC {
    
    @IBOutlet weak var lbVersion: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (Defaults.isFirstRun()) {
            L102Language.setAppleLanguageTo(lang: Const.LANG_LOCAL)
            L012Localizer.DoTheSwizzling()
            Defaults.setFirstRun()
        }
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.lbVersion.text = "Version \(version)"
        }
        if let userLogin: ModelUserLogin = ModelHelper.loadFromCache() {
            client.userLogin = userLogin
            goHome()
        } else {
            goLogin()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
