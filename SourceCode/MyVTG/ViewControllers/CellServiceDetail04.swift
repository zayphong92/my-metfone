//
//  CellServiceDetail04.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CellServiceDetail04: UITableViewCell {
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var btnSub: UIButton!
    var delegate: CellServiceDetailDelegate? = nil
    
    private var model: ModelServiceDetail? = nil
    private var action: ServiceAction! = .Register

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onBtnSub(_ sender: UIButton) {
        if (delegate != nil) {
            delegate!.regUnregService(model!)
        }
    }
    
    func setupUI(_ model: ModelServiceDetail, regStt: ServiceRegisterStatus) {
        self.model = model
        self.action = regStt.serviceAction
        btnSub.setTitle(model.textForButtonSubmit, for: .normal)
        lbStatus.isHidden = regStt == .ChuaDangKy
    }
}
