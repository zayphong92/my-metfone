//
//  QRScannerController.swift
//  QRCodeReader
//
//  Created by Simon Ng on 13/10/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerController: BaseVC, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var didFindResult: (String) -> Void = {_ in }
    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                        AVMetadataObjectTypeCode39Code,
                        AVMetadataObjectTypeCode39Mod43Code,
                        AVMetadataObjectTypeCode93Code,
                        AVMetadataObjectTypeCode128Code,
                        AVMetadataObjectTypeEAN8Code,
                        AVMetadataObjectTypeEAN13Code,
                        AVMetadataObjectTypeAztecCode,
                        AVMetadataObjectTypePDF417Code,
                        AVMetadataObjectTypeQRCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
        setupUI()
    }
    
    func setupNavBar() {
        self.setupNavBar(title: NSLocalizedString("ScreenTitle_NapCuoc03", comment: ""))
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    override func onBtnLeftNavBar() {
        dismiss(animated: true, completion: nil)
    }
    
    var centerView = UIView()
    
    func setupUI() {
        let focusZoneWidth: CGFloat = self.view.frame.size.width - 50
        let focusZoneHeight: CGFloat = focusZoneWidth
        
        let leftView = UIView(frame: CGRect(x: 0,
                                            y: 0,
                                            width: self.view.frame.size.width / 2 - focusZoneWidth / 2,
                                            height: self.view.frame.size.height))
        let rightView = UIView(frame: CGRect(x: self.view.frame.size.width / 2 + focusZoneWidth / 2,
                                             y: 0,
                                             width: leftView.frame.size.width,
                                             height: leftView.frame.size.height))
        let topView = UIView(frame: CGRect(x: leftView.frame.size.width,
                                           y: 0,
                                           width: focusZoneWidth,
                                           height: self.view.frame.size.height / 2 - focusZoneHeight / 2))
        let botView = UIView(frame: CGRect(x: leftView.frame.size.width,
                                           y: self.view.frame.size.height / 2 + focusZoneHeight / 2,
                                           width: focusZoneWidth,
                                           height: topView.frame.size.height))
        centerView = UIView(frame: CGRect(x: leftView.frame.size.width,
                                          y: self.view.frame.size.height / 2,
                                          width: focusZoneWidth,
                                          height: 1))
        let label = UILabel(frame: CGRect(x: leftView.frame.size.width,
                                          y: botView.frame.origin.y + 80,
                                          width: topView.frame.size.width,
                                          height: 30))
        label.text = NSLocalizedString("Home_NapCuocLabel_QuetMaTheCao", comment: "")
        label.font = UIFont(name: "roboto-medium", size: 16)
        label.textAlignment = .center
        label.textColor = UIColor.white
        Timer.scheduledTimer(timeInterval: 0.6,
                             target: self,
                             selector: #selector(self.redBarAnimation),
                             userInfo: nil,
                             repeats: true)
        centerView.backgroundColor = UIColor.red
        self.view.addSubview(centerView)
        let views = [topView, botView, leftView, rightView]
        views.forEach {
            $0.backgroundColor = UIColor.argbValue(hexValue: 0x77000000)
            self.view.addSubview($0)
        }
        self.view.addSubview(label)
    }
    
    func redBarAnimation() {
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.centerView.alpha = 0
        },
                       completion: {_ in
                        UIView.animate(withDuration: 0.3,
                                       animations: {
                                        self.centerView.alpha = 1
                        })
        })
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }

    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                self.didFindResult(metadataObj.stringValue)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { 
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
    }

}
