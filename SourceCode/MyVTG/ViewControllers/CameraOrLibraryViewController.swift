//
//  CameraOrLibraryViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/15/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CameraOrLibraryViewController: BaseVC {
    
    var onCamera: () -> Void = {}
    var onPhotoLibrary: () -> Void = {}
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openCamera(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        onCamera()    }
    
    @IBAction func openPhotoLibrary(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        onPhotoLibrary()
    }
}
