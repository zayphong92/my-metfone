//
//  PointToDataViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PointToDataViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var dataPackageStore: DataPackageStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataPackageStore = DataPackageStore()
        dataPackageStore.initDataPackages()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet04", comment: ""))
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataPackageStore.allDataPackages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exchangeDataPackage", for: indexPath) as! ExchangeDataPackageCell
        let dataPackage = dataPackageStore.allDataPackages[indexPath.row]
        cell.nameLabel.text = dataPackage.name
        cell.summaryLabel.text = dataPackage.expiration
        return cell
    }
}
