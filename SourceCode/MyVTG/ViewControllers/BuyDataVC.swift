//
//  BuyDataVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class BuyDataVC: BaseVC, UITableViewDelegate, UITableViewDataSource, DataPackageCellDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Variables
    
    var dataBalance : ModelDataAccountDetail? = nil
    private var dataPackages: [ModelDataPackage] = []
    private var isLoading = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_MuaThemData01", comment: ""))
        self.tabBarController?.tabBar.isHidden = true
        setupPullRefreshForScrollable(tableView, enable: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showProgressOnView(tableView)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        getData()
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataPackages.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MainDataPackgeCell = tableView.dequeueReusableCell(withIdentifier: "MainDataPackgeCell") as! MainDataPackgeCell
        let dataPackage = dataPackages[indexPath.row]
        let pkgNameText = dataPackage.name.characters.count == 0 ? "_" : dataPackage.name
        cell.nameLabel.text = NSLocalizedString("Home_ThongTinTaiKhoanLabel_Ten", comment: "") + " " + pkgNameText
        let volumeValue = Int(dataPackage.volume)
        var volumeText: String
        if (volumeValue != -1) {
            volumeText = Util.formatMB(volumeValue < 0 ? 0 : volumeValue)
        } else {
            volumeText = dataPackage.volumeStr
        }
        cell.remainDataLabel.text = NSLocalizedString("Home_ThongTinTaiKhoanLabel_Con", comment: "") + " " + volumeText
        
        var _expireDate = ""
        let expireDateValue = dataPackage.expiredDate
        if (expireDateValue != -1) {
            if (expireDateValue >= 0) {
                let expireDate: Date = Date.init(timeIntervalSince1970: expireDateValue/Double(1000))
                _expireDate = Util.formatDate(expireDate)
            }
        } else {
            _expireDate = dataPackage.expiredDateStr
        }
        
        let expriteDateStr: String = NSLocalizedString("Home_ThongTinTaiKhoanLabel_Han", comment: "")
        cell.expriedLabel.text = expriteDateStr + " " + _expireDate
        cell.infoLayout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoDetail)))
        // Bind data for volume layout
        
        if (dataPackage.pakagesToBuy.count > 0 ) {
            cell.generateVolumeLayout(dataPackage.pakagesToBuy, delegate: self)
            cell.noDataToBuyLabel.isHidden = true
            cell.heightOfNoDataAlert.constant = 0
            cell.purchaseButton.isHidden = false
            cell.heightOfPurchaseButton.constant = 40
        } else {
            cell.hidenDataVolumnLayout()
            cell.noDataToBuyLabel.isHidden = false
            cell.heightOfNoDataAlert.constant = 33
            cell.purchaseButton.isHidden = true
            cell.heightOfPurchaseButton.constant = 0
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
        return cell
    }
    
    func gotoDetail(_ sender: UITapGestureRecognizer) {
        let tapLocation = sender.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: tapLocation) {
            let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "DetailVC") as! DetailVC
            vc.serviceType = .BuyData
            vc.inputData = dataPackages[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - API request handle
    private var firstTimeLoadView: Bool = true
    func getData() {
        let tmpData: ModelDataAccountDetail? = ModelHelper.loadFromCache()
        self.dataBalance = tmpData
        self.dataBalance?.dataPackages.forEach {
            $0.pakagesToBuy = validateDataVolumeRespone($0.pakagesToBuy)
        }
        self.tableView.reloadData()
        let params = ["subType": client.getSubType()]
        requestObject(Client.WS_GET_DATA_ACCOUNT_DETAIL, params: params, complete: {(success: Bool, data: ModelDataAccountDetail?) -> Void in
            self.isLoading = false
            self.refreshDataDone()
            self.hideProgressOnView(self.tableView)
            self.firstTimeLoadView = false
            
            if (success && data != nil && data!.dataPackages.count > 0) {
                self.hideAlertOnView(self.tableView)
                self.dataPackages = data!.dataPackages
                self.dataBalance = data!
                self.dataPackages.forEach {
                    $0.pakagesToBuy = self.validateDataVolumeRespone($0.pakagesToBuy)
                }
                self.tableView.reloadData()
            } else {
                self.showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                               message: NSLocalizedString("Home_ThemDataLabel_ThongBao4", comment: ""),
                               cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                               agreeLabel: NSLocalizedString("LabeL_DongY", comment: ""),
                               onOK: { self.segueToChangeDataPackage() },
                               onCancel: {
                                self.navigationController?.popViewController(animated: true)
                })
            }
        })
    }
    
    func validateDataVolumeRespone(_ data: [ModelPackageDataToBuy]) -> [ModelPackageDataToBuy] {
        if data.count == 0 {
            return []
        }
        
        var output: [ModelPackageDataToBuy] = []
        
        func sortByVolumn(vol1: ModelPackageDataToBuy, vol2: ModelPackageDataToBuy) -> Bool {
            return vol1.volume < vol2.volume
        }
        output = data.sorted(by: sortByVolumn)
        
        if (output.count == 1 && output[0].volume == 0){
            return []
        }
        
        if (output[0].volume > 0){
            let zeroVol = (Mapper<ModelPackageDataToBuy>().map(JSONObject: [:]))!
            zeroVol.price = 0
            zeroVol.volume = 0
            output.insert(zeroVol, at: 0)
        }
        return output
    }
    
    override func refreshData() {
        if (isLoading) {
            refreshDataDone()
            return
        }
        getData()
    }
    
    func fakeData()-> [ModelPackageDataToBuy] {
        var volumes: [ModelPackageDataToBuy] = []
        for i in 0..<12 {
            let vol = (Mapper< ModelPackageDataToBuy>().map(JSONObject: [:]))!
            vol.price = i * 50000
            vol.volume =  i * 200
            volumes.append(vol)
        }
        return volumes
    }
    
    func didSelectedVolumn(volumnIndex: Int, button: UIButtonRegular01) {
        button.isEnabled = volumnIndex > 0
    }
    
    @IBAction func purchaseTapped(_ sender: UIButtonRegular01) {
        if let cell = sender.superview?.superview as? MainDataPackgeCell {
            let indexPath = tableView.indexPath(for: cell)!
            let selectedVol: ModelPackageDataToBuy =  dataPackages[indexPath.row].pakagesToBuy[cell.currentVolumeSelected]
            if (client.getMainAcc() < Double(selectedVol.price)) {
                showError(NSLocalizedString("Home_ThemDataLabel_ThongBaoHetTien", comment: ""))
            } else {
                super.buyExtraData(package: dataPackages[indexPath.row].code, dataVolume: selectedVol)
            }
        }
    }
}

















