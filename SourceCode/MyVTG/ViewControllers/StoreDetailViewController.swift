//
//  StoreDetailViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/6/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class StoreDetailViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var workingTimeLabel: UILabel!
    @IBOutlet weak var mapContainerView: UIView!
    
    // MARK: - Variables
    
    var store: ModelStore?
    var mapView: GMSMapView!
    var zoomLevel: Float = 10.0
    var placesClient: GMSPlacesClient!
    var locationManager = CLLocationManager()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressLabel.text = store?.addr
        distanceLabel.text = "\(String(format: "%.2f", store?.distance ?? 0.0)) Km"
        setupNavBar()
        setupLocationManager()
        addMapView()
        addMarker()
    }
    
    func setupNavBar() {
        setupNavBar(title: "")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
    }
    
    func addMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: (store?.latitude)!, longitude: (store?.longitude)!, zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: self.mapContainerView.bounds, camera: camera)
        mapView.settings.myLocationButton = false
        mapView.settings.compassButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapContainerView.addSubview(mapView)
    }
    
    override func getNavBarLeftImageName() -> String {
        return "back"
    }
    
    func addMarker() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: (store?.latitude)!, longitude: (store?.longitude)!)
        let markerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        markerView.image = #imageLiteral(resourceName: "ic_store1")
        marker.iconView = markerView
        marker.title = store?.name
        marker.snippet = store?.addr
        marker.map = mapView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapContainerView.addSubview(mapView!)
        setupNavBar()
    }
    
    // MARK: - Actions handle
    
    @IBAction func showDirection(_ sender: UIButton) {
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            let lat = Double((store?.latitude)!)
            let lng = Double((store?.longitude)!)
            let url = "comgooglemaps://?saddr=&daddr=\(lat),\(lng)&directionsmode=driving"
            UIApplication.shared.openURL(URL(string: url)!)
        } else {
            showError(NSLocalizedString("Home_TimCuaHangLabel_ThongBao3", comment: ""))
        }
        
        GAHelper.logGA(category: ScreenNames.FindStore.rawValue, actionGA: Action.Routing.rawValue, labelGA: nil, valueGA: nil)
    }
    
    @IBAction func callStore(_ sender: UIButton) {
        guard let store = store else {
            return
        }
        
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: String(format: NSLocalizedString("Home_TimCuaHangLabel_ThongBao1", comment: ""), store.name),
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Home_TimCuaHangLabel_GoiCuaHang", comment: ""),
                  onOK: {
                    if (self.store?.isdn) != nil {
                        let url = "tel://\((self.store?.isdn)!)"
                        if UIApplication.shared.canOpenURL(URL(string: url)!) {
                            UIApplication.shared.openURL(URL(string: url)!)
                        }
                    }
        })
        GAHelper.logGA(category: ScreenNames.FindStore.rawValue, actionGA: Action.Call.rawValue, labelGA: nil, valueGA: nil)
    }
}

// MARK: - Extension

extension StoreDetailViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}











