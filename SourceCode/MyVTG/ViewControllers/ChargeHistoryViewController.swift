//
//  ChargeHistoryViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChargeHistoryViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalChargesAmount: UILabel!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var currTotalChargeAmount: Double = 0
    var startDate: Date?
    var endDate: Date?
    var chargeStore: ChargeStore!
    var traTruoc: Bool = false // false nghia la Thue Bao Tra Sau
    var isDatePicked = false
    var modelPostageInfo: ModelPostageInfo? = nil
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenName = ScreenNames.ChargeHistory.rawValue
        actionGA = Action.View.rawValue
        setupNavBar(title: NSLocalizedString("ScreenTitle_TraCuoc01", comment: ""))
        
        chargeStore = ChargeStore()
        chargeStore.initCharges()
        
        startDate = Util.getFirstDateOfThisMonth()
        endDate = Date()
        
        if client.getSubType() == 1 {
            traTruoc = true
        } else if client.getSubType() == 2 {
            traTruoc = false
        }
        
        totalChargesAmount.text = "-"
        self.automaticallyAdjustsScrollViewInsets = false
        tabBarController?.tabBar.isHidden = true
        tableView.tableFooterView = UIView()
        setupPullRefreshForScrollable(tableView, enable: true)
        
        onBtnRightNavBar()
    }

    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    var subTitle: String?
    
    override func onBtnRightNavBar() {
        let vc = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "searchVC") as! SearchChargeHistoryViewController
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.popToRoot = {
            if !self.isDatePicked {
                self.navigationController?.popViewController(animated: true)
            }
        }
        vc.chargeStore = chargeStore
        vc.resetNavTitle = {
            self.startDate = vc.startDate
            self.endDate = vc.endDate
            if vc.prefixTitle == NSLocalizedString("Home_TraCuocLabel_HomNay", comment: "") {
                self.subTitle = "\(vc.prefixTitle) \(self.dateFormatter.string(from: self.startDate!))"
            } else {
                self.subTitle = "\(vc.prefixTitle) \(self.dateFormatter.string(from: self.startDate!)) - \(self.dateFormatter.string(from: self.endDate!))"
            }
            self.setupNavBar(title: "\(NSLocalizedString("ScreenTitle_TraCuoc01", comment: "")) \n \(self.subTitle!)")
            self.isDatePicked = true
            //self.getPostageInfo()
            self.resetChargeStore()
            self.getPostageInfoDefault()
            self.getPostageInfoData()
            self.getPostageInfoVas()
        }
        present(vc, animated: true, completion: nil)
    }
    
    private var firstTimeLoadView: Bool = true
    
    override func refreshData() {
        resetChargeStore()
        //getPostageInfo()
        getPostageInfoDefault()
        getPostageInfoData()
        getPostageInfoVas()
    }
    
    func resetChargeStore() {
        currTotalChargeAmount = 0
        let subType = client.getSubType()
        switch subType {
        case 1:
            traTruoc = true
            for charge in chargeStore.allPrepaidCharges[0] {
                switch charge.value {
                case .TaiKhoanChinh:
                    charge.amount = 0
                case .TaiKhoanKhuyenMai:
                    charge.amount = 0
                default:
                    break
                }
            }
            for charge in chargeStore.allPrepaidCharges[1] {
                switch charge.value {
                case .CuocGoi:
                    charge.amount = 0
                    charge.quantity = 0
                case .TinNhanSMS:
                    charge.amount = 0
                    charge.quantity = 0
                case .Khac:
                    charge.amount = 0
                    charge.quantity = 0
                case .Data:
                    charge.amount = 0
                    charge.quantity = 0
                case .Vas:
                    charge.amount = 0
                    charge.quantity = 0
                    
                default:
                    break
                }
            }
        case 2:
            traTruoc = false
            for charge in chargeStore.allPostpaidCharges {
                switch charge.value {
                case .ThueBaoThang:
                    charge.amount = 0
                case .CuocGoi:
                    charge.amount = 0
                    charge.quantity = 0
                case .TinNhanSMS:
                    charge.amount = 0
                    charge.quantity = 0
                case .Khac:
                    charge.amount = 0
                    charge.quantity = 0
                case .Data:
                    charge.amount = 0
                    charge.quantity = 0
                case .Vas:
                    charge.amount = 0
                    charge.quantity = 0
                default:
                    break
                }
            }
        default:
            break
        }
    }
    
    func getPostageInfo() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "subType": client.getSubType(),
            "startDate": Util.miliseconds(since: self.startDate!),
            "endDate": Util.miliseconds(since: self.endDate!),
            "type": "all"
            ] as [String : Any]
        requestObject(Client.WS_GET_POSTAGE_INFO, params: params, complete: {(success: Bool, data: ModelPostageInfo?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            self.refreshDataDone()
            self.getRightBarButton()?.isEnabled = true
            if (success && data != nil) {
                LogUtil.d("Successfully get postage info from server")
                self.setupUIChargeHistory(data!)
            }
        })
    }
    
    func getPostageInfoDefault() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "subType": client.getSubType(),
            "startDate": Util.miliseconds(since: self.startDate!),
            "endDate": Util.miliseconds(since: self.endDate!),
            "type": "default"
            ] as [String : Any]
        requestObject(Client.WS_GET_POSTAGE_INFO, params: params, complete: {(success: Bool, data: ModelPostageInfo?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            self.refreshDataDone()
            self.getRightBarButton()?.isEnabled = true
            if (success && data != nil) {
                LogUtil.d("Successfully get postage info from server")
                self.setupUIChargeHistoryDefault(data!)
            }
        })
    }
    
    func getPostageInfoData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "subType": client.getSubType(),
            "startDate": Util.miliseconds(since: self.startDate!),
            "endDate": Util.miliseconds(since: self.endDate!),
            "type": "data"
            ] as [String : Any]
        requestObject(Client.WS_GET_POSTAGE_INFO, params: params, complete: {(success: Bool, data: ModelPostageInfo?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            self.refreshDataDone()
            self.getRightBarButton()?.isEnabled = true
            if (success && data != nil) {
                LogUtil.d("Successfully get postage info from server")
                self.setupUIChargeHistoryData(data!)
            }
        })
    }
    
    func getPostageInfoVas() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "subType": client.getSubType(),
            "startDate": Util.miliseconds(since: self.startDate!),
            "endDate": Util.miliseconds(since: self.endDate!),
            "type": "vas"
            ] as [String : Any]
        requestObject(Client.WS_GET_POSTAGE_INFO, params: params, complete: {(success: Bool, data: ModelPostageInfo?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            self.refreshDataDone()
            self.getRightBarButton()?.isEnabled = true
            if (success && data != nil) {
                LogUtil.d("Successfully get postage info from server")
                self.setupUIChargeHistoryVas(data!)
            }
        })
    }
    
    func setupUIChargeHistory(_ data: ModelPostageInfo?) {
        if data == nil {
            return
        }
        let chargeData = data!
        let subType = client.getSubType()
        var totalAmount: Double = 0
        switch subType {
        case 1:
            traTruoc = true
            for charge in chargeStore.allPrepaidCharges[0] {
                switch charge.value {
                case .TaiKhoanChinh:
                    charge.amount = chargeData.basic
                case .TaiKhoanKhuyenMai:
                    charge.amount = chargeData.prom
                default:
                    break
                }
            }
            for charge in chargeStore.allPrepaidCharges[1] {
                switch charge.value {
                case .CuocGoi:
                    charge.amount = chargeData.callFee
                    charge.quantity = chargeData.callRc
                case .TinNhanSMS:
                    charge.amount = chargeData.smsFee
                    charge.quantity = chargeData.smsRc
                case .Khac:
                    charge.amount = chargeData.otherFee
                    charge.quantity = chargeData.otherRc
                case .Data:
                    charge.amount = chargeData.dataFee
                    charge.quantity = chargeData.dataRc
                case .Vas:
                    charge.amount = chargeData.vasFee
                    charge.quantity = chargeData.vasRc
                    
                default:
                    break
                }
            }
            totalAmount = chargeData.callFee + chargeData.smsFee + chargeData.otherFee + chargeData.vasFee
        case 2:
            traTruoc = false
            for charge in chargeStore.allPostpaidCharges {
                switch charge.value {
                case .ThueBaoThang:
                    charge.amount = chargeData.monthlyFee
                case .CuocGoi:
                    charge.amount = chargeData.callFee
                    charge.quantity = chargeData.callRc
                case .TinNhanSMS:
                    charge.amount = chargeData.smsFee
                    charge.quantity = chargeData.smsRc
                case .Khac:
                    charge.amount = chargeData.otherFee
                    charge.quantity = chargeData.otherRc
                case .Data:
                    charge.amount = chargeData.dataFee
                    charge.quantity = chargeData.dataRc
                case .Vas:
                    charge.amount = chargeData.vasFee
                    charge.quantity = chargeData.vasRc
                default:
                    break
                }
            }
            totalAmount = chargeData.monthlyFee + chargeData.callFee + chargeData.smsFee + chargeData.otherFee + chargeData.vasFee
        default:
            break
        }
        let x = totalAmount
        let y = (x*100).rounded()/100
        print(y)
        let roundedString = String(format: "%.2f", y)
        totalChargesAmount.text = roundedString + " $"
//        totalChargesAmount.text = "\(totalAmount) \(NSLocalizedString("Currency_Unit", comment: ""))"
        tableView.reloadData()
    }
    
    func setupUIChargeHistoryDefault(_ data: ModelPostageInfo?) {
        if data == nil {
            return
        }
        let chargeData = data!
        let subType = client.getSubType()
        var totalAmount: Double = 0
        switch subType {
        case 1:
            traTruoc = true
            for charge in chargeStore.allPrepaidCharges[0] {
                switch charge.value {
                case .TaiKhoanChinh:
                    charge.amount += chargeData.basic
                case .TaiKhoanKhuyenMai:
                    charge.amount += chargeData.prom
                default:
                    break
                }
            }
            for charge in chargeStore.allPrepaidCharges[1] {
                switch charge.value {
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity += chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                    
                default:
                    break
                }
            }
            totalAmount = chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        case 2:
            traTruoc = false
            for charge in chargeStore.allPostpaidCharges {
                switch charge.value {
                case .ThueBaoThang:
                    charge.amount += chargeData.monthlyFee
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity = chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                default:
                    break
                }
            }
            totalAmount = chargeData.monthlyFee + chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        default:
            break
        }
        
        currTotalChargeAmount += totalAmount
        let x = currTotalChargeAmount
        let y = (x*100).rounded()/100
        print(y)
        let roundedString = String(format: "%.2f", y)
        totalChargesAmount.text = roundedString + " $"
//        totalChargesAmount.text = "\(currTotalChargeAmount) \(NSLocalizedString("Currency_Unit", comment: ""))"
        tableView.reloadData()
    }
    
    func setupUIChargeHistoryData(_ data: ModelPostageInfo?) {
        if data == nil {
            return
        }
        let chargeData = data!
        let subType = client.getSubType()
        var totalAmount: Double = 0
        switch subType {
        case 1:
            traTruoc = true
            for charge in chargeStore.allPrepaidCharges[0] {
                switch charge.value {
                case .TaiKhoanChinh:
                    charge.amount += chargeData.basic
                case .TaiKhoanKhuyenMai:
                    charge.amount += chargeData.prom
                default:
                    break
                }
            }
            for charge in chargeStore.allPrepaidCharges[1] {
                switch charge.value {
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity += chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                    
                default:
                    break
                }
            }
            totalAmount = chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        case 2:
            traTruoc = false
            for charge in chargeStore.allPostpaidCharges {
                switch charge.value {
                case .ThueBaoThang:
                    charge.amount += chargeData.monthlyFee
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity += chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                default:
                    break
                }
            }
            totalAmount = chargeData.monthlyFee + chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        default:
            break
        }
        currTotalChargeAmount += totalAmount
        let x = currTotalChargeAmount
        let y = (x*100).rounded()/100
        print(y)
        let roundedString = String(format: "%.2f", y)
        totalChargesAmount.text = roundedString + " $"
//        totalChargesAmount.text = "\(currTotalChargeAmount) \(NSLocalizedString("Currency_Unit", comment: ""))"
        tableView.reloadData()
    }
    
    func setupUIChargeHistoryVas(_ data: ModelPostageInfo?) {
        if data == nil {
            return
        }
        let chargeData = data!
        let subType = client.getSubType()
        var totalAmount: Double = 0
        switch subType {
        case 1:
            traTruoc = true
            for charge in chargeStore.allPrepaidCharges[0] {
                switch charge.value {
                case .TaiKhoanChinh:
                    charge.amount += chargeData.basic
                case .TaiKhoanKhuyenMai:
                    charge.amount += chargeData.prom
                default:
                    break
                }
            }
            for charge in chargeStore.allPrepaidCharges[1] {
                switch charge.value {
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity += chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                    
                default:
                    break
                }
            }
            totalAmount = chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        case 2:
            traTruoc = false
            for charge in chargeStore.allPostpaidCharges {
                switch charge.value {
                case .ThueBaoThang:
                    charge.amount += chargeData.monthlyFee
                case .CuocGoi:
                    charge.amount += chargeData.callFee
                    charge.quantity = chargeData.callRc
                case .TinNhanSMS:
                    charge.amount += chargeData.smsFee
                    charge.quantity += chargeData.smsRc
                case .Khac:
                    charge.amount += chargeData.otherFee
                    charge.quantity += chargeData.otherRc
                case .Data:
                    charge.amount += chargeData.dataFee
                    charge.quantity += chargeData.dataRc
                case .Vas:
                    charge.amount += chargeData.vasFee
                    charge.quantity += chargeData.vasRc
                default:
                    break
                }
            }
            totalAmount = chargeData.monthlyFee + chargeData.callFee + chargeData.smsFee + chargeData.dataFee + chargeData.vasFee
        default:
            break
        }
        currTotalChargeAmount += totalAmount
        let x = currTotalChargeAmount
        let y = (x*100).rounded()/100
        print(y)
        let roundedString = String(format: "%.2f", y)
        totalChargesAmount.text = roundedString + " $"
//        totalChargesAmount.text = "\(currTotalChargeAmount) \(NSLocalizedString("Currency_Unit", comment: ""))"
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var number: Int = 0
        if traTruoc {
            number = chargeStore.allPrepaidCharges.count
        } else {
            number = 1
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int = 0
        if traTruoc {
            if section == 0 {
                count = chargeStore.allPrepaidCharges[0].count
            } else if section == 1 {
                count = chargeStore.allPrepaidCharges[1].count
            }
        } else {
            count = chargeStore.allPostpaidCharges.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = indexPath.section
        var cell1 = ChargeCell1()
        var cell2 = ChargeCell2()
        
        if traTruoc {
            if section == 0 {
                cell1 = tableView.dequeueReusableCell(withIdentifier: "chargeCell1", for: indexPath) as! ChargeCell1
                let balance = chargeStore.allPrepaidCharges[0][indexPath.row]
                cell1.nameLabel.text = balance.name
                let x = balance.amount
                let y = (x*100).rounded()/100
                print(y)
                let roundedString = String(format: "%.2f", y)
                cell1.amountLabel.text = roundedString + " $"
//                cell1.amountLabel.text = balance.amount == -1 ? "-" : "\(balance.amount) \(NSLocalizedString("Currency_Unit", comment: ""))"
            } else if section ==  1 {
                cell2 = tableView.dequeueReusableCell(withIdentifier: "chargeCell2", for: indexPath) as! ChargeCell2
                let charge = chargeStore.allPrepaidCharges[1][indexPath.row]
                cell2.iconImage.image = charge.icon
                cell2.nameLabel.text = charge.quantity == -1 ? charge.name + " (0)" : " \(charge.name)(\(charge.quantity))"
                let x = charge.amount
                let y = (x*100).rounded()/100
                let roundedString = String(format: "%.2f", y)
                cell2.amountLabel.text = roundedString + " $"
//                cell2.amountLabel.text = charge.amount == -1 ? "-" : "\(charge.amount) \(NSLocalizedString("Currency_Unit", comment: ""))"
                if indexPath.row == 2 {
                    cell2.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
                }
            }
        } else {
            cell2 = tableView.dequeueReusableCell(withIdentifier: "chargeCell2", for: indexPath) as! ChargeCell2
            let charge = chargeStore.allPostpaidCharges[indexPath.row]
            cell2.iconImage.image = charge.icon
            cell2.nameLabel.text = charge.name
            let x = charge.amount
            let y = (x*100).rounded()/100
            let roundedString = String(format: "%.2f", y)
            cell2.amountLabel.text = roundedString + " $"
//            cell2.amountLabel.text = charge.amount == -1 ? "-" : "\(charge.amount) \(NSLocalizedString("Currency_Unit", comment: ""))"
        }
        
        return traTruoc ? (section == 0 ? cell1 : cell2) : cell2
    }
    
    func getChargeAtIndexPath(_ indexPath: IndexPath) -> Charge {
        if client.getSubType() == 1 {
            return chargeStore.allPrepaidCharges[indexPath.section][indexPath.row]
        }
        return chargeStore.allPostpaidCharges[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onSelectedItem(getChargeAtIndexPath(indexPath))
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func onSelectedItem(_ charge: Charge) {
        switch charge.value {
        case .ThueBaoThang:
            break
        case .CuocGoi:
            segueToChargeDetail(charge)
        case .TinNhanSMS:
            segueToChargeDetail(charge)
        case .Khac:
            segueToChargeDetail(charge)
        case .Data:
            segueToChargeDetail(charge)
        case .Vas:
            segueToChargeDetail(charge)
        default:
            break
        }
    }
    
    func segueToChargeDetail(_ charge: Charge) {
        let chargeDetailVC = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "chargeDetailVC") as! ChargeDetailViewController
        chargeDetailVC.startDate = self.startDate
        chargeDetailVC.endDate = self.endDate
        chargeDetailVC.postType = charge.postType
        chargeDetailVC.subtitle = self.subTitle
        chargeDetailVC.quantity = charge.quantity
        chargeDetailVC.totalChargeAmount = charge.amount
        self.navigationController?.pushViewController(chargeDetailVC, animated: true)
    }
}















