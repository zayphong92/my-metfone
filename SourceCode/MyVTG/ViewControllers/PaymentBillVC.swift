//
//  PaymentBillVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PaymentBillVC: BaseVC, InputOTPDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var currentNumberLayout: UIView!
    @IBOutlet weak var newNumberLayout: UIView!
    @IBOutlet weak var messageLayout: UIView!
    @IBOutlet weak var debtLayout: UIView!
    @IBOutlet weak var mainBalanceLayout: UIView!
    @IBOutlet weak var needMoreLayout: UIView!
    @IBOutlet weak var currentPhoneLabel: UILabel!
    @IBOutlet weak var newPhoneLabel: UILabel!
    @IBOutlet weak var newPhoneTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var totalDealLabel: UILabel!
    @IBOutlet weak var serviceFeeLabel: UILabel!
    @IBOutlet weak var numberPriceLaybel: UILabel!
    @IBOutlet weak var debtLabel: UILabel!
    @IBOutlet weak var needMoreLabel: UILabel!
    @IBOutlet weak var mainBalanceLabel: UILabel!
    @IBOutlet weak var actionButton: UIButtonRegular01!
    
    // MARK: - Variables
    
    var phoneNumber: ModelIsdnToBuy? = nil
    var serviceFee: Double = 0
    var isdnOfKit:String? = nil
    var serialOfKit:String? = nil
    var serviceType : ServiceType = .None
    var accountInfo: ModelSubAccountInfo? = nil
    var actionType: AcctionType = .None
    var totalFee:Double = 0
    enum AcctionType {
        case None
        case ReCharge
        case BuyIsdn
        case ChangeIsdn
        case RestoreIsdn
    }
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScreenTitle()
        
        let tmpData:ModelSubAccountInfo? = ModelHelper.loadFromCache()
        accountInfo = tmpData
        
        bindingData()
    }
    
    func setScreenTitle() {
        switch serviceType {
        case .BuyPhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_MuaSo05", comment: ""))
            break
            
        case .ChangePhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DoiSo01", comment: ""))
            break
            
        case .ReStorePhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_KhoiPhucSo01", comment: ""))
            break
            
        default:
            break
        }
    }
    
    func bindingData() {
        newPhoneLabel.text = phoneNumber?.isdn ?? "---"

        let phonePrice = phoneNumber != nil ? phoneNumber!.price : 0
        numberPriceLaybel.text = Util.formatCurrency(NSNumber(value: phonePrice))
        totalFee += phonePrice
        
        serviceFeeLabel.text = Util.formatCurrency(NSNumber(value: serviceFee))
        totalFee += serviceFee
        
        //Todo : Công thêm tài khoản trả sau nếu có
        if(client.getSubType() == SubType.Postpaid.rawValue ) {
            debtLayout.isHidden = false
        } else {
            debtLayout.isHidden = true
        }
        
        totalDealLabel.text = Util.formatCurrency(NSNumber(value: totalFee))
        
        switch serviceType {
        case .BuyPhoneNumber:
            newPhoneTitleLabel.text = NSLocalizedString("Utilities_ThanhToanLabel_SoBanChon", comment: "")
            currentNumberLayout.isHidden = true
            
            messageLayout.isHidden = false
            messageLabel.text = NSLocalizedString("Utilities_BuyIsdn_Message01", comment: "")
            
            actionButton.localizedText = NSLocalizedString("Home_MuaSoButton_Mua", comment: "")
            actionType = .BuyIsdn
            break
            
        case .ChangePhoneNumber:
            newPhoneTitleLabel.text = NSLocalizedString("Utilities_ThanhToanLabel_SoMoi", comment: "")
            currentNumberLayout.isHidden = false
            currentPhoneLabel.text = "\(client.getSubId())"
            
            messageLayout.isHidden = false
            messageLabel.text = NSLocalizedString("Utilities_ChangeIsdn_Message01", comment: "")
            
            actionType = .ChangeIsdn
            actionButton.localizedText = NSLocalizedString("Utilities_Doi", comment: "")
            break
        
        case .ReStorePhoneNumber:
            newPhoneTitleLabel.text = NSLocalizedString("Utilities_ThanhToanLabel_SoKhoiPhuc", comment: "")
            
            actionType = .RestoreIsdn
            actionButton.localizedText = NSLocalizedString("Utilities_KhoiPhucSoButton", comment: "")
            
            messageLayout.isHidden = false
            let message:String = String(format: NSLocalizedString("Utilities_RestoreIsdn_Message01", comment: "---"), "\(phoneNumber?.isdn ?? "")", "\(client.getSubId())")
            messageLabel.text = message
            currentNumberLayout.isHidden = false
            currentPhoneLabel.text = "\(client.getSubId())"
            
            break
            
        default:
            break
        }

        
        if !client.isPrePaid() || isEnoughMoneyToPay() {
            mainBalanceLayout.isHidden = true
            needMoreLabel.isHidden = true
        } else {
            mainBalanceLayout.isHidden = false
            mainBalanceLabel.text = Util.formatCurrency(NSNumber(value: accountInfo!.mainAcc))
            
            needMoreLayout.isHidden = false
            needMoreLabel.text = Util.formatCurrency(NSNumber(value: totalFee - accountInfo!.mainAcc))
            
            actionButton.localizedText = NSLocalizedString("UtilitiesItem_NapCuoc", comment: "")
            actionType = .ReCharge
            
            messageLayout.isHidden = false
            messageLabel.text = NSLocalizedString("Utilities_Message_NotEnoughMoney", comment: "")
        }
        
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
    }

    func isEnoughMoneyToPay() -> Bool {
        return accountInfo!.mainAcc >= totalFee
    }
    
    // MARK: - API request handle
    
    func doAction() {
        switch actionType {
        case .ReCharge:
            segueToRecharge(showsTabBar: false)
            break
            
        case .BuyIsdn:
            doBuyIsdn(otp: nil)
            break
            
        case .ChangeIsdn:
            doChangeIsdn(otp: nil)
            break
            
        case .RestoreIsdn:
            doRestoreIsdn(otp: nil)
            break
            
            
        default:
            break
        }
    }
    
    func inputOTPDone(otp: String) {
        switch serviceType {
        case .BuyPhoneNumber:
            doBuyIsdn(otp: otp)
            break
            
        case .ChangePhoneNumber:
            doChangeIsdn(otp: otp)
            break
            
        case .ReStorePhoneNumber:
            doRestoreIsdn(otp: otp)
            break
            
        default:
            break
        }
    }
    
    @IBAction func closeButtonTaped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    func doBuyIsdn(otp: String?){
    
        let params = [
            "subType":client.getSubType(),
            "isdnToBuy": phoneNumber?.isdn ?? "",
            "isdnOfKit": isdnOfKit ?? "",
            "serialOfKit": serialOfKit ?? "",
            "price": totalFee,
            "otp": otp ?? ""
        ] as [String : Any]
        requestAction03(Client.WS_DO_BUY_ISDN, params: params, actionComplete:  {(success: Bool, data: ModelDefault?) -> Void in
            if(otp == nil) {
                let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "InputOTPDialogVC") as! InputOTPDialogVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.inputOTPDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    func doChangeIsdn(otp: String?) {
        let params = [
            "subType":client.getSubType(),
            "newIsdn": phoneNumber?.isdn ?? "",
            "price": totalFee,
            "otp": otp ?? ""
            ] as [String : Any]
        requestAction03(Client.WS_DO_EXCHANGE_ISDN, params: params, actionComplete:  {(success: Bool, data: ModelDefault?) -> Void in
            if (otp == nil) {
                let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "InputOTPDialogVC") as! InputOTPDialogVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.inputOTPDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    func doRestoreIsdn(otp: String?) {
        let params = [
            "subType":client.getSubType(),
            "newIsdn": phoneNumber?.isdn ?? "",
            "price": totalFee,
            "otp": otp ?? ""
            ] as [String : Any]
        requestAction03(Client.WS_DO_RESTORE_ISDN, params: params, actionComplete:  {(success: Bool, data: ModelDefault?) -> Void in
            if(otp == nil) {
                let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "InputOTPDialogVC") as! InputOTPDialogVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.inputOTPDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    
}
