//
//  ChangeDataDetailVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/29/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChangeDataDetailVC: CarouselVC {
    var dataPackages: [ModelUserService] = []
    var listPackageDetails: [String: ModelDataPackageInfo] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib("ChangeDataDetailPage", reuseId: "ChangeDataDetailPage")
        setupNavBar(title: dataPackages[startPosition].name)
        listPackageDetails = [:]
    }
    
    override func initPageCellAt(_ index: Int) -> CellCarouselItem {
        let cell: ChangeDataDetailPage = pagerView.dequeueReusableCell(withReuseIdentifier: "ChangeDataDetailPage", at: index) as! ChangeDataDetailPage
        cell.controller = self
        return cell
    }
    
    func getPackageItemAtIndex(_ index: Int) -> ModelUserService {
        return dataPackages[index]
    }
    
    func getPackageDetailByCode(_ code: String) -> ModelDataPackageInfo? {
        if let model: ModelDataPackageInfo = listPackageDetails[code] {return model}
        return nil
    }
    
    func savePackageDeatil(_ model: ModelDataPackageInfo) {
        listPackageDetails.updateValue(model, forKey: model.code)
    }
    
    override func pageDidChanged(_ position: Int) {
        setHeaderTitle(dataPackages[position].name)
    }
}
