//
//  AdvanceMoneyRegistrationViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class AdvanceMoneyRegistrationViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minImage: UIImageView!
    @IBOutlet weak var maxImage: UIImageView!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var leftSliderBorder: UIView!
    @IBOutlet weak var rightSliderBorder: UIView!
    @IBOutlet weak var advanceMoneyButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    let border = CALayer()
    let width = CGFloat(1.0)
    var startingValue: Float = 1000
    var minValue: Float = 0
    var maxValue: Float = 5000
    var smallestShare: Float = 1000 // Do chia nho nhat
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_UngTien02", comment: ""))
        slider.setThumbImage(#imageLiteral(resourceName: "orange_circle"), for: .normal)
        slider.value = Float(startingValue)
        currentLabel.sizeToFit()
        currentLabel.text = "\(Int(ceil(slider.value))) \(NSLocalizedString("Currency_Unit", comment: ""))"
        minLabel.text = "\(Int(minValue)) \(NSLocalizedString("Currency_Unit", comment: ""))"
        maxLabel.text = "\(Int(maxValue)) \(NSLocalizedString("Currency_Unit", comment: ""))"
        leadingConstraint.constant = CGFloat(startingValue / maxValue) * slider.frame.size.width - currentLabel.frame.size.width / 2
        
        showSuccess(NSLocalizedString("Home_UngtienLabel_ThongBao2", comment: ""))
    }
    
    // MARK: - Actions handle
    
    @IBAction func advanceMoney(_ sender: UIButton) {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: "\(NSLocalizedString("Home_UngtienLabel_ThongBao1", comment: "")) \(Int(slider.value)) \(NSLocalizedString("Currency_Unit", comment: ""))?",
            cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
            agreeLabel: NSLocalizedString("Button_OK", comment: ""))
    }

    @IBAction func moneySlider(_ sender: UISlider) {
        let remainder = sender.value.truncatingRemainder(dividingBy: smallestShare)
        if remainder < smallestShare / 2 {
            sender.value = ceil(sender.value - remainder)
        } else {
            sender.value = ceil(sender.value + smallestShare - remainder)
        }
        currentLabel.text = "\(Int(sender.value)) \(NSLocalizedString("Currency_Unit", comment: ""))"
        leadingConstraint.constant = CGFloat(sender.value) / CGFloat(maxValue) * sender.frame.size.width - currentLabel.frame.size.width / 2
        if sender.value == minValue {
            minImage.isHidden = true
            leadingConstraint.constant += 8
        } else {
            minImage.isHidden = false
        }
        if sender.value == maxValue {
            maxImage.isHidden = true
            leadingConstraint.constant -= 8
        } else {
            maxImage.isHidden = false
        }
    }
}










