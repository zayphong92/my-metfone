//
//  UtilitiesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class UtilitiesViewController: BaseVC, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableTopContraint: NSLayoutConstraint!
    
    var utilityStore: UtilityStore = {
        let us = UtilityStore()
        us.initUtilities()
        return us
    }()
    
    var filteredUtilities = UtilityStore()
    var searchActive: Bool = false
    var isGotoSubScreen: Bool = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        screenName = ScreenNames.Utilities.rawValue
        actionGA = Action.View.rawValue
        
        tableView.tableFooterView = UIView()
        setSearchType(.local)
    }
    
    override func contextIsNotCancelled() {
        super.contextIsNotCancelled()
        self.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_TienIch01", comment: ""))
        tabBarController?.tabBar.isHidden = false
        self.automaticallyAdjustsScrollViewInsets = false
        if !isGotoSubScreen { scrollUpWhenAppear(tableView)}
        isGotoSubScreen = false
    }
    
    override func normalVCPop() {
        tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - UI handle
    
    override func getNavBarLeftImageName() -> String {
        return ""
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        searchActive = false
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredUtilities.allUtilities.removeAll()
        var utility: Utility
        for i in 0..<utilityStore.allUtilities.count {
            filteredUtilities.allUtilities.append([Utility]())
            for j in 0..<utilityStore.allUtilities[i].count {
                utility = utilityStore.allUtilities[i][j]
                let name = utility.name
                let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
                if range != nil {
                    filteredUtilities.allUtilities[i].append(utility)
                }
            }
        }
        
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var number: Int = 0
        if searchActive {
            number = filteredUtilities.allUtilities.count
        } else {
            number = utilityStore.allUtilities.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int = 0
        if searchActive {
            number = filteredUtilities.allUtilities[section].count
        } else {
            number = utilityStore.allUtilities[section].count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 35))
        view.backgroundColor = Const.COLOR_GRAY_FOR_BACKGROUND
        let label = UILabel(frame: CGRect(x: 16, y: 5, width: self.view.frame.width, height: 25))
        label.font = UIFont(name: "Roboto-Medium", size: 13)!
        label.textColor = Const.COLOR_GRAY_FOR_NORMAL_TEXT
        if section == 0 {
            label.text = NSLocalizedString("UtilitiesLabel_CuocVienThong", comment: "")
        } else if section == 1 {
            label.text = NSLocalizedString("UtilitiesLabel_Data", comment: "")
        } else if section == 2 {
            label.text = NSLocalizedString("UtilitiesLabel_SimSo", comment: "")
        } else if section == 3 {
            label.text = NSLocalizedString("UtilitiesLabel_Khac", comment: "")
        }
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if searchActive {
            return filteredUtilities.allUtilities[section].count > 0 ? CGFloat(35.0) : CGFloat.leastNonzeroMagnitude
        }
        return CGFloat(35.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var utility: Utility
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "utility", for: indexPath) as! UtilityCell
        if searchActive {
            utility = filteredUtilities.allUtilities[indexPath.section][indexPath.row]
        } else {
            utility = utilityStore.allUtilities[indexPath.section][indexPath.row]
        }
        cell.nameLabel.text = utility.name
        cell.iconImage.image = utility.icon
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func onSelectItem(_ item: UtilitiesItems) {
        switch item {
        case .TraCuoc:
            segueIfFunctionIsEnabled(funcName: .TraCuoc, segueFunc: segueToChargeHistory)
        case .NapCuoc:
            segueIfFunctionIsEnabled(funcName: .NapCuoc, segueFunc: { segueToRecharge(showsTabBar: true) })
        case .MuaThemData:
            segueIfFunctionIsEnabled(funcName: .MuaThemData, segueFunc: segueToBuyData)
        case .DoiGoiData:
            segueIfFunctionIsEnabled(funcName: .DoiGoiData, segueFunc: segueToChangeDataPackage)
        case .MuaSo:
            segueIfFunctionIsEnabled(funcName: .MuaSo, segueFunc: segueToPhoneNumberPurchase)
        case .DoiSo:
            segueIfFunctionIsEnabled(funcName: .DoiSo, segueFunc: segueToChangePhoneNumber)
        case .KhoiPhucSo:
            segueIfFunctionIsEnabled(funcName: .KhoiPhucSo, segueFunc: segueToRestorePhoneNumber)
        case .DoiSim:
            segueIfFunctionIsEnabled(funcName: .DoiSim, segueFunc: segueToChangeSIM)
        case .KhoaGoiDi:
            segueIfFunctionIsEnabled(funcName: .KhoaGoiDi, segueFunc: segueToLockSIM)
        case .ChuyenTien:
            segueIfFunctionIsEnabled(funcName: .ChuyenTien, segueFunc: segueToIshare)
        case .UngTien:
            //TODO: fix code for Airtime
            //gotoVASFunctionPage(serviceCode: "EXCHANGE-MONEY", serviceName: L102Language.currentAppleLanguage() == Const.LANG_EN ? "Exchange Money" : "គម្រោងប្តូរលុយ")
            segueToVasService(serviceCode: "EXCHANGE-MONEY", title: L102Language.currentAppleLanguage() == Const.LANG_EN ? "Exchange Money" : "គម្រោងប្តូរលុយ")
            
        case .TimCuaHang:
            segueIfFunctionIsEnabled(funcName: .TimCuaHang, segueFunc: segueToFindStores)
        }
    }
    
//    func segueToVasService(serviceCode : String, title : String) {
//        let vc: ServiceDetailVC = CarouselVC.initViewController()
//        vc.headerTitleString = title
//        vc.listServices = []
//        vc.setPageCount(1, startPosition: 0)
//        vc.serviceCode = serviceCode
//
//        goNextHideTabbar(vc)
//    }
    
    func gotoVASFunctionPage(serviceCode: String, serviceName: String) {
        if client.getSubType() == 2 {
            showError(NSLocalizedString("Utilities_UngTienLabel_ThongBao1", comment: ""))
            return
        }
        
        let vc : VASFunctionsVC = VASFunctionsVC(navBarTitle: serviceName, serviceCode: serviceCode)
        vc.setupNavBar(title: serviceName)
        goNextHideTabbar(vc)
    }
    
    func segueIfFunctionIsEnabled(funcName: UtilitiesFunctions, segueFunc: () -> Void ) {
//        if client.getUtilitiesConfig()[funcName.rawValue].isEnabled {
//            segueFunc()
//        } else {
//            showNoti(message: NSLocalizedString("Msg_FunctionNotReady", comment: ""), isSuccessful: false)
//        }
        segueFunc()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var utility: Utility
        
        if searchActive {
            utility = filteredUtilities.allUtilities[indexPath.section][indexPath.row]
            onCancelSearch()
        } else {
            utility = utilityStore.allUtilities[indexPath.section][indexPath.row]
        }
        onSelectItem(utility.value)
        tableView.deselectRow(at: indexPath, animated: true)
        isGotoSubScreen = true
    }
}
