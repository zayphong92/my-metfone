//
//  TransferMoneyDetailViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/12/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class TranferMoneyDetailViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minImage: UIImageView!
    @IBOutlet weak var maxImage: UIImageView!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var leftSliderBorder: UIView!
    @IBOutlet weak var rightSliderBorder: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    var startingValue: Float = 1000
    var minValue: Float = 0
    var maxValue: Float = 5000
    var smallestShare: Float = 1000 // Do chia nho nhat
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_UngTien02", comment: ""))
        slider.setThumbImage(#imageLiteral(resourceName: "orange_circle"), for: .normal)
        slider.value = Float(startingValue)
        currentLabel.sizeToFit()
        currentLabel.text = "\(Int(ceil(slider.value))) Đ"
        minLabel.text = "\(Int(minValue)) Đ"
        maxLabel.text = "\(Int(maxValue)) Đ"
        leadingConstraint.constant = CGFloat(startingValue / maxValue) * slider.frame.size.width - currentLabel.frame.size.width / 2
    }
    
    // MARK: - Actions handle
    
    @IBAction func transferTapped(_ sender: UIButtonRegular01) {
        guard phoneNumber.text != "" else {
            showError(NSLocalizedString("Label_ThongBaoDienSoDienThoai", comment: ""))
            return
        }
        
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: "\(NSLocalizedString("Home_ChuyenTienLabel_ThongBao1_1", comment: "")) \(Int(slider.value))đ \(NSLocalizedString("Home_ChuyenTienLabel_ThongBao1_2", comment: "")) \(phoneNumber.text ?? "")?",
            cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
            agreeLabel: NSLocalizedString("Button_OK", comment: ""),
            onOK: {
            self.showSuccess(NSLocalizedString("Home_ChuyenTienLabel_ThongBao2", comment: ""))
        })
    }
    
    @IBAction func valueChanged(_ sender: UISlider) {
        let remainder = sender.value.truncatingRemainder(dividingBy: smallestShare)
        if remainder < smallestShare / 2 {
            sender.value = ceil(sender.value - remainder)
        } else {
            sender.value = ceil(sender.value + smallestShare - remainder)
        }
        currentLabel.text = "\(Int(sender.value)) Đ"
        leadingConstraint.constant = CGFloat(sender.value) / CGFloat(maxValue) * sender.frame.size.width - currentLabel.frame.size.width / 2
        if sender.value == minValue {
            minImage.isHidden = true
            leadingConstraint.constant += 8
        } else {
            minImage.isHidden = false
        }
        if sender.value == maxValue {
            maxImage.isHidden = true
            leadingConstraint.constant -= 8
        } else {
            maxImage.isHidden = false
        }
    }
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}









