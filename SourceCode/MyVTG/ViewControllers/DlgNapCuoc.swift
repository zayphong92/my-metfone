//
//  DlgNapCuoc.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DlgNapCuoc: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var vcTitleLabel: UILabel!
    @IBOutlet weak var option1Label: UILabel!
    @IBOutlet weak var option2Label: UILabel!
    @IBOutlet weak var option3Label: UILabel!
    @IBOutlet weak var dlgView: UIView!
    @IBOutlet weak var dlgBotConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    private var vcTitle = NSLocalizedString("Button_NapCuoc", comment: "")
    private var option1 = NSLocalizedString("Home_NapCuocLabel_TheCao", comment: "")
    private var option2 = NSLocalizedString("Home_NapCuocLabel_Emoney", comment: "")
    private var option3 = NSLocalizedString("Home_NapCuocLabel_TrucTuyen", comment: "")
    var option1Selected: () -> Void = {}
    var option2Selected: () -> Void = {}
    var option3Selected: () -> Void = {}
    var showTabBar: () -> Void = {}
    private let BUTTON_CORNER_RADIUS: CGFloat = 3.5
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.Recharge.rawValue
        actionGA = Action.View.rawValue
        self.tabBarController?.tabBar.isHidden = false
        setup()
        dlgView.frame.origin.y = UIScreen.main.bounds.height
        dlgBotConstraint.constant = -200
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           options: [.allowAnimatedContent, .curveEaseInOut],
                           animations: { self.dlgView.frame.origin.y -= 200 })
        })
    }
    
    func setup() {
        
        vcTitleLabel.text = vcTitle
        option1Label.text = option1
        option2Label.text = option2
        option3Label.text = option3
    }
    
    // MARK: - Actions handle
    
    func dismissView(completion: @escaping () -> Void = {}) {
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: [.allowAnimatedContent, .curveEaseInOut],
                       animations: { self.dlgView.frame.origin.y += 200 },
                       completion: {_ in
                        self.dismiss(animated: true, completion: nil)
                        completion()
        })
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismissView()
        self.showTabBar()
    }

    @IBAction func onCloseButton(_ sender: UIButton) {
        dismissView()
        self.showTabBar()
    }
    @IBAction func onOption1Selected(_ sender: UIButton) {
        GAHelper.logGA(category: self.screenName!, actionGA: Action.Click.rawValue, labelGA: "Thecao", valueGA: nil)
        dismissView(completion: {
            self.showTabBar()
            self.option1Selected()
        })
    }
    @IBAction func onOption2Selected(_ sender: UIButton) {
        GAHelper.logGA(category: self.screenName!, actionGA: Action.Click.rawValue, labelGA: "E-money", valueGA: nil)
        dismissView(completion: {
            self.showTabBar()
            self.option2Selected()
        })
    }
    @IBAction func onOption3Selected(_ sender: UIButton) {
        GAHelper.logGA(category: self.screenName!, actionGA: Action.Click.rawValue, labelGA: "TTTT", valueGA: nil)
        dismissView(completion: {
            self.showTabBar()
            self.option3Selected()
        })
    }
}










