//
//  EnterSIMViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import Contacts

class EnterSIMViewController: BaseVC, InputOTPDelegate {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var serialNumberText: UITextFieldRegular01!
    @IBOutlet weak var phoneNumberText: UITextFieldRegular01!
    @IBOutlet weak var changeSIMButton: UIButtonRegular01!
    
    var contact: CNContact?
    
    // MARK: - View life cycle
    override func loadView() {
        super.loadView()
        
        phoneNumberText.isEnabled = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serialNumberText.placeholder = NSLocalizedString("Utilities_DoiSIMLabel_NhapSoSerial", comment: "")
        phoneNumberText.placeholder = NSLocalizedString("Label_SoDienThoai", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_DoiSIM02", comment: ""))
    }
    
    // MARK: - Actions handle
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func segueToCodeReaderVC(_ sender: UIButton) {
        presentQRScannerController(withResultFilledIn: serialNumberText)
    }
    
    @IBAction func toCodeReaderVC(_ sender: UIButton) {
        presentQRScannerController(withResultFilledIn: serialNumberText)
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        
//        let result = ValidateUtil.validate(phoneNumber: phoneNumberText.text)
//        guard result.iscorrect else {
//            showError(result.msg!)
//            return
//        }
        
        guard serialNumberText.text != nil, serialNumberText.text! != "" else {
            showError(NSLocalizedString("Utilities_DoiSIMLabel_ThongBao5", comment: ""))
            return
        }
        
        let message = "\(NSLocalizedString("Utilities_DoiSIMLabel_ThongBao3", comment: "")) \(serialNumberText.text!)?"
        
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: message,
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: {
                    self.doChangeSIM(otp: nil)
        })
    }
    
    func inputOTPDone(otp: String) {
        doChangeSIM(otp: otp)
    }
    
    func doChangeSIM(otp: String?) {
        let params = [
            "otpOutside" : otp ?? "",
            "subType": client.getSubType(),
            "serialOfKit": serialNumberText.text!,
            "isdnOfKit": ""
            ] as [String : Any]
        requestAction(Client.WS_DO_CHANGE_SIM, params: params, actionComplete: {(success: Bool, model: ModelDoAction?) -> Void in
            if otp == nil {
                let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "InputOTPDialogVC") as! InputOTPDialogVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.inputOTPDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                if success {
                    //self.showSuccess(NSLocalizedString("Utilities_DoiSIMLabel_ThongBao2", comment: ""))
                    self.navigationController?.popViewController(animated: true)
                } else {
                    //self.showError(NSLocalizedString("Utilities_DoiSIMLabel_ThongBao4", comment: ""))
                    //self.navigationController?.popViewController(animated: true)
                }
            }
            
        })
    }
}








