//
//  EditPersonalInfoViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import DropDown

class EditPersonalViewController: BaseVC, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextFieldRegular01!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var imvLoadingOccupation: UIImageView!
    @IBOutlet weak var arrowIndicatorOccupation: UIImageView!
    @IBOutlet weak var imvLoadingHobby: UIImageView!
    @IBOutlet weak var arrowIndicatorHobby: UIImageView!
    @IBOutlet weak var occupationLbl: UILabelRegular03!
    @IBOutlet weak var occupationTextField: UITextFieldRegular01!
    @IBOutlet weak var hobbyLbl: UILabelRegular03!
    @IBOutlet weak var hobbyTextField: UITextFieldRegular01!
    @IBOutlet weak var updateButton: UIButtonRegular01!
    
    // MARK: - Variables
    
    var modelSubInfo: ModelSubInfo?
    var listCareers = [ModelCareer]()
    var listHobbies = [ModelHobby]()
    var occupationLoadDone: Bool = false
    var hobbyLoadDone: Bool = false
    let dropdownOccupation = DropDown()
    let dropdownHobby = DropDown()
    var chosenJobId: Int = -1
    var chosenHobbyId: Int = -1
    var chosenJobName: String = ""
    var chosenHobbyName: String = ""
    var updateSuccessfully: () -> Void = {}
    var avatarImage: UIImage?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        setupViews()
        setupTextFields()
        setDefaultColors()
    }
    
    func setupTextFields() {
        occupationTextField.title = NSLocalizedString("More_ThongTinCaNhan01Label_NgheNghiep", comment: "")
        occupationTextField.setTitleVisible(true)
        occupationTextField.text = modelSubInfo?.job
        
        hobbyTextField.title = NSLocalizedString("More_ThongTinCaNhan01Label_SoThich", comment: "")
        hobbyTextField.setTitleVisible(true)
        hobbyTextField.text = modelSubInfo?.hobby
        
        emailTextField.placeholder = NSLocalizedString("More_ThongTinCaNhan01Label_Email", comment: "")
        emailTextField.text = modelSubInfo?.email
        
        listenToTextFieldDidChange()
    }
    
    func setupViews() {
        baseScrollView = scrollView
        avatarView.layer.cornerRadius = avatarView.frame.size.height / 2
        avatarView.layer.masksToBounds = true
        updateButton.isEnabled = false
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func setDefaultColors() {
        setDefaultColorOccupation()
        setDefaultColorHobby()
    }
    
    func setDefaultColorOccupation() {
        self.occupationTextField.titleColor = Const.COLOR_GRAY_FOR_LABEL
        self.occupationTextField.lineColor = Const.COLOR_GRAY_FOR_BORDER
    }
    
    func setDefaultColorHobby() {
        self.hobbyTextField.titleColor = Const.COLOR_GRAY_FOR_LABEL
        self.hobbyTextField.lineColor = Const.COLOR_GRAY_FOR_BORDER
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_ThongTinCaNhan02", comment: ""))
        UIApplication.shared.statusBarStyle = .lightContent
        if avatarImage != nil {
            avatarView.image = avatarImage
        } else {
            setAvatarInView(avatarView)
        }
        setupDropDown()
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        setDefaultColors()
    }
    
    // MARK: - Setup UI
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    func setupDropDown() {
        DropDown.startListeningToKeyboard()
        
        imvLoadingOccupation.isHidden = true
        imvLoadingHobby.isHidden = true
        
        setupAnchorView()
        
        setupDirection()
        
        setupSelectionAction()
        
        setupCancelAction()
        
        // Chèn thêm khoảng trắng ở bên phải textField để khi chữ quá dài thì sẽ không bị trùng vào mũi tên
        occupationTextField.editWidthToAvoid(view: imvLoadingOccupation)
        hobbyTextField.editWidthToAvoid(view: imvLoadingHobby)
    }
    
    func setupAnchorView() {
        let occupationAnchorView = UIView()
        view.addSubview(occupationAnchorView)
        occupationAnchorView.translatesAutoresizingMaskIntoConstraints = false
        occupationAnchorView.topAnchor.constraint(equalTo: occupationTextField.bottomAnchor, constant: 5).isActive = true
        occupationAnchorView.leftAnchor.constraint(equalTo: occupationTextField.leftAnchor).isActive = true
        occupationAnchorView.widthAnchor.constraint(equalTo: occupationTextField.widthAnchor).isActive = true
        occupationAnchorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        dropdownOccupation.anchorView = occupationAnchorView
        
        let hobbyAnchorView = UIView()
        view.addSubview(hobbyAnchorView)
        hobbyAnchorView.translatesAutoresizingMaskIntoConstraints = false
        hobbyAnchorView.topAnchor.constraint(equalTo: hobbyTextField.bottomAnchor, constant: 5).isActive = true
        hobbyAnchorView.leftAnchor.constraint(equalTo: hobbyTextField.leftAnchor).isActive = true
        hobbyAnchorView.widthAnchor.constraint(equalTo: hobbyTextField.widthAnchor).isActive = true
        hobbyAnchorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        dropdownHobby.anchorView = hobbyAnchorView
    }
    
    func setupDirection() {
        dropdownOccupation.direction = .bottom
        dropdownHobby.direction = .bottom
    }
    
    func setupSelectionAction() {
        dropdownOccupation.selectionAction = { [unowned self] (index: Int, item: String) in
            self.occupationTextField.text = item
            self.chosenJobId = self.listCareers[index].id
            self.chosenJobName = item
            self.setDefaultColorOccupation()
        }
        
        dropdownHobby.selectionAction = { [unowned self] (index: Int, item: String) in
            self.hobbyTextField.text = item
            self.chosenHobbyId = self.listHobbies[index].id
            self.chosenHobbyName = item
            self.setDefaultColorHobby()
        }
    }
    
    func setupCancelAction() {
        dropdownOccupation.cancelAction = {
            self.setDefaultColorOccupation()
        }
        
        dropdownHobby.cancelAction = {
            self.setDefaultColorHobby()
        }
    }
    
    func getData() {
        if listCareers.count == 0 {
            occupationTextField.isEnabled = false
            imvLoadingOccupation.startProgress()
            arrowIndicatorOccupation.isHidden = true
            requestList(Client.WS_GET_CAREERS, params: nil, complete: {(success: Bool, data: [ModelCareer]?) -> Void in
                self.occupationLoadDone = true
                self.imvLoadingOccupation.stopProgress()
                self.arrowIndicatorOccupation.isHidden = false
                if (self.occupationLoadDone && self.hobbyLoadDone) {
                    self.updateButton.isEnabled = true
                }
                if (success && data != nil && data!.count > 0) {
                    self.occupationTextField.isEnabled = true
                    data!.forEach { $0.name.capitalizeFirstLetter() }
                    self.listCareers = data!.sorted { $0.name < $1.name }
                    self.dropdownOccupation.dataSource = self.listCareers.map { $0.name }
                }
            })
        }
        
        if listHobbies.count == 0 {
            hobbyTextField.isEnabled = false
            imvLoadingHobby.startProgress()
            arrowIndicatorHobby.isHidden = true
            requestList(Client.WS_GET_HOBBIES, params: nil, complete: {(success: Bool, data: [ModelHobby]?) -> Void in
                self.hobbyLoadDone = true
                self.imvLoadingHobby.stopProgress()
                self.arrowIndicatorHobby.isHidden = false
                if (self.occupationLoadDone && self.hobbyLoadDone) {
                    self.updateButton.isEnabled = true
                }
                if (success && data != nil && data!.count > 0) {
                    self.hobbyTextField.isEnabled = true
                    data!.forEach { $0.name.capitalizeFirstLetter() }
                    self.listHobbies = data!.sorted { $0.name < $1.name }
                    self.dropdownHobby.dataSource = self.listHobbies.map { $0.name }
                }
            })
        }
    }
    
    // MARK: - TextField handle
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let txtFld = textField as? UITextFieldRegular01 {
            txtFld.lineColor = Const.COLOR_MAIN
        }
        
        if textField == emailTextField {
            return true
        } else if textField == occupationTextField {
            showOptionsFor(occupationTextField, with: dropdownOccupation)
        } else if textField == hobbyTextField {
            showOptionsFor(hobbyTextField, with: dropdownHobby)
        }
        return false
    }
    
    func showOptionsFor(_  textField: UITextFieldRegular01, with dropdown: DropDown) {
        view.endEditing(true)
        dropdown.show()
        textField.titleColor = Const.COLOR_MAIN
        textField.lineColor = Const.COLOR_MAIN
    }
    
//    override func textFieldChanged() {
//        if let text = emailTextField.text {
//            if !Util.isValidEmail(testStr: text) && text != "" {
//                emailTextField.errorMessage = NSLocalizedString("More_ChamSocKhachHang_GuiMailLabel_NguoiGui_Error", comment: "")
//            } else {
//                emailTextField.errorMessage = ""
//            }
//        }
//    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        super.textFieldDidEndEditing(textField)
//        enableSendButton()
    }
    
    func enableSendButton() {
        if Util.isValidEmail(testStr: emailTextField.text!) || emailTextField.text == "" {
            updateButton.isEnabled = true
        } else {
            updateButton.isEnabled = false
        }
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // MARK: - Buttons handle
    
    @IBAction func updateTapped(_ sender: UIButton) {
        
//        guard Util.isValidEmail(testStr: emailTextField.text!) || emailTextField.text == "" else {
//            showError(NSLocalizedString("More_ThongTinCaNhan02Label_Thongbao2", comment: ""))
//            return
//        }
        if avatarImage != nil {
            let imageData: NSData = UIImagePNGRepresentation(avatarImage!)! as NSData
            
            let orientation: Int = (avatarImage?.imageOrientation.rawValue)!

            let dictionary: [String: Any] = [Const.KEY_FOR_AVATAR: imageData,
                                             Const.KEY_FOR_AVATAR_ORIENTATION: orientation]
            UserDefaults.standard.set(dictionary, forKey: client.getIsdn())
            
        }
        
        var params = [String : Any]()
        
        if chosenJobId == -1 && listCareers.count > 0 {
            listCareers.forEach { if $0.name == modelSubInfo?.job {
                chosenJobId = $0.id
                chosenJobName = $0.name
                }}
        }
        params.updateValue(chosenJobId, forKey: "jobId")
        
        if chosenHobbyId == -1 && listHobbies.count > 0 {
            listHobbies.forEach { if $0.name == modelSubInfo?.hobby {
                chosenHobbyId = $0.id
                chosenHobbyName = $0.name
                }}
        }
        params.updateValue(chosenHobbyId, forKey: "hobbyId")
        
        params.updateValue(emailTextField.text!, forKey: "email")
        requestAction(Client.WS_UPDATE_SUB_INFO, params: params, actionComplete: {(success: Bool, data: ModelDoAction?) -> Void in
            if (success) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    LogUtil.d("popViewController")
                    self.modelSubInfo?.job = self.chosenJobName
                    self.modelSubInfo?.hobby = self.chosenHobbyName
                    self.modelSubInfo?.email = self.emailTextField.text!
                    self.navigationController?.popViewController(animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
                        self.updateSuccessfully()
                    })
                })
            } else {
                self.showError(NSLocalizedString("More_ThongTinCaNhan02Label_Thongbao3", comment: ""))
            }
        })
    }
    
    @IBAction func cameraTapped(_ sender: UIButton) {
        let cameraVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ThongTinCaNhan", viewControllerName: "cameraVC") as! CameraOrLibraryViewController
        cameraVC.modalPresentationStyle = .overCurrentContext
        cameraVC.onCamera = {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = true
                imagePicker.isEditing = true
                imagePicker.setEditing(true, animated: true)
                imagePicker.showsCameraControls = true
                imagePicker.cameraCaptureMode = .photo
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        cameraVC.onPhotoLibrary = {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = true
                UIApplication.shared.statusBarStyle = .default
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        present(cameraVC, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        avatarImage = image
        avatarView.image = avatarImage
        dismiss(animated: true, completion: nil)
    }
}













