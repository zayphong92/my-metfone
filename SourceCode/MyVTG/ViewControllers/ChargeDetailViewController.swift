//
//  ChargeDetailViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChargeDetailViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberOfCalls: UILabel!
    @IBOutlet weak var totalCharges: UILabel!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    private enum Sort: String {
        case Descending = "desc"
        case Ascending = "asc"
    }
    
    private var sort: Sort = .Descending
    
    private var listCalls = [ModelPostageDetailInfo]()
    private var listCallsBackup = [ModelPostageDetailInfo]()
    var startDate: Date?
    var endDate: Date?
    var subtitle: String?
    var postType: Int = 0
    var quantity: Int = -1
    var totalChargeAmount: Double = -1
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch postType {
        case 0://call
            setupNavBar(title: "\(NSLocalizedString("ScreenTitle_TraCuoc02_0", comment: "")) \n \(subtitle!)")
        case 1://sms
            setupNavBar(title: "\(NSLocalizedString("ScreenTitle_TraCuoc02_1", comment: "")) \n \(subtitle!)")
        case 3://data
            setupNavBar(title: "\(NSLocalizedString("ScreenTitle_TraCuoc02_Data", comment: "")) \n \(subtitle!)")
        case 4://vas
            setupNavBar(title: "\(NSLocalizedString("ScreenTitle_TraCuoc02_Vas", comment: "")) \n \(subtitle!)")
        default:
            break
        }
        tableView.allowsSelection = false
        setupPullRefreshForScrollable(tableView, enable: true)
        getData { (success, data) in
            if (success && data != nil) {
                if data!.count == 0 {
                    self.showAlertOnView(self.tableView, NSLocalizedString("Label_Khong", comment: "") + " " + self.getNavBarTitle())
                } else {
                    self.getDataDone(data!)
                }
            } else {
                self.showAlertOnView(self.tableView, NSLocalizedString("Msg_DataNotFound", comment: ""))
            }
        }
        setupUI()
        getRightBarButton()?.isEnabled = false
    }
    
    func setupUI() {
        numberOfCalls.text = String(quantity)
        let x = totalChargeAmount
        let y = (x*100).rounded()/100
        let roundedString = String(format: "%.2f", y)
        totalCharges.text = roundedString + " $"
//        totalCharges.text = "\(totalChargeAmount) \(NSLocalizedString("Currency_Unit", comment: ""))"
    }
    
    func getDataDone(_ data: [ModelPostageDetailInfo]) {
        currentPage += 1
        if data.count < Client.PAGE_SIZE {
            needLoadMore = false
        }
        self.hideAlertOnView(tableView)
        self.hideProgressOnView(tableView)
        listCalls += data
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        tableView.reloadData()
    }
    
    override func refreshData() {
        currentPageBackUp = currentPage
        needLoadMoreBackup = needLoadMore
        listCallsBackup = listCalls
        
        needLoadMore = true
        currentPage = 1
        
        getData { (success, data) in
            if success && data != nil && data!.count > 0 {
                self.listCalls = []
                self.getDataDone(data!)
            } else {
                self.currentPage = self.currentPageBackUp
                self.needLoadMore = self.needLoadMoreBackup
                self.listCalls = self.listCallsBackup
            }
        }
    }
    
    private var currentPage: Int = 1
    private var currentPageBackUp: Int = 1
    private var needLoadMore: Bool = true
    private var needLoadMoreBackup: Bool = true
    private var firstTimeLoadView: Bool = true
    
    func getData(complete: @escaping(Bool, [ModelPostageDetailInfo]?) -> ()) {
        if !needLoadMore {
            return
        }
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        guard startDate != nil && endDate != nil else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        let params = [
            "subType": client.getSubType(),
            "postType": postType,
            "startDate": Util.miliseconds(since: startDate!),
            "endDate": Util.miliseconds(since: endDate!),
            "pageSize": Client.PAGE_SIZE,
            "pageNum": currentPage,
            "sort": sort.rawValue
        ] as [String : Any]
        
        requestList(Client.WS_GET_POSTAGE_DETAIL_INFO, params: params, complete: {(success: Bool, data: [ModelPostageDetailInfo]?) -> Void in
            self.imvLoading.stopProgress()
            self.refreshDataDone()
            self.firstTimeLoadView = false
            self.tableView.isHidden = false
            self.getRightBarButton()?.isEnabled = true
            complete(success, data)
        })
    }
    
    override func onBtnRightNavBar() {
        let vc = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "filterOptions") as! FilterOptionsViewController
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.oldestRearrange = {
            self.showProgressOnView(self.tableView)
            self.sort = .Ascending
            self.refreshData()
        }
        vc.newestRearrange = {
            self.showProgressOnView(self.tableView)
            self.sort = .Descending
            self.refreshData()
        }
        if sort == .Descending {
            vc.isNewest = true
        } else {
            vc.isNewest = false
        }
        present(vc, animated: true, completion: nil)
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_filter"
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCalls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "callCell", for: indexPath) as! CallCell
        let postageDetail = getModelAtIndex(indexPath.row)
        
        cell.number.text = postageDetail.isdn
        cell.time.text = timeFormatter.string(from: Date.init(timeIntervalSince1970: postageDetail.start_time/1000))
        cell.date.text = dateFormatter.string(from: Date.init(timeIntervalSince1970: postageDetail.start_time/1000))
        if (postType == 3 || postType == 4) {
            cell.duration.text = ""
        } else {
            cell.duration.text = "\(Util.formatDuration(postageDetail.duration))"
        }
        
        if (postType == 3) {
            cell.charge.text = "\(postageDetail.duration) MB"
        } else {
            let x = postageDetail.value
            let y = (x*100).rounded()/100
            let roundedString = String(format: "%.2f", y)
            cell.charge.text = roundedString + " $"
//            cell.charge.text = "\(postageDetail.value) \(NSLocalizedString("Currency_Unit", comment: ""))"
        }
        
        
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelPostageDetailInfo {
        if isRefreshing() {
            return listCallsBackup[index]
        } else {
            return listCalls[index]
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !needLoadMore {
            return
        }
        
        if indexPath.row == listCalls.count - 1 {
            enableLoadMore(needLoadMore, tableView: tableView)
            getData { (success, data) in
                if success {
                    self.getDataDone(data!)
                }
            }
        }
    }
}










