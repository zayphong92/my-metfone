//
//  CalendarViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CalendarViewController: BaseVC, CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    
    // MARK: - Variables
    
    var postAction: () -> Void = {}
    var startDate: Date?
    var endDate: Date?
    var earliestDate: Date?
    let lastestDate = Date()
    let month: [Int:String] = [
        1: NSLocalizedString("Home_TraCuoc_ThangMotLabel", comment: ""),
        2: NSLocalizedString("Home_TraCuoc_ThangHaiLabel", comment: ""),
        3: NSLocalizedString("Home_TraCuoc_ThangBaLabel", comment: ""),
        4: NSLocalizedString("Home_TraCuoc_ThangTuLabel", comment: ""),
        5: NSLocalizedString("Home_TraCuoc_ThangNamLabel", comment: ""),
        6: NSLocalizedString("Home_TraCuoc_ThangSauLabel", comment: ""),
        7: NSLocalizedString("Home_TraCuoc_ThangBayLabel", comment: ""),
        8: NSLocalizedString("Home_TraCuoc_ThangTamLabel", comment: ""),
        9: NSLocalizedString("Home_TraCuoc_ThangChinLabel", comment: ""),
        10: NSLocalizedString("Home_TraCuoc_ThangMuoiLabel", comment: ""),
        11: NSLocalizedString("Home_TraCuoc_ThangMuoiMotLabel", comment: ""),
        12: NSLocalizedString("Home_TraCuoc_ThangMuoiHaiLabel", comment: "")
    ]
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let today = CVDate(date: Date())
        if startDate != nil {
            titleLabel.text = NSLocalizedString("Home_TraCuocLabel_NgayBatDau", comment: "")
        }
        if endDate != nil {
            titleLabel.text = NSLocalizedString("Home_TraCuocLabel_NgayKetThuc", comment: "")
        }
        monthLabel.text = "\(month[today.month]!), \(today.year)"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    
    // MARK: - Calendar delegate functions
    
    func presentedDateUpdated(_ date: CVDate) {
        let currentDate = calendarView.presentedDate
        monthLabel.text = "\(month[(currentDate?.month)!]!), \(calendarView.presentedDate.year)"
    }

    func shouldAutoSelectDayOnMonthChange() -> Bool {
        return false
    }
    
    func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        if startDate != nil {
            startDate = calendarView.presentedDate.convertedDate()
        }
        if endDate != nil {
            endDate = calendarView.presentedDate.convertedDate()
        }
        dismiss(animated: true, completion: nil)
        postAction()
    }
    
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func firstWeekday() -> Weekday {
        return .monday
    }
    
    func latestSelectableDate() -> Date? {
        return lastestDate
    }
    
    func earliestSelectableDate() -> Date? {
        return earliestDate
    }
    
    func disableScrollingBeforeDate() -> Date? {
        return earliestDate
    }
    
    func disableScrollingAfterDate() -> Date? {
        return lastestDate
    }
    
    // MARK: - Taps handle
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}












