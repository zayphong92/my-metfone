//
//  ExperiencesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ExperiencesViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPager: ViewPager!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    fileprivate var modelExperience: ModelExperience3G4G? = nil
    fileprivate var filteredExperienceLinks = [ModelExperienceLink3G4G]()
    fileprivate var searchActive: Bool = false
    fileprivate var isHeaderHidden: Bool = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.Experience.rawValue
        actionGA = Action.View.rawValue
        setSearchType(.local)
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        viewPager.dataSource = self as ViewPagerDataSource
        self.automaticallyAdjustsScrollViewInsets = false
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    override func refreshData() {
        getData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        requestObject(Client.WS_GET_EXPERIENCE_LINK_3G_4G, params: nil, complete: {(success: Bool, data: ModelExperience3G4G?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil && ((data?.links.count)! > 0 || (data?.adBanner.count)! > 0)) {
                self.modelExperience = data
                self.viewPager.reloadData()
                self.tableView.reloadData()
                self.hideAlertOnView(self.tableView)
                self.isHeaderHidden = false
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.showAlertOnView(self.tableView, NSLocalizedString("Msg_DataNotFound", comment: ""))
                self.isHeaderHidden = true
                self.getRightBarButton()?.isEnabled = false
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        setupNavBar(title: NSLocalizedString("ScreenTitle_TraiNghiem01", comment: ""))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        getData()
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        view.endEditing(true)
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredExperienceLinks.removeAll()
        var experience: ModelExperienceLink3G4G
        if modelExperience != nil {
            for i in 0..<modelExperience!.links.count {
                experience = modelExperience!.links[i]
                let name = experience.name
                let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
                if range != nil {
                    filteredExperienceLinks.append(experience)
                }
            }
        }
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredExperienceLinks.count
        } else {
            if (modelExperience != nil) {
                number = modelExperience!.links.count
            }
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "experience", for: indexPath) as! ExperienceCell
        
        let experience: ModelExperienceLink3G4G = getModelAtIndex(indexPath.row)
        
        Util.setImage(cell.iconImage, url: experience.iconUrl)
        cell.nameLabel.text = experience.name
        cell.summaryLabel.text = experience.shortDes
        cell.sourceLink = experience.sourceLink
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("More_TraiNghiem3G4GLabel", comment: "")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isHeaderHidden {
            return CGFloat(0)
        }
        return CGFloat(40.0)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Roboto-Regular", size: 13)!
        header.textLabel?.textColor = UIColor.lightGray
    }
    
    func getModelAtIndex(_ index: Int) -> ModelExperienceLink3G4G {
        if searchActive {
            return filteredExperienceLinks[index]
        } else {
            return modelExperience!.links[index]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ExperienceCell
        let url = URL(string: cell.sourceLink)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
        if searchActive {
            onCancelSearch()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Extension

extension ExperiencesViewController: ViewPagerDataSource {
    
    //Tells how many pages we have
    func numberOfItems(viewPager: ViewPager) -> Int {
        var number = 0
        if modelExperience != nil {
            number = modelExperience!.adBanner.count
        }
        return number
    }
    
    //Load views for each page
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        var newView = view
        var imageView = UIImageView()
        if newView == nil {
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (newView?.frame.width)!, height: (newView?.frame.height)!))
            Util.setImage(imageView, url: modelExperience?.adBanner[index].adImgUrl, defaultImageName: "chuyen_tien_image")
            newView?.addSubview(imageView)
            
            let topContraints = NSLayoutConstraint(item: imageView,
                                                   attribute: .top,
                                                   relatedBy: .equal,
                                                   toItem: newView,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1.0,
                                                   constant: 0)                     // scrollView.top = viewPager.top * 1.0 + 0
            
            let bottomContraints = NSLayoutConstraint(item: imageView,
                                                      attribute: .bottom,
                                                      relatedBy: .equal,
                                                      toItem: newView,
                                                      attribute: NSLayoutAttribute.bottom,
                                                      multiplier: 1.0,
                                                      constant: 0)                  // scrollView.bottom = viewPager.bottom * 1.0 + 0
            
            let leftContraints = NSLayoutConstraint(item: imageView,
                                                    attribute: .leadingMargin,
                                                    relatedBy: .equal,
                                                    toItem: newView,
                                                    attribute: .leadingMargin,
                                                    multiplier: 1.0,
                                                    constant: 0)                    // scrollView.leading = viewPager.leading * 1.0 + 0
            
            let rightContraints = NSLayoutConstraint(item: imageView,
                                                     attribute: .trailingMargin,
                                                     relatedBy: .equal,
                                                     toItem: newView,
                                                     attribute: .trailingMargin,
                                                     multiplier: 1.0,
                                                     constant: 0)                   // scrollView.trailing = viewPager.trailing * 1.0 + 0
            
            imageView.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([topContraints, rightContraints, leftContraints, bottomContraints])
            
        }
        
        return newView!
    }
    
    func didSelectedItem(index: Int) {
        if modelExperience != nil {
            let adBanner = modelExperience!.adBanner[index - 1]
            let url = URL(string: adBanner.sourceLink)!
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
    }
}











