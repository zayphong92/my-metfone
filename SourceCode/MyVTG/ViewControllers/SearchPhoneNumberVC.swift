//
//  SearchPhoneNumberVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchPhoneNumberVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var searchLayout: UIView!
    @IBOutlet weak var tableLayoutMarginTopContrain: NSLayoutConstraint!
    @IBOutlet weak var headerTableMarginParentContrain: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButtonRegular01!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var minPriceTextField: UITextFieldRegular01!
    @IBOutlet weak var maxPriceTextField: UITextFieldRegular01!
    @IBOutlet weak var NumberTextField: UITextFieldRegular01!
    @IBOutlet weak var resultDescLabel: UILabelRegular02!
    @IBOutlet weak var searchContentLabel: UILabel!
    
    // MARK: - Variables
    
    var serviceType: ServiceType = .None
    var lastTableContentOffset: CGFloat = 0
    var isResulfLayoutOnTop: Bool = false
    var isLoadingMore: Bool = false
    var needLoadMore: Bool = true
    var currentPage: Int = 0
    
    var phoneNumbers: [ModelIsdnToBuy] = []
    var serviceFee:Double = 0
    private var isSearching = false

    
    //Match with value in storyboard
    let tableMarginTopDefault:Float = 220
    let searchLayoutHeightDefault:Float = 60.0
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var screenTitle: String = ""
        minPriceTextField.placeholder = NSLocalizedString("Home_MuaSoLabel_MinPrice", comment: "")
        maxPriceTextField.placeholder = NSLocalizedString("Home_MuaSoLabel_MaxPrice", comment: "")
        NumberTextField.placeholder = NSLocalizedString("Home_MuaSoLabel_SoDienThoai", comment: "")
        switch serviceType {
        case .BuyPhoneNumber:
            screenTitle = NSLocalizedString("ScreenTitle_MuaSo02", comment: "")
            break
            
        case .ChangePhoneNumber:
            screenTitle = NSLocalizedString("ScreenTitle_DoiSo02", comment: "")
            break
            
        default:
            break
        }
        setupNavBar(title: screenTitle)
        tableView.register(UINib(nibName: "CommonTableCell", bundle: nil), forCellReuseIdentifier: "CommonTableCell01")
        tableView.separatorStyle = .none
        showAlertOnView(tableView, NSLocalizedString("Utilities_SearchNumber_SearchGuideline", comment: ""))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.moveTableViewToDefault(_:)))
        searchLayout.addGestureRecognizer(tapGesture)
        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hidenKeyboard)))
        resultDescLabel.text = "0 " + NSLocalizedString("Utilities_SearchNumber_NumberFounds", comment: "")
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hidenKeyboard)))
        navigationController?.navigationBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hidenKeyboard)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
    }
    
    // MARK: - Table handle
    
    func moveTableViewToTop() {
        isResulfLayoutOnTop = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowAnimatedContent, animations: {
            self.headerTableMarginParentContrain.constant = CGFloat(self.searchLayoutHeightDefault)
            self.tableLayoutMarginTopContrain.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func moveTableViewToDefault(_ gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true)
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowAnimatedContent, animations: {
            self.headerTableMarginParentContrain.constant = 0
            self.tableLayoutMarginTopContrain.constant = CGFloat(self.tableMarginTopDefault)
            self.view.layoutIfNeeded()
        }, completion:{ (finished: Bool) in
            self.isResulfLayoutOnTop = false
            self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phoneNumbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CommonTableCell01 = tableView.dequeueReusableCell(withIdentifier: "CommonTableCell01") as! CommonTableCell01
        let phoneNumber = phoneNumbers[indexPath.row]
        cell.iconImageView.image =  #imageLiteral(resourceName: "ic_orange_contact")
        cell.nameLabel.text = phoneNumber.isdn
        let desc:String = "\(NSLocalizedString("Utilities_TimSoLabel_Gia", comment: "")) \(Int.withSeparator(phoneNumber.price)) \(NSLocalizedString("Currency_Unit", comment: ""))"
        cell.descLabel.text = desc
        
        var buttonTitle = "---"
        switch serviceType {
        case .BuyPhoneNumber:
            buttonTitle = NSLocalizedString("Home_MuaSoButton_Mua", comment: "")
            break
            
        case .ChangePhoneNumber:
            buttonTitle = NSLocalizedString("Utilities_Doi", comment: "")
            break
            
        default:
            break
            
        }
        cell.button.localizedText = buttonTitle
        cell.button.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(doAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if lastTableContentOffset < scrollView.contentOffset.y
            && (scrollView.contentOffset.y - lastTableContentOffset) > 5.0
            && !isResulfLayoutOnTop
        {
            moveTableViewToTop()
        }
        lastTableContentOffset = scrollView.contentOffset.y
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (!needLoadMore || isLoadingMore) {
            return
        }
        if (indexPath.row == phoneNumbers.count - 1) {
            enableLoadMore(needLoadMore, tableView: tableView)
            isLoadingMore = true
            searchPhoneNumber()
        }
    }
    
    @IBAction func PriceFieldChanged(_ sender: Any) {
        let textField:UITextField = sender as! UITextField
        
        var priceValue : NSNumber = Util.getCurrencyValue(textField.text!)
        if (priceValue == 0) {
            textField.text = nil
            return
        }
        
        if !Util.validateMonny(priceValue.doubleValue) {priceValue = NSNumber.init(value:  floor(priceValue.doubleValue/10))}
        
        textField.text = Util.formatCurrency(priceValue)
        
        let _currencyUnit = " " + NSLocalizedString("Currency_Unit", comment: "")
        if let newPosition = textField.position(from: textField.endOfDocument, offset: -1 * _currencyUnit.characters.count) {
            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        }
    }
    
    // MARK: - Support functions
    
    func fakeData() -> [ModelIsdnToBuy] {
        var data:[ModelIsdnToBuy] = []
        for _ in 0..<20 {
            let phoneNumber: ModelIsdnToBuy = (Mapper<ModelIsdnToBuy>().map(JSONObject: [:]))!
            phoneNumber.isdn = "0967245018"
            phoneNumber.price = 88888888
            data.append(phoneNumber)
        }
        return data
    }
    
    func getDataDone(_ data: [ModelIsdnToBuy]) {
        isLoadingMore = false
        currentPage += 1
        needLoadMore = data.count >= Client.PAGE_SIZE
        self.phoneNumbers += data
        tableView.separatorStyle = .singleLine
        self.tableView.reloadData()
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        tableView.reloadData()
        resultDescLabel.text = "\(phoneNumbers.count) " + NSLocalizedString("Utilities_SearchNumber_NumberFounds", comment: "")
    }
    
    func setSearchTitle() {
        var min: String = minPriceTextField.text!
        let max: String = maxPriceTextField.text!
        let num: String = NumberTextField.text!
        var str = ""
        
        if min.characters.count > 0 && max.characters.count > 0 {
            str = min + " ~ " + max
        } else if min.characters.count > 0 {
            str =  "> " + min
        } else if max.characters.count > 0 {
            str = "< " + max
        }
        
        if (num.characters.count > 0) {
            if (str.characters.count > 0) { str += " - "}
            str += num
        }
    
        searchContentLabel.text = str
    }
    
    // MARK: - Buttons handle
    
    @IBAction func ClearAllButtonTaped(_ sender: Any) {
        view.endEditing(true)
        minPriceTextField.text = nil
        maxPriceTextField.text = nil
        NumberTextField.text = nil
    }
    
    @IBAction func onSearchButtonTaped(_ sender: Any) {
        if isSearching { return }
        isSearching = true
        
        enabaleEditText(enable: false)
        view.endEditing(true)
        setSearchTitle()
        currentPage = 0
        searchPhoneNumber()
        resultDescLabel.text = "0 " + NSLocalizedString("Utilities_SearchNumber_NumberFounds", comment: "")
    }
    
    
    private func enabaleEditText(enable:Bool) {
        minPriceTextField.isEnabled = enable
        maxPriceTextField.isEnabled = enable
        NumberTextField.isEnabled = enable
    }
    
    func doAction(_ sender: UIButton) {
        switch serviceType {
        case .BuyPhoneNumber:
            buyNumberAction(phoneNumbers[sender.tag])
        case .ChangePhoneNumber:
            changeNumberAction(phoneNumbers[sender.tag])
        default:
            break
        }
    }
    
    func buyNumberAction(_ isdn: ModelIsdnToBuy) {
        let vc:BuyPhoneNumberVC = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "BuyPhoneNumberVC") as! BuyPhoneNumberVC
        vc.phoneNumber = isdn
        vc.serviceFee = serviceFee
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeNumberAction(_ isdn: ModelIsdnToBuy) {
        let vc:PaymentBillVC = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "PaymentBillVC") as! PaymentBillVC
        vc.serviceType = .ChangePhoneNumber
        vc.phoneNumber = isdn
        vc.serviceFee = serviceFee
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func searchPhoneNumber() {
        let minPrice = Util.getCurrencyValue(minPriceTextField.text!)
        var maxPrice:NSNumber = NSNumber.init(value: Const.MAX_PHONE_PRICE)
        if maxPriceTextField.text!.characters.count > 0 {
            maxPrice = Util.getCurrencyValue(maxPriceTextField.text!)
        }
        
        let phoneNumberPattern = NumberTextField.text!
        hideAlertOnView(tableView)
        if currentPage == 0 {
            phoneNumbers.removeAll()
            tableView.reloadData()
            enableLoadMore(false, tableView: tableView)
            tableView.reloadData()
            showProgressOnView(tableView)
        }
        
        let params = [
            "subType": client.getSubType(),
            "minPrice": minPrice,
            "maxPrice": maxPrice,
            "isdnPattern": phoneNumberPattern,
            "pageSize": Client.PAGE_SIZE,
            "pageNum": currentPage
            
            ] as [String : Any]
        requestList(Client.WS_FIND_ISDN_TO_BUY, params: params, complete: {(success: Bool, data: [ModelIsdnToBuy]?) -> Void in
            self.isSearching = false
            self.enabaleEditText(enable: true)
            
            if (success && data != nil) {
                self.hideProgressOnView(self.tableView)
                self.hideAlertOnView(self.tableView)
                self.getDataDone(data!)
            } else {
                self.hideProgressOnView(self.tableView)
                self.showAlertOnView(self.tableView, NSLocalizedString("More_Not_Found_Data", comment: ""));
            }
        })
    }
}





