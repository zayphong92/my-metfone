//
//  ChangeDataPackageVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChangeDataPackageVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    
    var dataPackages: [ModelUserService] = []
    var filteredPackages: [ModelUserService] = []
    var searchActive: Bool = false
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        setupNavBar(title: NSLocalizedString("ScreenTitle_DoiGoiData01", comment: ""))
        setupTableView()
        getListDataPackage()
    }
    
    func setupTableView() {
        setSearchType(.local)
        setupPullRefreshForScrollable(tableView, enable: true)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    override func onBtnLeftNavBar() {
        if let count = navigationController?.viewControllers.count, count > 1 {
            if let buyDataVC = navigationController?.viewControllers[count - 2] as? BuyDataVC {
                for controller in (self.navigationController?.viewControllers)! {
                    if controller.isKind(of: HomeViewController.self) {
                        self.navigationController?.popToViewController(controller, animated: true)
                        return
                    }
                }
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getListDataPackage() {
        if firstTimeLoadView {
            showProgressOnView(tableView)
        }
        requestList(Client.WS_GET_ALL_DATA_PACKAGES, params: nil, complete: {(success: Bool, data: [ModelUserService]?) -> Void in
            self.refreshDataDone()
            self.hideProgressOnView(self.tableView)
            self.firstTimeLoadView = false
            if success && data != nil && data!.count > 0 {
                self.dataPackages = data!
                self.tableView.reloadData()
                self.getRightBarButton()?.isEnabled = true
                self.hideEmptyPageOn(view: self.tableView)
            } else {
                if (self.dataPackages.count == 0) {
                    self.getRightBarButton()?.isEnabled = false
                    self.showEmptyPageOn(view: self.tableView, emptyPage: .DoiGoiData)
                }
                
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_DoiGoiData01", comment: ""))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
    }
    
    override func refreshData() {
        hideAlertOnView(self.tableView)
        getListDataPackage()
    }
    
    override func onBtnRightNavBar() {
        toggleSearchBar()
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredPackages.removeAll()
        var package: ModelUserService
        for i in 0..<dataPackages.count {
            package = dataPackages[i]
            let name = package.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredPackages.append(package)
            }
        }
        
        searchActive = keyword != ""
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        tableView.reloadData()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        searchActive = false
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredPackages.count
        } else {
            number = dataPackages.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataPackageCell") as! DataPackageCell
        let package = getModelAtIndex(indexPath.row)
        
        cell.nameLabel.text = package.name.characters.count == 0 ? "_" : package.name
        cell.descLabel.text = package.shortDes.characters.count == 0 ? "_" : package.shortDes
        Util.setImage(cell.iconImage, url: package.iconUrl, defaultImageName: "ic_data_default")
        
        if package.state == 1 {
            cell.changeButton.setTitle(NSLocalizedString("ServicesLabel_DaDangKy", comment: ""), for: .normal)
            cell.changeButton.isEnabled = false
        } else if package.state == 2 {
            cell.changeButton.setTitle(NSLocalizedString("Home_DoiGoiDataDangXuLy", comment: ""), for: .normal)
            cell.changeButton.isEnabled = false
        } else {
            cell.changeButton.setTitle(NSLocalizedString("Home_DoiGoiDataButton", comment: ""), for: .normal)
            cell.changeButton.isEnabled = true
        }
        cell.changeButton.tag = indexPath.row
        cell.changeButton.addTarget(self, action: #selector(changeDataPlan), for: .touchUpInside)
        
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelUserService {
        if searchActive {
            return filteredPackages[index]
        } else {
            return dataPackages[index]
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: ChangeDataDetailVC = CarouselVC.initViewController()
        vc.dataPackages = searchActive ? filteredPackages : dataPackages
        let count:Int = searchActive ? filteredPackages.count : dataPackages.count
        vc.setPageCount(count, startPosition: indexPath.row)
        goNext(vc)
        if searchActive {
            onCancelSearch()
        }
        navigationController?.navigationBar.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func changeDataPlan(_ sender : UIButtonRegular01) {
        let package = getModelAtIndex(sender.tag)
        registerDataPackage(package) { (success: Bool, data: ModelDoAction?) in
            if (success) {
                if (data != nil && data?.errorCode == Client.ERR_CODE_BACKEND_SUCCESS_1000) {
                    sender.setTitle(NSLocalizedString("ServicesLabel_DaDangKy", comment: ""), for: .normal)
                    sender.isEnabled = false
                } else if (data != nil && data?.errorCode == Client.ERR_CODE_BACKEND_SUCCESS) {
                    //sender.state = 2
                    sender.setTitle(NSLocalizedString("Home_DoiGoiDataDangXuLy", comment: ""), for: .normal)
                    sender.isEnabled = false
                }
            }
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        navigationController?.navigationBar.endEditing(true)
    }
    
}
