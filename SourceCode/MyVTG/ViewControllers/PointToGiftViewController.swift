//
//  PointToGiftViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PointToGiftViewController: BaseVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet05", comment: ""))
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
}
