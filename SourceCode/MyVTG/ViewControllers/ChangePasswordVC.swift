//
//  ChangePasswordViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/15/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var oldPasswordTxt: UITextFieldRegular01!
    @IBOutlet weak var newPasswordTxt: UITextFieldRegular01!
    @IBOutlet weak var confirmNewPasswordTxt: UITextFieldRegular01!
    @IBOutlet weak var changePassButton: UIButtonRegular01!
    
    @IBOutlet weak var oldPassErrorLabel: UILabel!
    @IBOutlet weak var newPassErrorLabel: UILabel!
    @IBOutlet weak var confirmNewPassErrorLabel: UILabel!
    
    private var isOldPassCorrect:Bool = false
    private var isPassCorrect:Bool = false
    private var isConfirmPassCorrect:Bool = false
    
    private var isPopedScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.ChangePassword.rawValue
        actionGA = Action.View.rawValue
        setupNavBar(title: NSLocalizedString("ScreenTitle_DoiMatKhau01", comment: ""))
        baseScrollView = scrollView
        tabBarController?.tabBar.isHidden = true
        accountNameLabel.text = client.getIsdn()
        setupTextFields()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isPopedScreen = true
    }
    
    func setupTextFields() {
        oldPasswordTxt.placeholder = NSLocalizedString("More_DoiMatKhauLabel_MatKhauCu", comment: "")
        newPasswordTxt.placeholder = NSLocalizedString("More_DoiMatKhauLabel_MatKhauMoi", comment: "")
        confirmNewPasswordTxt.placeholder = NSLocalizedString("More_DoiMatKhauLabel_XacNhanMatKhau", comment: "")
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    // MARK: - Actions handle
    
    @IBAction func onOldPassTextChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        
        textField.text = ValidateUtil.limitLength(password: textField.text)
        let (isCorrect, msg) = ValidateUtil.validate(password: textField.text)
        isOldPassCorrect = isCorrect
        oldPassErrorLabel.text = msg
        changePassButton.isEnabled = isOldPassCorrect && isPassCorrect && isConfirmPassCorrect
    }
    
    @IBAction func onNewPassTextChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        
        //reset confirm pass
        confirmNewPasswordTxt.text = ""
        confirmNewPassErrorLabel.text =  NSLocalizedString("Label_confirm_the_password", comment: "")
        isConfirmPassCorrect = false
        
        textField.text = ValidateUtil.limitLength(password: textField.text)
        let (isCorrect, msg) = ValidateUtil.validate(password: textField.text)
        isPassCorrect = isCorrect
        newPassErrorLabel.text = msg
        changePassButton.isEnabled = isOldPassCorrect && isPassCorrect && isConfirmPassCorrect

    }
    
    @IBAction func onConfirmPassTextChanged(_ sender: Any) {
        let textField:UITextFieldRegular01 = sender as! UITextFieldRegular01
        
        textField.text = ValidateUtil.limitLength(password: textField.text)
        let (isCorrect, msg) = ValidateUtil.isPasswordMatched(pass: newPasswordTxt.text, confirmPass: textField.text)
        isConfirmPassCorrect = isCorrect
        confirmNewPassErrorLabel.text = msg
        changePassButton.isEnabled = isOldPassCorrect && isPassCorrect && isConfirmPassCorrect

    }

    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func closeTapped(_ sender: UIButtonRegular02) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePasswordTapped(_ sender: UIButtonRegular01) {
        view.endEditing(true)
        
        let params : [String: Any] = [
                        "username": client.getIsdn(),
                        "passold": oldPasswordTxt.text!,
                        "passnew": newPasswordTxt.text!
                    ]
        requestAction02(Client.USER_CHANGE_PASSWORD, params: params, actionComplete: {(success:Bool, data: ModelDefault?) -> Void in
            if(success) {
                self.showSuccess(NSLocalizedString("More_DoiMatKhauLabel_ThongBao4", comment: ""))
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    if self.isPopedScreen { return }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
        
    }
}








