//
//  CellServiceDetail02.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

protocol CellServiceDetailDelegate {
    func regUnregService(_ model: ModelRegistable)
}

class CellServiceDetail02: UITableViewCell {
    @IBOutlet weak var imv: UIImageView!
    @IBOutlet weak var btnSub: UIButton!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tvDes: UITextView!
    
    private var model: ModelServiceDetailSubPackage? = nil
    var delegate: CellServiceDetailDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbName.text = ""
        lbName.numberOfLines = 1
        btnSub.contentEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 7)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onBtnSub(_ sender: UIButton) {
        if (delegate != nil) {
            delegate!.regUnregService(model!)
        }
    }
    
    func setupUI(_ subPkg: ModelServiceDetailSubPackage, isRegisterAble: Int) {
        self.model = subPkg
        Util.setImage(imv, url: subPkg.iconUrl, defaultImageName: "ic_service_package")
        lbName.text = subPkg.name
        tvDes.text = subPkg.shortDes
        
        if isRegisterAble != 0 {
            if subPkg.state == 2 {
                btnSub.setTitle(NSLocalizedString("Services_DangXuLyButton", comment: ""), for: .normal)
                btnSub.isEnabled = false
            } else {
                btnSub.setTitle(subPkg.textForButtonSubmit, for: .normal)
                btnSub.isEnabled = true
            }
            btnSub.sizeToFit()
            btnSub.isHidden = false
            
            if ((subPkg.isRegistered && isRegisterAble == 1) || (!subPkg.isRegistered && isRegisterAble == 2)) {
                btnSub.isHidden = true //only allows register or un-register
            }
            
        } else {
            btnSub.isHidden = true
        }
    }
}
