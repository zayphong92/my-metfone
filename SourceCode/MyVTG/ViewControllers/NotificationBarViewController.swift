//
//  NotificationBarViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/20/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class NotificationBarViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var notiIcon: UIImageView!
    @IBOutlet weak var notiLabel: UILabel!
    @IBOutlet weak var barViewHeight: NSLayoutConstraint!
    
    // MARK: - Variables
    
    let successColor = UIColor.rgbValue(hexValue: 0x8dc63f) //UIColor(red: 141.0 / 255.0, green: 197.0 / 255.0, blue: 80.0 / 255.0, alpha: 1.0)
    let failColor = UIColor.rgbValue(hexValue: 0xed1c24) //UIColor(red: 245.0 / 255.0, green: 13.0 / 255.0, blue: 41.0 / 255.0, alpha: 1.0)
    var isSuccessful: Bool = true
    var notiString: String?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notiLabel.text = notiString
        if isSuccessful {
            notiIcon.image = #imageLiteral(resourceName: "ic_success")
            barView.backgroundColor = successColor
        } else {
            notiIcon.image = #imageLiteral(resourceName: "ic_fail")
            barView.backgroundColor = failColor
        }
        
        barView.layer.cornerRadius = 3
        barView.layer.masksToBounds = true
        notiLabel.sizeToFit()
        barViewHeight.constant = notiLabel.frame.size.height + CGFloat(30.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 3,
                       animations: {self.barView.alpha = 0.0},
                       completion: {_ in
                        self.dismiss(animated: false, completion: nil)
        })
    }
    
    // MARK: - Actions handle
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
}









