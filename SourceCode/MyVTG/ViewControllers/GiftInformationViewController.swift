//
//  GiftInformationViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import DropDown

class GiftInformationViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var districtTextField: UITextFieldRegular01!
    @IBOutlet weak var wardTextField: UITextFieldRegular01!
    @IBOutlet weak var dateTextField: UITextFieldRegular01!
    @IBOutlet weak var streetTextField: UITextFieldRegular01!
    @IBOutlet weak var dateSymbol: UILabel!
    
    // MARK: - Variables
    
    var city: City?
    let districtDropDown = DropDown()
    let wardDropDown = DropDown()
    var districtName = [String]()
    var wardName = [String]()
    var activeField: UITextField?
    var deliveryDate = Date()
    let dateFormatterFull: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    let dateFormatterShort: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM"
        return formatter
    }()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet07", comment: ""))
        cityLabel.text = city?.name
        
        districtDropDown.anchorView = districtTextField
        wardDropDown.anchorView = wardTextField
        
        districtDropDown.direction = .any
        wardDropDown.direction = .any
        
        if let count = city?.districtList.count {
            for i in 0..<count {
                districtName.append((city?.districtList[i].name)!)
                if let count1 = city?.districtList[i].ward.count {
                    for j in 0..<count1 {
                        wardName.append((city?.districtList[i].ward[j])!)
                    }
                }
            }
        }
        districtDropDown.dataSource = districtName
        wardDropDown.dataSource = wardName
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    // MARK: - Text field handle
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        super.textFieldDidBeginEditing(textField)
        if textField == districtTextField {
            districtDropDown.show()
            view.endEditing(true)
        } else if textField == wardTextField {
            wardDropDown.show()
            view.endEditing(true)
        }
        
        districtDropDown.selectionAction = { (Int, String) -> Void in
            self.districtTextField.text = self.districtDropDown.selectedItem
            self.wardName.removeAll()
            let i = self.districtDropDown.indexForSelectedRow!
            if let count = self.city?.districtList[i].ward.count {
                for j in 0..<count {
                    self.wardName.append((self.city?.districtList[i].ward[j])!)
                }
            }
            self.wardDropDown.dataSource = self.wardName
        }
        wardDropDown.selectionAction = { (Int, String) -> Void in
            self.wardTextField.text = self.wardDropDown.selectedItem
        }
    }
    
    // MARK: - Actions handle
    
    @IBAction func chooseDate(_ sender: UIButton) {
        presentCalendar()
    }
    
    func presentCalendar() {
        let calendarVC = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "calendarVC") as! CalendarViewController
        calendarVC.startDate = self.deliveryDate
        calendarVC.postAction = {
            self.deliveryDate = calendarVC.startDate!
            self.dateTextField.text = self.dateFormatterFull.string(from: self.deliveryDate)
            self.dateSymbol.text = self.dateFormatterShort.string(from: self.deliveryDate)
        }
        calendarVC.modalPresentationStyle = .overCurrentContext
        present(calendarVC, animated: true, completion: nil)
    }
    
    @IBAction func closeTapped(_ sender: UIButtonRegular02) {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: NSLocalizedString("More_KhachHangThanThietLabel_ThongBao1", comment: ""),
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: { self.navigationController?.popViewController(animated: true)})
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}















