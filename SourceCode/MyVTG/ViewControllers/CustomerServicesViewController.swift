//
//  CustomerServicesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CustomerServicesViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var tableView: UITableView!
    var customerServiceStore: CustomerServiceStore!
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.CustomerServices.rawValue
        actionGA = Action.View.rawValue
        customerServiceStore = CustomerServiceStore()
        customerServiceStore.initCustomerServices()
        GAHelper.logGA(category: screenName!, actionGA: actionGA!, labelGA: nil, valueGA: nil)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        setupNavBar(title: NSLocalizedString("ScreenTitle_ChamSocKhachHang01", comment: ""))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customerServiceStore.allCustomerSerives.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customerServiceCell", for: indexPath) as! CustomerServiceCell
        let customerService = customerServiceStore.allCustomerSerives[indexPath.row]
        cell.iconImage.image = customerService.icon
        cell.nameLabel.text = customerService.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellActionGA = Action.Click.rawValue
        let customerService = customerServiceStore.allCustomerSerives[indexPath.row]
        switch customerService.value {
        case .ChatOnline, .CongDong, .CauHoi:
            UIApplication.shared.openURL(NSURL(string: customerService.link)! as URL)
        case .TongDai:
            customerServicesCall(phoneNumber: customerService.link)
        case .GuiMail:
            let vc = UIStoryboard.getViewController(storyBoardName: "TabMore_ChamSocKhachHang", viewControllerName: "feedbackEmailVC")
            goNext(vc)
        }
        GAHelper.logGA(category: screenName!, actionGA: cellActionGA, labelGA: nil, valueGA: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func customerServicesCall(phoneNumber: String) {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: NSLocalizedString("More_ChamSocKhachHang_TongDaiLabel_Content", comment: ""),
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: {
                    let url = "tel://" + phoneNumber
                    if UIApplication.shared.canOpenURL(URL(string: url)!) {
                        UIApplication.shared.openURL(URL(string: url)!)
                    }
        })
    }
}
