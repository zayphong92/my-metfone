//
//  BuyDataController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/10/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class BuyDataController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    var inUseDataPackageStore: InUseDataPackageStore!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    private var listDataVolumeLevels = [ModelDataVolumeLevel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        setupNavBar(title: NSLocalizedString("ScreenTitle_MuaThemData01", comment: ""))
        screenName = ScreenNames.BuyData.rawValue
        actionGA = Action.View.rawValue
        self.automaticallyAdjustsScrollViewInsets = false
        getData()
    }
    
    func getData() {
        if listDataVolumeLevels.count == 0 {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "packageCode": "LT30"
        ]
        requestList(Client.WS_GET_DATA_VOLUME_LEVEL_TO_BUY, params: params, complete: {(success: Bool, data: [ModelDataVolumeLevel]?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            if (success && data != nil) {
                self.listDataVolumeLevels = data!
                self.tableView.reloadData()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        inUseDataPackageStore = InUseDataPackageStore()
        inUseDataPackageStore.initInUseDataPackages()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat
        if indexPath.section == 0 {
            height = 100.0
        } else {
            height = 200.0
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if section == 0 {
            number = inUseDataPackageStore.allInUseDataPackages.count
        } else if section == 1 {
            number = 1
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "inUseDataPackage", for: indexPath) as! InUseDataPackageCell
            
            let inUseDataPackage = inUseDataPackageStore.allInUseDataPackages[indexPath.row]
            
            cell.nameLabel.text = "\(NSLocalizedString("Home_ThemDataLabel_GoiDataChinh", comment: "")) \(inUseDataPackage.name)"
            cell.expirationLabel.text = "\(NSLocalizedString("Home_ThemDataLabel_Han", comment: "")) \(inUseDataPackage.expiration)"
            cell.dataLeftLabel.text = "\(NSLocalizedString("Home_ThemDataLabel_Con", comment: "")) \(inUseDataPackage.dataAmountLeft)"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dataPackageLevel", for: indexPath) as! DataVolumeLevelCell
            cell.listModels = listDataVolumeLevels
            cell.setupUICell()
            return cell
        }
    }
    
    @IBAction func purchaseTapped(_ sender: UIButtonRegular01) {
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = tableView.cellForRow(at: indexPath) as! DataVolumeLevelCell
        if let selectedLevel = cell.getSelectedItem() {
            let price = selectedLevel.price
            let volume = selectedLevel.volume
            showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                      message: "\(NSLocalizedString("Home_ThemDataLabel_ThongBao2_1", comment: "")) \(volume)MB \(NSLocalizedString("Home_ThemDataLaebl_ThongBao2_2", comment: "")) \(price) \(NSLocalizedString("Currency_Unit", comment: ""))?",
                cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                onOK: {
                    let params = [
                        "subType": self.client.getSubType(),
                        "packageCode": "M2",
                        "price": price,
                        "volume": volume
                    ] as [String : Any]
                    self.requestAction(Client.WS_DO_BUY_DATA, params: params)
            })
        } else {
            showNoti(message: NSLocalizedString("Home_ThemDataLabel_ThongBao3", comment: ""), isSuccessful: false)
        }
    }
    
    @IBAction func detailTapped(_ sender: UIButton) {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "detailScreen") as! DetailViewController
        vc.actionName = NSLocalizedString("Home_ThemDataButton_Huy", comment: "")
        func getDataPackageInfo() {
            if vc.firstTimeRequestInfo {
                vc.imvLoading.startProgress()
                vc.scrollView.isHidden = true
                vc.bottomView.isHidden = true
            }
            let params = [
                "packageCode": "MI2"
            ]
            requestObject(Client.WS_GET_DATA_PACKAGE_INFO, params: params, complete: {(success: Bool, data: ModelDataPackageInfo?) -> Void in
                vc.imvLoading.stopProgress()
                vc.scrollView.isHidden = false
                vc.bottomView.isHidden = false
                if (success && data != nil) {
                    vc.firstTimeRequestInfo = false
                    vc.navTitle = (data?.code)!
                    vc.imgUrl = data?.imgDesUrl
                    vc.name = data?.name
                    vc.content = data?.fullDes
                    vc.setupUI()
                }
            })
        }
        vc.getData = {
            getDataPackageInfo()
        }
        vc.buttonTapped = {
            self.showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                           message: NSLocalizedString("Home_ThemDataLabel_ThongBao1", comment: ""),
                           cancelLabel: NSLocalizedString("Label_KhongDongY", comment: ""),
                           agreeLabel: NSLocalizedString("LabeL_DongY", comment: ""),
                           onOK: {
                            let params = [
                                "actionType": "1",
                                "serviceCode": "MI2"
                            ]
                            vc.requestAction(Client.WS_DO_ACTION_SERVICE, params: params)
            })
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}











