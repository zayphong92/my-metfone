//
//  FilterOptionsViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class FilterOptionsViewController: BaseVC {
    
    @IBOutlet weak var newestIcon: UIImageView!
    @IBOutlet weak var newestLabel: UILabel!
    @IBOutlet weak var oldestIcon: UIImageView!
    @IBOutlet weak var oldestLabel: UILabel!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var boxViewHeight: NSLayoutConstraint!
    @IBOutlet weak var boxViewWidth: NSLayoutConstraint!
    
    var newestRearrange: () -> Void = {}
    var oldestRearrange: () -> Void = {}
    var isNewest: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: { self.boxAppears() })
    }
    
    func setupUI() {
        boxView.layer.cornerRadius = 3
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOffset = CGSize.zero
        boxView.layer.shadowOpacity = 1
        boxView.layer.shadowRadius = 5
        boxView.frame.origin.x += boxView.frame.size.width
        boxViewWidth.constant = 0
        boxViewHeight.constant = 0
        for i in 100...103 {
            view.viewWithTag(i)?.alpha = 0
        }
        if isNewest {
            highlightNewest()
        } else {
            highlightOldest()
        }
    }
    
    private func highlightNewest() {
        newestIcon.image = #imageLiteral(resourceName: "ic_tick_s")
        newestLabel.textColor = Const.COLOR_MAIN
        oldestIcon.image = #imageLiteral(resourceName: "ic_tick")
        oldestLabel.textColor = Const.COLOR_GRAY_FOR_LABEL
    }
    
    private func highlightOldest() {
        newestIcon.image = #imageLiteral(resourceName: "ic_tick")
        newestLabel.textColor = Const.COLOR_GRAY_FOR_LABEL
        oldestIcon.image = #imageLiteral(resourceName: "ic_tick_s")
        oldestLabel.textColor = Const.COLOR_MAIN
    }
    
    func boxAppears() {
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: [.allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.boxView.frame.origin.x -= self.boxView.frame.size.width
                        self.boxViewHeight.constant = 105
                        self.boxViewWidth.constant = 150
                        self.boxView.subviews.forEach { $0.alpha = 1 }
                        self.view.layoutIfNeeded()
        })
    }
    
    func boxDisappears() {
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: [.allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.boxView.subviews.forEach { $0.alpha = 0 }
                        self.view.layoutIfNeeded()
        })
        
        UIView.animate(withDuration: 0.2,
                       delay: 0.1,
                       options: [.allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.boxView.frame.origin.x += self.boxView.frame.size.width
                        self.boxViewHeight.constant = 0
                        self.boxViewWidth.constant = 0
                        self.view.layoutIfNeeded()
        },
                       completion: {_ in
                        self.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        boxDisappears()
    }
    
    @IBAction func newestTapped(_ sender: UIButton) {
        highlightNewest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.boxDisappears()
        })
        newestRearrange()
    }
    
    @IBAction func oldestTapped(_ sender: UIButton) {
        highlightOldest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.boxDisappears()
        })
        oldestRearrange()
    }
    
}
