//
//  InputOTPDialogVC.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class InputOTPDialogVC: BaseVC {
    
    @IBOutlet weak var textFieldOTP: UITextFieldRegular01!
    var inputOTPDelegate: InputOTPDelegate? = nil
    @IBOutlet weak var OKButton: UIButtonRegular01!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = false
        textFieldOTP.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: UIButtonRegular02) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func OK(_ sender: UIButtonRegular01) {
        dismiss(animated: true, completion: nil)
        if inputOTPDelegate != nil {
            inputOTPDelegate?.inputOTPDone(otp: textFieldOTP.text!)
        }
    }
    
    @IBAction func otpTextFieldChanged(_ sender: Any) {
        OKButton.isEnabled = (textFieldOTP.text?.characters.count)! > 0
    }
    
}
