//
//  RechargeViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class RechargeViewController: BaseVC, CNContactPickerDelegate {

    // MARK: - Outlets
    
    @IBOutlet weak var iconPhonebook: UIImageView!
    @IBOutlet weak var iconScratchCard: UIImageView!
    @IBOutlet weak var scratchCardCode: UITextFieldRegular01!
    @IBOutlet weak var phoneNumberText: UITextFieldRegular01!
    @IBOutlet weak var rechargeButton: UIButtonRegular01!
    
    // MARK: - Variables
    
    var contact: CNContact?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBars()
        setupTextFields()
    }
    
    func setupBars() {
        setupNavBar(title: NSLocalizedString("ScreenTitle_NapCuoc01", comment: ""))
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        tabBarController?.tabBar.isHidden = true
    }
    
    func setupTextFields() {
        phoneNumberText.text = client.getIsdn()
        phoneNumberText.placeholder = NSLocalizedString("Label_SoDienThoai", comment: "")
        phoneNumberText.editWidthToAvoid(view: iconPhonebook)
        scratchCardCode.placeholder = NSLocalizedString("Home_NapCuocLabel_MaTheCao", comment: "")
        scratchCardCode.editWidthToAvoid(view: iconScratchCard)
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    // MARK: - Text field handle
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        super.textFieldDidEndEditing(textField)
    }
    
    // MARK: - Taps handle
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction func closeTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rechargeTapped(_ sender: UIButtonRegular01) {

//        let result = ValidateUtil.validate(phoneNumber: phoneNumberText.text!)
//        guard result.iscorrect else {
//            showError(result.msg!)
//            return
//        }
        
        guard scratchCardCode.text != nil, scratchCardCode.text! != "" else {
            showError(NSLocalizedString("Home_NapCuocLabel_ThongBao3", comment: ""))
            return
        }
                
        let params = [
            "desIsdn": phoneNumberText.text!,
            "serial": scratchCardCode.text!
        ]
        requestAction(Client.WS_DO_RECHARGE, params: params, actionComplete:{(success: Bool, model: ModelDoAction?) in
            if success {
                self.phoneNumberText.text = nil
                self.scratchCardCode.text = nil
            }
        })
        
        GAHelper.logGA(category: ScreenNames.ScratchCard.rawValue, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
    }
    
    // MARK: - Segue handle
    
    @IBAction func segueToContactVC(_ sender: UIButton) {
//        let contactVC = UIStoryboard.getViewController(storyBoardName: "NapCuoc", viewControllerName: "contactVC") as! ContactViewController
//        contactVC.afterPopVC = {
//            self.contact = contactVC.chosenContact
//            self.phoneNumberText.text = self.contact?.phoneNumbers[0].value.stringValue
//        }
//        self.navigationController?.pushViewController(contactVC, animated: true)
        let contactVC = CNContactPickerViewController()
        contactVC.delegate = self
        UIApplication.shared.statusBarStyle = .default
        self.present(contactVC, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let phoneNumbers = contact.phoneNumbers
        if phoneNumbers.count > 0 {
            self.phoneNumberText.text = contact.phoneNumbers[0].value.stringValue
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.showError(NSLocalizedString("Home_NapCuocLabel_ThongBao1", comment: ""))
                self.scratchCardCode.text = nil
            })
        }
    }
    
    @IBAction func segueToCodeReaderVC(_ sender: UIButton) {
        presentQRScannerController(withResultFilledIn: scratchCardCode)
    }
    
    @IBAction func toCodeReaderVC(_ sender: UIButton) {
        presentQRScannerController(withResultFilledIn: scratchCardCode)
    }
}







