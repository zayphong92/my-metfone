//
//  ChooseDistrictViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/5/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChooseDistrictViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var districtTableView: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var province: ModelProvince? = nil
    var district: ModelDistrict? = nil
    var listDistricts = [ModelDistrict]()
    var filteredDistricts = [ModelDistrict]()
    var searchActive: Bool = false
    var afterPopVC: () -> Void = {}
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang03", comment: ""))
        districtTableView.tableFooterView = UIView()
        setupPullRefreshForScrollable(districtTableView, enable: !searchActive)
        setTextFieldAsInputSearchBar(searchBar)
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_TimCuaHang03", comment: ""))
        districtTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(districtTableView.frame.size.width), height: 1))
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func refreshData() {
        getData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            districtTableView.isHidden = true
        }
        if province != nil {
            let params = [
                "provinceId": province!.id
            ]
            requestList(Client.WS_GET_DISTRICTS, params: params, complete: {(success: Bool, data: [ModelDistrict]?) -> Void in
                self.imvLoading.stopProgress()
                self.districtTableView.isHidden = false
                self.firstTimeLoadView = false
                self.refreshDataDone()
                if (success && data != nil) {
                    self.listDistricts = data!
                    self.districtTableView.reloadData()
                    self.animateTable()
                }
            })
        }
    }
    
    // MARK: - Table handle

    func animateTable() {
        districtTableView.reloadData()
        
        let cells = districtTableView.visibleCells
        let tableHeight: CGFloat = districtTableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 0.8,
                           delay: 0.05 * Double(index),
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: [],
                           animations: {
                            cell.transform = CGAffineTransform(translationX: 0, y: 0);
            },
                           completion: nil)
            index += 1
        }
    }
    
    override func onHeaderSearch(keyword: String) {
        filteredDistricts.removeAll()
        var districtToCompare: ModelDistrict
        for i in 0..<listDistricts.count {
            districtToCompare = listDistricts[i]
            let name = districtToCompare.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredDistricts.append(districtToCompare)
            }
        }
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(districtTableView, enable: !searchActive)
        districtTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredDistricts.count
        } else {
            number = listDistricts.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "district", for: indexPath) as! ChooseDistrictCell
        var district: ModelDistrict
        if searchActive {
            district = filteredDistricts[indexPath.row]
        } else {
            district = listDistricts[indexPath.row]
        }
        cell.nameLabel.text = district.name
        cell.icon.image = UIImage(named: "ic_tick_s")
        cell.icon.isHidden = true
        if let selectedDistrict = self.district {
            if selectedDistrict.id == district.id {
                cell.icon.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var chosenDistrict: ModelDistrict
        if searchActive {
            chosenDistrict = filteredDistricts[indexPath.row]
        } else {
            chosenDistrict = listDistricts[indexPath.row]
        }
        self.district = chosenDistrict
        districtTableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.navigationController?.popViewController(animated: true)
            self.afterPopVC()
            if self.searchActive {
                self.searchBar.text = ""
                self.searchActive = false
                self.districtTableView.reloadData()
                self.view.endEditing(true)
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}






