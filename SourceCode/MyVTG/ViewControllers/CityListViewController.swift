//
//  CityListViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CityListViewController: BaseVC, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var tableView: UITableView!
    
    var cityStore: CityStore!
    var filteredCities: CityStore!
    var searchActive = false
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("More_KhachHangThanThietLabel_ChonThanhPho", comment: ""))
        cityStore = CityStore()
        cityStore.initCities()
        filteredCities = CityStore()
        setSearchType(.local)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        view.endEditing(true)
        searchActive = false
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredCities.allCities.removeAll()
        var city: City
        for i in 0..<cityStore.allCities.count {
            city = cityStore.allCities[i]
            let name = city.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredCities.allCities.append(city)
            }
        }
        
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int = 0
        if searchActive {
            number = filteredCities.allCities.count
        } else {
            number = cityStore.allCities.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "city", for: indexPath) as! CityCell
        var city: City
        if searchActive {
            city = filteredCities.allCities[indexPath.row]
        } else {
            city = cityStore.allCities[indexPath.row]
        }
        cell.nameLabel.text = city.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let city = cityStore.allCities[row]
        let vc = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "giftListVC") as! GiftListViewController
        vc.city = city
        if searchActive {
            onCancelSearch()
        }
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}












