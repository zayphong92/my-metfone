//
//  PointToMoneyViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PointToMoneyViewController: BaseVC {
    @IBOutlet weak var pointTextField: UITextFieldRegular01!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet03", comment: ""))
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func getNavBarLeftImageName() -> String {
        return "ic_close_white"
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
