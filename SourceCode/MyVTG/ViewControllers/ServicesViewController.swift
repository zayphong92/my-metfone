//
//  ServicesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ServicesViewController: BaseVC, UITableViewDataSource, UITableViewDelegate, IndicatorInfoProvider,  ServiceRowDelegate {
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    var isGotoSubScreen: Bool = false
    
    // MARK: - Variables
    
    var listServiceGroups = [ModelServiceGroup]()
    var filteredServiceGroups = [ModelServiceGroup]()
    
    var itemInfo: IndicatorInfo = "View"
    
    weak var controller: PagerTabViewController?
    
    var searchActive: Bool = false
        
    var searchKeyword: String = "" {
        didSet {
            filteredServiceGroups.removeAll()
            var service: ModelServiceItem
            for i in 0..<listServiceGroups.count {
                let emptyModel: ModelServiceGroup = ModelHelper.createEmptyModel()
                filteredServiceGroups.append(emptyModel)
                filteredServiceGroups[i].groupName = listServiceGroups[i].groupName
                filteredServiceGroups[i].groupCode = listServiceGroups[i].groupCode
                var count = 0
                for j in 0..<listServiceGroups[i].services.count {
                    service = listServiceGroups[i].services[j]
                    let name = service.name
                    let range = name.range(of: searchKeyword, options: NSString.CompareOptions.caseInsensitive)
                    if range != nil {
                        filteredServiceGroups[i].services.append(service)
                        count += 1
                    }
                }
            }
            
            filteredServiceGroups = filteredServiceGroups.filter { $0.services.count > 0 }
            
            if searchKeyword == "" {
                searchActive = false
            } else {
                searchActive = true
            }
            setupPullRefreshForScrollable(tableView, enable: !searchActive)
            self.tableView.reloadData()
            for i in 0..<tableView.numberOfSections {
                for j in 0..<tableView.numberOfRows(inSection: i) {
                    let cell = tableView.cellForRow(at: IndexPath.init(row: j, section: i)) as? ServiceRow
                    cell?.collectionView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false
        setSearchType(.local)
        setupPullRefreshForScrollable(tableView, enable: true)
        let tapRecognizer = UITapGestureRecognizer(target: controller, action: #selector(controller?.dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
        
        if listServiceGroups.count > 0, !isGotoSubScreen { scrollUpWhenAppear(tableView) }
        isGotoSubScreen = false
    }
    
    override func refreshData() {
        getData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    private func getData() {
        controller?.getRightBarButton()?.isEnabled = false
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        requestList(Client.WS_GET_SERVICES, params: nil, complete: {(success: Bool, data: [ModelServiceGroup]?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil && data!.count > 0) {
                self.listServiceGroups = data!
                self.resetUI()
//                self.hideAlertOnView(self.tableView)
                self.controller?.getRightBarButton()?.isEnabled = true
            } else {
//                self.showAlertOnView(self.tableView, NSLocalizedString("Msg_DataNotFound", comment: ""))
                self.controller?.getRightBarButton()?.isEnabled = false
            }
        })
    }
    
    func resetUI() {
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: true)
        tableView.reloadData()
        for i in 0..<tableView.numberOfSections {
            for j in 0..<tableView.numberOfRows(inSection: i) {
                let cell = tableView.cellForRow(at: IndexPath.init(row: j, section: i)) as? ServiceRow
                cell?.collectionView.reloadData()
            }
        }
    }
    
    // MARK: - Table handle
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var number = 0
        if searchActive {
            number = filteredServiceGroups.count
        } else {
            number = listServiceGroups.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let model: ModelServiceGroup = getModelAtIndex(section)
        let headerView = tableView.dequeueReusableCell(withIdentifier: "customHeader") as! CustomServiceHeader
        headerView.backgroundColor = Const.COLOR_GRAY_FOR_BACKGROUND
        headerView.headerLabel.text = model.groupName
        headerView.onSeeAll = {
            self.isGotoSubScreen = true
            let seeAllServicesVC = UIStoryboard.getViewController(storyBoardName: "TabServices", viewControllerName: "seeAllServices") as! SeeAllServicesViewController
            seeAllServicesVC.serviceGroup = model
            if self.searchActive {
                self.controller?.onCancelSearch()
            }
            self.navigationController?.pushViewController(seeAllServicesVC, animated: true)
        }
        return headerView
    }
    
    func getModelAtIndex(_ index: Int) -> ModelServiceGroup {
        if searchActive {
            return filteredServiceGroups[index]
        } else {
            return listServiceGroups[index]
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceRow", for: indexPath) as! ServiceRow
        cell.delegate = self
        cell.currentSection = indexPath.section
        cell.data = getModelAtIndex(indexPath.section).services
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !tableView.visibleCells.contains(cell), let cell = cell as? ServiceRow {
            cell.collectionView.reloadData()
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func getCurrentDisplayServicesGroup(_ section: Int) -> ModelServiceGroup {
        return searchActive ? filteredServiceGroups[section] : listServiceGroups[section]
    }
    
    func didSelectItemAt(_ section: Int, row: Int) {
        isGotoSubScreen = true
    
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        let group = getCurrentDisplayServicesGroup(section)
        vc.headerTitleString = group.groupName
        vc.listServices = group.services
        vc.regStt = .ChuaDangKy
        vc.setPageCount(vc.listServices.count, startPosition: row)
        if searchActive {
            self.controller?.onCancelSearch()
        }
        goNextHideTabbar(vc)
    }
    
    func currentTabChanged(_ tabIndex:Int) {
        isGotoSubScreen = true
    }
    
    
}














