//
//  MoreViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class MoreViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables and outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPhoneNo: UILabel!
    @IBOutlet weak var imvMemberShip: UIImageView!

    var moreFunctionStore: MoreFunctionStore!
    var isGotoSubScreen: Bool = false
    
    // MARK: - Segue
    
    @IBAction func toPersonalInfo(_ sender: UIButton) {
        let personalInfoVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ThongTinCaNhan", viewControllerName: "personalInfoVC") as! PersonalInfoViewController
        self.navigationController?.pushViewController(personalInfoVC, animated: true)
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moreFunctionStore = MoreFunctionStore()
        moreFunctionStore.initFunctions()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        self.automaticallyAdjustsScrollViewInsets = false
        avatarView.layer.cornerRadius = avatarView.frame.size.height / 2
        avatarView.layer.masksToBounds = true
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        GAHelper.logGA(category: ScreenNames.More.rawValue, actionGA: Action.View.rawValue, labelGA: nil, valueGA: nil)
        setAvatarInView(avatarView)
        
        if !isGotoSubScreen  { scrollUpWhenAppear(tableView) }
        isGotoSubScreen = false
    }
    
    
    private func setupUI() {
        avatarView.image = UIImage.init(named: "avatar_default")
        lbName.text = client.subMainInfo?.name
        lbPhoneNo.text = client.getIsdn()
        imvMemberShip.isHidden = true
    }
    
    // MARK: - Table handle
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if section == 0 || section == 1 {
            number = moreFunctionStore.allMoreFunctions[section].count
        } else {
            number = 1
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! MoreCell
            let moreFunction = moreFunctionStore.allMoreFunctions[indexPath.section][indexPath.row]
            cell.icon.image = moreFunction.icon
            cell.nameLabel.text = moreFunction.name
            return cell
        }else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! MoreCell
            let moreFunction = moreFunctionStore.allMoreFunctions[indexPath.section][indexPath.row]

            if L102Language.currentAppleLanguage() == Const.LANG_LOCAL {
                if(indexPath.row == 0){
                    cell.icon.image = moreFunction.icon
                    cell.nameLabel.text = "Change to English language"
                }else{
                    cell.icon.image = moreFunction.icon
                    cell.nameLabel.text = moreFunction.name
                }
            } else if L102Language.currentAppleLanguage() == Const.LANG_EN {
                if(indexPath.row == 0){
                    cell.icon.image = moreFunction.icon
                    cell.nameLabel.text = "ប្ដូរទៅភាសា អង់គ្លេស"
                }else{
                    cell.icon.image = moreFunction.icon
                    cell.nameLabel.text = moreFunction.name
                }
            }
            
            return cell
            //lifesup_sprint_2_T8_2018_end
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footerCell", for: indexPath) as! MoreFooterCell
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsetsMake(0, UIScreen.main.bounds.width, 0, 0)
            cell.versionLabel.text = NSLocalizedString("More_VersionLabel", comment: "") + " " + String(Const.APP_VERSION)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 15
        }
        return 0		
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return 90.0
        }
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        isGotoSubScreen = true
        
        guard indexPath.section < 2 else {
            return
        }
        let function = moreFunctionStore.allMoreFunctions[indexPath.section][indexPath.row]
        switch function.value {
        case .KhuyenMai:
            segueToPromotionScreen()
            break
        case .ChamSocKhachHang:
            let customerServicesVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ChamSocKhachHang", viewControllerName: "customerServicesVC")
            self.navigationController?.pushViewController(customerServicesVC, animated: true)
            break
            
        case .KhachHangThanThiet:
            let loyaltySchemeVC = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "loyaltySchemeVC")
            self.navigationController?.pushViewController(loyaltySchemeVC, animated: true)
            break
            
        case .TinTuc:
            segueToListNews()
            break
        case .TraiNghiem:
            let experiencesVC = UIStoryboard.getViewController(storyBoardName: "TabMore_TraiNghiem3G:4G", viewControllerName: "experiencesVC")
            self.navigationController?.pushViewController(experiencesVC, animated: true)
            break
            
        case .DanhGia:
            self.showAlert(title: NSLocalizedString("More_DanhGiaSanPhamLabel_Title", comment: ""),
                           message: NSLocalizedString("More_DanhGiaSanPhamLabel_Content", comment: ""),
                           cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                           agreeLabel: NSLocalizedString("More_DanhGiaSanPhamButton_OK", comment: ""),
                           onOK: {
                            UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/us/app/mmetfone/id1321717513?mt=8")! as URL)
                            GAHelper.logGA(category: ScreenNames.RatingApp.rawValue,
                                           actionGA: Action.View.rawValue,
                                           labelGA: nil,
                                           valueGA: nil)
            },
                           onCancel: {})
            break
            
        case .DoiNgonNgu:
            var languageToChange = ""
            var isLanguageLocal: Bool = false
            if L102Language.currentAppleLanguage() == Const.LANG_LOCAL {
                languageToChange = NSLocalizedString("Language_English", comment: "")
                isLanguageLocal = true
            } else if L102Language.currentAppleLanguage() == Const.LANG_EN {
                languageToChange = NSLocalizedString("Language_Local", comment: "")
                isLanguageLocal = false
            }
            
            let message = String(format: NSLocalizedString("More_DoiNgonNguLabel_Message", comment: ""), languageToChange)
            showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                      message: message,
                      cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                      agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                      onOK: {
                        let lang:String = isLanguageLocal ?  Const.LANG_EN : Const.LANG_LOCAL
                        self.changeLanguage(lang, reloadScreen: {
                            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                            let tabBarController:UITabBarController  = UIStoryboard.getViewController(storyBoardName: "Main", viewControllerName: "rootTabBar") as! UITabBarController
                            tabBarController.selectedIndex = 4
                            rootviewcontroller.rootViewController = tabBarController
                        })
            })
            break
            
        case .DoiMatKhau:
            let changePasswordVC = UIStoryboard.getViewController(storyBoardName: "TabMore_ChangePassword", viewControllerName: "changePasswordVC")
            self.navigationController?.pushViewController(changePasswordVC, animated: true)
            break
            
        case .DangXuat:
            showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                      message: NSLocalizedString("More_DangXuatLabel_Message", comment: ""),
                      cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                      agreeLabel: NSLocalizedString("LabeL_DongY", comment: ""),
                      onOK: {
                        self.logout()
            })
            break
        }
        
    }
    
}






