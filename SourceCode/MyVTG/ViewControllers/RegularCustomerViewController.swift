//
//  RegularCustomerViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/23/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class RegularCustomerViewController: BaseVC {
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rankingPointLabel: UILabel!
    @IBOutlet weak var exchangedPointLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.LoyaltyScheme.rawValue
        actionGA = Action.View.rawValue
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet01", comment: ""))
        self.automaticallyAdjustsScrollViewInsets = false
        tabBarController?.tabBar.isHidden = true
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
    
    override func onBtnRightNavBar() {
        let vc = UIStoryboard.getViewController(storyBoardName: "DetailScreen", viewControllerName: "detailScreen") as! DetailViewController
        vc.navTitle = NSLocalizedString("ScreenTitle_KhachHangThanThiet02", comment: "")
        vc.coverImage = #imageLiteral(resourceName: "chuyen_tien_image")
        vc.name = NSLocalizedString("MoreLabel_KhachHangThanThiet", comment: "")
        vc.content = loremIpsum
        vc.isButtonEnabled = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func redeemForCredit(_ sender: UIButton) {
        GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
    }
    
    @IBAction func redeemForData(_ sender: UIButton) {
        GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
    }
    
    @IBAction func redeemForGifts(_ sender: UIButton) {
        GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
    }
    
    @IBAction func chooseBirthdayGifts(_ sender: UIButton) {
        GAHelper.logGA(category: screenName!, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
    }
}









