//
//  AccountInfoVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class AccountInfoVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    
    var accountsDetail: [ModelAccountDetail] = []
    var userSevices : [ModelSubServices] = []
    
    var isAccountsDetailLoading: Bool = false
    var isUserServiceLoading : Bool = false
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        screenName = ScreenNames.AccountInfo.rawValue
        actionGA = Action.View.rawValue
        tabBarController?.tabBar.isHidden = false
        setupNavBar(title: NSLocalizedString("ScreenTitle_ThongTinTaiKhoan01", comment: ""))
        setupPullRefreshForScrollable(tableView, enable: true)
        getdata()
    }
    
    // MARK: - API request handle
    
    func getdata() {
        getAccountsDetail()
        //getCurrentUsedSubServices()
    }
    
    func getAccountsDetail() {
        self.isAccountsDetailLoading = true
        let subType: SubType = client.isPrePaid() ? SubType.Prepay : SubType.Postpaid
        let params = ["subType": subType.rawValue]
        requestList(Client.WS_GET_ACCOUNTS_DETAIL, params: params, complete: {(success: Bool, data: [ModelAccountDetail]?) -> Void in
            self.isAccountsDetailLoading = false
            self.hideRefreshIndicator()
            if (success && data != nil) {
                self.accountsDetail = data!
                self.tableView.reloadData()
            }
        })
    }
    
    func getCurrentUsedSubServices(){
        isUserServiceLoading = true
        requestList(Client.WS_GET_CURRENT_USED_SUB_SERVICES, params: nil, complete: {(success: Bool, data: [ModelSubServices]?) -> Void in
            self.isUserServiceLoading = false
            self.hideRefreshIndicator()
            if (success && data != nil) {
                self.userSevices = data!
                self.tableView.reloadData()
            }
        })
    }
    
    func hideRefreshIndicator() {
        if  (isAccountsDetailLoading || isUserServiceLoading) {
            return
        }
        refreshDataDone()
    }
    
    override func refreshData() {
        getdata()
    }
    
    // MARK: - Table handle
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountsDetail.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section < accountsDetail.count) {
            return accountsDetail[section].values.count
        } else {
            return userSevices.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //lifesup_sprint_2_T8_2018_start
        let section = indexPath.section
        let valueDetail = self.accountsDetail[indexPath.section].values[indexPath.row]
        if section == 1 {
            let str = valueDetail.value
//            let lastChar = str.last!

            if str.range(of:"$") != nil{
                var strValue = ""
                
                if str != "" {
                    strValue = str
                }else{
                    strValue = "0$"
                }
                
                strValue.remove(at: strValue.index(before: strValue.endIndex))
                //                let endIndex = str.index(str.endIndex, offsetBy: -2)
                //                let truncated = str.substring(to: endIndex)
                let convertStrValue = Double(strValue)
                
                let y: Double = 0.0
                if convertStrValue! <= y{
                    return 0.0
                }else if Int(self.accountsDetail[indexPath.section].values[0].value) == 0 {
                    return UITableViewAutomaticDimension
                }else{
                    return UITableViewAutomaticDimension
                }

            }else if str.range(of:"MB") != nil{
                var strValue = ""
                
                if str != "" {
                    strValue = str
                }else{
                    strValue = "0MB"
                }
                
                let endIndex = strValue.index(strValue.endIndex, offsetBy: -2)
                let truncated = strValue.substring(to: endIndex)
                
                
                let convertStrValue = Double(truncated)
                print("convertStrValue111 \(convertStrValue)")
                let y: Double = 0.0
                
                if convertStrValue == nil  {
                    return 0.0
                }else if convertStrValue! <= y  {
                    return 0.0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else if str.range(of:"Minute") != nil{
                var strValue = ""
                
                if str != "" {
                    strValue = str
                }else{
                    strValue = "0Minute"
                }
                
                let endIndex = strValue.index(strValue.endIndex, offsetBy: -6)
                let truncated = strValue.substring(to: endIndex)
                
                
                let convertStrValue = Double(truncated)
                
                let y: Double = 0.0
                
                if convertStrValue == nil  {
                    return 0.0
                }else if convertStrValue! <= y  {
                    return 0.0
                }else{
                    return UITableViewAutomaticDimension
                }
                
            }else{
                return UITableViewAutomaticDimension
            }
        }
        //lifesup_sprint_2_T8_2018_end
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section < accountsDetail.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VoiceBalanceCell")as! VoiceBalanceCell
            let valueDetail = self.accountsDetail[indexPath.section].values[indexPath.row]
            bindVoiceBalanceCell(cell, data: valueDetail)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell") as! SubServiceCell
            let service: ModelSubServices = self.userSevices[indexPath.row]
            cell.nameLabel.text = service.name.characters.count == 0 ? " " : service.name
            cell.descLabel.text = service.des.characters.count == 0 ? " " : service.des
            Util.setImage(cell.iconImage, url: service.iconUrl, defaultImageName: "ic_service_default")
            return cell
        }
    }
    
    private func bindVoiceBalanceCell(_ cell: VoiceBalanceCell, data: ModelAccountValueDetail) {
        if data.title.characters.count > 0 {
            cell.nameLabel.text = data.title
        }
        //lifesup_sprint_2_T8_2018_start
        let str = data.value
        if str.range(of:"$") != nil{
            var strValue = str
            
            strValue.remove(at: strValue.index(before: strValue.endIndex))

            let convertStrValue = Double(strValue)
 
            let y: Double = 1.0
            if convertStrValue! < y{
                cell.valueLabel.text = "0" + data.value
            }else{
                cell.valueLabel.text = data.value
            }
        }else{
            cell.valueLabel.text = data.value
        }
        //lifesup_sprint_2_T8_2018_end
        
        
        if (data.exp == "") {
            cell.descLable.isHidden = true
            cell.heightOfDescContrain.constant = 0.0
        } else {
            cell.descLable.isHidden = false
            cell.heightOfDescContrain.constant = 30.0
            cell.descLable.text = data.exp
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UINib(nibName: "TableSectionHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TableSectionHeaderView
        if (section < accountsDetail.count) {
            let accountDetail = accountsDetail[section]
            if (accountDetail.values.count > 0) {
                //lifesup_sprint_2_T8_2018_start
                if section == 2 || accountDetail.title == "PRODUCTS" {
                    headerView.label.text = ""
                }else{
                    let headerLabel: String = accountDetail.title
                    headerView.label.text = headerLabel
                }
                return headerView
                //lifesup_sprint_2_T8_2018_end
            } else {
                return UIView()
            }
        } else {
            if (userSevices.count > 0) {
                let headerLabel: String = NSLocalizedString("Home_ThongTinTaiKhoanLabel_DichVuDangSuDung", comment: "")
                headerView.label.text = headerLabel
                return headerView
            } else {
                return UIView()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UINib(nibName: "AccountInfoFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AccountInfoFooterView
        footerView.tag = 900 + section
        var buttonLable: String = ""
        if section < accountsDetail.count {
            return UIView()
        } else {
            if (userSevices.count > 0) {
                buttonLable = NSLocalizedString("Home_ThongTinTaiKhoanButton_DangKyThem", comment: "")
            } else {
                buttonLable = ""
            }
        }
        
        if (buttonLable != "") {
            footerView.button.setTitle( buttonLable, for: UIControlState.normal)
            footerView.button.tag = section
            footerView.button.addTarget(self, action: #selector(AccountInfoVC.buttonTaped(_:)), for: .touchUpInside)
        } else {
            return UIView()
        }
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerHeight: CGFloat = 40.0
        let minHeight: CGFloat = CGFloat.leastNonzeroMagnitude

        if section < accountsDetail.count {
            let accountDetail = accountsDetail[section]
            if section == 2 || accountDetail.title == "PRODUCTS" {
                return minHeight
            }else{
                let isShowing = accountDetail.title.count > 0 && accountDetail.values.count > 0
                return isShowing ? headerHeight : minHeight
            }
        } else {
            return userSevices.count > 0 ? headerHeight : minHeight
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let footerHeight: CGFloat = 60.0
        let minHeight: CGFloat = CGFloat.leastNonzeroMagnitude
        if section < accountsDetail.count {
            return minHeight
        } else {
            return userSevices.count > 0 ? footerHeight : minHeight
        }
    }
    
    // MARK: - Actions handle
    
    @objc func buttonTaped(_ sender: UIButtonRegular01) {
        if sender.tag < accountsDetail.count {
            segueToRecharge(showsTabBar: true)
            return
        } else {
            subscribeMore(sender)
            return
        }
    }
    
    func subscribeMore(_ sender: UIButtonRegular01) {
        self.tabBarController?.selectedIndex = 2
    }
}










