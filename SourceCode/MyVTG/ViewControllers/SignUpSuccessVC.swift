//
//  SignUpSuccessVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/28/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class SignUpSuccessVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_DangKy01", comment: ""))
        self.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSignInButtonTaped(_ sender: Any) {
        goLogin()
    }
    
    override func getNavBarLeftImageName() -> String {
        return ""
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
