//
//  GiftListViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class GiftListViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets and variables
    
    @IBOutlet weak var cityLabel: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var city: City?
    var giftList: [Gift]?
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhachHangThanThiet06", comment: ""))
        cityLabel.setTitle(city?.name, for: .normal)
        giftList = (city?.giftList)!
        tableView.tableFooterView = UIView()
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_info"
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gift", for: indexPath) as! GiftCell
        let gift = giftList?[indexPath.row]
        cell.nameLabel.text = gift?.name
        cell.pointValueLabel.text = "\(gift?.pointValue ?? 0) \(NSLocalizedString("More_KhachHangThanThietLabel_Diem", comment: ""))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.getViewController(storyBoardName: "TabMore", viewControllerName: "giftInfoVC") as! GiftInformationViewController
        vc.city = city
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
