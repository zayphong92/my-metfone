//
//  DlgLoading.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DlgLoading: UIViewController {
    @IBOutlet weak var imv: UIImageView!
    let listImageNames = [
        "ic_loading_01",
        "ic_loading_02",
        "ic_loading_03",
        "ic_loading_04",
        "ic_loading_05",
        "ic_loading_06",
        "ic_loading_07",
        "ic_loading_08",
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        var listImages: [UIImage] = []
        for imageName in listImageNames {
            listImages.append(UIImage.init(named: imageName)!)
        }
        imv.animationImages = listImages
        imv.animationDuration = 0.3
        imv.animationRepeatCount = 0
        imv.startAnimating()
    }

}
