//
//  Login01.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

class Login01: BaseVC, InputOTPDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var inputPhoneNo: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    @IBOutlet weak var imvLoading: UIImageView!
    @IBOutlet weak var btnGetOTP: UIView!
    @IBOutlet weak var captchaView: UIView!
    @IBOutlet weak var inputCaptcha: UITextFieldRegular01!
    @IBOutlet weak var captchaImage: UIImageView!
    @IBOutlet weak var captchaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPwd: UIButton!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var languageSwitch: UILanguageSwitch!
    @IBOutlet weak var leftPaddingConstraint: NSLayoutConstraint!
    
    
    // MARK: - Variables
    var userAccount: UserAccount!
    
    private let SIM_TEST_6_TRA_TRUOC: String = "2095366809";
    private let SIM_TEST_9_TRA_TRUOC: String = "2092173400";
    private let SIM_TEST_10_TRA_TRUOC: String = "2092173401";
    private let SIM_LAY_TU_API_CONFIG_FILE: String = "2091600152";
    
    //2091600152 - tra sau
    //2097093258 - tra truoc
    
    /* Added by Hà 12/7 - SIM này em lấy từ trên file API để làm
    vì 1 số thông tin mấy SIM kia không request được nhưng SIM này lại request được.*/
    
    private let test_password: String = "123456a@";
    private var inputOTPVC:InputOTPVC? = nil
    
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        baseScrollView = scrollView
        userAccount = UserAccount()
        userAccount.fakeData()
        self.automaticallyAdjustsScrollViewInsets = false
        setupTextFields()
        
    }
    
    func setupTextFields() {
        inputPhoneNo.placeholder = Const.PHONE_NUMBER_PLACEHOLDER
//        inputPhoneNo.title = NSLocalizedString("Label_SoDienThoai", comment: "")
//        inputPhoneNo.setTitleVisible(true)
//        inputPassword.title = NSLocalizedString("LoginLabel_MatKhau", comment: "")
//        inputPassword.setTitleVisible(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - UI handle
    
    private func fakeDataToTest() {
        inputPhoneNo.text = SIM_LAY_TU_API_CONFIG_FILE
        inputPassword.text = test_password
    }
    
    func setupView() {
        //Util.setGradientLayer(view: btnGetOTP, startColor: Const.COLOR_START, endColor: Const.COLOR_END)
//        inputPassword.rightView = btnForgotPwd
//        inputPassword.rightViewMode = .always;
//        underlineLabel(signUpLabel, withText: NSLocalizedString("LoginLabel_DangKy", comment: ""))
//        languageSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.languageSwitch.IsAnimation = false
        self.languageSwitch.TitleOne = Const.LAOS_LANGUAGE_CODE
        self.languageSwitch.TitleTwo = Const.ENGLISH_LANGUAGE_CODE
        
        self.languageSwitch.ValueOne = Const.ENGLISH_LANGUAGE
        self.languageSwitch.ValueTwo = Const.LAOS_LANGUAGE
        languageSwitch.IsOpen = L102Language.currentAppleLanguage() == Const.LANG_EN
        self.languageSwitch.IsAnimation = true
        
//        if ScreenSize.WIDTH > ScreenSize.WIDTH_IPHONE6P {
//            leftPaddingConstraint.constant = 100
//        } else {
//            leftPaddingConstraint.constant = 40
//        }
        
        self.languageSwitch.ValueChanged = { value in

            let lang: String = self.languageSwitch.IsOpen ? Const.LANG_EN : Const.LANG_LOCAL
            
            self.changeLanguage(lang) {
                self.goLogin()
            }
        }
        
        if let lastPhoneNo: Any = UserDefaults.standard.object(forKey: Const.LAST_ACCOUNT_LOGIN_KEY){
            inputPhoneNo.text =  lastPhoneNo as? String
        }
    }
    
//    func captchaViewHidden() {
//        captchaViewHeight.constant = 0
//        captchaView.isHidden = true
//    }
//
//    func captchaViewShown() {
//        captchaViewHeight.constant = 80
//        captchaView.isHidden = false
//    }
    @IBAction func loginTapped(_ sender: UIButton) {
        if (warningRequireTextField(inputPhoneNo, msg: NSLocalizedString("Msg_Pls_Enter_PhoneNo", comment: ""))) {
            return
        }
        if (warningRequireTextField(inputPassword, msg: NSLocalizedString("Msg_Pls_Enter_Password", comment: ""))) {
            return
        }
        let isdn = inputPhoneNo.text!
        let pwd = inputPassword.text!
        
        showProgress()
        client.login(isdn: isdn, password: pwd, complete: {(resp: DataResponse<Any>) -> Void in
            self.hideProgress()
            if (self.handleApiError(resp, apiCode: "UserLogin")) {
                if self.isLostConnectionToServer(resp) {
                    self.showError(NSLocalizedString("Msg_LostConnectionToServer", comment: ""))
                }
                return
            }
            
            guard let modelUser: ModelUserLogin = ModelHelper.from(response: resp) else {
                return
            }
            ModelHelper.saveToCache(modelUser)
            self.client.userLogin = modelUser
            self.goHome()
            
            UserDefaults.standard.set(isdn, forKey: Const.LAST_ACCOUNT_LOGIN_KEY)
        })
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction func onForgotPasswordTaped(_ sender: Any) {
        doForgotPassword(otp: nil,  complete: { (success: Bool, data:ModelDefault?) -> Void in
            if(!success) {return}
            
            self.inputOTPVC = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "InputOTPVC") as? InputOTPVC
            self.inputOTPVC?.delegate = self
            self.inputOTPVC?.serviceType = .ForgotPassword
            self.navigationController?.pushViewController(self.inputOTPVC!, animated: true)
        })
    }
    
    func doForgotPassword(otp: String?, isShowProgress: Bool = true, complete: @escaping (Bool, ModelDefault?) -> ()) {
        let username:String = inputPhoneNo.text!
        let params : [String: Any] = [
            "username": username,
            "otp": otp ?? ""
        ]
        requestAction02(Client.USER_FORGOT_PASSWORD, params: params, isShowProgress: isShowProgress, actionComplete: complete)
    }
    
    //Callback function from inputOTPVC
    func inputOTPDone(otp: String) {
        let inputOTPType:ServiceType = self.inputOTPVC?.serviceType ?? .None
        
        switch inputOTPType {
        case .ForgotPassword:
            doForgotPassword(otp: otp, complete:  {(seccess: Bool, data: ModelDefault?) -> Void in
                if(!seccess) { return }
                self.navigationController?.popViewController(animated: true)
            })
            break
            
        case .GetOTP:
            loginWithOtp(otp: otp, isdn: inputPhoneNo.text!)
            break
            
        default:
            break
        }
    }
    
    //Callback function from inputOTPVC
    func resendOTP() {
        let inputOTPType:ServiceType = self.inputOTPVC?.serviceType ?? .None
        
        switch inputOTPType {
        case .ForgotPassword:
            doForgotPassword(otp: nil, isShowProgress: false, complete: {(seccess: Bool, data: ModelDefault?) -> Void in
                if(self.inputOTPVC != nil) {
                    self.inputOTPVC?.resentOtpDone()
                }
            })
            break
            
        case .GetOTP:
            doGetOtp { (success:Bool) in
                if(self.inputOTPVC != nil) {
                    self.inputOTPVC?.resentOtpDone()
                }
            }
            break
            
        default:
            break
        }
    }
    
    @IBAction func getOtpTapped(_ sender: UIButton) {
        if (warningRequireTextField(inputPhoneNo, msg: NSLocalizedString("Msg_Pls_Enter_PhoneNo", comment: ""))) {
            return
        }
        
        btnGetOTP.isHidden = true
        imvLoading.startProgress()
        doGetOtp {(success:Bool) in
            self.btnGetOTP.isHidden = false
            self.imvLoading.stopProgress()
            if !success { return }
            
            self.inputOTPVC = UIStoryboard.getViewController(storyBoardName: "Login", viewControllerName: "InputOTPVC") as? InputOTPVC
            self.inputOTPVC?.delegate = self
            self.inputOTPVC?.serviceType = .GetOTP
            self.navigationController?.pushViewController(self.inputOTPVC!, animated: true)
        }
    }
    
    func doGetOtp( complete :@escaping(_ success:Bool) -> ()) {
        getOtp(isdn: inputPhoneNo.text!, complete: complete)
    }
    
    
    func autoLogin() {
        //Test
        let isdn:String = "2097398139"        
        showProgress()
        client.autoLogin(isdn:isdn, complete: {(resp: DataResponse<Any>) -> Void in
            self.hideProgress()
            if (self.handleApiError(resp, apiCode: "UserLogin")) {
                return
            }
            
            guard let modelUser: ModelUserLogin = ModelHelper.from(response: resp) else {
                return
            }
            ModelHelper.saveToCache(modelUser)
            self.client.userLogin = modelUser
            self.goHome()
        })
    }
    
//    @IBAction func onLanguageSwitchChanged(_ sender: Any) {
//        let switcher: UISwitch = sender as! UISwitch
//        let lang: String = switcher.isOn ? Const.LANG_EN : Const.LANG_LOCAL
//
//        self.changeLanguage(lang) {
//            goLogin()
//        }
//    }
}
