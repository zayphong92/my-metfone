//
//  CellServiceDetail01.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CellServiceDetail01: UITableViewCell {
    @IBOutlet weak var imvCover: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbWebsite: UILabel!
    @IBOutlet weak var lbWebUrl: UILabel!
    @IBOutlet weak var webUrlHeightConstraint: NSLayoutConstraint!
    //lifesup_sprint_2_T8_2018_start
    var url = ""
    //lifesup_sprint_2_T8_2018_end
    var webUrlWidthOriginal: CGFloat = 0
    private var serviceDetail: ModelServiceDetail? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        lbName.textColor = Const.COLOR_MAIN
        lbWebUrl.textColor = Const.COLOR_MAIN
        lbWebsite.textColor = Const.COLOR_TEXT_DLG_COLOR_01
        lbName.text = ""
        lbName.numberOfLines = 1
        lbWebUrl.text = ""
        lbWebUrl.numberOfLines = 2
        lbWebUrl.lineBreakMode = .byWordWrapping
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        adjustWebUrlHeight()

    }
    
    func onWebLinkClick(_ sender: UITapGestureRecognizer) {
        Util.openWebUrl(serviceDetail!.webLink)
    }
    
    func setupUI(_ model: ModelServiceDetail) {
        serviceDetail = model
        Util.setImage(imvCover, url: model.imgDesUrl)
        lbName.text = model.name
        lbWebUrl.text = model.webLink
        lbWebUrl.underline(withText: model.webLink)
        //lifesup_sprint_2_T8_2018_start
        url = model.webLink
        //lifesup_sprint_2_T8_2018_end
        let tap = UITapGestureRecognizer(target: self, action: #selector(onWebLinkClick))
        lbWebUrl.isUserInteractionEnabled = true
        lbWebUrl.addGestureRecognizer(tap)
    }
    
    //lifesup_sprint_2_T8_2018_start
    @IBAction func websiteLink(_ sender: Any) {
        if let urlLink = NSURL(string: url){
            UIApplication.shared.openURL(urlLink as URL)
        }
    }
    //lifesup_sprint_2_T8_2018_end
    private func adjustWebUrlHeight() {
        if (webUrlWidthOriginal == 0) {
            webUrlWidthOriginal = lbWebUrl.frame.size.width
        }
        let size = lbWebUrl.sizeThatFits(CGSize(webUrlWidthOriginal, CGFloat.greatestFiniteMagnitude))
        webUrlHeightConstraint.constant = size.height
        lbWebUrl.needsUpdateConstraints()
        lbWebUrl.layoutIfNeeded()
    }
    
}
