//
//  BuyPhoneNumberVC.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class BuyPhoneNumberVC: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var storeView: UIView!
    @IBOutlet weak var appView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var isdnTextField: UITextFieldRegular01!
    @IBOutlet weak var serialTextField: UITextFieldRegular01!
    @IBOutlet weak var purchaseBtn: UIButtonRegular01!
    
    // MARK: - Variables
    
    var phoneNumber: ModelIsdnToBuy?
    var serviceFee: Double = 0
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseScrollView = scrollView
        isdnTextField.text = phoneNumber?.isdn
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: "\(NSLocalizedString("ScreenTitle_MuaSo03", comment: "")) \(phoneNumber?.isdn ?? "")")
    }
    
    // MARK: - Actions handle
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func registerForPurchasing(_ sender: UIButtonRegular01) {
        let messange = String(format: NSLocalizedString("Utilities_BuyIsdn_Message02", comment: ""), "\(phoneNumber?.isdn ?? "")")
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: messange,
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: {
                    self.registerBuyIsdn()
        })
    }
    
    func registerBuyIsdn() {
        if phoneNumber != nil {
            let params = [ "isdnToBuy": phoneNumber!.isdn]
            requestAction(Client.WS_REGISTER_BUY_ISDN, params: params, actionComplete: { (success:Bool, data:ModelDoAction?) in
                if success {
                    self.showSuccess(NSLocalizedString("Utilities_BuyIsdn_Message03", comment: ""))
                } else {
                    let message = String.init(format: NSLocalizedString("Utilities_BuyIsdn_Message04", comment: ""), self.phoneNumber!.isdn)
                    self.showError(message)
                }
            })
        }
    }
    
    @IBAction func purchaseTapped(_ sender: UIButtonRegular01) {
        
        let validatePhoneNumber = ValidateUtil.validate(phoneNumber: isdnTextField.text!)
        if !validatePhoneNumber.iscorrect {
            showError(validatePhoneNumber.msg!)
            return
        }
        
        let validateOTP = ValidateUtil.validate(otp: serialTextField.text!)
        if !validateOTP.isCorrect {
            showError(validateOTP.msg!)
            return
        }
        
        let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "PaymentBillVC") as! PaymentBillVC
        vc.serviceType = .BuyPhoneNumber
        vc.phoneNumber = phoneNumber
        vc.serviceFee = serviceFee
        vc.isdnOfKit = isdnTextField.text
        vc.serialOfKit = serialTextField.text
        goNext(vc)
    }

    @IBAction func onQRCodeButtonTaped(_ sender: Any) {
        presentQRScannerController(withResultFilledIn: serialTextField)
    }
    
    @IBAction func textFieldChanged(_ sender: Any) {
//        purchaseBtn.isEnabled = (isdnTextField.text?.characters.count)! > 0 && (serialTextField.text?.characters.count)! > 0
    }
}













