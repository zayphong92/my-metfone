//
//  PromotionViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/22/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class PromotionViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    private var filteredPromotions: [ModelPromotion] = []
    var searchActive = false
    private var listPromotions: [ModelPromotion] = []
    private var currentPage: Int = 1
    private var needLoadMore: Bool = true
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.Promotions.rawValue
        actionGA = Action.View.rawValue
        self.automaticallyAdjustsScrollViewInsets = false
        setSearchType(.local)
        setupPullRefreshForScrollable(tableView, enable: true)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_KhuyenMaiChoBan01", comment: ""))
        getData(complete: {(success: Bool, data: [ModelPromotion]?) -> Void in
            if (success && data != nil && data!.count > 0) {
                self.getDataDone(data!)
                self.getRightBarButton()?.isEnabled = true
            } else {
                let message:String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.tableView, message)
                self.getRightBarButton()?.isEnabled = false
            }
        })
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        view.endEditing(true)
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        
        enableLoadMore(needLoadMore, tableView: tableView)
        tableView.reloadData()
    }
    
    // MARK: - API request handle
    
    private var currentPageBackup: Int = 0
    private var needLoadMoreBackup: Bool = true
    private var listPromotionsBackup: [ModelPromotion] = []
    
    override func refreshData() {
        currentPageBackup = currentPage
        needLoadMoreBackup = needLoadMore
        listPromotionsBackup = listPromotions
        
        needLoadMore = true
        currentPage = 1
//        listPromotions = []
        getData(complete: {(success: Bool, data: [ModelPromotion]?) -> Void in
            if (!success) {
                // refresh lỗi thì giữ nguyên data như cũ
                self.currentPage = self.currentPageBackup
                self.needLoadMore = self.needLoadMoreBackup
                self.listPromotions = self.listPromotionsBackup
            } else {
                self.listPromotions = []
                self.getRightBarButton()?.isEnabled = true
                self.getDataDone(data!)
            }
        })
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getData(complete: @escaping(Bool, [ModelPromotion]?) -> ()) {
        if (!needLoadMore) {
            return
        }
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "pageSize": Client.PAGE_SIZE,
            "pageNum": currentPage
        ]
        
        requestList(Client.WS_GET_NEW_PROMOTIONS, params: params, complete: {(success: Bool, data: [ModelPromotion]?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            complete(success, data)
        })
    }
    
    func getDataDone(_ data: [ModelPromotion]) {
        currentPage += 1
        if (data.count < Client.PAGE_SIZE) {
            needLoadMore = false
        }
        listPromotions += data
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredPromotions.removeAll()
        var promotion: ModelPromotion
        for i in 0..<listPromotions.count {
            promotion = listPromotions[i]
            let name = promotion.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredPromotions.append(promotion)
            }
        }
        
        searchActive = keyword != ""
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        
        enableLoadMore(false, tableView: tableView)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchActive ? filteredPromotions.count : listPromotions.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let promotion: ModelPromotion = getModelAtIndex(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionCell", for: indexPath) as! PromotionCell
        
        Util.setImage(cell.iconImageView, url: promotion.iconUrl, defaultImageName: "ic_service_package")
        cell.nameLabel.text = promotion.name
        cell.shortDesLabel.text = promotion.shortDes
        
        switch promotion.actionType {
        case PromotionAction.Rechange.rawValue:
            cell.showButton()
            cell.actionButton.localizedText = "Button_NapCuoc"
            break
        case PromotionAction.Register.rawValue:
            cell.showButton()
            cell.actionButton.localizedText = "Services_DangKyButton"
            break
        default:
            cell.hideButton()
            break
        }
        cell.actionButton.tag = indexPath.row
        cell.actionButton.addTarget(self, action: #selector(doAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: PromotionDetailVC = CarouselVC.initViewController()
        vc.promotions = searchActive ? filteredPromotions : listPromotions
        let count: Int = searchActive ? filteredPromotions.count : listPromotions.count
        vc.setPageCount(count, startPosition: indexPath.row)
        goNext(vc)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func getModelAtIndex(_ index: Int) -> ModelPromotion {
        if (searchActive) {
            return filteredPromotions[index]
        }
        if (isRefreshing()) {
            return listPromotionsBackup[index]
        } else {
            return listPromotions[index]
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (searchActive || !needLoadMore) {
            return
        }
        
        if (indexPath.row == listPromotions.count - 1) {
            enableLoadMore(needLoadMore, tableView: tableView)
            getData(complete: {(success: Bool, data: [ModelPromotion]?) -> Void in
                if (success && data != nil) {
                    self.getDataDone(data!)
                }
            })
        }
    }
    
    func fakeData() -> [ModelPromotion] {
        var data:[ModelPromotion] = []
        
        for i: Int in 0...20 {
            let promotion: ModelPromotion = (Mapper<ModelPromotion>().map(JSONObject: [:]))!
            promotion.actionType = Int(arc4random_uniform(3))
            promotion.code  = "MI5-\(i)"
            promotion.iconUrl = ""
            promotion.name = "Promotion \(i)"
            promotion.shortDes = "This is Promotion: \(i)"
            promotion.publishDate = Date().timeIntervalSince1970.hashValue
            
            data.append(promotion)
        }
        return data
    }
    
    func doAction(_ sender:UIButton) {
        let promotion: ModelPromotion = getModelAtIndex(sender.tag)
        switch promotion.actionType {
        case PromotionAction.Rechange.rawValue:
            segueToRecharge(showsTabBar: false)
            break
            
        case PromotionAction.Register.rawValue:
            registerService(serviceCode: promotion.code)
            break
            
        default:
            break
        }
    }
    
}








