//
//  ContactViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import Contacts

class ContactViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    // MARK: - Variables
    
    var afterPopVC: () -> Void = {}
    var chosenContact: CNContact?
    var sectionContacts = [[CNContact]]()
    var filteredSectionContacts = [[CNContact]]()
    var searchActive: Bool = false
    let headers = [0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F", 6: "G", 7: "H", 8: "I", 9: "J", 10: "K", 11: "L", 12: "M", 13: "N", 14: "O", 15: "P", 16: "Q", 17: "R", 18: "S", 19: "T", 20: "U", 21: "V", 22: "W", 23: "X", 24: "Y", 25: "Z"]
    
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey ] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate,
                                                                        keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        return results
    }()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(title: NSLocalizedString("ScreenTitle_NapCuoc02", comment: ""))
        setSearchType(.local)
        setTextFieldAsInputSearchBar(searchField)
        for i in 0...25 {
            sectionContacts.append(namesStartWith(headers[i]!))
        }
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredSectionContacts.removeAll()
        var contact: CNContact
        for i in 0..<sectionContacts.count {
            filteredSectionContacts.append([CNContact]())
            for j in 0..<sectionContacts[i].count {
                contact = sectionContacts[i][j]
                let name = /* contact.namePrefix + */contact.givenName + contact.middleName + contact.familyName /*+ contact.previousFamilyName + contact.nameSuffix + contact.nickname + contact.organizationName + contact.departmentName*/
                let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
                if range != nil {
                    filteredSectionContacts[i].append(contact)
                }
            }
        }
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        self.tableView.reloadData()
    }
    
    func namesStartWith(_ character: String) -> [CNContact] {
        var filteredContacts = [CNContact]()
        for contact in contacts {
            if contact.familyName.hasPrefix(character) && contact.phoneNumbers.count != 0 {
                filteredContacts.append(contact)
            }
        }
        return filteredContacts
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var number = 0
        if searchActive {
            number = filteredSectionContacts.count
        } else {
            number = sectionContacts.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredSectionContacts[section].count
        } else {
            number = sectionContacts[section].count
        }
        return number
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        view.backgroundColor = Const.COLOR_GRAY_FOR_BACKGROUND
        let label = UILabel(frame: CGRect(x: 16, y: 10, width: self.view.frame.width, height: 20))
        label.font = UIFont(name: "Roboto-Regular", size: 15)!
        label.textColor = Const.COLOR_BLACK_FOR_SELECTED_TEXT
        label.text = headers[section]
        view.addSubview(label)
        let contacts = sectionContacts[section]
        if contacts.count != 0 {
            return view
        }
        return nil
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath) as! ContactCell
        var contact: CNContact
        if searchActive {
            contact = filteredSectionContacts[indexPath.section][indexPath.row]
        } else {
            contact = sectionContacts[indexPath.section][indexPath.row]
        }
        
        cell.nameLabel.text = "\(contact.givenName) \(contact.familyName)"
        
        cell.phoneNumberLabel.text = contact.phoneNumbers[0].value.stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if searchActive {
            if filteredSectionContacts[section].count != 0 {
                return CGFloat(40.0)
            } else {
                return CGFloat(0)
            }
        } else {
            if sectionContacts[section].count != 0 {
                return CGFloat(40.0)
            } else {
                return CGFloat(0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            chosenContact = filteredSectionContacts[indexPath.section][indexPath.row]
        } else {
            chosenContact = sectionContacts[indexPath.section][indexPath.row]
        }
        self.navigationController?.popViewController(animated: true)
        afterPopVC()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}














