//
//  CellServiceDetail03.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CellServiceDetail03: UITableViewCell {
    @IBOutlet weak var tvFullDes: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupUI(_ model: ModelServiceDetail) {
        tvFullDes.setHTMLFromString (htmlText: model.fullDes)
    }
}
