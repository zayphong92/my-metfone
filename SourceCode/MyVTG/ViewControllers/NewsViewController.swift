//
//  NewsViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class NewsViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    private var listNews = [ModelNews]()
    private var filteredNews = [ModelNews]()
    private var listNewsBackup = [ModelNews]()
    private var needLoadMore: Bool = true
    private var currentPage: Int = 1
    private var currentPageBackup: Int = 0
    private var needLoadMoreBackup: Bool = true
    private var searchActive = false
    private var searchText: String? = nil
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = .none
        return formatter
    }()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenName = ScreenNames.News.rawValue
        actionGA = Action.View.rawValue
        setSearchType(.local)
        
        setupTableView()
    }
    
    func setupTableView() {
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        self.tabBarController?.tabBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        setSearchType(.local)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_TinTuc01", comment: ""))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        getNews(complete: {(success: Bool, data: [ModelNews]?) -> Void in
            if (success && data != nil && data?.count != 0) {
                self.getNewsDone(data!)
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.getRightBarButton()?.isEnabled = false
                self.showAlertOnView(self.tableView, NSLocalizedString("Msg_DataNotFound", comment: ""))
            }
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            if self.searchActive {
                self.showSearchBar()
                self.getSearchTextField()?.text = self.searchText
            }
        }
    }
    
    override func refreshData() {
        currentPageBackup = currentPage
        needLoadMoreBackup = needLoadMore
        listNewsBackup = listNews
        
        needLoadMore = true
        currentPage = 1
        
        getNews(complete: {(success: Bool, data: [ModelNews]?) -> Void in
            if (!success || data == nil || data?.count == 0) {
                self.currentPage = self.currentPageBackup
                self.needLoadMore = self.needLoadMoreBackup
                self.listNews = self.listNewsBackup 
            } else {
                self.getRightBarButton()?.isEnabled = true
                self.listNews = []
                self.getNewsDone(data!)
            }
        })
    }
    
    func getNewsDone(_ data: [ModelNews]) {
        currentPage += 1
        if (data.count < Client.PAGE_SIZE) {
            needLoadMore = false
        }
        self.hideAlertOnView(tableView)
        listNews += data
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        tableView.reloadData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    func getNews(complete: @escaping(Bool, [ModelNews]?) -> ()) {
        if (!needLoadMore) {
            return
        }
        if firstTimeLoadView {
            imvLoading.startProgress()
            self.tableView.isHidden = true
        }
        let params = [
            "pageSize": Client.PAGE_SIZE,
            "pageNum": currentPage
        ]
        requestList(Client.WS_GET_NEWS, params: params, complete: {(success: Bool, data: [ModelNews]?) -> Void in
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            self.refreshDataDone()
            complete(success, data)
        })
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    override func onHeaderSearch(keyword: String) {
        filteredNews.removeAll()
        var news: ModelNews
        for i in 0..<listNews.count {
            news = listNews[i]
            let name = news.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredNews.append(news)
            }
        }
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredNews.count
        } else {
            number = listNews.count
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "news", for: indexPath) as! NewsCell
        let news = getModelAtIndex(indexPath.row)
        Util.setImage(cell.iconImage, url: news.iconUrl)
        cell.summaryLabel.text = news.name
        cell.publishDateLabel.text = dateFormatter.string(from: Date.init(timeIntervalSince1970: news.publishDate/1000))
        cell.id = news.id
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelNews {
        if searchActive {
            return filteredNews[index]
        } else {
            if (isRefreshing()) {
                return listNewsBackup[index]
            } else {
                return listNews[index]
            }
        }
    }
    
    func getListModel() -> [ModelNews] {
        if searchActive {
            return filteredNews
        } else {
            if (isRefreshing()) {
                return listNewsBackup
            } else {
                return listNews
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (searchActive || !needLoadMore) {
            return
        }
        
        if (indexPath.row == listNews.count - 1) {
            enableLoadMore(needLoadMore, tableView: tableView)
            getNews(complete: {(success: Bool, data: [ModelNews]?) -> Void in
                if (success && data != nil) {
                    self.getNewsDone(data!)
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //segue()
        goDetail(indexPath.row)
        searchText = getSearchTextField()?.text
        GAHelper.logGA(category: ScreenNames.HotNews.rawValue, actionGA: Action.Click.rawValue, labelGA: nil, valueGA: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func goDetail(_ index: Int) {
        let vc: NewsDetailVC = CarouselVC.initViewController()
        vc.listNews = getListModel()
        vc.setPageCount(vc.listNews.count, startPosition: index)
        goNext(vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}

