//
//  SubscribedServicesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SubscribedServicesViewController: BaseVC, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var itemInfo: IndicatorInfo = "View"
    
    var listServices: [ModelServiceItem] = []
    var filteredServices: [ModelServiceItem] = []
    var searchActive: Bool = false
    weak var controller: PagerTabViewController?
    
    var searchKeyword: String = "" {
        didSet {
            filteredServices.removeAll()
            var service: ModelServiceItem
            for i in 0..<listServices.count {
                service = listServices[i]
                let name = service.name
                let range = name.range(of: searchKeyword, options: NSString.CompareOptions.caseInsensitive)
                if range != nil {
                    filteredServices.append(service)
                }
            }
            
            if searchKeyword == "" {
                searchActive = false
            } else {
                searchActive = true
            }
            setupPullRefreshForScrollable(tableView, enable: !searchActive)
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPullRefreshForScrollable(tableView, enable: true)
        setSearchType(.local)
        let tapRecognizer = UITapGestureRecognizer(target: controller, action: #selector(controller?.dismissKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        refreshData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    override func refreshData() {
        controller?.getRightBarButton()?.isEnabled = false
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        requestList(Client.WS_GET_CURRENT_USED_SERVICES, params: nil, complete: {(success: Bool, data: [ModelServiceItem]?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.tableView.isHidden = false
            self.firstTimeLoadView = false
            if (success && data != nil && data?.count != 0) {
                self.listServices = data!
                self.tableView.reloadData()
                self.controller?.getRightBarButton()?.isEnabled = true
                self.hideEmptyPageOn(view: self.tableView)
            } else {
                self.listServices = []
                self.controller?.getRightBarButton()?.isEnabled = false
                self.tableView.reloadData()
                self.showEmptyPageOn(view: self.tableView, emptyPage: .DichVuDaDangKy)
            }
        })
    }
    
    func resetUI() {
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: true)
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        if searchActive {
            number = filteredServices.count
        } else {
            number = listServices.count
        }
        return number
    }
    
    private func getCurrentDisplayServicesList() -> [ModelServiceItem] {
        return searchActive ? filteredServices : listServices
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subscribedService", for: indexPath) as! SubscribedServiceCell
        let model: ModelServiceItem = getModelAtIndex(indexPath.row)
        Util.setImage(cell.icon, url: model.iconUrl, defaultImageName: "ic_service_package")
        cell.nameLabel.text = model.name
        cell.summaryLabel.text = model.getShortDes()
        
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelServiceItem {
        if searchActive {
            return filteredServices[index]
        } else {
            return listServices[index]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        vc.headerTitleString = NSLocalizedString("ScreenTitle_CurrentUsedServices", comment: "")
        let list = getCurrentDisplayServicesList();
        vc.listServices = list
        vc.regStt = .DangSuDung
        vc.setPageCount(list.count, startPosition: indexPath.row)
        if searchActive {
            self.controller?.onCancelSearch()
        }
        goNextHideTabbar(vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}





