//
//  NewsDetailVC.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class NewsDetailVC: CarouselVC {
    var listNews: [ModelNews] = []
    var listNewsDetail: [String: ModelNewsDetail] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib("NewsDetailPage", reuseId: "newsDetailPage")
        listNewsDetail = [:]
        setupNavBar(title: NSLocalizedString("ScreenTitle_TinTuc02", comment: ""))
    }
    
    override func initPageCellAt(_ index: Int) -> CellCarouselItem {
        let cell: NewsDetailPage = pagerView.dequeueReusableCell(withReuseIdentifier: "newsDetailPage", at: index) as! NewsDetailPage
        cell.owner = self
        return cell
    }
    
    override func pageDidChanged(_ position: Int) {
        setHeaderTitle(listNews[position].name)
    }

    func getModelNewsAt(_ index: Int) -> ModelNews {
        return listNews[index]
    }
    
    func getNewsDetailById(_ id: String) -> ModelNewsDetail? {
        if let model:ModelNewsDetail = listNewsDetail[id] {
            return model
        }
        return nil
    }
    
    func putNewsDetail(_ model: ModelNewsDetail) {
        listNewsDetail.updateValue(model, forKey: model.id)
    }

}
