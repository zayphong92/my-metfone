//
//  SeeAllServicesViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

class SeeAllServicesViewController: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imvLoading: UIImageView!
    
    // MARK: - Variables
    
    var serviceGroup: ModelServiceGroup?
    private var listServices: [ModelServiceItem] = []
    private var filteredServices: [ModelServiceItem] = []
    
    var searchActive: Bool = false
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleStr = serviceGroup == nil ? "" : serviceGroup!.groupName
        setupNavBar(title: titleStr)
        setupPullRefreshForScrollable(tableView, enable: true)
        tabBarController?.tabBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleStr = serviceGroup == nil ? "" : serviceGroup!.groupName
        setupNavBar(title: titleStr)
        tabBarController?.tabBar.isHidden = true
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 1))
        refreshData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func contextIsCancelled() {
        refreshData()
    }
    
    private var firstTimeLoadView: Bool = true
    
    override func refreshData() {
        if firstTimeLoadView {
            imvLoading.startProgress()
            tableView.isHidden = true
        }
        let params = [
            "serviceGroupId": serviceGroup?.groupCode as Any
        ]
        requestList(Client.WS_GET_SERVICES_BY_GROUP, params: params, complete: {(success: Bool, data: [ModelServiceItem]?) -> Void in
            self.refreshDataDone()
            self.imvLoading.stopProgress()
            self.firstTimeLoadView = false
            self.tableView.isHidden = false
            if (success && data != nil && data!.count > 0) {
                for model in data! {
                    // Màn hình này đi từ danh sách dịch vụ chưa đăng ký nên isRegistered luôn bằng false
                    model.isRegistered = false
                }
                self.listServices = data!
                self.tableView.reloadData()
                self.getRightBarButton()?.isEnabled = true
            } else {
                self.getRightBarButton()?.isEnabled = false
            }
        })
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onHeaderSearch(keyword: String) {
        filteredServices.removeAll()
        var service: ModelServiceItem
        for i in 0..<listServices.count {
            service = listServices[i]
            let name = service.name
            let range = name.range(of: keyword, options: NSString.CompareOptions.caseInsensitive)
            if range != nil {
                filteredServices.append(service)
            }
        }
        
        if keyword == "" {
            searchActive = false
        } else {
            searchActive = true
        }
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        self.tableView.reloadData()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        searchActive = false
        setupPullRefreshForScrollable(tableView, enable: !searchActive)
        tableView.reloadData()
    }
    
    // MARK: - Table handle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchActive ? filteredServices.count : listServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceDetail", for: indexPath) as! ServiceDetailCell
        let model: ModelServiceItem = getModelAtIndex(indexPath.row)
        Util.setImage(cell.icon, url: model.iconUrl, defaultImageName: "ic_service_package")
        cell.nameLabel.text = model.name
        cell.summaryLabel.text = model.getShortDes()

        if (model.isRegisterAble == 0 || model.isMultPlan || model.state == 1) {
            // Theo spec thì nếu isMultPlan = true thì hiển thị button Detail thay cho button Subscribe,
            // click vào button Detail thì nhảy đến màn hình service detail.
            // Tuy nhiên như thế ko cần thiết vì click vào cell cũng nhảy đến màn hình detail rồi
            cell.btnSub.isHidden = true
        } else {
            cell.btnSub.isHidden = false
            cell.btnSub.tag = indexPath.row
            cell.btnSub.setTitle(model.textForButtonSubmit, for: .normal)
        }
        
        if model.state == 2 {
            cell.btnSub.setTitle(NSLocalizedString("Services_DangXuLyButton", comment: ""), for: .normal)
            cell.btnSub.isEnabled = false
        } else if model.state == 0 {
            cell.btnSub.setTitle(NSLocalizedString("Services_DangKyButton", comment: ""), for: .normal)
            cell.btnSub.isEnabled = true
        }
        return cell
    }
    
    func getModelAtIndex(_ index: Int) -> ModelServiceItem {
        return searchActive ? filteredServices[index] : listServices[index]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        goDetail(indexPath.row)
        if searchActive {
            onCancelSearch()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func goDetail(_ index: Int) {
        let vc: ServiceDetailVC = CarouselVC.initViewController()
        vc.headerTitleString = serviceGroup!.groupName
        let currentList = searchActive ? filteredServices : listServices
        vc.listServices = currentList
        vc.setPageCount(currentList.count, startPosition: index)
        vc.regStt = .ChuaDangKy
        goNextHideTabbar(vc)
    }
    
    func subscribe(_ model: ModelServiceItem) {
        regUnregService(model: model, needConfirm: false, onComplete: {(success: Bool, data: ModelDoAction?) -> Void in
            self.tableView.reloadData()
        })
    }
    
    @IBAction func subscribeTapped(_ sender: UIButton) {
        let model: ModelServiceItem = getModelAtIndex(sender.tag)
        let message = model.action == .Register ? NSLocalizedString("Services_DangKyLabel_ThongBao1", comment: "") : NSLocalizedString("Services_DangKyLabel_ThongBao3", comment: "")
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: message,
                  cancelLabel: NSLocalizedString("Label_Button_Detail", comment: ""),
                  agreeLabel: model.textForButtonSubmit,
                  onOK: {
                    self.subscribe(model)
        },
                  onCancel: {
                    self.goDetail(sender.tag)
        })
    }
}












