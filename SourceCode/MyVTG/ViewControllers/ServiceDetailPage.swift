//
//  ServiceDetailPage.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ServiceDetailPage: CellCarouselItem, UITableViewDataSource, UITableViewDelegate, CellServiceDetailDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var owner: ServiceDetailVC? = nil
    var modelServiceDetail: ModelServiceDetail? = nil
    
    let cellId01 = "cellServiceDetail01"
    let cellId02 = "cellServiceDetail02"
    let cellId03 = "cellServiceDetail03"
    let cellId04 = "cellServiceDetail04"

    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.backgroundColor = UIColor.white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib.init(nibName: "CellServiceDetail01", bundle: nil), forCellReuseIdentifier: cellId01)
        tableView.register(UINib.init(nibName: "CellServiceDetail02", bundle: nil), forCellReuseIdentifier: cellId02)
        tableView.register(UINib.init(nibName: "CellServiceDetail03", bundle: nil), forCellReuseIdentifier: cellId03)
        tableView.register(UINib.init(nibName: "CellServiceDetail04", bundle: nil), forCellReuseIdentifier: cellId04)
    }

    override func setContentHidden(_ visible: Bool) {
        tableView.isHidden = visible
    }
    
    override func getData() -> String {
        var serviceCode: String = ""
        if owner!.listServices.count == 0 {
            serviceCode = owner!.serviceCode
        } else {
            let service: ModelServiceItem = owner!.getModelServiceItemAt(index)
            serviceCode = service.code
            let model = owner!.getServiceDetailByCode(service.code)
            guard model == nil else {
                alreadyHasData()
                modelServiceDetail = model
                tableView.reloadData()
                return service.code
            }
        }
        _ = super.getData()
        let params = ["serviceCode": serviceCode]
        owner!.requestObject(Client.WS_GET_SERVICE_DETAIL, params: params, complete: {(success: Bool, data: ModelServiceDetail?) -> Void in
            if (data != nil) {
                data!.isRegistered = self.owner!.regStt == .DangSuDung
                self.modelServiceDetail = data
                self.owner!.putServiceDetail(data!)
            }
            if (self.shouldBindData(serviceCode)) {
                self.getDataDone(success && data != nil)
                self.tableView.reloadData()
            }
        })
        return serviceCode
    }
    
    private func isCell01(_ indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    private func isCell02(_ indexPath: IndexPath) -> Bool {
        if (indexPath.section == 1) {
            return modelServiceDetail!.hasSubPackages()
        }
        return false
    }
    
    private func isCell03(_ indexPath: IndexPath) -> Bool {
        if (modelServiceDetail!.hasSubPackages()) {
            return indexPath.section == 2
        } else {
            return indexPath.section == 1
        }
    }
    
    private func isCell04(_ indexPath: IndexPath) -> Bool {
        if (modelServiceDetail!.hasSubPackages()) {
            return false
        } else {
            return indexPath.section == 2
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return modelServiceDetail == nil ? 0 : 3
    }
    
    ////////////////////////////////////////////////////////////////
    // màn hình service detail có 2 kiểu layout
    // nếu có sub packages
    //     Cell01     image, name, web url
    //   [ Cell02 ]   mảng các item của sub package
    //     Cell03     full desccription của service detail
    //
    // nếu ko có sub packages
    //     Cell01     image, name, web url
    //     Cell03     full desccription của service detail
    //     Cell04     hiển thị trạng thái đã đăng ký hay chưa & button để đăng ký/huỷ đăng ký
    ////////////////////////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return modelServiceDetail!.hasSubPackages() ? modelServiceDetail!.packages!.count : 1
        case 2: return 1
        default:
            return 0
        }
    }
    
    var btnSub: UIButton?
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (isCell01(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId01, for: indexPath) as! CellServiceDetail01
            cell.setupUI(modelServiceDetail!)
            return cell
        }
        if (isCell02(indexPath)) {
            let subPkg: ModelServiceDetailSubPackage = modelServiceDetail!.packages![indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId02, for: indexPath) as! CellServiceDetail02
            cell.delegate = self
            btnSub = cell.btnSub
            cell.setupUI(subPkg, isRegisterAble: (modelServiceDetail?.isRegisterAble)!)
            return cell
        }
        if (isCell03(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId03, for: indexPath) as! CellServiceDetail03
            cell.setupUI(modelServiceDetail!)
            return cell
        }
        if (isCell04(indexPath)) {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId04, for: indexPath) as! CellServiceDetail04
            cell.delegate = self
            cell.setupUI(modelServiceDetail!, regStt: owner!.regStt)
            return cell
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isCell01(indexPath)) {
            let height = ScreenSize.HEIGHT / 3.2
            return height > 200 ? height : 200
        }
        if (isCell02(indexPath) || isCell04(indexPath)) {
            let height = ScreenSize.HEIGHT / 7.5
            return height > 80 ? height : 80
        }
        if (isCell03(indexPath)) {
            let width = owner!.pagerView.itemSize.width - 20
            let tv = UITextViewRegular01()
            tv.setHTMLFromString (htmlText:modelServiceDetail!.fullDes)
            let size = tv.sizeThatFits(CGSize(width, CGFloat.greatestFiniteMagnitude))
            return size.height + 20
        }
        return 0
    }

    // MARK: - CellServiceDetailDelegate
    
    func regUnregService(_ model: ModelRegistable) {
        owner!.regUnregService(model: model,
                               onCancel: {
                                self.registerServiceDone(false, model: model)
        },
                               onComplete: {(success: Bool, data: ModelDoAction?) -> Void in
                                self.registerServiceDone(success, model: model)
                                if success {
                                    self.btnSub?.setTitle(NSLocalizedString("Services_DangXuLyButton", comment: ""), for: .normal)
                                    self.btnSub?.isEnabled = false
                                }
        })
    }
    
    private func registerServiceDone(_ success: Bool, model: ModelRegistable) {
//        tableView.reloadData()
    }
}











