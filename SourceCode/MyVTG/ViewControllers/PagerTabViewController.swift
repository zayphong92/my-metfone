//
//  PagerTabViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PagerTabViewController: ButtonBarPagerTabStripViewController {
    
    // MARK: - Variables
    
    let lightGray = UIColor(red: 156.0 / 255.0, green: 156.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0)
    var vc1 = ServicesViewController()
    var vc2 = SubscribedServicesViewController()
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        GAHelper.logGA(category: ScreenNames.Services.rawValue, actionGA: Action.View.rawValue, labelGA: nil, valueGA: nil)
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = Const.COLOR_ORANGE
        settings.style.buttonBarItemFont = UIFont(name: "roboto-medium", size: 15.0)!
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = lightGray
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = self.lightGray
            newCell?.label.textColor = Const.COLOR_ORANGE
        }
        self.automaticallyAdjustsScrollViewInsets = false
        self.changeCurrentIndex = changeCurrentTab
        super.viewDidLoad()
    }
    
    override func getNavBarLeftImageName() -> String {
        return ""
    }
    
    override func getNavBarRightImageName() -> String {
        return "ic_search"
    }
    
    override func onBtnRightNavBar() {
        super.onBtnRightNavBar()
        toggleSearchBar()
    }
    
    override func onHeaderSearch(keyword: String) {
        if self.currentIndex == 0 {
            vc1.searchKeyword = keyword
            
        } else if self.currentIndex == 1 {
            vc2.searchKeyword = keyword
        }
    }
    
    func dismissKeyboard() {
        getSearchTextField()?.resignFirstResponder()
    }
    
    override func onCancelSearch() {
        super.onCancelSearch()
        if self.currentIndex == 0 {
            vc1.resetUI()
        } else if self.currentIndex == 1 {
            vc2.resetUI()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        if vc1.searchActive || vc2.searchActive {
            self.onCancelSearch()
        }
        if self.currentIndex != 0 { vc1.currentTabChanged(self.currentIndex) }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar(title: NSLocalizedString("ScreenTitle_DichVu01", comment: ""))
        getRightBarButton()?.isEnabled = false
        tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "TabServices", bundle: nil).instantiateViewController(withIdentifier: "serviceVC") as! ServicesViewController
        child_1.itemInfo = IndicatorInfo(title: NSLocalizedString("ServicesLabel_ChuaDangKy", comment: ""))
        let child_2 = UIStoryboard(name: "TabServices", bundle: nil).instantiateViewController(withIdentifier: "subscribedServices") as! SubscribedServicesViewController
        child_2.itemInfo = IndicatorInfo(title: NSLocalizedString("ServicesLabel_DaDangKy", comment: ""))
        vc1 = child_1
        vc1.controller = self
        vc2 = child_2
        vc2.controller = self
  
        return [child_1, child_2]
    }
    
    func changeCurrentTab(_ oldCell: ButtonBarViewCell?, _ newCell: ButtonBarViewCell?, _ animated: Bool) {
        LogUtil.d("changeCurrentIndex")
    }
    

    
}
