//
//  DetailVC.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DetailVC: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var featureImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabelLight01!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var descriptionLable: UILabel!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var actionButton: UIButtonRegular01!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var moreInfoLayout: UIView!
    @IBOutlet weak var moreInfoTitleLabel: UILabel!
    @IBOutlet weak var moreInfoValueLabel: UILabel!
    
    @IBOutlet weak var heightOfButtonLayoutContraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    let BUTTON_HEIGH: CGFloat = 76.0
    var serviceType: ServiceType = .None
    var inputData: ModelBase? = nil
    var serverData: ModelBase? = nil
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        setNavTitle()
        super.viewDidLoad()
        getDetailData()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavTitle()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - Setup UI and actions for cases
    
    func setNavTitle() {
        switch serviceType {
        case .BuyData:
            setupNavBar(title: (inputData as! ModelDataPackage).name)
            
        case .BuyPhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_MuaSo01", comment: ""))
            
        case .ChangePhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DoiSo01", comment: ""))
            
        case .LockSim:
            setupNavBar(title: NSLocalizedString("ScreenTitle_KhoaChieuGoiDi01", comment: ""))
            
        case .ReStorePhoneNumber:
            setupNavBar(title: NSLocalizedString("ScreenTitle_KhoiPhucSo01", comment: ""))
            
        case .ChangeSIM:
            setupNavBar(title: NSLocalizedString("ScreenTitle_DoiSIM01", comment: ""))
            
        case .Promotion:
            setupNavBar(title: NSLocalizedString("ScreenTitle_KhuyenMaiChoBan01", comment: ""))
            
        case .News:
            setupNavBar(title: NSLocalizedString("ScreenTitle_TinTuc01", comment: ""))
            
        case .Airtime:
            setupNavBar(title: NSLocalizedString("ScreenTitle_UngTien01", comment: ""))
            
        case .IShare:
            setupNavBar(title: NSLocalizedString("ScreenTitle_ChuyenTien01", comment: ""))
            
        default:
            setupNavBar(title: "")
        }
    }
    
    func doAction() {
        switch serviceType {
            
        case .BuyData:
            let params = [
                "actionType": ActionType.Unregister.rawValue,
                "serviceCode": (inputData as? ModelDataPackage)!.code
                ] as [String : Any]
            showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                      message: NSLocalizedString("Home_ThemDataLabel_ThongBao1", comment: ""),
                      cancelLabel: NSLocalizedString("Button_Cancel", comment: ""),
                      agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                      onOK: {
                        self.requestAction(Client.WS_UNREGISTER_DATA_PACKAGE, params: params) { (success: Bool, model: ModelDoAction?) in
                            if (success) {
                                self.actionButton.setTitle(NSLocalizedString("Home_DoiGoiDataDangXuLy", comment: ""), for: .normal)
                                self.actionButton.isEnabled = false
                            }
                        }
            })
            
            break
            
        case .BuyPhoneNumber:
            let searchVC = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "SearchPhoneNumberVC") as! SearchPhoneNumberVC
            searchVC.serviceType = .BuyPhoneNumber
            searchVC.serviceFee = (serverData as! ModelServiceInfo).price
            self.navigationController?.pushViewController(searchVC, animated: true)
            
        case .ChangePhoneNumber:
            let searchVC = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "SearchPhoneNumberVC") as! SearchPhoneNumberVC
            searchVC.serviceType = .ChangePhoneNumber
            searchVC.serviceFee = (serverData as! ModelServiceInfo).price
            self.navigationController?.pushViewController(searchVC, animated: true)
            
        case .ReStorePhoneNumber:
            let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_PhoneNumberServices", viewControllerName: "PaymentBillVC") as! PaymentBillVC
            vc.serviceType = .ReStorePhoneNumber
            vc.serviceFee = (serverData as! ModelServiceInfo).price
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .LockSim:
            doLockSim()
            
        case .ChangeSIM:
            let vc = UIStoryboard.getViewController(storyBoardName: "TabUtilities_DoiSIM", viewControllerName: "newSIMInfo") as! EnterSIMViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .Promotion:
            let promotion:ModelPromotionInfo = serverData as! ModelPromotionInfo
            
            switch promotion.actionType {
            case PromotionAction.Rechange.rawValue:
                segueToRecharge(showsTabBar: false)
                
            case PromotionAction.Register.rawValue:
                registerService(serviceCode: promotion.code)
                
            default:
                break
            }
            
            break
            
        case .Airtime:
            //Todo: Chua co spec cho chuc nang ung tien
            break
            
        case .IShare:
            //Todo: Chua co spec cho chuc nang chuyen tien
            break
            
        default:
            break
        }
    }

    func getDetailData() {
        switch serviceType {
        case .BuyPhoneNumber:
            getServiceInfo(ServiceCodes.MuaSo)
        case .ChangePhoneNumber:
            getServiceInfo(ServiceCodes.DoiSo)
        case .ReStorePhoneNumber:
            getServiceInfo(ServiceCodes.ReSorteIsdn)
        case .LockSim:
            getServiceInfo(ServiceCodes.KhoaChieuGoiDi)
        case .ChangeSIM:
            getServiceInfo(ServiceCodes.DoiSIM)
        case .BuyData:
            let dataPackage: ModelDataPackage  = inputData as! ModelDataPackage
            getDataPackageDetail(dataPackage.code)
        case .Promotion:
            getPromotionInfo()
        case .News:
            getNewsInfo()
        case .Airtime:
            //Todo: Chua co spec cho chuc nang ung tien
            fakeRequestApi()
        case .IShare:
            //Todo: Chua co spec cho chuc nang chuyen tien
            fakeRequestApi()
        default:
            break
        }
    }
    
    // MARK: - Case: Data package request
    
    func getDataPackageDetail(_ packageCode: String) {
        /*let cacheData: ModelDataPackageInfo? = ModelHelper.loadFromCache(subCacheKey: packageCode)
        if cacheData != nil {
            self.updateScreenWithDataPackageDetail(cacheData!)
        } else {
            showProgressOnView(mainScrollView)
        }*/
        
        let params = [
            "packageCode": packageCode
        ]
        requestObject(Client.WS_GET_DATA_PACKAGE_INFO, params: params, subCacheKey: packageCode, complete: {(success: Bool, data: ModelDataPackageInfo?) -> Void in
            self.hideProgressOnView(self.mainScrollView)
            if (success && data != nil) {
                self.hideAlertOnView(self.mainScrollView)
                self.updateScreenWithDataPackageDetail(data!)
            } else {
                let message: String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.mainScrollView, message)
            }
        })
    }
    
    func updateScreenWithDataPackageDetail(_ packageInfo: ModelDataPackageInfo){
        featureImage.isHidden = false
        Util.setImage(featureImage, url: packageInfo.imgDesUrl)
        
        nameLabel.isHidden = false
        nameLabel.text = packageInfo.name
        
        descriptionLable.isHidden = false
        descriptionLable.setHTMLFromString(htmlText: packageInfo.fullDes)
        
        actionButton.isHidden = false
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
        
        if [0, 1].contains(packageInfo.enableAct) {
            actionButton.isEnabled = false
        } else {
            actionButton.isEnabled = true
        }
        
        switch serviceType {
        case .BuyData:
            actionButton.localizedText = NSLocalizedString("Home_ThemDataButton_Huy", comment: "")
            actionButton.isEnabled = true
            break
            
        default:
            break
        }
    }
    
    // MARK: Case: Service request
    
    func getServiceInfo(_ serviceCode : ServiceCodes) {
        let cacheData:ModelServiceInfo? = ModelHelper.loadFromCache(subCacheKey: serviceCode.rawValue)
        if cacheData != nil {
            self.serverData = cacheData
            self.updateUIForServiceInfo(cacheData)
        } else {
            showProgressOnView(mainScrollView)
        }
        
        let params = [
            "subType": client.getSubType(),
            "serviceCode": serviceCode.rawValue
            ] as [String : Any]
        requestObject(Client.WS_GET_SERVICE_INFO, params: params, subCacheKey: serviceCode.rawValue, complete: {(success: Bool, data: ModelServiceInfo?) -> Void in
            self.hideProgressOnView(self.mainScrollView)
            if (success && data != nil) {
                self.serverData = data
                self.updateUIForServiceInfo(data)
                self.hideAlertOnView(self.mainScrollView)
            } else {
                let message: String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.mainScrollView, message)
            }
        })
    }
    
    func updateUIForServiceInfo(_ serviceInfo: ModelServiceInfo?) {
        guard let serviceInfo = serviceInfo else { return }
        featureImage.isHidden = false
        Util.setImage(featureImage, url: serviceInfo.imgDesUrl)

        nameLabel.isHidden = false
        nameLabel.text = serviceInfo.name
        
        descriptionLable.isHidden = false
        descriptionLable.setHTMLFromString(htmlText: serviceInfo.fullDes)
        
        actionButton.isHidden = false
        switch serviceType {
        case .BuyPhoneNumber:
            actionButton.localizedText = NSLocalizedString("Home_MuaSoButton_MuaSo", comment: "")
            
        case .ChangePhoneNumber:
            actionButton.localizedText = NSLocalizedString("Utilities_TimSoButton_TimSoDoi", comment: "")
            
        case .ReStorePhoneNumber:
            actionButton.localizedText = NSLocalizedString("Utilities_KhoiPhucSoButton", comment: "")
            
            moreInfoLayout.isHidden = false
            moreInfoTitleLabel.text = NSLocalizedString("Utilities_KhoiPhucSoLabel_SoCu", comment: "")
            moreInfoValueLabel.text = "---"
            
            //Todo
            //Show notice + disable button follow response account
            
        case .ChangeSIM:
            actionButton.localizedText = NSLocalizedString("Utilities_DoiSIMButton", comment: "")
            noticeLabel.isHidden = false
            noticeLabel.text = NSLocalizedString("Utilities_DoiSIMLabel_ThongBao1", comment: "")
            
        case .LockSim:
            actionButton.localizedText = NSLocalizedString("Utilities_KhoaSoButton", comment: "")
            noticeLabel.isHidden = false
            noticeLabel.text = String.init(format: NSLocalizedString("Utilities_KhoaSoLabel_msg03", comment:""), client.getIsdn())
            
        default:
            break
        }
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
    }
    
    // MARK: Case: Promotion request
    
    func getPromotionInfo() {
        let packCode:String = (inputData as! ModelPromotion).code
        let cachData:ModelPromotionInfo? = ModelHelper.loadFromCache(subCacheKey: packCode)
        if cachData != nil {
            self.serverData = cachData!
            self.updateUIForPromotion(cachData!)
        } else {
            showProgressOnView(mainScrollView)
        }
        
        let params = [
            "packageCode": packCode
        ]
        requestObject(Client.WS_GET_PROMOTION_INFO, params: params, subCacheKey: packCode, complete: {(success: Bool, data: ModelPromotionInfo?) -> Void in
            
            self.hideProgressOnView(self.mainScrollView)
            if(success && data != nil ) {
                self.serverData = data
                self.updateUIForPromotion(data!)
                
                self.hideAlertOnView(self.mainScrollView)
            } else {
                let message:String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.mainScrollView, message)
            }
        })
    }
    
    func updateUIForPromotion(_ promotion: ModelPromotionInfo) {
        
        setupNavBar(title: promotion.name)
        
        featureImage.isHidden = false
        Util.setImage(featureImage, url: promotion.imgDesUrl)
        
        nameLabel.isHidden = false
        nameLabel.text = promotion.name
        
        descriptionLable.isHidden = false
        descriptionLable.setHTMLFromString(htmlText: promotion.fullDes)
        
        switch promotion.actionType {
        case PromotionAction.Rechange.rawValue:
            showButtonLayout()
            actionButton.localizedText = "Nap cuoc"
            
        case PromotionAction.Register.rawValue:
            showButtonLayout()
            actionButton.localizedText = "Dang ki"
            
        default:
            hideButtonLayout()
            break
        }
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
    }
    
    func showButtonLayout() {
        actionButton.isHidden = false
        heightOfButtonLayoutContraint.constant = BUTTON_HEIGH
    }
    
    func hideButtonLayout() {
        actionButton.isHidden = true
        heightOfButtonLayoutContraint.constant = 0
    }
    
    // MARK: Case: News request
    
    func getNewsInfo() {
        let newId = (inputData as! ModelNews).id
        let cachData:ModelNewsDetail? = ModelHelper.loadFromCache(subCacheKey: newId)
        if cachData != nil {
            self.serverData = cachData!
            self.updateUIForNewsInfo(cachData!)
        } else {
            showProgressOnView(mainScrollView)
        }
        
        let params = ["newsId": newId]
        requestObject(Client.WS_GET_NEWS_DETAIL, params: params, complete: {(success: Bool, data: ModelNewsDetail?) -> Void in
            self.hideProgressOnView(self.mainScrollView)
            if (success &&  data != nil ) {
                self.serverData = data
                self.updateUIForNewsInfo(data!)
            } else {
                let message:String = NSLocalizedString("More_Not_Found_Data", comment: "")
                self.showAlertOnView(self.mainScrollView, message)
            }
        })
    }
    
    func  updateUIForNewsInfo(_ news: ModelNewsDetail) {
        setupNavBar(title: news.name)
        
        featureImage.isHidden = false
        Util.setImage(featureImage, url: news.imgDesUrl)
        
        nameLabel.isHidden = false
        nameLabel.text = news.name
        
        descriptionLable.isHidden = false
        descriptionLable.setHTMLFromString(htmlText: news.content)
        
        hideButtonLayout()
    }
    
    
    func fakeRequestApi() {
        showProgressOnView(mainScrollView)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            self.hideProgressOnView(self.mainScrollView)
            let message: String = NSLocalizedString("More_Not_Found_Data", comment: "")
            self.showAlertOnView(self.mainScrollView, message)
        }
    }
    
    
    
    
}
