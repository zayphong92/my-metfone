//
//  CarouselVC.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CarouselVC: BaseVC, FSPagerViewDataSource, FSPagerViewDelegate {
    let COLOR_BG = UIColor.rgbValue(hexValue: 0xECECEC)
    var count: Int = 0
    var startPosition: Int = 0
    var isFirst: Bool = true

    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.backgroundColor = COLOR_BG
            self.pagerView.itemSize = CGSize(width: ScreenSize.WIDTH - 20, height: ScreenSize.HEIGHT - getNavigationBarHeight() - 30)
            self.pagerView.interitemSpacing = 5.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (pagerView.currentIndex != startPosition) {
            pagerView.scrollToItem(at: startPosition, animated: false)
        }
    }
    
    func registerNib(_ nibName: String, reuseId: String) {
        pagerView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: reuseId)
    }
    
    func setPageCount(_ count: Int, startPosition: Int = 0) {
        self.startPosition = startPosition
        self.count = count
        self.isFirst = true
    }
    
    func pageDidChanged(_ position: Int) {
    }
    
    func initPageCellAt(_ index: Int) -> CellCarouselItem {
        fatalError("The derived class must override this method \(#function)")
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = initPageCellAt(index)
        var delayTime: Int = 0
        /*
         Fix bug: 
            Description: Page chưa kịp scroll từ 0 -> start Position thì api đã load xong và bind dữ liệu dẫn đến page bị giật.
            Solutions: Trong lần đầu tiên, khi chưa scroll đến vị trí startPosition thì delay lại 0.3s
         */
        if isFirst, startPosition != index {
            delayTime = 300
        } else {
            isFirst = false
            
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(500)) {
            cell.setupUI(index, size: pagerView.itemSize)
        }
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool {
        return false
    }
    
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        pageDidChanged(pagerView.currentIndex)
    }
    
    static func initViewController<T: CarouselVC>() -> T {
        let instance = UIStoryboard.getViewController(storyBoardName: "CarouselVC", viewControllerName: "CarouselVC") as! CarouselVC
        object_setClass(instance, T.self)
        return (instance as? T)!
    }
}











