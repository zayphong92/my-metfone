//
//  CellPagerServiceDetail.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class CellCarouselItem: FSPagerViewCell {
    let TAG: String = "CellCarouselItem"
    
    var imvLoading: UIImageView!
    var btnRetry: UIButton!
    var lbNotFound: UILabel!
    private var size: CGSize! = CGSize()
    private var code: String = ""
    
    var index: Int = 0
    
    func setupUI(_ index: Int, size: CGSize) {
        if imvLoading != nil,  lbNotFound != nil, btnRetry != nil {
            imvLoading.removeFromSuperview()
            lbNotFound.removeFromSuperview()
            btnRetry.removeFromSuperview()
        }
        self.size = size
        let font = UIFont.init(name: "roboto-regular", size: 16)
        imvLoading = UIImageView.init(frame: CGRect.init(0, 0, 50, 50))
        
        lbNotFound = UILabel.init()
        lbNotFound.textColor = Const.COLOR_TEXT_DLG_COLOR_01
        lbNotFound.text = NSLocalizedString("Msg_DataNotFound", comment: "")
        lbNotFound.font = font
        lbNotFound.sizeToFit()
        
        btnRetry = UIButton.init()
        btnRetry.setTitle(NSLocalizedString("Label_Retry", comment: ""), for: .normal)
        btnRetry.setTitleColor(Const.COLOR_MAIN, for: .normal)
        btnRetry.setTitleColor(Const.COLOR_MAIN_HILIGHTED, for: .highlighted)
        btnRetry.titleLabel?.font = font
        btnRetry.sizeToFit()
        btnRetry.addTarget(self, action: #selector(getData), for: .touchUpInside)
        
        let w:CGFloat = lbNotFound.frame.size.width > btnRetry.frame.size.width ? lbNotFound.frame.size.width : btnRetry.frame.size.width
        let h:CGFloat = lbNotFound.frame.size.height + lbNotFound.frame.size.height * 3
        let layout:UIView = UIView.init(frame: CGRect.init(0, 0, w, h))
        layout.center = CGPoint(x: size.width/2, y: size.height/2)
        
        let layoutCenter = CGPoint.init(w/2, h/2)
        imvLoading.center = layoutCenter
        btnRetry.center = layoutCenter
        
        layout.addSubview(imvLoading)
        layout.addSubview(lbNotFound)
        layout.addSubview(btnRetry)
        self.addSubview(layout)
        
        imvLoading.isHidden = true
        lbNotFound.isHidden = true
        btnRetry.isHidden = true

        
        self.backgroundColor = UIColor.white
        self.roundCorners([.topLeft, .topRight], radius: 10)
        
        //Get data
        self.index = index
        code = getData()
    }
    
    func getData() -> String {
        onStartLoadData()
        return code
    }
    
    func shouldBindData(_ code: String) -> Bool {
        return code == self.code
    }
    
    func setContentHidden(_ visible: Bool) {
    }
    
    func getSize() -> CGSize {
        return size
    }
    
    func alreadyHasData() {
        setContentHidden(false)
        lbNotFound.isHidden = true
        btnRetry.isHidden = true
        imvLoading.stopProgress()
    }
    
    private func onStartLoadData() {
        
        if !(NetworkReachabilityManager()?.isReachable)! {
            lbNotFound.isHidden = false
            btnRetry.isHidden = false
            setContentHidden(true)
            return
        }
        
        imvLoading.startProgress()
        lbNotFound.isHidden = true
        btnRetry.isHidden = true
        setContentHidden(true)
    }
    
    func getDataDone(_ dataFound: Bool) {
        imvLoading.stopProgress()
        if (!dataFound) {
            setContentHidden(true)
            lbNotFound.isHidden = false
            btnRetry.isHidden = false
        } else {
            setContentHidden(false)
            lbNotFound.isHidden = true
            btnRetry.isHidden = true
        }
    }
    
}
