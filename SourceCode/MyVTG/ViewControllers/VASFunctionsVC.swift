//
//  VASFunctionsVC.swift
//  MyVTG
//
//  Created by luatnc on 11/1/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import WebKit
import ObjectMapper
import Alamofire
import SwiftyJSON

class VASFunctionsVC: BaseVC, WKUIDelegate, WKScriptMessageHandler {
    var webView: WKWebView!
    var navBarTitle: String = "VAS Service"
    var serviceCode: String?
    var servicePage: String?
    
    init(navBarTitle : String, serviceCode: String) {
        super.init(nibName: nil, bundle: nil)
        self.navBarTitle = navBarTitle
        self.serviceCode = serviceCode
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setNavBarTitle(navBarTitle : String) {
        self.navBarTitle = navBarTitle
    }
    
    func setServiceCode(serviceCode : String) {
        self.serviceCode = serviceCode
    }
    
    override func loadView() {
        super.loadView()
        
        let contentController = WKUserContentController();
        contentController.add(self, name: "callbackHandler")
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = contentController
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup Nav bar
        setupNavBar(title: self.navBarTitle)
        
        //get data
        let params = [
            "serviceCode": self.serviceCode
        ]
        requestObject(Client.WS_GET_ADVANCED_SERVICE_INFO, params: params, subCacheKey: self.serviceCode, complete: {(success: Bool, data: ModelAdvancedServiceInfo?) -> Void in
            if (success && data != nil) {
                self.servicePage = data?.servicePage
                let myURL = URL(string: self.servicePage!)
                let req = URLRequest(url: myURL!)
                self.webView!.load(req)
                
//                let htmlPath = Bundle.main.path(forResource: "index", ofType: "html")
//                let url = URL(fileURLWithPath: htmlPath!)
//                let request = URLRequest(url: url)
//                self.webView.load(request)
                
            } else {
                let message: String = NSLocalizedString("More_Not_Found_Data", comment: "")
            }
        })
        
        //let myURL = URL(string: "https://firebasestorage.googleapis.com/v0/b/myvtg-ab171.appspot.com/o/lao_airtime_en.html?alt=media&token=d43f9ecd-11ba-4b3a-9535-2be22f5399bd")
        //let myURL = URL(string: self.servicePage!)
        //let req = URLRequest(url: myURL!)
        //self.webView!.load(req)
        
        //let htmlPath = Bundle.main.path(forResource: "index", ofType: "html")
        //let url = URL(fileURLWithPath: htmlPath!)
        //let request = URLRequest(url: url)
        //webView.load(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // setup Nav bar
        setupNavBar(title: self.navBarTitle)
    }
    
    //Javascript bridge
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "callbackHandler") {
            let jsonString : String = message.body as! String
            let jcParams : JCParams = Mapper<JCParams>().map(JSONString: jsonString)!
            
            if (jcParams.apiName == "wsDoActionService" || jcParams.apiName == "wsDoActionByWebService" ) {
                if (jcParams.apiFunctName == "performDoActionService") {
                    doActionService(apiName: jcParams.apiName!, jcParams: jcParams)
                } else if (jcParams.apiFunctName == "requestObject") {
                    
                }
            } else if (jcParams.apiName == "sendUssd") {
                
            } else if (jcParams.apiName == "wsGetServiceFunctionPage") {
                
            }
        }
    }
    
    func doActionService(apiName: String, jcParams : JCParams) {
        showAlert(title: NSLocalizedString("Label_XacNhanThongBao", comment: ""),
                  message: jcParams.confirmMsg!,
                  cancelLabel: NSLocalizedString("Button_Dong", comment: ""),
                  agreeLabel: NSLocalizedString("Button_OK", comment: ""),
                  onOK: {
                    let params: [String : Any] = ["actionType": jcParams.actionType, "serviceCode": jcParams.serviceCode]
                    self.requestAction(apiName, params: params)
        })
    }
    
    func sendUssd() {
        
    }
    
    func wsGetServiceFunctionPage() {
        
    }
    
}
