//
//  SearchChargeHistoryViewController.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class SearchChargeHistoryViewController: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var todayLbl: UILabel!
    @IBOutlet weak var todayTick: UIImageView!
    @IBOutlet weak var thisWeekLbl: UILabel!
    @IBOutlet weak var thisWeekTick: UIImageView!
    @IBOutlet weak var thisMonthLbl: UILabel!
    @IBOutlet weak var thisMonthTick: UIImageView!
    @IBOutlet weak var lastMonthLbl: UILabel!
    @IBOutlet weak var lastMonthTick: UIImageView!
    @IBOutlet weak var startDateLbl: UITextFieldRegular01!
    @IBOutlet weak var endDateLbl: UITextFieldRegular01!
    @IBOutlet weak var startDateSymbol: UILabel!
    @IBOutlet weak var endDateSymbol: UILabel!
    
    // MARK: - Variables
    
    var popToRoot: () -> Void = {}
    var chargeStore: ChargeStore!
    var postAction: () -> Void = {}
    var currentTick: UIImageView?
    var resetNavTitle: () -> Void = {}
    var startDate: Date!
    var endDate: Date!
    var prefixTitle: String = ""
    let dateFormatterFull: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    let dateFormatterShort: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM"
        return formatter
    }()
    
    enum DateStatus {
        case start
        case end
    }
    
    // MARK: - Setup UI
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupTextFields()
        searchThisMonth()
    }
    
    func setupViews() {
        boxView.layer.cornerRadius = 3
        boxView.layer.masksToBounds = true
        closeButton.contentMode = .scaleAspectFit
    }
    
    func setupTextFields() {
        startDateLbl.title = NSLocalizedString("Home_TraCuocLabel_NgayBatDau", comment: "")
        startDateLbl.setTitleVisible(true)
        startDateLbl.titleColor = Const.COLOR_MAIN
        
        endDateLbl.title = NSLocalizedString("Home_TraCuocLabel_NgayKetThuc", comment: "")
        endDateLbl.setTitleVisible(true)
        endDateLbl.titleColor = Const.COLOR_MAIN
    }
    
    // MARK: - Tap functions
    
    @IBAction func closeTapped(_ sender: UIButton) {
        view.endEditing(true)
        closeTab()
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
        popToRoot()
    }

    @IBAction func chooseStartDate(_ sender: UIButton) {
        presentCalendar(.start)
    }
    
    @IBAction func chooseEndDate(_ sender: UIButton) {
        presentCalendar(.end)
    }
    
    @IBAction func todayTapped(_ sender: UIButton) {
        searchToday()
    }
    
    func searchToday() {
        highlightTick(todayTick)
        highlightLabel(todayLbl)
        startDate = Date()
        endDate = Date()
        setLabels()
        prefixTitle = NSLocalizedString("Home_TraCuocLabel_HomNay", comment: "")
    }
    
    @IBAction func thisWeekTapped(_ sender: UIButton) {
        searchThisWeek()
    }
    
    func searchThisWeek() {
        highlightTick(thisWeekTick)
        highlightLabel(thisWeekLbl)
        startDate = Util.getFirstDateOfThisWeek()
        endDate = Date()
        setLabels()
        prefixTitle = NSLocalizedString("Home_TraCuocLabel_TuanNay", comment: "")
    }
    
    @IBAction func thisMonthTapped(_ sender: UIButton) {
        searchThisMonth()
    }
    
    func searchThisMonth() {
        highlightTick(thisMonthTick)
        highlightLabel(thisMonthLbl)
        startDate = Util.getFirstDateOfThisMonth()
        endDate = Date()
        setLabels()
        prefixTitle = NSLocalizedString("Home_TraCuocLabel_ThangNay", comment: "")
    }
    
    @IBAction func lastMonthTapped(_ sender: UIButton) {
        searchLastMonth()
    }
    
    func searchLastMonth() {
        highlightTick(lastMonthTick)
        highlightLabel(lastMonthLbl)
        startDate = Util.getFirstDateOfLastMonth()
        endDate = Util.getLastDateOfLastMonth()
        setLabels()
        prefixTitle = NSLocalizedString("Home_TraCuocLabel_ThangTruoc", comment: "")
    }
    
    func setLabels() {
        startDateLbl.text = dateFormatterFull.string(from: startDate)
        endDateLbl.text = dateFormatterFull.string(from: endDate)
        startDateSymbol.text = dateFormatterShort.string(from: startDate)
        endDateSymbol.text = dateFormatterShort.string(from: endDate)
    }
    
    // MARK: - Support functions
    
    func presentCalendar(_ dateStatus: DateStatus) {
        let calendarVC = UIStoryboard.getViewController(storyBoardName: "ChargeHistory", viewControllerName: "calendarVC") as! CalendarViewController
        
        calendarVC.modalPresentationStyle = .overCurrentContext
        if dateStatus == .start {
            calendarVC.startDate = startDate
            calendarVC.postAction = {
                self.startDate = calendarVC.startDate
                self.startDateLbl.text = self.dateFormatterFull.string(from: self.startDate)
                self.startDateSymbol.text = self.dateFormatterShort.string(from: self.startDate)
                self.prefixTitle = ""
                self.unhighlightAllTicks()
                self.unhighlightAllLabels()
            }
        } else {
            calendarVC.earliestDate = startDate
            calendarVC.startDate = endDate
            calendarVC.postAction = {
                self.endDate = calendarVC.startDate
                self.endDateLbl.text = self.dateFormatterFull.string(from: self.endDate)
                self.endDateSymbol.text = self.dateFormatterShort.string(from: self.endDate)
                self.prefixTitle = ""
                self.unhighlightAllTicks()
                self.unhighlightAllLabels()
            }
        }
        present(calendarVC, animated: true, completion: nil)
    }
    
    func closeTab() {
        dismiss(animated: true, completion: nil)
        popToRoot()
    }
    
    func highlightTick(_ imageView: UIImageView) {
        currentTick?.image = #imageLiteral(resourceName: "ic_tick")
        imageView.image = #imageLiteral(resourceName: "ic_tick_s")
        currentTick = imageView
    }
    
    func unhighlightAllTicks() {
        currentTick = nil
        let ticks = [todayTick, thisWeekTick, thisMonthTick, lastMonthTick]
        ticks.forEach { $0?.image = #imageLiteral(resourceName: "ic_tick") }
    }
    
    func highlightLabel(_ label: UILabel) {
        unhighlightAllLabels()
        label.textColor = Const.COLOR_MAIN
    }
    
    func unhighlightAllLabels() {
        let labels = [todayLbl, thisWeekLbl, thisMonthLbl, lastMonthLbl]
        labels.forEach { $0?.textColor = Const.COLOR_GRAY_FOR_LABEL }
    }
    
    @IBAction func searchTapped(_ sender: UIButtonRegular01) {
        dismiss(animated: true, completion: nil)
        resetNavTitle()
    }
}

















