//
//  ServiceCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/27/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
}
