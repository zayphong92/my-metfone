//
//  ContactCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
}
