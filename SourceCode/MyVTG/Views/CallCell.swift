//
//  CallCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CallCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var charge: UILabel!
    @IBOutlet weak var duration: UILabel!
    
}
