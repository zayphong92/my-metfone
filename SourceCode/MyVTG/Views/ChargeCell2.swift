//
//  ChargeCell2.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChargeCell2: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
}
