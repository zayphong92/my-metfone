//
//  AppCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class AppCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var explanationLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    
}
