//
//  DataPackageLevelCell.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DataVolumeLevelCell: UITableViewCell {
    let TAG_LABEL_VOLUME: Int = 1000
    let TAG_LABEL_PRICE: Int = 1001
    let TAG_LINE_LEFT: Int = 1002
    let TAG_LINE_RIGHT: Int = 1003
    let TAG_CIRCLE: Int = 1004
    let circleNormal: String = "gray_circle"
    let circleHilight: String = "orange_circle"
    let lineColorNormal: UIColor = Const.COLOR_TEXT_TAB_N
    let lineColorHilight: UIColor = Const.COLOR_MAIN
    let lb1ColorHilight: UIColor = Const.COLOR_MAIN
    let lb2ColorHilight: UIColor = Const.COLOR_TEXT_DLG_COLOR_01
    
    @IBOutlet weak var cellView: UIView!
    
    var listModels: [ModelDataVolumeLevel] = []
    var listItemViews: NSMutableArray = []
    private var selectedItem: ModelDataVolumeLevel? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUICell()
    }
    
    func setupUICell() {
        let itemHeight: CGFloat = 80.0
        let itemWidth: CGFloat = ScreenSize.WIDTH / CGFloat(listModels.count)
        let circleSize: CGFloat = 20.0
        let labelHeight: CGFloat = 20.0
        let lineWidth: CGFloat = (itemWidth - circleSize)/2
        for i in 0..<listModels.count {
            let model: ModelDataVolumeLevel = listModels[i]
            let x = itemWidth*CGFloat(i)
            let itemView: UIView = UIView.init(frame: CGRect(x, 40, itemWidth, itemHeight))
            let lb1: UILabel = UILabelRegular01_DataVolumeLevel.init(frame: CGRect(0, 0, itemWidth, labelHeight))
            lb1.tag = TAG_LABEL_VOLUME
            lb1.text = "\(model.volume) MB"
            let lb2: UILabel = UILabelRegular01_DataVolumeLevel.init(frame: CGRect(0, itemHeight-labelHeight, itemWidth, labelHeight))
            lb2.tag = TAG_LABEL_PRICE
            lb2.text = Util.formatCurrency(NSNumber(value: model.price))
            let imvX = (itemWidth-circleSize)/2
            let imvY = (itemHeight-circleSize)/2
            let imv: UIImageView = UIImageView.init(frame: CGRect(imvX, imvY, circleSize, circleSize))
            imv.tag = TAG_CIRCLE
            imv.image = UIImage.init(named: circleNormal)
            itemView.addSubview(lb1)
            itemView.addSubview(lb2)
            itemView.addSubview(imv)
            let lineLeft: UIImageView = UIImageView.init(frame: CGRect(0, itemHeight/2-1, lineWidth, 2))
            lineLeft.tag = TAG_LINE_LEFT
            lineLeft.backgroundColor = lineColorNormal
            itemView.addSubview(lineLeft)
            let lineRight: UIImageView = UIImageView.init(frame: CGRect(imvX+circleSize, itemHeight/2-1, lineWidth, 2))
            lineRight.tag = TAG_LINE_RIGHT
            lineRight.backgroundColor = lineColorNormal
            itemView.addSubview(lineRight)
            if (i == 0) {
                lineLeft.isHidden = true
            }
            if (i == listModels.count - 1) {
                lineRight.isHidden = true
            }
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.onItemSelect(sender:)))
            itemView.tag = i
            itemView.addGestureRecognizer(gesture)
            listItemViews.add(itemView)
            cellView.addSubview(itemView)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func onItemSelect(sender : UITapGestureRecognizer) {
        let index: Int = (sender.view?.tag)!
        selectedItem = listModels[index]
        for i in 0..<listModels.count {
            let itemView: UIView = listItemViews.object(at: i) as! UIView
            let lb1: UILabel = itemView.viewWithTag(TAG_LABEL_VOLUME) as! UILabel
            let lb2: UILabel = itemView.viewWithTag(TAG_LABEL_PRICE) as! UILabel
            let circle: UIImageView = itemView.viewWithTag(TAG_CIRCLE) as! UIImageView
            let lineLeft: UIImageView = itemView.viewWithTag(TAG_LINE_LEFT) as! UIImageView
            let lineRight: UIImageView = itemView.viewWithTag(TAG_LINE_RIGHT) as! UIImageView
            if (i <= index) {
                lb1.textColor = lb1ColorHilight
                lb2.textColor = lb2ColorHilight
                circle.image = UIImage.init(named: circleHilight)
                if (i == index) {
                    lineLeft.backgroundColor = lineColorHilight
                    lineRight.backgroundColor = lineColorNormal
                } else {
                    lineLeft.backgroundColor = lineColorHilight
                    lineRight.backgroundColor = lineColorHilight
                }
            } else {
                lb1.textColor = UILabelRegular01_DataVolumeLevel.TEXT_COLOR
                lb2.textColor = UILabelRegular01_DataVolumeLevel.TEXT_COLOR
                circle.image = UIImage.init(named: circleNormal)
                lineLeft.backgroundColor = lineColorNormal
                lineRight.backgroundColor = lineColorNormal
            }
        }
    }
    
    func getSelectedItem() -> ModelDataVolumeLevel? {
        return selectedItem
    }
}
