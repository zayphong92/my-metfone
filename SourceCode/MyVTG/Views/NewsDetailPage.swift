//
//  NewsDetailPage.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class NewsDetailPage: CellCarouselItem {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imvCover: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var lbContentHeightConstraint: NSLayoutConstraint!
    
    var owner: NewsDetailVC!

    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.backgroundColor = UIColor.white
    }

    override func getData() -> String {
        let newsItem: ModelNews = owner!.getModelNewsAt(index)
        let newsDetail = owner!.getNewsDetailById(newsItem.id)
        if (newsDetail != nil) {
            alreadyHasData()
            bindData(newsDetail!)
        } else {
            _ = super.getData()
            let params = ["newsId": newsItem.id]
            owner!.requestObject(Client.WS_GET_NEWS_DETAIL, params: params, complete: {(success: Bool, data: ModelNewsDetail?) -> Void in
                let shouldBind = self.shouldBindData(newsItem.id)
                if (data != nil) {
                    self.owner!.putNewsDetail(data!)
                    if (shouldBind) {
                        self.bindData(data!)
                    }
                }
                if (shouldBind) {
                    self.getDataDone(success && data != nil)
                }
            })
        }
        return newsItem.id
    }
    
    override func setContentHidden(_ visible: Bool) {
        scrollView.isHidden = visible
    }
    
    private func bindData(_ newsDetail: ModelNewsDetail) {
        Util.setImage(imvCover, url: newsDetail.imgDesUrl)
        lbTitle.text = newsDetail.name
        lbContent.setHTMLFromString(htmlText: newsDetail.content)
        lbContent.sizeToFit()
        lbContentHeightConstraint.constant = lbContent.frame.size.height
        let contentH = lbContent.frame.origin.y + lbContentHeightConstraint.constant + 20
        scrollView.contentSize = CGSize(getSize().width, contentH)
    }
}
