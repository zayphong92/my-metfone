//
//  ChangeDataDetailPage.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/29/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChangeDataDetailPage: CellCarouselItem {
    
    @IBOutlet weak var mainLayout: UIView!
    @IBOutlet weak var corverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabelLight01!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var actionButton: UIButtonRegular01!
    
    var controller: ChangeDataDetailVC!
    
    override func getData() -> String {
        let packageItem:ModelUserService = controller.getPackageItemAtIndex(index)
        let packageDetail = controller.getPackageDetailByCode(packageItem.code)
        if (packageDetail != nil) {
            alreadyHasData()
            bindData(packageDetail!)
            return packageItem.code
        }
        
        //Load data from cached
        let cacheData:ModelDataPackageInfo? = ModelHelper.loadFromCache(subCacheKey: packageItem.code)
        if let cacheData = cacheData {
            self.bindData(cacheData)
        } else {
            _ = super.getData()
        }
        
        let params = [
            "packageCode": packageItem.code
        ]
        controller!.requestObject(Client.WS_GET_DATA_PACKAGE_INFO, params: params, subCacheKey:packageItem.code, complete: {(success: Bool, data: ModelDataPackageInfo?) -> Void in
            let shouldBind = self.shouldBindData(packageItem.code)
            if let data = data {
                self.controller!.savePackageDeatil(data)
                if (shouldBind) { self.bindData(data) }
            }
            
            if (shouldBind) {
                self.getDataDone(success && data != nil)
            }
        })
        return packageItem.code
    }
    
    override func setContentHidden(_ visible: Bool) {
        mainLayout.isHidden = visible
    }
    
    func bindData(_ packageInfo: ModelDataPackageInfo) {
        Util.setImage(corverImageView, url: packageInfo.imgDesUrl)
        nameLabel.isHidden = false
        nameLabel.text = packageInfo.name
        contentLabel.isHidden = false
        contentLabel.setHTMLFromString(htmlText: packageInfo.fullDes)
        
        actionButton.isHidden = false
        actionButton.isHidden = packageInfo.regState == 1 ? true : false
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
    }
    
    func doAction() {
        let packageItem:ModelUserService = controller.getPackageItemAtIndex(index)
        controller!.registerDataPackage(packageItem)
    }
    
}
