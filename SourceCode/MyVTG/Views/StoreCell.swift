//
//  StoreCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/6/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class StoreCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
}
