//
//  ServiceRow.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

protocol ServiceRowDelegate {
    func didSelectItemAt(_ section: Int, row: Int)
}

class ServiceRow: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var data: [ModelServiceItem]?
    var delegate: ServiceRowDelegate?
    var currentSection: Int = 0
}

extension ServiceRow: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (data?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath) as! ServiceCollectionCell
        let service = data?[indexPath.item]
        Util.setImage(cell.image, url: service!.iconUrl, defaultImageName: nil)
        
        cell.tvTitle.text = service!.name
        cell.tvTitle.numberOfLines = 1
        cell.tvDes.numberOfLines = 2
        cell.tvDes.contentMode = .topLeft
        cell.tvDes.text = service!.getShortDes()
            
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (delegate != nil) {
            delegate!.didSelectItemAt(currentSection, row: indexPath.item)
        }
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        
//        return CGSize(width: 120, height: 100)
//    }
 
}
