//
//  NewsCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabelRegular02!
    var id: String = ""
}
