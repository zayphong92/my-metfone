//
//  CustomerServiceCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/1/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CustomerServiceCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
