//
//  UILanguageSwitch.swift
//  TestProduct
//
//  Created by Pham Van Ngoc on 11/9/17.
//  Copyright © 2017 mq. All rights reserved.
//

import UIKit

class UILanguageSwitch: UIView {
    
    //MARK: Constant
    fileprivate let CIRCLE_BORDER_COLOR                            = "#E0E0E0"
    fileprivate let CIRCLE_BG_COLOR                                = "#F03031"
    fileprivate let PANEL_BORDER_COLOR                             = "#BDBDBD"
    fileprivate let PANEL_BG_COLOR                                 = "#F5F5F5"
    fileprivate let LABEL_LEFT_RIGHT_FONT_SIZE: CGFloat            = 14
    fileprivate let LABEL_CIRCLE_FONT_SIZE: CGFloat                = 14
    fileprivate let PANEL_PADDING: CGFloat                         = 8.0
    fileprivate let PANEL_BORDER_WIDTH:CGFloat                     = 3.0
    fileprivate let CIRCLE_BORDER_WIDTH:CGFloat                    = 2.0
    
    //MARK: Properties
    fileprivate var circleView: UIView?
    fileprivate var circleLbl : UILabel?
    fileprivate var bgPanel : UIView?
    fileprivate var lblPanel : UIView?
    fileprivate var lblLeft : UILabel?
    fileprivate var lblRight : UILabel?
    
    var IsAnimation:Bool = true
    
    var IsOpen : Bool = false {
        didSet{
            circleLbl?.text =  !IsOpen ?  TitleOne : TitleTwo
            
            let duration = IsAnimation ? 0.25 : 0
            
            UIView.animate(withDuration: duration, animations: {
                if self.IsOpen {
                    self.lblLeft?.frame.origin.x = -(self.frame.size.width - self.frame.size.height/2)
                    self.lblRight?.frame.origin.x = self.frame.size.height/2 - 2
                    self.circleView?.frame.origin.x = -self.frame.size.height/2
                 
                } else {
                    self.lblLeft?.frame.origin.x = 2
                    self.lblRight?.frame.origin.x = self.frame.size.width
                    self.circleView?.frame.origin.x = self.frame.size.width - self.frame.size.height/2
        
                }
            }) { finish in
                self.ValueChanged?(self.IsOpen ? self.ValueOne : self.ValueTwo)
            }
        }
    }
    var ValueOne: String = "English" {
        didSet{
            self.lblLeft?.text = ValueOne
        }
    }
    
    var ValueTwo: String = "Laos" {
        didSet{
            self.lblRight?.text = ValueTwo
        }
    }
    
    var TitleOne: String = "LA" {
        didSet{
            self.circleLbl?.text = self.IsOpen ? TitleOne : TitleTwo
        }
    }
    var TitleTwo: String = "EN" {
        didSet{
            if self.IsOpen {
                self.circleLbl?.text = self.IsOpen ? TitleOne : TitleTwo
            }
        }
    }
    
    var ValueChanged: ((String)-> Void)?
    
    //MARK: Constructor
    override init(frame: CGRect) {
        super.init(frame: frame)
        InitView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        InitView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    fileprivate func InitView(){
        //Background
        self.layer.backgroundColor = UIColor.clear.cgColor
    
        //BG Panel
        bgPanel = UIView(frame: CGRect(x: 0, y: PANEL_PADDING/2 , width: self.frame.size.width, height: self.frame.size.height - PANEL_PADDING))
        bgPanel?.layer.borderColor = UIColor(hex: PANEL_BORDER_COLOR).cgColor
        bgPanel?.layer.borderWidth = PANEL_BORDER_WIDTH
        bgPanel?.layer.cornerRadius = (self.frame.size.height - PANEL_PADDING)/2
        bgPanel?.backgroundColor = UIColor(hex: PANEL_BG_COLOR)
        
        //Lbl Panel
        lblPanel = UIView(frame: CGRect(x: 2, y: PANEL_PADDING/2 , width: self.frame.size.width - 4, height: self.frame.size.height - PANEL_PADDING))
        lblPanel?.clipsToBounds = true
        
        //Lbl Left
        lblLeft = UILabel(frame: CGRect(x: 0 , y: 0, width: self.frame.size.width - self.frame.size.height/2 - 4, height: self.frame.size.height - PANEL_PADDING))
        lblLeft?.font = UIFont.systemFont(ofSize: LABEL_LEFT_RIGHT_FONT_SIZE, weight: UIFontWeightMedium)
        lblLeft?.textColor = UIColor.black
        lblLeft?.text = ValueOne
        lblLeft?.textAlignment = .center
        
        //Lbl Right
        lblRight = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - self.frame.size.height/2 - 4, height: self.frame.size.height - PANEL_PADDING))
        lblRight?.font = UIFont.systemFont(ofSize: LABEL_LEFT_RIGHT_FONT_SIZE, weight: UIFontWeightMedium)
        lblRight?.textColor = UIColor.black
        lblRight?.text = ValueTwo
        lblRight?.textAlignment = .center
        
        if IsOpen {
            lblLeft?.frame.origin.x = 2
            lblRight?.frame.origin.x = self.frame.size.width
        } else {
            lblLeft?.frame.origin.x = -(self.frame.size.width - self.frame.size.height/2)
            lblRight?.frame.origin.x = self.frame.size.height/2 - 2
        }
        
        lblPanel?.addSubview(lblLeft!)
        lblPanel?.addSubview(lblRight!)
        
        //Label Circle
        circleView = UIView(frame: CGRect(x: -self.frame.size.height/2, y: 0, width: self.frame.size.height, height: self.frame.size.height))
        circleView?.backgroundColor = UIColor(hex: CIRCLE_BG_COLOR)
        circleView?.layer.cornerRadius = self.frame.size.height/2
        circleView?.layer.borderWidth = CIRCLE_BORDER_WIDTH
        circleView?.layer.borderColor = UIColor(hex: CIRCLE_BORDER_COLOR).cgColor
        
        //Circle Lbl
        circleLbl = UILabel(frame: ((circleView?.frame)?.offsetBy(dx: self.frame.size.height/2, dy: 0))!)
        circleLbl?.font = UIFont.systemFont(ofSize: LABEL_CIRCLE_FONT_SIZE, weight: UIFontWeightBold)
        circleLbl?.textColor = UIColor.white
        circleLbl?.text = IsOpen ? TitleOne : TitleTwo
        circleLbl?.textAlignment = .center
        circleView?.addSubview(circleLbl!)
        
        self.addSubview(bgPanel!)
        self.addSubview(lblPanel!)
        self.addSubview(circleView!)
        
        let singleFingerTap = UITapGestureRecognizer(target: self , action: #selector(SwitchTouchEvent(sender:)))
        self.isUserInteractionEnabled = true;
        self.addGestureRecognizer(singleFingerTap)
    }
    
    @objc func SwitchTouchEvent(sender: UIGestureRecognizer){
            if self.IsOpen {
                self.IsOpen = false
            } else {
                self.IsOpen = true
            }
      
    }
}

extension UIColor {
    convenience init(hex: String) {
        
        let noHashString = hex.replacingOccurrences(of: "#", with: "")
        let scanner:Scanner = Scanner(string: noHashString)
        scanner.charactersToBeSkipped = CharacterSet.symbols
        
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
