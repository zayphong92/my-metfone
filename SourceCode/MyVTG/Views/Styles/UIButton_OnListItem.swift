//
//  UIBtn01.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/9/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Button Login
//

import UIKit

class UIButton_OnListItem: UIButtonRegular01 {
    
    override func setupInternal(){
        super.setupInternal()
        self.titleLabel?.font = UIFont(name: "roboto-regular", size: 13)
    }

}
