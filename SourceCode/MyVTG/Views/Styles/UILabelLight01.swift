//
//  UILabelLight01.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Title Label - I-Share
//

import UIKit

class UILabelLight01: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-light", size: 17)!
        self.textColor = Const.COLOR_MAIN
    }
}
