//
//  UILabelRegular01.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Title - Mi30 - Change Data Package VC
//

import UIKit

class UILabelRegular01_DataVolumeLevel: UILabel {
    static let TEXT_COLOR = Const.COLOR_TEXT_TAB_N
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-regular", size: 14)!
        self.textColor = UILabelRegular01_DataVolumeLevel.TEXT_COLOR
        self.textAlignment = .center
    }
}
