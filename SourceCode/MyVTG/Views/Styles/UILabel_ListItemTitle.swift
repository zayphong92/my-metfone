//
//  UILabelRegular01.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Title - Mi30 - Change Data Package VC
//

import UIKit

class UILabel_ListItemTitle: UILabel {
    let TEXT_COLOR = Const.COLOR_TEXT_DLG_COLOR_01
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-medium", size: 14)!
        self.textColor = TEXT_COLOR
    }
}
