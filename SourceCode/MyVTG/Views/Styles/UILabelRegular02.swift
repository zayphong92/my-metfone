//
//  UILabelRegular02.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Subtitle - expires in 30 days - Change Data Packge VC
//

import UIKit

class UILabelRegular02: UILabel {
    static let TEXT_COLOR = UIColor.argbValue(hexValue: 0xff8c8c8c)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-regular", size: 13)!
        self.textColor = UILabelRegular02.TEXT_COLOR
    }
}
