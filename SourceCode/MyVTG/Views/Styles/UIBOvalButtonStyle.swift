//
//  UIBorderButtonStyle.swift
//  TestProduct
//
//  Created by Pham Van Ngoc on 11/9/17.
//  Copyright © 2017 mq. All rights reserved.
//

import UIKit

class UIOvalButtonStyle: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.size.height/2
    }

}
