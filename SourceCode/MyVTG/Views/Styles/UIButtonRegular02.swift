//
//  UIButtonRegular02.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Button Cancel - DlgConfirmYesNo
//

import UIKit

class UIButtonRegular02: UIButton {
    static let BG_NORMAL = UIColor.argbValue(hexValue: 0xfff2f2f2)
    static let BG_HILIGHT = UIColor.argbValue(hexValue: 0xff646464)
    static let TEXT_COLOR_NORMAL = UIButtonRegular02.BG_HILIGHT
    static let TEXT_COLOR_HILIGHT = UIButtonRegular02.BG_NORMAL
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupInternal()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal(){
        self.titleLabel?.font = UIFont(name: "roboto-regular", size: 16)
        self.setBackgroundImage(UIImage.imageWithColor(color: UIButtonRegular02.BG_NORMAL), for: .normal)
        self.setBackgroundImage(UIImage.imageWithColor(color: UIButtonRegular02.BG_HILIGHT), for: .highlighted)
        self.setTitleColor(UIButtonRegular02.TEXT_COLOR_NORMAL, for: .normal)
        self.setTitleColor(UIButtonRegular02.TEXT_COLOR_HILIGHT, for: .highlighted)
        self.layer.masksToBounds = true
        self.layer.cornerRadius  = 3.0
    }
    
}
