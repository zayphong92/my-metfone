//
//  UISplashView.swift
//  TestProduct
//
//  Created by Pham Van Ngoc on 11/9/17.
//  Copyright © 2017 mq. All rights reserved.
//

import UIKit

class UISplashView: UIView {
    
    //Constant
    fileprivate let START_COLOR = "#FBD901"
    fileprivate let END_COLOR =   "#F1513A"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        InitView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        InitView()
    }

    init() {
        super.init(frame: CGRect.zero)
    }
    
    fileprivate func InitView(){
        
        // Create Gradient Color View
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [UIColor(hex: START_COLOR).cgColor, UIColor(hex: END_COLOR).cgColor]
        gradientLayer.locations = [0.0,1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
