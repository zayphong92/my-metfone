//
//  UITextViewRegular01.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Content Text View - I-Share
//

import UIKit

class UITextViewRegular01: UITextView {
    let TEXT_COLOR = Const.COLOR_TEXT_DLG_COLOR_01
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-regular", size: 14)!
        self.textColor = TEXT_COLOR
        setStyleSimilarUILabel()
    }
}
