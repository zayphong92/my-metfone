//
//  UILabelRegular01.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Title - Mi30 - Change Data Package VC
//

import UIKit

class UILabelRegular01: UILabel {
    static let TEXT_COLOR = Const.COLOR_TEXT_DLG_COLOR_01 //UIColor.argbValue(hexValue: 0xff323232)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-bold", size: 15)!
        self.textColor = UILabelRegular01.TEXT_COLOR
    }
}
