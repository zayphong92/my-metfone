//
//  UITextView_ListItemTitle.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class UITextView_DetailTitle: UITextView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setupInternal()
    }
    
    private func setupInternal() {
        font = UIFont(name: "roboto-light", size: 17)!
        textColor = Const.COLOR_MAIN
        textContainer.lineBreakMode = .byTruncatingTail
        setStyleSimilarUILabel()
    }

}
