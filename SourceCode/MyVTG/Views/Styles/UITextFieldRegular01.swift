//
//  UITextFieldRegular01.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/9/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  TextField with Orange bottom border - Login
//

import UIKit

class UITextFieldRegular01: SkyFloatingLabelTextField {
    static let TEXT_COLOR = UIColor.argbValue(hexValue: 0xff636466)
    static let BG_TEXTFIELD_ORANGE = #imageLiteral(resourceName: "bg_textfield_orange")
    static let BG_TEXTFIELD_GRAY = #imageLiteral(resourceName: "bg_textfield_gray")
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupInternal()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        
        self.textColor = Const.COLOR_GRAY_FOR_NORMAL_TEXT
        self.selectedTextColor = Const.COLOR_BLACK_FOR_SELECTED_TEXT
        self.lineColor = Const.COLOR_ORANGE
        self.titleColor = Const.COLOR_ORANGE
        self.selectedLineColor = Const.COLOR_ORANGE
        self.selectedTitleColor = Const.COLOR_ORANGE
        self.font = UIFont(name: "roboto-regular", size: 16)!
        self.titleFont = UIFont(name: "roboto-regular", size: 15)!
        self.textAlignment = .left
        self.contentVerticalAlignment = .center
        self.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.lineHeight = 1
        self.selectedLineHeight = 1.5
    }
    
    func editWidthToAvoid(view: UIView) {
        setupRightView(frame: view.frame)
    }
    
    func setupRightView(frame: CGRect) {
        self.rightView = UIView(frame: CGRect(x: frame.origin.x - frame.size.width - 8,
                                              y: frame.origin.y,
                                              width: frame.size.width + 8,
                                              height: frame.size.height))
        self.rightViewMode = .always
    }
}







