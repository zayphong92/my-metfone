//
//  UILabelRegular03.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Phone number - Login
//

import UIKit

class UILabelRegular03: UILabel {
    static let TEXT_COLOR_ORANGE = UIColor.argbValue(hexValue: 0xfff47920)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupInternal()
    }
    
    private func setupInternal() {
        self.font = UIFont(name: "roboto-regular", size: 15)!
        self.textColor = Const.COLOR_GRAY_FOR_LABEL
    }
}





