//
//  UITextView_ListItemTitle.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/13/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class UITextView_ListItemDes: UITextView {
    let TEXT_COLOR = Const.COLOR_TEXT_HOME_COLOR_01
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInternal()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setupInternal()
    }
    
    private func setupInternal() {
        font = UIFont(name: "roboto-regular", size: 12.5)!
        textColor = TEXT_COLOR
        textContainer.lineBreakMode = .byTruncatingTail
        setStyleSimilarUILabel()
    }

}
