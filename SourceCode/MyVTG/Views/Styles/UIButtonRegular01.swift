//
//  UIBtn01.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/9/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//  Button Login
//

import UIKit

class UIButtonRegular01: UIButton {
    
    //hight
    static let BG_HILIGHT = UIColor.argbValue(hexValue: 0xff01A19A)
    static let TEXT_COLOR_NORMAL = UIButtonRegular01.BG_HILIGHT
    
    //normal
    static let BG_NORMAL = UIColor.argbValue(hexValue: 0xffD4FAFC)
    static let TEXT_COLOR_HILIGHT = UIButtonRegular01.BG_NORMAL
    
    //disable
    static let BG_DISABLE = UIColor.argbValue(hexValue: 0xfff2f2f2)
    static let TEXT_COLOR_DISABLE = UIColor.argbValue(hexValue: 0xff646464)
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupInternal()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    func setupInternal(){
        self.titleLabel?.font = UIFont(name: "roboto-regular", size: 16)
        
        //highlighted
        self.setBackgroundImage(UIImage.imageWithColor(color: UIButtonRegular01.BG_HILIGHT), for: .highlighted)
        self.setTitleColor(UIButtonRegular01.TEXT_COLOR_HILIGHT, for: .highlighted)
        
        //normal
        self.setBackgroundImage(UIImage.imageWithColor(color: UIButtonRegular01.BG_NORMAL), for: .normal)
        self.setTitleColor(UIButtonRegular01.TEXT_COLOR_NORMAL, for: .normal)
        
        //disabled
        self.setBackgroundImage(UIImage.imageWithColor(color: UIButtonRegular01.BG_DISABLE), for: .disabled)
        self.setTitleColor(UIButtonRegular01.TEXT_COLOR_DISABLE, for: .disabled)
        
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius  = 3.0	
    }

}
