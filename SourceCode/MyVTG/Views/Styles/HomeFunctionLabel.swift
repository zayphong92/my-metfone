//
//  HomeFunctionLabel.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 10/20/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

class HomeFunctionLabel: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupInternal()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        setupInternal()
    }
    
    private func setupInternal() {
        if ScreenSize.WIDTH > ScreenSize.WIDTH_IPHONE6P {
            self.font = UIFont(name: "roboto-regular", size: 16)!
        } else {
            self.font = UIFont(name: "roboto-regular", size: 12)!
        }
    }

}
