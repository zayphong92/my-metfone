//
//  ChooseCityCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/5/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChooseCityCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
}
