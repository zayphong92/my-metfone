//
//  DlgConfirmYesNo.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 5/18/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DlgConfirmYesNo: BaseVC {
    
    // MARK: - Outlets
    
    @IBOutlet weak var dlgView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lineTitle: UIView!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var dlgHeight: NSLayoutConstraint!
    @IBOutlet weak var closeButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var okButtonHeight: NSLayoutConstraint!
    
    // MARK: - Variables
    
    typealias DlgConfirmResponse = () -> Void
    public var strTitle: String!
    public var strContent: String!
    public var heightOfCloseButton: Double = -1
    public var heightOfOKButton: Double = -1
    public var widthOfCloseButton: Double = -1
    public var lbOK: String!
    public var lbCancel: String!
    public var onConfirmYes: DlgConfirmResponse!
    public var onConfirmNo: DlgConfirmResponse!
    public var isCloseButtonHidden: Bool = false
    private let BTN_CORNER_RADIUS: CGFloat = 3.5
    private var defaultLbContentHeight: CGFloat = 0
    
    // MARK: - UI Handle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaultLbContentHeight = lbContent.frame.size.height
        setupView()
    }
    
    func setupView() {
        dlgView.layer.cornerRadius = BTN_CORNER_RADIUS
        

        if (lbOK != nil && lbOK.characters.count > 0) {
            btnOK.setTitle(lbOK, for: .normal)
        }
        
        if (lbCancel != nil && lbCancel.characters.count > 0) {
            btnCancel.setTitle(lbCancel, for: .normal)
        }
        
        if (strTitle == nil || strTitle.characters.count == 0) {
            hideTitle()
        } else {
            lbTitle.text = strTitle
        }
        lbContent.text = strContent
        lbContent.sizeToFit()
        let delta = lbContent.frame.size.height - defaultLbContentHeight
        let newHeight = dlgHeight.constant + delta
        if newHeight > 180 {
            dlgHeight.constant = newHeight
        }
        if heightOfCloseButton >= 0 {
            self.closeButtonHeight.constant = CGFloat(heightOfCloseButton)
        }
        if heightOfOKButton >= 0 {
            self.okButtonHeight.constant = CGFloat(heightOfOKButton)
        }
        if isCloseButtonHidden {
            self.btnCancel.isHidden = true
        } else {
            self.btnCancel.isHidden = false
        }
    }
    
    func hideTitle() {
        lbTitle.isHidden = true
        lineTitle.isHidden = true
        btnClose.isHidden = true
        let height = headerHeight.constant
        headerHeight.constant = 0;
        dlgHeight.constant -= height
    }
    
    // MARK: - Actions handle
    
    @IBAction func onBackground(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
        if (onConfirmNo != nil) {
            onConfirmNo()
        }
    }
    
    @IBAction func onBtnClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if (onConfirmNo != nil) {
            onConfirmNo()
        }
    }
    
    @IBAction func onBtnOK(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if (onConfirmYes != nil) {
            onConfirmYes()
        }
    }
    
    @IBAction func onBtnCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if (onConfirmNo != nil) {
            onConfirmNo()
        }
    }
    
}
