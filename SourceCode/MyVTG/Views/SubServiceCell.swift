//
//  SubServiceCell.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class SubServiceCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabelRegular01!
    @IBOutlet weak var descLabel: UILabelRegular02!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
