//
//  ViewPager.swift
//  ViewPager
//
//  Created by Septiyan Andika on 6/26/16.
//  Copyright © 2016 sailabs. All rights reserved.
//

import UIKit

@objc public protocol ViewPagerDataSource {
    func numberOfItems(viewPager: ViewPager) -> Int
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView
    @objc optional func didSelectedItem(index: Int)
}

public class ViewPager: UIView {
    
    // MARK: - Declare variables
    
    var pageControl: VTGPageControl = VTGPageControl()
    var scrollView: UIScrollView = UIScrollView()
    var currentPosition: Int = 1 // run from 1 to 5, different from pageNumber of pageControl, which runs from 0 to 4
    
    var dataSource: ViewPagerDataSource? = nil {
        didSet {
            reloadData()
            animationNext()
        }
    }
    
    var numberOfItems: Int = 0
    var itemViews: Dictionary<Int, UIView> = [:]
    
    // MARK: - Setup
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
        self.addSubview(scrollView)
        self.addSubview(pageControl)
        setupScrollView();
        setupPageControl();
    }
    
    func setupScrollView() {
        
        scrollView.isPagingEnabled = true
        scrollView.alwaysBounceHorizontal = false
        scrollView.bounces = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        
        let topContraints = NSLayoutConstraint(item: scrollView,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1.0,
                                               constant: 0)                     // scrollView.top = viewPager.top * 1.0 + 0
        
        let bottomContraints = NSLayoutConstraint(item: scrollView,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: NSLayoutAttribute.bottom,
                                                  multiplier: 1.0,
                                                  constant: 0)                  // scrollView.bottom = viewPager.bottom * 1.0 + 0
        
        let leftContraints = NSLayoutConstraint(item: scrollView,
                                                attribute: .leadingMargin,
                                                relatedBy: .equal,
                                                toItem: self,
                                                attribute: .leadingMargin,
                                                multiplier: 1.0,
                                                constant: 0)                    // scrollView.leading = viewPager.leading * 1.0 + 0
        
        let rightContraints = NSLayoutConstraint(item: scrollView,
                                                 attribute: .trailingMargin,
                                                 relatedBy: .equal,
                                                 toItem: self,
                                                 attribute: .trailingMargin,
                                                 multiplier: 1.0,
                                                 constant: 0)                   // scrollView.trailing = viewPager.trailing * 1.0 + 0
        
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([topContraints, rightContraints, leftContraints, bottomContraints])
    }
    
    func setupPageControl() {
        self.pageControl.numberOfPages = numberOfItems
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = UIColor.clear
        self.pageControl.currentPageIndicatorTintColor = UIColor.clear
        self.pageControl.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
        let heightContraints = NSLayoutConstraint(item: pageControl,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutAttribute.notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: 30)                   // pageControl.height = 25
        let bottomContraints = NSLayoutConstraint(item: pageControl,
                                                  attribute: .top,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: NSLayoutAttribute.top,
                                                  multiplier: 1.0,
                                                  constant: 0)                    // pageControl.top = viewPager.top * 1.0 + 0
        let leftContraints = NSLayoutConstraint(item: pageControl,
                                                attribute: .leadingMargin,
                                                relatedBy: .equal,
                                                toItem: self,
                                                attribute: .leadingMargin,
                                                multiplier: 1.0,
                                                constant: 0)                      // pageControl.leading = viewPager.leading * 1.0 + 0
        let rightContraints = NSLayoutConstraint(item: pageControl,
                                                 attribute: .trailingMargin,
                                                 relatedBy: .equal,
                                                 toItem: self,
                                                 attribute: .trailingMargin,
                                                 multiplier: 1.0,
                                                 constant: 0)                     // pageControl.trailing = viewPager.trailing * 1.0 + 0
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([heightContraints, rightContraints, leftContraints, bottomContraints])
    }
    
    // MARK: - Load views
    
    // Load views for one previous and one following pages. That's enough
    func reloadViews(index: Int) {
        for i in (index - 1)...(index + 1) {
            if i >= 0 && i < numberOfItems {
                loadViewAtIndex(index: i)
            }
        }
    }
    
    // Load view at a specific index
    func loadViewAtIndex(index: Int) {
        let view: UIView?
        if dataSource != nil {
            view = (dataSource?.viewAtIndex(viewPager: self, index: index, view: itemViews[index]))!
        } else {
            view = UIView()
        }
        
        setFrameForView(view: view!, index: index);
        
        if itemViews[index] == nil {
            itemViews[index] = view
            let tap = UITapGestureRecognizer(target: self, action:  #selector(self.handleTapSubView))
            tap.numberOfTapsRequired = 1
            itemViews[index]!.addGestureRecognizer(tap)
            
            scrollView.addSubview(itemViews[index]!)
        } else {
            itemViews[index] = view
        }
    }
    
    // Set frame for view of each page
    func setFrameForView(view: UIView, index: Int) {
        view.frame = CGRect(x: self.scrollView.frame.width * CGFloat(index),
                            y: 0,
                            width: self.scrollView.frame.width,
                            height: self.scrollView.frame.height)
    }
    
    // What to do next when a page is selected
    func handleTapSubView() {
        if dataSource?.didSelectedItem != nil {
            dataSource?.didSelectedItem!(index: currentPosition)
        }
    }
    
    func reloadData() {
        
        if dataSource != nil {
            numberOfItems = (dataSource?.numberOfItems(viewPager: self))!
        }
        
        self.pageControl.numberOfPages = numberOfItems
        
        itemViews.removeAll()
        
        for view in self.scrollView.subviews {
            view.removeFromSuperview()
        }
        
        DispatchQueue.main.async {
            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(self.numberOfItems),
                                                 height: self.scrollView.frame.height)
            self.reloadViews(index: 0)
        }
    }
}

// MARK: - Move actions with views

extension ViewPager: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: scrollView)
        self.perform(#selector(self.scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        var pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageNumber = pageNumber + 1
        pageControl.currentPage = Int(pageNumber)
        currentPosition = pageControl.currentPage
        scrollToPage(index: Int(pageNumber))
    }
}

extension ViewPager {
    
    // Make viewPager automatically move to next page after 1 sec
    func animationNext() {
        Timer.scheduledTimer(timeInterval: 1.5,
                             target: self,
                             selector: #selector(ViewPager.moveToNextPage),
                             userInfo: nil,
                             repeats: true)
    }
    
    // Scroll to page needed then increase index by 1
    func moveToNextPage() {
        if currentPosition <= numberOfItems && currentPosition > 0 {
            scrollToPage(index: currentPosition)
            currentPosition = currentPosition + 1
            
            if currentPosition > numberOfItems {
                currentPosition = 1
            }
        }
    }
    
    // Move page function
    func scrollToPage(index: Int) {
        if index <= numberOfItems && index > 0 {
            let zIndex = index - 1
            let iframe = CGRect(x: self.scrollView.frame.width * CGFloat(zIndex),
                                y: 0,
                                width: self.scrollView.frame.width,
                                height: self.scrollView.frame.height)
            
            scrollView.setContentOffset(iframe.origin, animated: true)
            pageControl.currentPage = zIndex
            reloadViews(index: zIndex)
            currentPosition = index
        }
    }
}
