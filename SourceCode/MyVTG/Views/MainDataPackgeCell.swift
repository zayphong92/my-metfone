//
//  BuyData_PackageCell.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

@objc protocol DataPackageCellDelegate {
    @objc func didSelectedVolumn(volumnIndex: Int, button: UIButtonRegular01)
}

class MainDataPackgeCell: UITableViewCell {

    @IBOutlet weak var infoLayout: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabelRegular01!
    @IBOutlet weak var expriedLabel: UILabelRegular02!
    @IBOutlet weak var remainDataLabel: UILabelRegular02!
    
    @IBOutlet weak var volumesLayout: UIView!
    @IBOutlet weak var volumesLayoutHieghtContrain: NSLayoutConstraint!
    @IBOutlet weak var dataVolumeLayout: UIView!
    @IBOutlet weak var heightOfDataVolumnLayoutContraint: NSLayoutConstraint!
    
    @IBOutlet weak var noDataToBuyLabel: UILabel!
    @IBOutlet weak var heightOfNoDataAlert: NSLayoutConstraint!
    @IBOutlet weak var purchaseButton: UIButtonRegular01!
    @IBOutlet weak var heightOfPurchaseButton: NSLayoutConstraint!
    @IBOutlet weak var changePackageButton: UIButtonRegular01!
    
    var volumeViews: [DataVolumeView] = []
    var currentVolumeSelected = 0
    var delegate: DataPackageCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        heightOfNoDataAlert.constant = 0
    }
    
    func generateVolumeLayout(_ volumnes: [ModelPackageDataToBuy], delegate: DataPackageCellDelegate? = nil ) {
        dataVolumeLayout.isHidden = false
        volumesLayout.removeAllSubviews()
        volumeViews.removeAll()
        self.delegate = delegate
        
//        let parentWidth = UIScreen.main.bounds.width - CGFloat(10*2)
        let parentWidth = self.bounds.width - CGFloat(10 * 2)
        let itemPerLine = volumnes.count < 4 ?  volumnes.count : 4
        
        let itemWidth: CGFloat = parentWidth/CGFloat(itemPerLine)
        let itemHeight: CGFloat = 86
        var line: Int = 0
        let countLine = volumnes.count % itemPerLine == 0 ? volumnes.count/itemPerLine : volumnes.count/itemPerLine + 1
        volumesLayoutHieghtContrain.constant = CGFloat(countLine) * itemHeight
        heightOfDataVolumnLayoutContraint.constant = volumesLayoutHieghtContrain.constant + 30.0
        for (i, vol)in volumnes.enumerated() {
            line = i/itemPerLine + 1
            let x: CGFloat = CGFloat(i%itemPerLine) * itemWidth
            let y: CGFloat =  CGFloat(line - 1) * itemHeight
            let view: DataVolumeView = DataVolumeView.init(frame:CGRect.init( x, y, itemWidth, itemHeight))
            
            view.volumnLabel.text = Util.formatMB(Int(vol.volume))
            view.priceLabel.text = Util.formatCurrency(NSNumber(value: vol.price))
            
            if (i == 0) {
                view.leftView.isHidden = true
            } else if(i == volumnes.count - 1) {
                view.rightView.isHidden = true
            }
            view.tag = i
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didVolumnSelected(_:))))
            volumeViews.append(view)
            volumesLayout.addSubview(view)
        }
        selectVolumn(volumnIndex: 0)
    }
    
    func didVolumnSelected(_ sender: UITapGestureRecognizer ) {
        selectVolumn(volumnIndex: (sender.view?.tag)!)
    }
    
    private func selectVolumn( volumnIndex: Int) {
        currentVolumeSelected = volumnIndex
        for (i, view ) in volumeViews.enumerated() {
            if (i < currentVolumeSelected) {
                view.isSelected(true, isSelectedVolumn: false)
            } else if (i == currentVolumeSelected){
                view.isSelected(true, isSelectedVolumn: true)
            } else {
                view.isSelected(false, isSelectedVolumn: false)
            }
        }
        delegate?.didSelectedVolumn(volumnIndex: currentVolumeSelected, button: purchaseButton)
    }
    
    
    func hidenDataVolumnLayout() {
        dataVolumeLayout.isHidden = true
        volumesLayoutHieghtContrain.constant = 0
        heightOfDataVolumnLayoutContraint.constant = 0
    }
}
















