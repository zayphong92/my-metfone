//
//  CustomServiceHeader.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CustomServiceHeader: UITableViewCell {
    
    var onSeeAll: () -> Void = {}
    @IBOutlet weak var headerLabel: UILabel!
    @IBAction func onSeeAll(_ sender: UIButton) {
        onSeeAll()
    }
}
