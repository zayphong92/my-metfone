//
//  LoyaltySchemeServiceCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class LoyaltySchemeServiceCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
