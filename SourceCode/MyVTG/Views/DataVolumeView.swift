//
//  DataVolumeView.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class DataVolumeView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var volumnLabel: UILabel!
    @IBOutlet weak var centerCicrleView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("DataVolumeView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        centerCicrleView.layer.cornerRadius = centerCicrleView.frame.size.width/2
    }
    
    func isSelected(_ isSelected: Bool, isSelectedVolumn: Bool) {
        
        if(isSelected) {
            volumnLabel.textColor = Const.COLOR_ORANGE
            centerCicrleView.backgroundColor = Const.COLOR_ORANGE
            leftView.backgroundColor = Const.COLOR_ORANGE
            rightView.backgroundColor = isSelectedVolumn ? Const.COLOR_GRAY: Const.COLOR_ORANGE
        } else {
            volumnLabel.textColor = Const.COLOR_GRAY
            centerCicrleView.backgroundColor = Const.COLOR_GRAY
            leftView.backgroundColor = Const.COLOR_GRAY
            rightView.backgroundColor = Const.COLOR_GRAY
        }
    }
}
