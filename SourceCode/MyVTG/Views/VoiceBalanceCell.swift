//
//  VoiceBalanceCell.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class VoiceBalanceCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descLable: UILabel!
    @IBOutlet weak var heightOfDescContrain: NSLayoutConstraint!

}
