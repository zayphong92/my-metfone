//
//  InUseDataPackageCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/10/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class InUseDataPackageCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var dataLeftLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!    
}
