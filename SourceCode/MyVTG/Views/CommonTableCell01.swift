//
//  CommonTableCell01.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CommonTableCell01: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var button: UIButtonRegular01!
    @IBOutlet weak var nameLabel: UILabelRegular01!
    @IBOutlet weak var descLabel: UILabelRegular02!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
