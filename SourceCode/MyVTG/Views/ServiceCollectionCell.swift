//
//  ServiceCollectionCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ServiceCollectionCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tvDes: UILabel!
    @IBOutlet weak var tvTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tvDes.textColor = Const.COLOR_TEXT_DLG_COLOR_01
        tvDes.numberOfLines = 2
    }
}
