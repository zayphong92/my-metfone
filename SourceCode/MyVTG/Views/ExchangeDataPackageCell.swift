//
//  ExchangeDataPackageCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ExchangeDataPackageCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
}
