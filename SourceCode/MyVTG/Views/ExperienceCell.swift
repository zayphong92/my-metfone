//
//  ExperienceCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ExperienceCell: UITableViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    var sourceLink: String = ""
}
