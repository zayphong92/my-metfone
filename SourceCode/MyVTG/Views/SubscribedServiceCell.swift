//
//  SubscribedServiceCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class SubscribedServiceCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabelRegular01!
    @IBOutlet weak var summaryLabel: UILabelRegular02!
    
}
