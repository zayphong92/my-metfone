//
//  PromotionDatailPage.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/31/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class PromotionDatailPage: CellCarouselItem {
    
    @IBOutlet weak var mainLayout: UIView!
    @IBOutlet weak var corverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabelLight01!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var actionButton: UIButtonRegular01!
    @IBOutlet weak var buttonLayoutHeightContraint: NSLayoutConstraint!
    
    private let BUTTON_HEIGH: CGFloat = 76.0
    
    var controller: PromotionDetailVC!
    
    override func getData() -> String {
        let promotionItem: ModelPromotion = controller.getPromotionAtIndex(index)
        let promotionDetail = controller.getPromotionDetailByCode(promotionItem.code)
        if (promotionDetail != nil) {
            alreadyHasData()
            bindData(promotionDetail!)
            return promotionItem.code
        }
        _ = super.getData()
        
        let params = [
            "packageCode": promotionItem.code
        ]
        
        controller!.requestObject(Client.WS_GET_PROMOTION_INFO, params: params, complete: {(success: Bool, data: ModelPromotionInfo?) -> Void in
            
            //Test
//            let data: ModelPromotionInfo? = Mapper<ModelPromotionInfo>().map(JSONObject: [:])
//            data!.actionType = Int(arc4random_uniform(3))
//            data!.code  = promotionItem.code
//            data!.imgDesUrl = ""
//            data!.name = "Promotion \(self.index)"
//            data!.fullDes = "This is Promotion: \(self.index)"
//            data!.publishDate = Double(Date().timeIntervalSince1970.hashValue)
//            
            
            let shouldBind = self.shouldBindData(promotionItem.code)
            if (data != nil) {
                self.controller!.savePromotionDetail(data!)
                if (shouldBind) {
                    self.bindData(data!)
                }
            }
            if (shouldBind) {
                self.getDataDone(success && data != nil)
            }
        })
        return promotionItem.code
    }
    
    override func setContentHidden(_ visible: Bool) {
        mainLayout.isHidden = visible
    }
    
    func bindData(_ promotionInfo: ModelPromotionInfo) {
        Util.setImage(corverImageView, url: promotionInfo.imgDesUrl)
        nameLabel.text = promotionInfo.name
        contentLabel.setHTMLFromString(htmlText: promotionInfo.fullDes)
        
        switch promotionInfo.actionType {
        case PromotionAction.Rechange.rawValue:
            buttonLayoutHeightContraint.constant = BUTTON_HEIGH
            actionButton.localizedText = "Button_NapCuoc"
            break
            
        case PromotionAction.Register.rawValue:
            buttonLayoutHeightContraint.constant = BUTTON_HEIGH
            actionButton.localizedText = "Button_DangKy"
            break
            
        default:
            buttonLayoutHeightContraint.constant = 0
            break
        }
        actionButton.addTarget(self, action: #selector(doAction), for: .touchUpInside)
    }
    
    func doAction(_ sender: UIButton) {
        let promotion: ModelPromotion = controller.getPromotionAtIndex(index)
        let promotionDetail = controller.getPromotionDetailByCode(promotion.code)
        switch promotionDetail!.actionType {
        case PromotionAction.Rechange.rawValue:
            controller.segueToRecharge(showsTabBar: false)
            break
            
        case PromotionAction.Register.rawValue:
            controller!.registerService(serviceCode: (promotionDetail?.code)!)
            break
            
        default:
            break
        }
    }
    
    
}
