//
//  PhoneNumberCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PhoneNumberCell: UITableViewCell {
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    
}
