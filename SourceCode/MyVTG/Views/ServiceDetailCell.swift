//
//  ServiceDetailCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/7/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ServiceDetailCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var btnSub: UIButton!
}
