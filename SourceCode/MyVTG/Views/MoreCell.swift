//
//  MoreCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentCell: UIView!
    
}
