//
//  DataPackageCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/10/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class DataPackageCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabelRegular01!
    @IBOutlet weak var descLabel: UILabelRegular02!
    @IBOutlet weak var changeButton: UIButtonRegular01!
    
}
