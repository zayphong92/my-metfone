//
//  Charge.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class Charge: NSObject {
    var icon: UIImage?
    var value: ChargeTypes
    var name: String
    var amount: Double
    var postType: Int
    var quantity: Int
    
    init(icon: UIImage?, value: ChargeTypes, amount: Double = -1, postType: Int = -1, quantity: Int = -1) {
        self.icon = icon
        self.value = value
        self.name = NSLocalizedString(value.rawValue, comment: "")
        self.amount = amount
        self.postType = postType
        self.quantity = quantity
        
        super.init()
    }
}
