//
//  MoreFunction.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class MoreFunction: NSObject {
    var value: MoreFunctions
    var name: String
    var icon: UIImage?
    
    init(value: MoreFunctions, icon: UIImage?) {
        self.value = value
        self.name = NSLocalizedString(value.rawValue, comment: "")
        self.icon = icon
        
        super.init()
    }
}
