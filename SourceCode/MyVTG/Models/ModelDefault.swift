//
//  ModelDefault.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/28/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDefault: ModelBase {
    var errorCode: String = ""
    var errorMessage: String = ""

    override func mapping(map: Map) {
        errorCode <- map["errorCode"]
        errorMessage <- map["errorMessage"]
    }
    
    func isSuccess() -> Bool {
        return errorCode == Client.ERR_CODE_API_GATEWAY_SUCCESS
    }
    
}
