//
//  PromotionCell.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/22/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PromotionCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var shortDesLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var buttonWidthContraint: NSLayoutConstraint!
    
    let BUTTON_WIDTH:CGFloat = 100.0

    
    func hideButton() {
        buttonWidthContraint.constant = 0
    }
    
    func showButton() {
        buttonWidthContraint.constant = BUTTON_WIDTH
    }
    
}
