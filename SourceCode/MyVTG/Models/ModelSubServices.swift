//
//  ModelSubServices.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 8/16/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class ModelSubServices: ModelBase {
    
    var name:String = ""
    var code:String = ""
    var des:String = ""
    var iconUrl:String = ""
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        des <- map["des"]
        iconUrl <- map["iconUrl"]
    }
    
}
