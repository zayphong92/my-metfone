//
//  LoyaltySchemeService.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class LoyaltySchemeService: NSObject {
    var value: LoyaltySchemeServices
    var icon: UIImage
    var name: String
    init(value: LoyaltySchemeServices, icon: UIImage) {
        self.value = value
        self.name = NSLocalizedString(value.rawValue, comment: "")
        self.icon = icon
        super.init()
    }
}
