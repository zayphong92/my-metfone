//
//  JCParams.swift
//  MyVTG
//
//  Created by luatnc on 11/2/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper
import Alamofire
import SwiftyJSON

class JCParams : Mappable {
    var apiName: String?
    var apiFunctName: String?
    var confirmMsg: String?
    var okMsg: String?
    var errMsg: String?
    var actionType: String?
    var serviceCode: String?
    var params: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        apiName         <- map["apiName"]
        apiFunctName    <- map["apiFunctName"]
        confirmMsg      <- map["confirmMsg"]
        okMsg           <- map["okMsg"]
        errMsg          <- map["errMsg"]
        actionType      <- map["actionType"]
        serviceCode     <- map["serviceCode"]
        params          <- map["params"]
    }
}
