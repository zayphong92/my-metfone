//
//  ModelPostageInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/6/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelPostageInfo: ModelBase {
    var monthlyFee: Double = 0
    var basic: Double = 0
    var prom: Double = 0
    var callFee: Double = 0
    var callRc: Int = 0
    var smsFee: Double = 0
    var smsRc: Int = 0
    var otherFee: Double = 0
    var otherRc: Int = 0
    var dataFee: Double = 0
    var dataRc: Int = 0
    var vasFee: Double = 0
    var vasRc: Int = 0
    
    override func mapping(map: Map) {
        monthlyFee <- map["monthlyFee"]
        basic <- map["basic"]
        prom <- map["prom"]
        callFee <- map["callFee"]
        smsFee <- map["smsFee"]
        otherFee <- map["otherFee"]
        callRc <- map["callRc"]
        smsRc <- map["smsRc"]
        otherRc <- map["otherRc"]
        dataFee <- map["dataFee"]
        dataRc <- map["dataRc"]
        vasFee <- map["vasFee"]
        vasRc <- map["vasRc"]
    }
}
