//
//  ModelNews.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelNews: ModelBase {
    var id: String = ""
    var name: String = ""
    var iconUrl: String = ""
    var publishDate: Double = 0
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        iconUrl <- map["iconUrl"]
        publishDate <- map["publishDate"]
    }
}
