//
//  ModelNewsDetail.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelNewsDetail: ModelBase {
    var id: String = ""
    var name: String = ""
    var imgDesUrl: String = ""
    var content: String = ""
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        imgDesUrl <- map["imgDesUrl"]
        content <- map["content"]
    }
}
