//
//  CityStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CityStore {
    var allCities = [City]()
    var giftStore = GiftStore()

    func initCities() {
        giftStore.initGifts()
        for i in 0...49 {
            allCities.append(City())
            allCities[i] = City(name: "City \(i+1)")
            allCities[i].giftList = [giftStore.allGifts[2*i], giftStore.allGifts[2*i+1]]
            for j in 0...9 {
                allCities[i].districtList.append(District(name: "District \(j+1) of city \(i+1)", ward: ["1", "2", "3", "4", "5"]))
            }
        }
    }
}
