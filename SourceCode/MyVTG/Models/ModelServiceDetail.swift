//
//  ModelServiceDetail.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelServiceDetail: ModelRegistable {
    var fullDes: String = ""
    var imgDesUrl: String = ""
    var webLink: String = ""
    var isRegisterAble: Int = 0
    var packages: [ModelServiceDetailSubPackage]? = nil
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        fullDes <- map["fullDes"]
        imgDesUrl <- map["imgDesUrl"]
        webLink <- map["webLink"]
        price <- map["price"]
        isRegisterAble <- map["isRegisterAble"]
        packages <- map["packages"]
    }
    
    func hasSubPackages() -> Bool {
        return packages != nil && packages!.count > 0
    }
    
    func canRegister() -> Bool {
        return isRegisterAble == 1
    }
}
