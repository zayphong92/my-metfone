//
//  ModelBase.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper
import Alamofire

class ModelHelper {
    
    static let ud = UserDefaults.standard
    
    static func from<T: ModelBase>(response: DataResponse<Any>) -> [T]? {
        if let value = response.result.value as? [String: Any] {
            if let result = value["result"] as? [String: Any] {
                if let wsResponse = result["wsResponse"] as? [[String : Any]] {
                    return Mapper<T>().mapArray(JSONArray: wsResponse)
                } else {
                    return []
                }
            }
        }
        return nil
    }
    
    static func from<T: ModelBase>(response: DataResponse<Any>) -> T? {
        if response.result.value == nil {
            return nil
        }
        
       let value = response.result.value  as! [String: Any]
        
        if (T.self == ModelUserLogin.self || T.self == ModelDefault.self) {
            // use for API login, get OTP
            return Mapper<T>().map(JSONObject: value)
        } else {
            if let result = value["result"] as? [String: Any] {
                if (T.self == ModelDoAction.self) {
                    // only for API doActionService
                    return Mapper<T>().map(JSONObject: result)
                } else {
                    if let wsResponse = result["wsResponse"] as? [String : Any] {
                        return Mapper<T>().map(JSONObject: wsResponse)
                    }
                }
            }
        }
        return nil
    }
    
    static func createEmptyModel<T: ModelBase>() -> T {
        let map = Map(mappingType: .fromJSON, JSON: [:])
        return T.self.init(map: map)!
    }
    
    static func saveToCache<T: ModelBase>(_ model: T?, subCacheKey:String? = nil) {
        if (model == nil || T.self == ModelDoAction.self) {
            return
        }
        let key = ModelHelper.getKeyForCacheData(String.init(describing: T.self), subCacheKey)
        let json: String = (model!).toJSONString()!
        ud.set(json, forKey: key)
    }
    
    static func loadFromCache<T: ModelBase>(subCacheKey:String? = nil) -> T? {
        let key = ModelHelper.getKeyForCacheData(String.init(describing: T.self), subCacheKey)
        let json = ud.string(forKey: key)
        if (json != nil) {
            return Mapper<T>().map(JSONString: json!)
        } else {
            return nil
        }
    }
    
    private static func getKeyForCacheData(_ modelType:String, _ subCacheKey:String? = nil) -> String {
        var key: String = modelType + Client.getInstance().getIsdn()
        if modelType != String.init(describing: ModelUserLogin.self) {
            key += Client.getInstance().lang
        }
        if subCacheKey != nil { key += subCacheKey! }
        return key
    }
    
    
    static func onLogout() {
        ud.removeObject(forKey: String(describing: ModelUserLogin.self))
    }
}
