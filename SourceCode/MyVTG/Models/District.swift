//
//  District.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class District: NSObject {
    var name: String
    var ward: [String]
    
    init(name: String, ward: [String]) {
        self.name = name
        self.ward = ward
        super.init()
    }
    
    override init() {
        self.name = ""
        self.ward = [String]()
        super.init()
    }
    func set(name: String, ward: [String]) {
        self.name = name
        self.ward = ward
    }
}
