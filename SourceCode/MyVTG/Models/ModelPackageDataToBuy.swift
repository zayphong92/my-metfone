//
//  ModelPackageDataToBuy.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelPackageDataToBuy: ModelBase {
    
    var volume: Int = 0
    var price: Int = 0
    
    override func mapping(map: Map) {
        volume <- map["volume"]
        price <- map["price"]
    }
}
