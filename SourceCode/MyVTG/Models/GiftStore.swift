//
//  GiftStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class GiftStore {
    var allGifts = [Gift]()
    
    func initGifts() {
        for i in 0...99 {
            allGifts.append(Gift())
            allGifts[i] = Gift(name: "Gift \(i+1)", pointValue: Double((i+1)*1000))
        }
    }
}
