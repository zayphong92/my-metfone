//
//  ModelHomeConfig.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelHomeConfig: ModelBase {
    var isActive: Int = -1 // 0: disabled, 1: chỉ enable cho trả trước, 2: chỉ enable cho trả sau, 3: enable cho cả hai
    var localMsg: String = ""
    var enMsg: String = ""
    var localTitle: String = ""
    var enTitle: String = ""
    var normalIconUrl: String = ""
    var functType: Int = -1
    var uri: ModelFunctionName?
    
    override func mapping(map: Map) {
        isActive <- map["isActive"]
        localMsg <- map["localMsg"]
        enMsg <- map["enMsg"]
        localTitle <- map["localTitle"]
        enTitle <- map["enTitle"]
        normalIconUrl <- map["normalIconUrl"]
        functType <- map["functType"]
        uri <- map["uri"]
    }
}
