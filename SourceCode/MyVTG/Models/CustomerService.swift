//
//  CustomerService.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/1/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CustomerService: NSObject {
    var value: CustomerServiceItems
    var name: String
    var icon: UIImage
    var link: String
    
    init(value: CustomerServiceItems, icon: UIImage, link: String = "") {
        self.value = value
        self.name = NSLocalizedString(value.rawValue, comment: "")
        self.icon = icon
        self.link = link
        super.init()
    }
}
