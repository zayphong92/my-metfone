//
//  Utility.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class Utility: NSObject {
    var value: UtilitiesItems
    var name: String
    var icon: UIImage
    init(value: UtilitiesItems, icon: UIImage) {
        self.value = value
        self.name = NSLocalizedString(value.rawValue, comment: "")
        self.icon = icon
        super.init()
    }
}
