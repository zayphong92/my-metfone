//
//  ModelServiceInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/6/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelServiceInfo: ModelBase {
    var imgDesUrl: String = ""
    var name: String = ""
    var fullDes: String = ""
    var price: Double = 0
    var registerState: Int = 0
    var checkCondition: Int = 0
    var lockState: Int = 0
    
    override func mapping(map: Map) {
        imgDesUrl <- map["imgDesUrl"]
        name <- map["name"]
        fullDes <- map["fullDes"]
        price <- map["price"]
        registerState <- map["registerState"]
        checkCondition <- map["checkCondition"]
        lockState <- map["lockState"]
    }
}
