//
//  ModelUtilitiesConfig.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelUtilitiesConfig: ModelBase {
    var isEnabled: Bool = false
    
    override func mapping(map: Map) {
        isEnabled <- map["isEnaled"]
    }
}
