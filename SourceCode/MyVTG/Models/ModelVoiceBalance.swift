//
//  VoiceBalanceModel.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelVoiceBalance: ModelBase {
    var name: String = ""
    var expiredDate: Int64 = 0
    var money: Double = 0
    
    override func mapping(map: Map) {
        name <- map["name"]
        money <- map["money"]
        expiredDate <- map["expiredDate"]
    }
    
}
