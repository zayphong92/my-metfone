//
//  ModelHotNews.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/4/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelHotNews: ModelBase {
    public static let TYPE_KHUYEN_MAI = 0;
    public static let TYPE_DICH_VU = 1;
    public static let TYPE_TIN_TUC = 2;
    
    var newsId: String = ""
    var advImgUrl: String = ""
    var type: Int = 0
    
    override func mapping(map: Map) {
        newsId <- map["newsId"]
        advImgUrl <- map["advImgUrl"]
        type <- map["type"]
    }
}
