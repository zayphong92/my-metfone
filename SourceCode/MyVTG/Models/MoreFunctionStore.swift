//
//  MoreFunctionStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class MoreFunctionStore {
    var allMoreFunctions = [[MoreFunction]]()
    
    let changeLanguageFunc = MoreFunction(value: MoreFunctions.DoiNgonNgu, icon: #imageLiteral(resourceName: "ic_change_language"))
    
    func initFunctions() {
        if L102Language.currentAppleLanguage() == Const.LANG_LOCAL {
            changeLanguageFunc.name = String(format: NSLocalizedString(changeLanguageFunc.value.rawValue, comment: ""), NSLocalizedString("Language_English", comment: ""))
        } else if L102Language.currentAppleLanguage() == Const.LANG_EN {
            changeLanguageFunc.name = String(format: NSLocalizedString(changeLanguageFunc.value.rawValue, comment: ""), NSLocalizedString("Language_Laos", comment: ""))
        }
        allMoreFunctions = [
            [
                //lifesup_sprint_2_T8_2018_start
                //MoreFunction(value: MoreFunctions.KhuyenMai, icon: #imageLiteral(resourceName: "ic_gift")),
                MoreFunction(value: MoreFunctions.ChamSocKhachHang, icon: #imageLiteral(resourceName: "ic_headphone")),
                /*MoreFunction(value: MoreFunctions.KhachHangThanThiet, icon: #imageLiteral(resourceName: "ic_friends")),
                MoreFunction(value: MoreFunctions.TinTuc, icon: #imageLiteral(resourceName: "ic_news")),
                MoreFunction(value: MoreFunctions.TraiNghiem, icon: #imageLiteral(resourceName: "ic_experience")),*/
                //lifesup_sprint_2_T8_2018_end
                MoreFunction(value: MoreFunctions.DanhGia, icon: #imageLiteral(resourceName: "ic_rating"))
            ],
            [
                changeLanguageFunc,
                MoreFunction(value: MoreFunctions.DoiMatKhau, icon: #imageLiteral(resourceName: "ic_changepassword.png")),
                MoreFunction(value: MoreFunctions.DangXuat, icon: #imageLiteral(resourceName: "ic_logout"))
            ]
        ]
    }
}
