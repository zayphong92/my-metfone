//
//  CustomerServiceStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/1/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class CustomerServiceStore {
    var allCustomerSerives = [CustomerService]()
    func initCustomerServices() {
        allCustomerSerives = [
            CustomerService(value: .ChatOnline, icon: #imageLiteral(resourceName: "ic_convo"), link: "https://metfone.com.kh/en"),
            CustomerService(value: .CongDong, icon: #imageLiteral(resourceName: "ic_talk"), link: "https://www.facebook.com/metfone.closer"),
            CustomerService(value: .TongDai, icon: #imageLiteral(resourceName: "ic_headphone"), link: "1777"),
            CustomerService(value: .GuiMail, icon: #imageLiteral(resourceName: "ic_email")),
            CustomerService(value: .CauHoi, icon: #imageLiteral(resourceName: "ic_question"), link: "https://metfone.com.kh/en/support/faqs")
        ]
    }
}
