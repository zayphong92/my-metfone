//
//  ModelPromotionInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelPromotionInfo: ModelBase {
    var name: String = ""
    var code: String = ""
    var fullDes: String = ""
    var imgDesUrl: String = ""
    var publishDate: Double = 0
    var actionType: Int = 0
    // Nếu actionType = 0 No action.
    // Nếu actionType = 1 Rechange
    // Nếu actionType = 2 Register
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        fullDes <- map["fullDes"]
        imgDesUrl <- map["imgDesUrl"]
        publishDate <- map["publishDate"]
        actionType <- map["actionType"]
    }
}
