//
//  ModelPromotion.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelPromotion: ModelBase {
    var name: String = ""
    var code: String = ""
    var shortDes: String = ""
    var iconUrl: String = ""
    var publishDate: Int = 0
    var actionType: Int = 0
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        shortDes <- map["shortDes"]
        iconUrl <- map["iconUrl"]
        publishDate <- map["publishDate"]
        actionType <- map["actionType"]
    }

}
