//
//  ModelApp.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelApp: ModelBase {
    var id: String = ""
    var name: String = ""
    var shortDes: String = ""
    var iconUrl: String = ""
    var iosLink: String = ""
    var androidLink: String = ""
    var fullDes: String = ""
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        shortDes <- map["shortDes"]
        iconUrl <- map["iconUrl"]
        iosLink <- map["iosLink"]
        androidLink <- map["androidLink"]
        fullDes <- map["fullDes"]
    }
}
