//
//  ModelAccountValueDetail.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 10/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAccountValueDetail: ModelBase {
    var title: String = ""
    var value: String = ""
    var exp: String = ""
    
    override func mapping(map: Map) {
        title <- map["title"]
        value <- map["value"]
        exp <- map["exp"]
    }
}
