//
//  ModelDataAccountDetail.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDataAccountDetail: ModelBase {
    
    var totalVolume: Double = -1
    var totalVolumeStr: String = ""
    var dataPackages: [ModelDataPackage] = []
    
    override func mapping(map: Map) {
        totalVolume <- map["totalVolume"]
        totalVolumeStr <- map["totalVolumeStr"]
        dataPackages <- map["dataPackages"]
    }
}
