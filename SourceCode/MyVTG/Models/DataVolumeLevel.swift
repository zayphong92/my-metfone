//
//  DataPackageLevel.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 6/21/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import Foundation

class DataVolumeLevel: NSObject {
    var volume: Int = 0
    var price: Int = 0
    
    init(volume:Int, price:Int) {
        self.volume = volume
        self.price = price
        super.init()
    }
}
