//
//  DataPackage.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/10/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

class DataPackage: NSObject {
    
    var icon: UIImage
    var name: String
    var size: String
    var expiration: String
    var explanation: String
    
    init(icon: UIImage, name: String, size: String, expiration: String, explanation: String) {
        
        self.icon = icon
        self.name = name
        self.size = size
        self.expiration = expiration
        self.explanation = explanation
        
        super.init()
    }
    
    override init() {
        self.icon = #imageLiteral(resourceName: "Mi30")
        self.name = ""
        self.size = ""
        self.expiration = ""
        self.explanation = ""
        
        super.init()
    }
}
