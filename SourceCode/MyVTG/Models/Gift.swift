//
//  Gift.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class Gift: NSObject {
    var name: String
    var pointValue: Double
    
    init(name: String, pointValue: Double) {
        self.name = name
        self.pointValue = pointValue
        super.init()
    }
    override init() {
        self.name = ""
        self.pointValue = 0
        super.init()
    }
}
