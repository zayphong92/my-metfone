//
//  ModelHobby.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelHobby: ModelBase {
    var id: Int = 0
    var name: String = ""
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
