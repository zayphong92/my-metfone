//
//  UserAccount.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/19/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class UserAccount: NSObject {
    var avatar: UIImage
    var fullName: String
    var phoneNumber: String
    var dob: Date?
    var occupation: String
    var hobbies: String
    var email: String
    
    init(avatar: UIImage, fullName: String, phoneNumber: String, dob: Date?, occupation: String, hobbies: String, email: String) {
        self.avatar = avatar
        self.fullName = fullName
        self.phoneNumber = phoneNumber
        self.dob = dob
        self.occupation = occupation
        self.hobbies = hobbies
        self.email = email
        
        super.init()
    }
    
    override init() {
        self.avatar = #imageLiteral(resourceName: "main_data_pack")
        self.fullName = ""
        self.phoneNumber = ""
        self.dob = nil
        self.occupation = ""
        self.hobbies = ""
        self.email = ""
    }
    
    func fakeData() {
        self.avatar = #imageLiteral(resourceName: "avatar_default_gray")
        self.fullName = "Nguyen Khac Hung"
        self.phoneNumber = "0966996683"
        self.dob = Date()
        self.occupation = "Programmer"
        self.hobbies = "Soccer"
        self.email = "hungnk@gmail.com"
    }
}
