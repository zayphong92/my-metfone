//
//  ModelStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelStore: ModelBase {
    var name: String = ""
    var addr: String = ""
    var openTime: String = ""
    var provinceName: String = ""
    var districtName: String = ""
    var isdn: String = ""
    var distance: Double = 0
    var latitude: Double = 0
    var longitude: Double = 0
    
    override func mapping(map: Map) {
        name <- map["name"]
        addr <- map["addr"]
        openTime <- map["openTime"]
        provinceName <- map["provinceName"]
        districtName <- map["districtName"]
        isdn <- map["isdn"]
        distance <- map["distance"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
