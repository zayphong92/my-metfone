//
//  ModelUserService.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelUserService: ModelBase {
    
    var name: String = ""
    var code: String = ""
    var shortDes: String = ""
    var iconUrl: String = ""
    var isMultPlan: Bool = false
    var isRegisterAble: Bool = true
    var state: Int = -1

    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        shortDes <- map["shortDes"]
        iconUrl <- map["iconUrl"]
        isMultPlan <- map["isMultPlan"]
        isRegisterAble <- map["isRegisterAble"]
        state <- map["state"]
    }
    
}
