//
//  ModelFunctionName.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelFunctionName: ModelBase {
    var functionName: String = ""
    var serviceCode: String = ""

    override func mapping(map: Map) {
        functionName <- map["functionName"]
        serviceCode <- map["serviceCode"]
    }
}
