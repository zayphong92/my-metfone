//
//  ModelEmptyPage.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelEmptyPage: ModelBase {
    var name: String = ""
    var localLink: String = ""
    var enLink: String = ""
    
    override func mapping(map: Map) {
        name <- map["name"]
        localLink <- map["localLink"]
        enLink <- map["enLink"]
    }
}
