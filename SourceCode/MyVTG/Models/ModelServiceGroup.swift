//
//  ModelServiceGroup.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelServiceGroup: ModelBase {
    var groupName: String = ""
    var groupCode: String = ""
    var services: [ModelServiceItem] = []
    
    override func mapping(map: Map) {
        groupName <- map["groupName"]
        groupCode <- map["groupCode"]
        services <- map["services"]
    }
}
