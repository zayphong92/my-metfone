//
//  ModelSubInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelSubInfo: ModelBase {
    var name: String = ""
    var birthday: Double = 0
    var job: String = ""
    var hobby: String = ""
    var email: String = ""
    
    override func mapping(map: Map) {
        name <- map["name"]
        birthday <- map["birthday"]
        job <- map["job"]
        hobby <- map["hobby"]
        email <- map["email"]
    }
}
