//
//  ModelAppStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAppStore: ModelBase {
    var adBanner: [ModelAdBannerApp] = []
    var apps: [ModelApp] = []
    
    override func mapping(map: Map) {
        adBanner <- map["adBanner"]
        apps <- map["apps"]
    }
}
