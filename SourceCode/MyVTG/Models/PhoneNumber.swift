//
//  PhoneNumber.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/25/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class PhoneNumber: NSObject {
    var number: String
    var price: Double
    
    init(number: String, price: Double) {
        self.number = number
        self.price = price
        
        super.init()
    }
    
    override init() {
        self.number = ""
        self.price = 0
        
        super.init()
    }
}
