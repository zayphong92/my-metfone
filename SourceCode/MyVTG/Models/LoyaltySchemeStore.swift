//
//  LoyaltySchemeStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class LoyaltySchemeStore {
    var allLoyaltySchemeServices = [LoyaltySchemeService]()
    
    func initAllLoyaltySchemeServices() {
        allLoyaltySchemeServices = [
            LoyaltySchemeService(value: .DoiCuoc, icon: #imageLiteral(resourceName: "main_data_pack")),
            LoyaltySchemeService(value: .DoiData, icon: #imageLiteral(resourceName: "main_data_pack")),
            LoyaltySchemeService(value: .DoiQua, icon: #imageLiteral(resourceName: "main_data_pack")),
            LoyaltySchemeService(value: .ChonQua, icon: #imageLiteral(resourceName: "main_data_pack"))
        ]
    }
}
