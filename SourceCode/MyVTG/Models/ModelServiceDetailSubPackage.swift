//
//  ModelServiceDetailSubPackage.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelServiceDetailSubPackage: ModelRegistable {
    var shortDes: String = ""
    var fullDes: String = ""
    var iconUrl: String = ""
    var regisState: String = ""
    var state: Int = -1
    
    override func mapping(map: Map) {
        regisState <- map["regisState"]
        name <- map["name"]
        code <- map["code"]
        shortDes <- map["shortDes"]
        fullDes <- map["fullDes"]
        iconUrl <- map["iconUrl"]
        price <- map["price"]
        state <- map["state"]
        isRegistered = regisState.lowercased().contains("true")
    }
}
