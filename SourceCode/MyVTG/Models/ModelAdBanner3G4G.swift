//
//  ModelAdBanner3G4G.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAdBanner3G4G: ModelBase {
    var adImgUrl: String = ""
    var sourceLink: String = ""
    
    override func mapping(map: Map) {
        adImgUrl <- map["adImgUrl"]
        sourceLink <- map["sourceLink"]
    }
}
