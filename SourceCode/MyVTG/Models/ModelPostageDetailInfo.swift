//
//  ModelPostageDetailInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelPostageDetailInfo: ModelBase {
    var isdn: String = ""
    var direction: Int = -1
    var start_time: Double = 0
    var duration: Double = -1
    var value: Double = 0
    
    override func mapping(map: Map) {
        isdn <- map["isdn"]
        direction <- map["direction"]
        start_time <- map["start_time"]
        duration <- map["duration"]
        value <- map["value"]
    }
}
