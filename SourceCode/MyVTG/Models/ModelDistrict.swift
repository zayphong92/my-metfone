//
//  ModelDistrict.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDistrict: ModelBase {
    var id: String = ""
    var name: String = ""
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
    
    func setId(_ id: String) {
        self.id = id
    }
    
    func setName(_ name: String) {
        self.name = name
    }
}
