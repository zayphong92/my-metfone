//
//  ModelAccountDetail.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 10/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAccountDetail: ModelBase {
    var title: String = ""
    var values: [ModelAccountValueDetail] = []
    
    override func mapping(map: Map) {
        title <- map["title"]
        values <- map["values"]
    }
}
