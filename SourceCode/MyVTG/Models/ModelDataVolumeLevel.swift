//
//  ModelDataVolumeLevel.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDataVolumeLevel: ModelBase {
    var volume: Double = 0
    var price: Double = 0
    
    override func mapping(map: Map) {
        volume <- map["volume"]
        price <- map["price"]
    }
}
