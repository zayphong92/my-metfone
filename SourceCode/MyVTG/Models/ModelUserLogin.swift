//
//  ModelUserLogin.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper
import Alamofire

class ModelUserLogin: ModelBase {
    var sessionId: String = ""
    var username: String = ""
    var token: String = ""
    
    override func mapping(map: Map) {
        sessionId <- map["sessionId"]
        username <- map["username"]
        token <- map["token"]
    }
}
