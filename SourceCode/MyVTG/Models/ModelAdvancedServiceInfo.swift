//
//  ModelAdvancedServiceInfo.swift
//  MyVTG
//
//  Created by luatnc on 11/4/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper
import Alamofire
import SwiftyJSON

class ModelAdvancedServiceInfo : ModelBase {
    var servicePage : String = ""
    
    // Mappable
    override func mapping(map: Map) {
        servicePage <- map["servicePage"]
    }
}
