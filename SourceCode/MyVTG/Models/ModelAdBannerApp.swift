//
//  ModelAdBannerApp.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAdBannerApp: ModelBase {
    var adImgUrl: String = ""
    var sourceLink: String = ""
    
    override func mapping(map: Map) {
        adImgUrl <- map["adImgUrl"]
        sourceLink <- map["sourceLink"]
    }
}
