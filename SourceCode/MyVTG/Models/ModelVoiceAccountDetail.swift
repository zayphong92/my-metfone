//
//  ModelAccountInfo.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelVoiceAccountDetail: ModelBase {
    var mainAcc_money: Double = 0
    var mainAcc_expiredDate: Int64 = 0
    var promAcc_money: Double = 0
    var promAcc_expiredDate: Int64 = 0
    var others: [ModelVoiceBalance]  = []
    var prePost:Double = 0
    var debPost:Double = 0
    var debPreMonthPost:Double = 0
    
    
    override func mapping(map: Map) {
        mainAcc_money <- map["mainAcc.money"]
        mainAcc_expiredDate <- map["mainAcc.expiredDate"]
        promAcc_money <- map["promAcc.money"]
        promAcc_expiredDate <- map["promAcc.expiredDate"]
        others <- map["others"]
        
        //Tra sau
        prePost <- map["prePost"]
        debPost <- map["debPost"]
        debPreMonthPost <- map["debPreMonthPost"]
    }
    
    func getBalanceInfo(subtype: SubType) -> [ModelVoiceBalance] {
        switch subtype {
        case .Prepay:
            return getPrepayBalanceInfo()
        case .Postpaid:
            return getPostpaidBalanceInfo()
        }
    }
    
    private func getPrepayBalanceInfo() -> [ModelVoiceBalance] {
        var voiceBlances: [ModelVoiceBalance] = []
        
        let mainBalance =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])
        mainBalance?.name = NSLocalizedString("Label_TaiKhoanChinh", comment: "")
        mainBalance?.money = self.mainAcc_money
        mainBalance?.expiredDate = self.mainAcc_expiredDate
        voiceBlances.append(mainBalance!)
        
        let promBalance =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])
        promBalance?.name = NSLocalizedString("Label_TaiKhoanKhuyenMai", comment: "")
        promBalance?.money = self.promAcc_money
        promBalance?.expiredDate = self.promAcc_expiredDate
        voiceBlances.append(promBalance!)
        
        for balance : ModelVoiceBalance in self.others {
            voiceBlances.append(balance)
        }
        return voiceBlances
    }
    
    private func getPostpaidBalanceInfo() -> [ModelVoiceBalance] {
        var voiceBlances: [ModelVoiceBalance] = []
        
        let prePost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
        prePost.name = NSLocalizedString("Label_Hot_Charge", comment: "")
        prePost.money = self.prePost
        prePost.expiredDate = 0
        voiceBlances.append(prePost)
        
        
        let debPost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
        debPost.name = NSLocalizedString("Label_Debt", comment: "")
        debPost.money = self.debPost
        debPost.expiredDate = 0
        voiceBlances.append(debPost)
        
        let debPreMonthPost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
        debPreMonthPost.name = NSLocalizedString("Label_Previous_month_debt", comment: "")
        debPreMonthPost.money = self.debPreMonthPost
        debPreMonthPost.expiredDate = 0
        voiceBlances.append(debPreMonthPost)
        
        return voiceBlances
    }
    
    
    static func getDefaultVoiceBalance(subtype: SubType) -> [ModelVoiceBalance] {
        var voiceBlances: [ModelVoiceBalance] = []
        switch subtype {
        case .Prepay:
            let mainBalance =  (Mapper<ModelVoiceBalance>().map(JSONObject: [:]))!
            mainBalance.name = NSLocalizedString("Label_TaiKhoanChinh", comment: "")
            voiceBlances.append(mainBalance)
            
            let promBalance =  (Mapper<ModelVoiceBalance>().map(JSONObject: [:]))!
            promBalance.name = NSLocalizedString("Label_TaiKhoanKhuyenMai", comment: "")
            voiceBlances.append(promBalance)
            break
            
        case .Postpaid:
            let prePost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
            prePost.name = NSLocalizedString("Label_Hot_Charge", comment: "")
            voiceBlances.append(prePost)
            
            let debPost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
            debPost.name = NSLocalizedString("Label_Debt", comment: "")
            voiceBlances.append(debPost)
            
            let debPreMonthPost =  Mapper<ModelVoiceBalance>().map(JSONObject: [:])!
            debPreMonthPost.name = NSLocalizedString("Label_Previous_month_debt", comment: "")
            voiceBlances.append(debPreMonthPost)
            
            break
        }
        return voiceBlances
    }
    
    
    
}
