//
//  ModelDataPackageInfo.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDataPackageInfo: ModelBase {
    var name: String = ""
    var code: String = ""
    var fullDes: String = ""
    var imgDesUrl: String = ""
    var regState: Int = 0
    var enableAct: Int = -1
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        fullDes <- map["fullDes"]
        imgDesUrl <- map["imgDesUrl"]
        regState <- map["regState"]
        enableAct <- map["enableAct"]
    }
}
