//
//  ChargeStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 6/17/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ChargeStore {
    var allPrepaidCharges = [[Charge]]()
    var allPostpaidCharges = [Charge]()
    var totalCharges: Double = -1
    
    func initCharges() {
        initPrepaidCharges()
        initPostpaidCharges()
    }
    
    func initPrepaidCharges() {
        allPrepaidCharges.append([Charge]())
        allPrepaidCharges[0] = [
            Charge(icon: nil, value: .TaiKhoanChinh),
            Charge(icon: nil, value: .TaiKhoanKhuyenMai)
        ]
        
        allPrepaidCharges.append([Charge]())
        allPrepaidCharges[1] = [
            Charge(icon: #imageLiteral(resourceName: "ic_calling_charge"), value: .CuocGoi, postType: 0),
            Charge(icon: #imageLiteral(resourceName: "ic_sms_charge"), value: .TinNhanSMS, postType: 1),
            Charge(icon: #imageLiteral(resourceName: "ic_other_charge"), value: .Data, postType: 3),
            Charge(icon: #imageLiteral(resourceName: "ic_other_charge"), value: .Vas, postType: 4)
        ]
    }
    
    func initPostpaidCharges() {
        allPostpaidCharges = [
            Charge(icon: #imageLiteral(resourceName: "ic_subscription_fee"), value: .ThueBaoThang),
            Charge(icon: #imageLiteral(resourceName: "ic_calling_charge"), value: .CuocGoi, postType: 0),
            Charge(icon: #imageLiteral(resourceName: "ic_sms_charge"), value: .TinNhanSMS, postType: 1),
            Charge(icon: #imageLiteral(resourceName: "ic_other_charge"), value: .Data, postType: 3),
            Charge(icon: #imageLiteral(resourceName: "ic_other_charge"), value: .Vas, postType: 4)
        ]
    }
}
