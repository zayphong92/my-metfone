//
//  UtilityStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class UtilityStore {
    var allUtilities = [[Utility]]()
    func initUtilities() {
        allUtilities.append([Utility]())
        allUtilities[0] = (
            [Utility(value: UtilitiesItems.TraCuoc, icon: #imageLiteral(resourceName: "tra_cuoc")),
             Utility(value: UtilitiesItems.NapCuoc, icon: #imageLiteral(resourceName: "nap_cuoc"))]
        )
        
        allUtilities.append([Utility]())
        allUtilities[1] = (
            [//Utility(value: UtilitiesItems.MuaThemData, icon: #imageLiteral(resourceName: "them_data")),
             Utility(value: UtilitiesItems.DoiGoiData, icon: #imageLiteral(resourceName: "doi_goi_data"))
            ]
        )
        var i = 2
        if Const.MARKET_NAME == .All {
            allUtilities.append([Utility]())
            allUtilities[2] = (
                [Utility(value: UtilitiesItems.MuaSo, icon: #imageLiteral(resourceName: "mua_so")),
                 Utility(value: UtilitiesItems.DoiSo, icon: #imageLiteral(resourceName: "mua_so")),
                 Utility(value: UtilitiesItems.KhoiPhucSo, icon: #imageLiteral(resourceName: "mua_so")),
                 Utility(value: UtilitiesItems.DoiSim, icon: #imageLiteral(resourceName: "mua_so")),
                 Utility(value: UtilitiesItems.KhoaGoiDi, icon: #imageLiteral(resourceName: "mua_so"))]
            )
            i += 1
        } else if Const.MARKET_NAME == .Local {
//            allUtilities.append([Utility]())
//            allUtilities[2] = (
//                [//Utility(value: UtilitiesItems.MuaSo, icon: #imageLiteral(resourceName: "mua_so")),
//                 //Utility(value: UtilitiesItems.DoiSo, icon: #imageLiteral(resourceName: "mua_so")),
//                 //Utility(value: UtilitiesItems.KhoiPhucSo, icon: #imageLiteral(resourceName: "mua_so")),
//                 //Utility(value: UtilitiesItems.DoiSim, icon: #imageLiteral(resourceName: "mua_so"))
//                 //Utility(value: UtilitiesItems.KhoaGoiDi, icon: #imageLiteral(resourceName: "mua_so"))
//                ]
//            )
//            i += 1
        }
        
        allUtilities.append([Utility]())
        allUtilities[i] = (
            [/*Utility(value: UtilitiesItems.ChuyenTien, icon: #imageLiteral(resourceName: "chuyen_tien")),*/
                Utility(value: UtilitiesItems.UngTien, icon: #imageLiteral(resourceName: "ung_tien")),
                Utility(value: UtilitiesItems.TimCuaHang, icon: #imageLiteral(resourceName: "tim_cua_hang"))]
        )
    }
}
