//
//  ModelExperience3G4G.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelExperience3G4G: ModelBase {
    var adBanner: [ModelAdBanner3G4G] = []
    var links: [ModelExperienceLink3G4G] = []
    
    override func mapping(map: Map) {
        adBanner <- map["adBanner"]
        links <- map["links"]
    }
}
