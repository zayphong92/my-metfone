//
//  ModelIsdnToBuy.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelIsdnToBuy: ModelBase {
    var isdn: String = ""
    var price: Double = 0
    
    override func mapping(map: Map) {
        isdn <- map["isdn"]
        price <- map["price"]
    }
}
