//
//  ModelDoAction.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/11/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDoAction: ModelBase {
    var errorCode: String = "0"
    var message: String = ""
    var userMsg: String = ""
    
    override func mapping(map: Map) {
        errorCode <- map["errorCode"]
        message <- map["message"]
        userMsg <- map["userMsg"]
    }
    
    func isSuccess() -> Bool {
        return (errorCode == Client.ERR_CODE_BACKEND_SUCCESS) || (errorCode == Client.ERR_CODE_BACKEND_SUCCESS_1000)
    }
}
