//
//  ModelMainDataPackage.swift
//  MyVTG
//
//  Created by Vu Ngoc Vuong on 7/12/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelDataPackage: ModelBase {
    
    var name: String = ""
    var code: String = ""
    var volume: Double = -1
    var volumeStr: String = ""
    var price: Int = 0
    var expiredDateStr: String = ""
    var expiredDate: Double = -1
    var pakagesToBuy: [ModelPackageDataToBuy] = []
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        volume <- map["volume"]
        volumeStr <- map["volumeStr"]
        price <- map["price"]
        expiredDate <- map["expiredDate"]
        expiredDateStr <- map["expiredDateStr"]
        pakagesToBuy <- map["pakagesToBuy"]
    }
}
