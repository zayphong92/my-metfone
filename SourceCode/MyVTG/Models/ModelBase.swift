//
//  ModelBase.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper
import Alamofire
import SwiftyJSON



class ModelBase: Mappable {
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
    }
    
    func isBaseClass() -> Bool {
        let className = String(describing: type(of: self))
        return className == String(describing: ModelBase.self)
    }
}
