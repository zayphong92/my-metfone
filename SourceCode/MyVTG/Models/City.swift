//
//  City.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/24/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class City: NSObject {
    var name: String
    var giftList = [Gift]()
    var districtList = [District]()

    init(name: String) {
        self.name = name
        super.init()
    }
    override init() {
        self.name = ""
        super.init()
    }
    func set(name: String, giftList: [Gift], districtList: [District]) {
        self.name = name
        self.giftList = giftList
        self.districtList = districtList
    }
}
