//
//  ModelFindIsdnToBuy.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/6/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelFindIsdnToBuy: ModelBase {
    var isdn: String = ""
    var price: Double = 0
    
    override func mapping(map: Map) {
        isdn <- map["isdn"]
        price <- map["price"]
    }
}
