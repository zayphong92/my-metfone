//
//  DataPackageStore.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 5/10/17.
//  Copyright © 2017 Nguyen Thanh Ha. All rights reserved.
//

import UIKit

let explanation = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum nisi non blandit fermentum. Vestibulum lacinia sit amet odio et iaculis. Donec sagittis lectus ut fermentum facilisis. Mauris sagittis sodales odio. Etiam consequat lorem ut ipsum pretium facilisis. Nullam condimentum ex leo, ut lacinia nisl consequat a. Nam quis semper ligula, et vestibulum justo. Integer consequat lobortis leo, in imperdiet est interdum non. Integer consectetur ex quis consectetur cursus. Vestibulum ac eleifend nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum nisi non blandit fermentum. Vestibulum lacinia sit amet odio et iaculis. Donec sagittis lectus ut fermentum facilisis. Mauris sagittis sodales odio. Etiam consequat lorem ut ipsum pretium facilisis. Nullam condimentum ex leo, ut lacinia nisl consequat a. Nam quis semper ligula, et vestibulum justo. Integer consequat lobortis leo, in imperdiet est interdum non. Integer consectetur ex quis consectetur cursus. Vestibulum ac eleifend nunc."

class DataPackageStore {
    
    var allDataPackages = [DataPackage]()
    
    var dataPackage = DataPackage()
    
    func initDataPackages() {
        for i in 0...8 {
            
            allDataPackages.append(dataPackage)
            allDataPackages[i] = DataPackage(icon: #imageLiteral(resourceName: "main_data_pack"), name: "MI\(i+1)0", size: "MI\(i+1)0 - \(i+1)0000 = \(i+1)00 MB", expiration: "30 days", explanation: explanation)
        }
    }
}
