//
//  ModelSubAccountInfo.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//


import ObjectMapper

class ModelSubAccountInfo: ModelBase {
    var name: String = ""
    var mainAcc: Double = 0
    var proAcc: Double = 0
    var prePost: Double = 0
    var debPost: Double = 0
    var dataPkgName: String = ""
    var dataVolume: Int = 0
    
    override func mapping(map: Map) {
        name <- map["name"]
        mainAcc <- map["mainAcc"]
        proAcc <- map["proAcc"]
        prePost <- map["prePost"]
        debPost <- map["debPost"]
        dataPkgName <- map["dataPkgName"]
        dataVolume <- map["dataVolume"]
    }
}
