//
//  ModelRegistable.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/14/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import UIKit

class ModelRegistable: ModelBase {
    var name: String = ""
    var code: String = ""
    var price: Double = 0
    
    // để quản lý trạng thái đăng ký/huỷ đăng ký của model mà ko cần gọi API get data để update thay đổi
    private var currentRegisterState: RegisterState = .Unknown
    
    // class kế thừa phải set biến này trong hàm mapping
    // hoặc là ngay sau khi get data từ API xong
    var isRegistered: Bool {
        set(value) {
            currentRegisterState = value ? .Registered : .UnRegistered
        }
        get {
            return currentRegisterState == .Registered
        }
    }
    
    var action: ServiceAction {
        get {
            return currentRegisterState.serviceAction
        }
    }
    
    var textForButtonSubmit: String {
        get {
            return currentRegisterState.localizableStringForBtnSubmit
        }
    }
    
    // trước khi gọi API thì phải gọi hàm này
    func willUpdateRegisterState() {
        currentRegisterState = RegisterState.willUpdate(state: currentRegisterState)
    }
    
    // sau khi API trả về kết quả thì phải gọi hàm này
    func didUpdateRegisterState(success: Bool) {
        currentRegisterState = RegisterState.didUpdate(state: currentRegisterState, success: success)
    }
}
