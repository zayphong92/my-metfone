//
//  ModelServiceItem.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/8/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelServiceItem: ModelRegistable {
    var iconUrl: String = ""
    var isMultPlan: Bool = false
    var isRegisterAble: Int = 0
    var state: Int = -1
    
    // API wsGetCurrentUsedSubServices trả về "des"
    // API wsGetServices trả về "shortDes"
    // Cả 2 thằng này bản chất như nhau, nhưng 2 API lại trả về 2 tên trường khác nhau nên phải làm trò này
    private var shortDes: String = ""
    private var des: String = ""
    func getShortDes() -> String {
        return shortDes.characters.count > 0 ? shortDes : des
    }
    
    override func mapping(map: Map) {
        name <- map["name"]
        code <- map["code"]
        iconUrl <- map["iconUrl"]
        isMultPlan <- map["isMultPlan"]
        isRegisterAble <- map["isRegisterAble"]
        shortDes <- map["shortDes"]
        des <- map["des"]
        state <- map["state"]
    }
}
