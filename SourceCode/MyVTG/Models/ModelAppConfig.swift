//
//  ModelAppConfig.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 8/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelAppConfig: ModelBase {
    
    var appConfig: String = "" {
        didSet {
            convertJsonToConfigModel()
        }
    }
    
    var home: [ModelHomeConfig] = []
    var utilities: [ModelUtilitiesConfig] = []
    var more: [ModelMoreConfig] = []
    var emptyPages: [ModelEmptyPage] = []
    
    override func mapping(map: Map) {
        appConfig <- map["appConfig"]
    }
    
    func convertJsonToConfigModel() {
        if let dic:[String:Any] = Util.convertToDictionary(text: appConfig) {
            let appConfigMap: Map = Map.init(mappingType: .fromJSON, JSON: dic);
            home <- appConfigMap["home"]
            utilities <- appConfigMap["utilities"]
            more <- appConfigMap["more"]
            emptyPages <- appConfigMap["emptyPages"]
        }
    }
    
}
