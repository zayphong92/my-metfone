//
//  ModelExperienceLink3G4G.swift
//  MyVTG
//
//  Created by Nguyen Thanh Ha on 7/10/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelExperienceLink3G4G: ModelBase {
    var name: String = ""
    var shortDes: String = ""
    var sourceLink: String = ""
    var iconUrl: String = ""
    
    override func mapping(map: Map) {
        name <- map["name"]
        shortDes <- map["shortDes"]
        sourceLink <- map["sourceLink"]
        iconUrl <- map["iconUrl"]
    }
}
