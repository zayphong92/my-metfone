//
//  ModelSubMainInfo.swift
//  MyVTG
//
//  Created by Cao Manh Quang on 7/3/17.
//  Copyright © 2017 MQ Solutions. All rights reserved.
//

import ObjectMapper

class ModelSubMainInfo: ModelBase {
    public static let SUBTYPE_TRATRUOC: Int = 1;
    public static let SUBTYPE_TRASAU: Int = 2;
    
    var name: String = ""
    var subType: Int = 0
    var language: String = ""
    var avatar_url: String = ""
    var subId: Int = 0
    
    func isTraTruoc() -> Bool {
        return subType == ModelSubMainInfo.SUBTYPE_TRATRUOC
    }
    
    override func mapping(map: Map) {
        name <- map["name"]
        subType <- map["subType"]
        language <- map["language"]
        avatar_url <- map["avatar_url"]
        subId <- map["subId"]
    }
}
